create table gen_pop_tools.patient_census_tract
(patient_id integer,
 census_tract_id integer,
 lat varchar(25),
 long varchar(25),
 constraint patient_census_tract_pk primary key (patient_id));
 