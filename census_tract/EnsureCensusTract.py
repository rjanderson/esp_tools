#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, ssl
#need to install configparser in the esp virtualenv: pip install configparser
import configparser
import psycopg2
import urllib
import urllib2
import json

try:
    eMsg = ''
    config = configparser.ConfigParser()
    config.read('census.properties')
    sqlUser = config['sql']['user']
    db = config['sql']['database']
    sqlPassword = config['sql']['password']
    del_addr = config['options']['delete']

    con1 = None
    con2 = None
    con1 = psycopg2.connect(host='localhost', dbname=db, user=sqlUser, password=sqlPassword)
    con2 = psycopg2.connect(host='localhost', dbname=db, user=sqlUser, password=sqlPassword)
    con1.set_session(autocommit=True)
    con2.set_session(autocommit=True)
    cursor1 = con1.cursor()
    cursor2 = con2.cursor()
    context = ssl._create_unverified_context()

# This program will process about 5,000 rows an hour.    
    qry = ("SELECT e.id, e.address1, e.city, e.state, e.zip FROM public.emr_patient e " 
        + " LEFT OUTER JOIN gen_pop_tools.patient_census_tract p ON e.id=p.patient_id "
        + " WHERE p.patient_id is NULL ORDER BY e.id LIMIT 10000;")
    eMsg = qry
    print('Selecting patients without census_tract')
    cursor1.execute(qry)
    row = cursor1.fetchone()
    print('got patient list')
    row = cursor1.fetchone()
    while(row != None):
        census_tract = -1
        try:
            eMsg="Calling US census geocoder" 
            #We take the EMR address and get lat,lon from US census
            patient_id = row[0]
            if(row[1] is None or row[2] is None or row[3] is None or row[4] is None):
            # Insufficient address data, set c to zero
                c = 0            
            else:
                address = row[1]
                city = row[2]
                state = row[3]
                zip = row[4]
                oneline_address = urllib.quote_plus(address + " " + city + " " + state + " " + zip)
                url = ("https://geocoding.geo.census.gov/geocoder/locations/onelineaddress"+ 
                        "?address=" + oneline_address + 
                        "&benchmark=9&format=json")
                eMsg = url
                results = urllib2.urlopen(url, context=context).read()
                eMsg = results
                js = json.loads(results)
                c = len(js['result']['addressMatches'])
                #if no address returned, c is zero. 
                
            print('patient id:' + str(patient_id) + ' num address matches: ' + str(c))
            if c <= 0:
            #address can't be resolved
                census_tract = -1
            else:
                try:
                    coordinates = js['result']['addressMatches'][0]['coordinates']
                    x = str(coordinates['x']) #lat
                    y = str(coordinates['y']) #long
                    url=("https://geocoding.geo.census.gov/geocoder/geographies/coordinates?x="
                        + x + "&y=" + y +"&benchmark=4&vintage=4&format=json")
                    eMsg = url
                    results = urllib2.urlopen(url, context=context).read()
                    eMsg = results
                    js = json.loads(results)
                    census_tract = js['result']['geographies']['Census Tracts'][0]['TRACT']
                    
                except Exception as e0:
                    print('msg 0: ' + eMsg)
                    print('Exception 0: ' + str(e0))

            qry = "SELECT patient_id FROM gen_pop_tools.patient_census_tract WHERE patient_id=" + str(patient_id)
            eMsg = qry
            cursor2.execute(qry)
            if(cursor2.fetchone()):
                print('Update patient: ' + str(patient_id) + ' census:' + str(census_tract))
                qry = ("UPDATE gen_pop_tools.patient_census_tract SET census_tract_id=" + str(census_tract)
                     + ", lat='" + x + "', long='" + y 
                     + "' WHERE patient_id=" + str(patient_id) + ";" )
            else:
                print('Insert patient: ' + str(patient_id) + ' census:' + str(census_tract))
                qry = ("INSERT INTO gen_pop_tools.patient_census_tract(patient_id, census_tract_id, lat, long) "
                       + "VALUES (" + str(patient_id) + "," + str(census_tract) + ",'" + x + "', '" + y + "');" )
            eMsg = qry
            cursor2.execute(qry)
        except Exception as e1:
            print('msg 1: ' + eMsg)
            print('Exception 1: ' + str(e1))

        row = cursor1.fetchone()

except Exception as e2:
    print('msg 2: ' + eMsg)
    print('Exception 2: ' + str(e2))

finally:
    if con1:
        con1.close()
    if con2:
        con2.close()

