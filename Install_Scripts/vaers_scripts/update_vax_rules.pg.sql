--currenlty this is only updating the covid vax rules, but needs to be expanded to include other vax categories that
-- have special rules in VAERS.  e.g.: influenza, measles, rubella
insert into vaers_diagnosticseventrule_dxrule_vxs (diagnosticseventrule_id, vaccine_id)
select * from
(select t00.diagnosticseventrule_id, t01.vaccine_id
from
(select distinct t0.id as diagnosticseventrule_id
from vaers_diagnosticseventrule t0
join vaers_diagnosticseventrule_dxrule_vxs t1 on t0.id=t1.diagnosticseventrule_id
join static_vaccine t2 on t2.id=t1.vaccine_id
where t0.applies_to_all=False and t2.name ilike 'SARS-COV-2%') as t00, 
(select id as vaccine_id from static_vaccine where name ilike 'SARS-COV-2%') t01) t001
where not exists
(select null from vaers_diagnosticseventrule_dxrule_vxs t1 
where t001.diagnosticseventrule_id=t1.diagnosticseventrule_id and t001.vaccine_id=t1.vaccine_id);
