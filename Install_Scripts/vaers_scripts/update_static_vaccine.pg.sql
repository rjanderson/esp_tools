--the file web_cvx.csv is from the CDC site https://www2a.cdc.gov/vaccines/iis/iisstandards/vaccines.asp?rpt=cvx
--download table as excel, then save as csv.
--this source data is updated by the CDC whenever new immunizations come on the market. 
--drop table if exists temp_cvx;
create table temp_cvx
(cvx_code varchar(5),
 cvx_short_description varchar(100),
 full_vax_name varchar(200),
 note varchar(400),
 vax_status varchar(15),
 internal_id integer,
 nonvax varchar(5),
 update_date varchar(10));
copy temp_cvx from '/home/rzambarano/web_cvx.csv' delimiter ',' csv header;
insert into static_vaccine (code, short_name, name)
select cvx_code as code, cvx_short_description as short_name, full_vax_name as name
from temp_cvx cvx
where not exists (select null from static_vaccine vax where vax.code=cvx.cvx_code)
  and nonvax='False';
drop table temp_cvx;
