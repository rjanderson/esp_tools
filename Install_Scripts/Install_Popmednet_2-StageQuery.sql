drop table if exists gen_pop_tools.pmn_queries;
create table gen_pop_tools.pmn_queries (
  id serial primary key,
  tname varchar(10),
  query_txt text,
  submitted boolean,
  status varchar(2000));
--
CREATE OR REPLACE FUNCTION gen_pop_tools.submitquery(
	query_txt text)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE 
AS $BODY$
declare
  idval int;
  tname varchar(10);
  insrt_str text;
begin
  select nextval('gen_pop_tools.pmn_queries_id_seq') into idval;
  tname:='query_' || trim(to_char(idval,'0000'));
  insrt_str:='insert into gen_pop_tools.pmn_queries (id, tname, query_txt, submitted) values (' ||
             to_char(idval,'9999') || ', ''' || 
             tname || ''', ''' || query_txt || ''', FALSE)';
  execute insrt_str;
  return tname;
end;
$BODY$;
--
CREATE OR REPLACE FUNCTION gen_pop_tools.runquery()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$
declare
  run_str text;
begin
  raise info 'trigger procedure runquery is executing';
      run_str:='create table gen_pop_tools.' || NEW.tname || ' as ' || NEW.query_txt;
      execute run_str;
  NEW.submitted:=TRUE;
  NEW.status:='Complete';
  return NEW;
exception when others then
  raise info 'Something broke';
  NEW.submitted:=TRUE;
  NEW.status:=SQLERRM;
  return NEW;
end;
$BODY$;
--
DROP TRIGGER pmn_runquery ON gen_pop_tools.pmn_queries;
CREATE TRIGGER pmn_runquery
    AFTER UPDATE ON gen_pop_tools.pmn_queries
    FOR EACH ROW
    EXECUTE PROCEDURE gen_pop_tools.runquery();
