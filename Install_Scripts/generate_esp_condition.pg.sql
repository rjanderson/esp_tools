drop table if exists esp_condition;
CREATE TABLE esp_condition
( centerid character varying(1),
  patid character varying(128) NOT NULL,
  condition character varying(100) NOT NULL,
  date integer NOT NULL,
  age_at_detect_year integer,
  age_group_5yr character varying(5),
  age_group_10yr character varying(5),
  age_group_ms character varying(5),
  criteria character varying(2000),
  status character varying(32),
  notes text,
  cond_start_date date,
  cond_expire_date date,
  CONSTRAINT esp_condition_r_pkey1 PRIMARY KEY (patid, condition, date)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE esp_condition
  OWNER TO esp;
GRANT ALL ON TABLE esp_condition TO esp;

