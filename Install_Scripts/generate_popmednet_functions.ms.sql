IF OBJECT_ID('esp_popmednet.center', 'FN') IS NOT NULL DROP FUNCTION esp_popmednet.center;
GO
CREATE FUNCTION esp_popmednet.center(@fake_patid character varying) 
RETURNS character varying 
AS 
BEGIN
DECLARE   @c char;
DECLARE   @center character varying(10) = '';

   SET @c =  UPPER(SUBSTRING(@fake_patid, 6, 1));

   SET @center = 
	   CASE
		 WHEN @c >= '0' and @c <= '9' THEN 'center 0-9'
		 WHEN @c >= 'A' and @c <= 'M' THEN 'center A-M'
		 WHEN @c >= 'N' and @c <= 'Z' THEN 'center N-Z'
		 ELSE 'UNKNOWN'
	   END;

   RETURN @center;
END;
GO

IF OBJECT_ID('esp_popmednet.age_at_year_start', 'FN') IS NOT NULL DROP FUNCTION esp_popmednet.age_at_year_start;
GO
CREATE FUNCTION age_at_year_start(@encounterdate date, @birthdate date)
RETURNS int
AS
BEGIN
    DECLARE @age_at_year_start int;
    IF @encounterdate IS NULL OR @birthdate IS NULL
        set @age_at_year_start = NULL;
    ELSE
        set @age_at_year_start = (cast(substring(convert(varchar(8), @encounterdate, 112),1,4)+'0101' as int) -
                                 cast(convert(varchar(8), @birthdate, 112) as int))/10000;

    RETURN @age_at_year_start;
END;
GO

IF OBJECT_ID('esp_popmednet.age_group_5yr', 'FN') IS NOT NULL DROP FUNCTION esp_popmednet.age_group_5yr;
GO
CREATE FUNCTION age_group_5yr(encounterdate date, birthdate date)
RETURNS character varying AS $$
DECLARE
   age integer;
   age_group character varying(5) := '';
BEGIN
   IF encounterdate IS NULL OR birthdate IS NULL THEN
      RETURN NULL;
   END IF;

   age := age_at_year_start(encounterdate, birthdate);

   CASE
     WHEN age <= 4  THEN age_group := '0-4';
     WHEN age <= 9  THEN age_group := '5-9'; 
     WHEN age <= 14 THEN age_group := '10-14'; 
     WHEN age <= 19 THEN age_group := '15-19'; 
     WHEN age <= 24 THEN age_group := '20-24'; 
     WHEN age <= 29 THEN age_group := '25-29'; 
     WHEN age <= 34 THEN age_group := '30-34'; 
     WHEN age <= 39 THEN age_group := '35-39'; 
     WHEN age <= 44 THEN age_group := '40-44'; 
     WHEN age <= 49 THEN age_group := '45-49'; 
     WHEN age <= 54 THEN age_group := '50-54'; 
     WHEN age <= 59 THEN age_group := '55-59'; 
     WHEN age <= 64 THEN age_group := '60-64'; 
     WHEN age <= 69 THEN age_group := '65-69'; 
     WHEN age <= 74 THEN age_group := '70-74'; 
     WHEN age <= 79 THEN age_group := '75-79'; 
     WHEN age <= 84 THEN age_group := '80-84'; 
     WHEN age <= 89 THEN age_group := '85-89'; 
     WHEN age <= 94 THEN age_group := '90-94'; 
     WHEN age <= 99 THEN age_group := '95-99'; 
     ELSE age_group := '100+'; 
   END CASE;

   RETURN age_group;
END;
$$ ;
GO

IF OBJECT_ID('esp_popmednet.age_group_10yr', 'FN') IS NOT NULL DROP FUNCTION esp_popmednet.age_group_10yr;
GO
CREATE FUNCTION age_group_10yr(encounterdate date, birthdate date)
RETURNS character varying AS $$
DECLARE
   age integer;
   age_group character varying(5) := '';
BEGIN
   IF encounterdate IS NULL OR birthdate IS NULL THEN
      RETURN NULL;
   END IF;

   age := age_at_year_start(encounterdate, birthdate);

   CASE
     WHEN age <= 9  THEN age_group := '0-9'; 
     WHEN age <= 19 THEN age_group := '10-19'; 
     WHEN age <= 29 THEN age_group := '20-29'; 
     WHEN age <= 39 THEN age_group := '30-39'; 
     WHEN age <= 49 THEN age_group := '40-49'; 
     WHEN age <= 59 THEN age_group := '50-59'; 
     WHEN age <= 69 THEN age_group := '60-69'; 
     WHEN age <= 79 THEN age_group := '70-79'; 
     WHEN age <= 89 THEN age_group := '80-89'; 
     WHEN age <= 99 THEN age_group := '90-99'; 
     ELSE age_group := '100+'; 
   END CASE;

   RETURN age_group;
END;
$$ ;
GO

IF OBJECT_ID('esp_popmednet.icd9_prefix', 'FN') IS NOT NULL DROP FUNCTION esp_popmednet.icd9_prefix;
GO
CREATE FUNCTION icd9_prefix(icd9_code character varying, len integer)
RETURNS character varying AS $$
DECLARE
   first_char character varying(1) := '';
   adjusted_len int := len;
   icd9_no_period character varying(10);
BEGIN
   IF icd9_code IS NULL THEN
      RETURN NULL;
   END IF;

   first_char := substr(icd9_code, 6, 1);
   IF first_char NOT IN ('0','1','2','3','4','5','6','7','8','9') THEN
      adjusted_len := adjusted_len + 1;
   ELSIF strpos(substr(icd9_code,6),'.')=3 THEN
      adjusted_len := adjusted_len - 1;
   END IF;

   icd9_no_period = replace(substr(icd9_code,6), '.', '');

   RETURN substr(icd9_no_period, 1, adjusted_len);
END;
$$ ;
GO
