﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2020 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some Transact-SQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/

--This table is used to map local race values to standard set.
IF object_id('gen_pop_tools.cc_racemap', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_racemap;
create table gen_pop_tools.cc_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the set here.
insert into gen_pop_tools.cc_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK')
;
--find patients with HIV diagnosis
DROP TABLE IF EXISTS cc_hiv_case;
CREATE TABLE cc_hiv_case AS
SELECT patient_id, date hiv_case_dt 
FROM nodis_case
where condition='hiv' 
;

--find patients with HIV lab tests
drop table if exists cc_hiv_labs;
select distinct patient_id, to_char(date,'yyyy') lab_yr
into cc_hiv_labs
from emr_labresult l
join conf_labtestmap tm on tm.native_code=l.native_code
where tm.test_name in ('cd4','hiv_elisa', 'hiv_ag_ab', 'hiv_pcr', 'hiv_wb',
                         'hiv_rna_viral', 'hiv_multispot', 'hiv_geenius');

--risk scores for non-hiv pats, joined to hiv pats (subsequent diagnosis) and hiv lab tests
drop table if exists cc_hiv_risk;
select t1.patient_id,
       t1.rpt_year, 
       t2.patient_id as subsequent_hiv,
       t3.patient_id as tested_at_risk,
       t1.hiv_risk_score,
       t1.hiv_risk_score/(12.1/100000) ratio,
       t1.truvada_rx_yn
into cc_hiv_risk
from cc_hiv_risk_score t1
left join cc_hiv_case t2 on t1.patient_id=t2.patient_id
left join cc_hiv_labs t3 on t1.patient_id=t3.patient_id and t1.rpt_year=t3.lab_yr;

--top deciles
drop table if exists cc_hiv_plus_risk;
select t0.patient_id,
       t0.rpt_year rpt_yr, 
       case when t0.hiv_risk_score>.01 then t0.patient_id else null end hiv_risk_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 then t0.patient_id else null end hiv_risk_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 then t0.patient_id else null end hiv_risk_5_10,
       case when t0.hiv_risk_score>.01 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_5_10,
       case when t0.hiv_risk_score>.01 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_5_10,
       case when t0.hiv_risk_score>.01 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_5_10,
       case when t0.hiv_risk_score>.01 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_5_10,
       case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
	 when upper(substr(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
       end sex,
       case 
         when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=p.ethnicity)
         when (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=p.race)
         else 'UNKNOWN'
       end race_ethnicity
    ,  case 
         when datediff(year, p.date_of_birth, current_date)<=19 then '0-19'
         when datediff(year, p.date_of_birth, current_date)>=20 and datediff(year, p.date_of_birth, current_date) <=39 then '20-39'
         when datediff(year, p.date_of_birth, current_date)>=40 and datediff(year, p.date_of_birth, current_date) <=59 then '40-59'
         when datediff(year, p.date_of_birth), current_date)>=60 then 'Over 59'
         else 'UNKNOWN'
       end as agegroup            
into cc_hiv_plus_risk
from cc_hiv_risk t0
JOIN emr_patient p ON t0.patient_id = p.id;

--build the table for summarization:
DROP TABLE IF EXISTS cc_hiv_risk_by_pat;
SELECT t0.patient_id, t0.sex, t0.race_ethnicity, t0.agegroup, t0.rpt_yr
 , t0.hiv_risk_85
 , t0.hiv_risk_10_85
 , t0.hiv_risk_5_10
 , t0.subsequent_hiv_85
 , t0.subsequent_hiv_10_85
 , t0.subsequent_hiv_5_10
 , t0.tested_at_risk_85
 , t0.tested_at_risk_10_85
 , t0.tested_at_risk_5_10
 , truvada_rx_85
 , truvada_rx_10_85
 , truvada_rx_5_10
 , truvada_hiv_85
 , truvada_hiv_10_85
 , truvada_hiv_5_10
into cc_hiv_risk_by_pat
FROM cc_hiv_plus_risk t0;

--Now get the crossing of all stratifiers
drop table if exists strat1;
create temporary table strat1 (id1 varchar(2), name1 varchar(15));
insert into strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists strat2;
create temporary table strat2 (id2 varchar(2), name2 varchar(15));
insert into strat2 values ('2','agegroup'),('x','x');
drop table if exists strat4;
create temporary table strat4 (id4 varchar(2), name4 varchar(15));
insert into strat4 values ('4','sex'),('x','x');
drop table if exists strat5;
create temporary table strat5 (id5 varchar(2), name5 varchar(15));
insert into strat5 values ('5','race_ethnicity'),('x','x');
drop table if exists cc_stratifiers_crossed;
create table cc_stratifiers_crossed as
select * from strat1, strat2, strat4, strat5;

--create stub table to contain summary results
drop table if exists gen_pop_tools.cc_hiv_risk_summaries;
create table gen_pop_tools.cc_hiv_risk_summaries (
  rpt_yr varchar(4),
  agegroup varchar(10), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total numeric(15,1),
  hiv_risk_85 numeric(15,1),
  hiv_risk_10_85 numeric(15,1),
  hiv_risk_5_10 numeric(15,1),
  subsequent_hiv_85 numeric(15,1),
  subsequent_hiv_10_85 numeric(15,1),
  subsequent_hiv_5_10 numeric(15,1),
  tested_at_risk_85 numeric(15,1),
  tested_at_risk_10_85 numeric(15,1),
  tested_at_risk_5_10 numeric(15,1),
  truvada_rx_85 numeric(15,1),
  truvada_rx_10_85 numeric(15,1),
  truvada_rx_5_10 numeric(15,1),
  truvada_hiv_85 numeric(15,1),
  truvada_hiv_10_85 numeric(15,1),
  truvada_hiv_5_10 numeric(15,1));

 --Run the various groupings.  
    for cursrow in execute 'select name1, name2, name4, name5 from cc_stratifiers_crossed'
    loop

insert into gen_pop_tools.cc_hiv_risk_summaries (rpt_yr, agegroup, sex, race_ethnicity, total,
      hiv_risk_85,  hiv_risk_10_85, hiv_risk_5_10,  
      subsequent_hiv_85, subsequent_hiv_10_85, subsequent_hiv_5_10, 
      tested_at_risk_85, tested_at_risk_10_85, tested_at_risk_5_10, 
      truvada_rx_85, truvada_rx_10_85, truvada_rx_5_10, 
      truvada_hiv_85, truvada_hiv_10_85, truvada_hiv_5_10) 
        select coalesce (rpt_yr,'x') as rpt_yr,
               coalesce (agegroup,'x') as agegroup,
               coalesce (sex,'x') as sex,
               coalesce (race_ethnicity,'x') as race_ethnicity,
               count(distinct patient_id) total,
               count(distinct hiv_risk_85) hiv_risk_85,
               count(distinct hiv_risk_10_85) hiv_risk_10_85,
               count(distinct hiv_risk_5_10) hiv_risk_5_10,
               count(distinct subsequent_hiv_85) subsequent_hiv_85,
               count(distinct subsequent_hiv_10_85) subsequent_hiv_10_85,
               count(distinct subsequent_hiv_5_10) subsequent_hiv_5_10,
               count(distinct tested_at_risk_85) tested_at_risk_85,
               count(distinct tested_at_risk_10_85) tested_at_risk_10_85,
               count(distinct tested_at_risk_5_10) tested_at_risk_5_10,
               count(distinct truvada_rx_85) truvada_rx_85,
               count(distinct truvada_rx_10_85) truvada_rx_10_85,
               count(distinct truvada_rx_5_10) truvada_rx_5_10,
               count(distinct truvada_hiv_85) truvada_hiv_85,
               count(distinct truvada_hiv_10_85) truvada_hiv_10_85,
               count(distinct truvada_hiv_5_10) truvada_hiv_5_10
         from cc_hiv_risk_by_pat
         group by cube (rpt_yr, agegroup, sex, race_ethnicity);

/*
select * from cc_hiv_risk_summaries;
*/

--Gather up the values of the stratifiers and assign code.
drop table if exists gen_pop_tools.cc_agegroup_codevals;
Create table gen_pop_tools.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');

drop table if exists gen_pop_tools.cc_sex_codevals;
Create table gen_pop_tools.cc_sex_codevals (sex varchar(7), codeval varchar(1));
insert into gen_pop_tools.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2');

drop table if exists gen_pop_tools.cc_race_ethnicity_codevals;
Create table gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

--now write out to JSON
select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]' filters,
'{"a":{"name":"Non-HIV patients, total with calculable risk","description":"Patients with sufficient data to calculate a score for risk of HIV acquisition during the measurent period according to an electronic prediction rule.","nest":"0","pcalc":"a"},"b":{"name":"Absolute risk of HIV acquisition ≥1%","description":"This is ≥85 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"c":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"b"},"d":{"name":"Tested for HIV during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"b"},"e":{"name":"Prescribed Truvada during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"b"},"f":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"e"},"g":{"name":"“Absolute risk of HIV acquisition 0.1 – 0.99%","description":"This is approx. ≥10 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"h":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"g"},"i":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"g"},"j":{"name":"Prescribed Truvada during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"g"},"k":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"j"},"l":{"name":"Absolute risk of HIV acquisition 0.05 – 0.09%","description":"This is approx. ≥5 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"m":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"l"},"n":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"l"},"o":{"name":"Prescribed Truvada during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada","nest":"2","pcalc":"l"},"p":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed Truvada during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"o"}}' rowmeta,
(select '{'+row_list+'}'   
into cc_hiv_rsk_json
from  (select row_ + ', ' from (select ('"'+
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end +
          case when agegroup <> 'x' 
            then (select codeval from gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end +
          case when sex <> 'x' 
            then (select codeval from gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end +
          case when race_ethnicity <> 'x' 
            then (select codeval from gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end + '":{"a":'+ str(total, 16, 1)
		  + ',"b":' + str(hiv_risk_85, 16, 1)
		  + ',"c":' + str(subsequent_hiv_85, 16, 1)
		  + ',"d":' + str(tested_at_risk_85, 16, 1)
		  + ',"e":' + str(truvada_rx_85, 16, 1)
		  + ',"f":' + str(truvada_hiv_85, 16, 1)
		  + ',"g":' + str(hiv_risk_10_85, 16, 1)
		  + ',"h":' + str(subsequent_hiv_10_85, 16, 1)
		  + ',"i":' + str(tested_at_risk_10_85, 16, 1)
		  + ',"j":' + str(truvada_rx_10_85, 16, 1)
		  + ',"k":' + str(truvada_hiv_10_85, 16, 1)
		  + ',"l":' + str(hiv_risk_5_10, 16, 1)
		  + ',"m":' + str(subsequent_hiv_5_10, 16, 1)
		  + ',"n":' + str(tested_at_risk_5_10, 16, 1)
		  + ',"o":' + str(truvada_rx_5_10, 16, 1)
		  + ',"p":' + str(truvada_hiv_5_10, 16, 1)
		  + '}') row_
from gen_pop_tools.cc_hiv_risk_summaries ) as t0
for XML path ('') ) as row_list) rowdata;




  

