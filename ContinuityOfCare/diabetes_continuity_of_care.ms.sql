/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
-- Using tt_pat_seq_enc, so this code must be run after tt stuff
-- This table has one row per year (FROM gen_pop_tools.2010) per patient (including current year)
--  where patient has had at least one encounter for that year.
--  This is the "Total Population Observed"
IF object_id('gen_pop_tools.cc_pat_seq_enc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_pat_seq_enc;
GO
select patient_id, 
  SUBSTRING(year_month,1,4) index_enc_yr 
INTO gen_pop_tools.cc_pat_seq_enc
FROM gen_pop_tools.tt_pat_seq_enc
where (SUBSTRING(year_month,6,2)='12' 
        or year_month=(select max(year_month) FROM gen_pop_tools.tt_pat_seq_enc)) 
      and prior1>=1 and SUBSTRING(year_month,1,4)>='2017';
GO

-- This table joins in patient demographic data	  
IF object_id('gen_pop_tools.cc_pat_seq_enc_patinfo', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_pat_seq_enc_patinfo;
GO
SELECT ip.patient_id, ip.index_enc_yr, 
case when upper(SUBSTRING(p.gender,1,1)) = 'F' then 'Female'
	 when upper(SUBSTRING(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
end sex,
case 
     when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity
                  and t00.src_table='emr_patient' and t00.mapped_value='HISPANIC') is not null
	   then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity
                  and t00.src_table='emr_patient' and t00.mapped_value='HISPANIC')
	 else (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=p.race
                  and t00.src_table='emr_patient')

  end race_ethnicity
,  case 
    when gen_pop_tools.age_2(getdate(), date_of_birth))<=19 then '0-19'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=20 and gen_pop_tools.age_2(getdate(), date_of_birth) <=39 then '20-39'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=40 and gen_pop_tools.age_2(getdate(), date_of_birth) <=59 then '40-59'
    when gen_pop_tools.age_2(getdate(), date_of_birth)>=60 then 'Over 59'
    else 'UNKNOWN'
  end as agegroup
INTO gen_pop_tools.cc_pat_seq_enc_patinfo
FROM gen_pop_tools.cc_pat_seq_enc ip
JOIN dbo.emr_patient p ON ip.patient_id = p.id;
GO
	   
--get a1c lab tests
IF object_id('gen_pop_tools.cc_diab_a1c_dt', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_a1c_dt;
GO
select patient_id, date a1c_dt, result_float
INTO gen_pop_tools.cc_diab_a1c_dt
FROM dbo.emr_labresult t0
join dbo.conf_labtestmap t1 on t0.native_code=t1.native_code
where t1.test_name='a1c';
GO

IF object_id('gen_pop_tools.cc_diab_a1c', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_a1c;
GO
select patient_id, CAST(DATEPART(year, alc_dt) AS varchar(4)) as a1c_yr, result_float
INTO gen_pop_tools.cc_diab_a1c
FROM gen_pop_tools.cc_diab_a1c_dt t0;
GO
--ever tested
IF object_id('gen_pop_tools.cc_diab_a1c_yr', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_a1c_yr;
GO
select distinct patient_id, a1c_yr
INTO gen_pop_tools.cc_diab_a1c_yr
FROM gen_pop_tools.cc_diab_a1c;
GO

--get diabetes cases
IF object_id('gen_pop_tools.cc_diab_case', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_case;
GO
SELECT c.patient_id
	, seq.index_enc_yr 
INTO gen_pop_tools.cc_diab_case
FROM dbo.nodis_case c
join gen_pop_tools.cc_pat_seq_enc seq on seq.patient_id=c.patient_id
join (select h.case_id, h.status, h.date as strtdt, 
             case 
                 when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
                 when c.isactive then getdate()
             end enddt 
      FROM dbo.nodis_caseactivehistory h 
      join nodis_case c on c.id=h.case_id) cah 
  on c.id=cah.case_id 
     and CAST(DATEPART(year, cah.strtdt) AS varchar(4))<=seq.index_enc_yr
     and CAST(DATEPART(year, cah.enddt) AS varchar(4))>=seq.index_enc_yr
WHERE c.condition = 'diabetes:type-2' and cah.status in ('I','R')
group by c.patient_id, seq.index_enc_yr;
GO

-- Find all diab meds for index patients
IF object_id('gen_pop_tools.cc_diab_rx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_rx;
GO
select patient_id, CAST(DATEPART(year, date) AS varchar(4)) rx_yr
INTO gen_pop_tools.cc_diab_rx
FROM dbo.hef_event 
WHERE name in ('metformin','glyburide','test-strips','lancets','pramlintide','exenatide','sitagliptin','meglitinide','nateglinide','repaglinide','glimepiride','glipizide','gliclazide','rosiglitizone','pioglitazone','acetone','glucagon','miglitol','insulin')
group by patient_id, CAST(DATEPART(year, date) AS varchar(4));
GO

--bring in last a1c for each year.
IF object_id('gen_pop_tools.cc_diab_case_a1c', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_case_a1c;
GO
select distinct t0.patient_id, t0.index_enc_yr, result_float a1c
INTO gen_pop_tools.cc_diab_case_a1c
FROM gen_pop_tools.cc_diab_case t0
left join (select distinct patient_id, CAST(DATEPART(a1c_dt, date) AS varchar(4)) a1c_yr, result_float, 
                  row_number() over (partition by patient_id, CAST(DATEPART(a1c_dt, date) AS varchar(4)) 
                                     order by a1c_dt desc) rn
           FROM gen_pop_tools.cc_diab_a1c_dt)		   
          t1 on t1.patient_id=t0.patient_id and t1.a1c_yr=t0.index_enc_yr
where rn=1 or rn is null;
GO

--treated cases
IF object_id('gen_pop_tools.cc_diab_case_rx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_case_rx;
GO
select t0.patient_id, t0.index_enc_yr case_rx_yr, t0.a1c
INTO gen_pop_tools.cc_diab_case_rx
FROM gen_pop_tools.cc_diab_case_a1c t0
join gen_pop_tools.cc_diab_rx t1 on t1.patient_id = t0.patient_id 
  and to_number(nullif(t0.index_enc_yr,''),'9999')>=to_number(nullif(t1.rx_yr,''),'9999')
  and to_number(nullif(t0.index_enc_yr,''),'9999')<=to_number(nullif(t1.rx_yr,''),'9999')+1
group by t0.patient_id, t0.index_enc_yr, t0.a1c;
GO
--untreated case
IF object_id('gen_pop_tools.cc_diab_case_norx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_case_norx;
GO
select t0.patient_id, t0.index_enc_yr case_norx_yr, t0.a1c
INTO gen_pop_tools.cc_diab_case_norx
FROM gen_pop_tools.cc_diab_case_a1c t0
left join gen_pop_tools.cc_diab_case_rx t1 on t1.patient_id=t0.patient_id
  and t1.case_rx_yr=t0.index_enc_yr
where t1.patient_id is null;
GO

-- Join it all together
IF object_id('gen_pop_tools.cc_diab_by_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_by_pat;
GO
SELECT t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.agegroup
 ,t1.patient_id as a1ctest
 ,t2.patient_id as a1ctest3
 ,t3.patient_id as a1ctestever
 ,t4.patient_id as diabetic
 ,t5.patient_id as norx
 ,t6.patient_id as norx7
 ,t7.patient_id as norx79
 ,t8.patient_id as norx9
 ,t9.patient_id as rx
 ,t10.patient_id as rx7
 ,t11.patient_id as rx79
 ,t12.patient_id as rx9
 ,t14.patient_id as all7
 ,t15.patient_id as all79
 ,t16.patient_id as all9
 ,t17.patient_id as norx_noa1c
 ,t18.patient_id as rx_noa1c
 ,t19.patient_id as all_noa1c 
INTO gen_pop_tools.cc_diab_by_pat
FROM gen_pop_tools.cc_pat_seq_enc_patinfo t0
left join gen_pop_tools.cc_diab_a1c_yr t1 
  on t1.patient_id=t0.patient_id and t1.a1c_yr=t0.index_enc_yr
left join gen_pop_tools.cc_diab_a1c_yr t2
  on t2.patient_id=t0.patient_id 
    and convert(int,nullif(t0.index_enc_yr,'')) >= convert(int,nullif(t2.a1c_yr,'')) 
    and convert(int,nullif(t0.index_enc_yr,'')) <= convert(int,nullif(t2.a1c_yr,'')) + 2
left join gen_pop_tools.cc_diab_a1c_yr t3
  on t3.patient_id=t0.patient_id 
    and convert(int,nullif(t0.index_enc_yr,'')) >= convert(int,nullif(t3.a1c_yr,'')) 
left join gen_pop_tools.cc_diab_case_a1c t4
  on t4.patient_id=t0.patient_id and t0.index_enc_yr=t4.index_enc_yr
left join gen_pop_tools.cc_diab_case_norx t5 on t5.patient_id=t0.patient_id
  and t5.case_norx_yr=t0.index_enc_yr
left join gen_pop_tools.cc_diab_case_norx t6 on t6.patient_id=t0.patient_id
  and t6.case_norx_yr=t0.index_enc_yr and t6.a1c<7
left join gen_pop_tools.cc_diab_case_norx t7 on t7.patient_id=t0.patient_id
  and t7.case_norx_yr=t0.index_enc_yr and t7.a1c>=7 and t7.a1c<=9
left join gen_pop_tools.cc_diab_case_norx t8 on t8.patient_id=t0.patient_id
  and t8.case_norx_yr=t0.index_enc_yr and t8.a1c>9
left join gen_pop_tools.cc_diab_case_norx t17 on t17.patient_id=t0.patient_id
  and t17.case_norx_yr=t0.index_enc_yr and t17.a1c is null
left join gen_pop_tools.cc_diab_case_rx t9 on t9.patient_id=t0.patient_id
  and t9.case_rx_yr=t0.index_enc_yr
left join gen_pop_tools.cc_diab_case_rx t10 on t10.patient_id=t0.patient_id
  and t10.case_rx_yr=t0.index_enc_yr and t10.a1c<7
left join gen_pop_tools.cc_diab_case_rx t11 on t11.patient_id=t0.patient_id
  and t11.case_rx_yr=t0.index_enc_yr and t11.a1c>=7 and t11.a1c<=9
left join gen_pop_tools.cc_diab_case_rx t12 on t12.patient_id=t0.patient_id
  and t12.case_rx_yr=t0.index_enc_yr and t12.a1c>9
left join gen_pop_tools.cc_diab_case_rx t18 on t18.patient_id=t0.patient_id
  and t18.case_rx_yr=t0.index_enc_yr and t18.a1c is null
left join gen_pop_tools.cc_diab_case_a1c t14 on t14.patient_id=t0.patient_id
  and t14.index_enc_yr=t0.index_enc_yr and t14.a1c<7
left join gen_pop_tools.cc_diab_case_a1c t15 on t15.patient_id=t0.patient_id
  and t15.index_enc_yr=t0.index_enc_yr and t15.a1c>=7 and t15.a1c<=9
left join gen_pop_tools.cc_diab_case_a1c t16 on t16.patient_id=t0.patient_id
  and t16.index_enc_yr=t0.index_enc_yr and t16.a1c>9
left join gen_pop_tools.cc_diab_case_a1c t19 on t19.patient_id=t0.patient_id
  and t19.index_enc_yr=t0.index_enc_yr and t19.a1c is null
group by t0.patient_id, t0.index_enc_yr
 , t0.sex, t0.race_ethnicity, t0.agegroup
 , t1.patient_id 
 , t2.patient_id
 , t3.patient_id
 , t4.patient_id
 , t5.patient_id
 , t6.patient_id
 , t7.patient_id
 , t8.patient_id
 , t9.patient_id
 , t10.patient_id
 , t11.patient_id
 , t12.patient_id
 , t14.patient_id
 , t15.patient_id
 , t16.patient_id
 , t17.patient_id
 , t18.patient_id
 , t19.patient_id
;
GO

--Now get the crossing of all stratifiers
IF object_id('gen_pop_tools.strat1', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat1;
GO
create table gen_pop_tools.strat1 (id1 varchar(2), name1 varchar(15));
GO
insert into gen_pop_tools.strat1 values ('1','index_enc_yr'),('x','x');
GO
IF object_id('gen_pop_tools.strat2', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat2;
GO
create table gen_pop_tools.strat2 (id2 varchar(2), name2 varchar(15));
GO
insert into gen_pop_tools.strat2 values ('2','agegroup'),('x','x');
GO
IF object_id('gen_pop_tools.strat4', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat4;
GO
create table gen_pop_tools.strat4 (id4 varchar(2), name4 varchar(15));
GO
insert into gen_pop_tools.strat4 values ('4','sex'),('x','x');
GO
IF object_id('gen_pop_tools.strat5', 'U') IS NOT NULL DROP TABLE gen_pop_tools.strat5;
GO
create table gen_pop_tools.strat5 (id5 varchar(2), name5 varchar(15));
GO
insert into gen_pop_tools.strat5 values ('5','race_ethnicity'),('x','x');
GO
IF object_id('gen_pop_tools.cc_diab_stratifiers_crossed', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc__diab_stratifiers_crossed;
GO
select * 
INTO gen_pop_tools.cc_diab_stratifiers_crossed
FROM gen_pop_tools.strat1, gen_pop_tools.strat2, gen_pop_tools.strat4, gen_pop_tools.strat5;
GO

--create stub table to contain summary results
IF object_id('gen_pop_tools.cc_diab_summaries', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_diab_summaries;
GO
create table gen_pop_tools.cc_diab_summaries (
  id int not null identity primary key,
  index_enc_yr varchar(4),
  agegroup varchar(10), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total integer,
  a1ctest integer,
  a1ctest3 integer,
  a1ctestever integer,
  diabetic integer,
  norx integer,
  norx7 integer,
  norx79 integer,
  norx9 integer,
  norx_noa1c integer,
  rx integer,
  rx7 integer,
  rx79 integer,
  rx9 integer,
  rx_noa1c integer,
  all7 integer,
  all79 integer,
  all9 integer,
  all_noa1c integer);
GO

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
BEGIN
  DECLARE @groupclauses TABLE(id int, val nvarchar(max));
  DECLARE @groupby nvarchar(max);
  DECLARE @insrtsql nvarchar(max);
  DECLARE @partsql nvarchar(max);
  DECLARE @tmpsql nvarchar(max);
  DECLARE @i integer;
  DECLARE @name1 varchar(max);
  DECLARE @name2 varchar(max);
  DECLARE @name4 varchar(max);
  DECLARE @name5 varchar(max);
  DECLARE cur CURSOR FOR select name1, name2, name4, name5 FROM gen_pop_tools.cc_diab_stratifiers_crossed;
  OPEN cur  
  FETCH NEXT FROM cur INTO @name1, @name2, @name4, @name5 
  WHILE @@FETCH_STATUS = 0 BEGIN
    SET @i=0;
    SET @insrtsql='insert into gen_pop_tools.cc_diab_summaries (index_enc_yr, agegroup, sex, race_ethnicity, 
      total, a1ctest, a1ctest3, a1ctestever, diabetic, norx, norx7, norx79, norx9, norx_noa1c, rx, rx7, rx79, rx9, rx_noa1c,
      all7, all79, all9, all_noa1c) 
      (select';
    if @name1='x' BEGIN 
      SET @tmpsql=' CAST(''x'' AS varchar(max)) index_enc_yr,';
    else
      SET @tmpsql=' index_enc_yr,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'index_enc_yr');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name2='x' BEGIN 
      SET @tmpsql=' CAST(''x'' AS varchar(max)) agegroup,';
    else
      SET @tmpsql=' agegroup,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'agegroup');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name4='x' BEGIN 
      SET @tmpsql=' CAST(''x'' AS varchar(max)) sex,';
    else
      SET @tmpsql=' sex,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'sex');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    if @name5='x' BEGIN 
      SET @tmpsql=' CAST(''x'' AS varchar(max)) race_ethnicity,';
    else
      SET @tmpsql=' race_ethnicity,';
      SET @i=@i+1;
      insert into @groupclauses(id, val) VALUES(@i, 'race_ethnicity');
    END;
    SET @insrtsql=@insrtsql+@tmpsql;
    SET @insrtsql=@insrtsql+'
       count(distinct patient_id) total,
       count(distinct a1ctest) a1ctest,
       count(distinct a1ctest3) a1ctest3,
       count(distinct a1ctestever) a1ctestever,
       count(distinct diabetic) diabetic,
       count(distinct norx) norx,
       count(distinct norx7) norx7,
       count(distinct norx79) norx79,
       count(distinct norx9) norx9,
       count(distinct norx_noa1c) norx_noa1c,
       count(distinct rx) rx,
       count(distinct rx7) rx7,
       count(distinct rx79) rx79,
       count(distinct rx9) rx9,
       count(distinct rx_noa1c) rx_noa1c,
       count(distinct all7) all7,
       count(distinct all79) all79,
       count(distinct all9) all9,
       count(distinct all_noa1c) all_noa1c
       FROM gen_pop_tools.cc_diab_by_pat
       ';
    if @i> 0 BEGIN 
      SET @insrtsql=@insrtsql+'group by ';
      SET @i=1;
      SET @length = (SELECT Count(*) FROM @groupclauses);
      while @i <= @length BEGIN
        if @i >1 BEGIN
          SET @insrtsql=@insrtsql+', '; 
        END;
        SET @groupby = (SELECT val FROM gen_pop_tools.@groupclauses WHERE id=@i);
        SET @insrtsql=@insrtsql+@groupby;
        SET @i=@i+1;
      END;
    END;
    SET @insrtsql=@insrtsql+')';
    execute @insrtsql;
    delete from @groupclauses;
    --raise notice '%', @insrtsql;
    FETCH NEXT FROM gen_pop_tools.cur INTO @name1, @name2, @name4, @name5 
  END;
  CLOSE cur;
  DEALLOCATE cur;
END;
GO

--Gather up the values of the stratifiers and assign code.
IF object_id('gen_pop_tools.cc_agegroup_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_agegroup_codevals;
GO
Create table gen_pop_tools.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
GO
insert into gen_pop_tools.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');
GO

IF object_id('gen_pop_tools.cc_agegroup_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_agegroup_codevals;
GO
Create table gen_pop_tools.cc_agegroup_codevals (sex varchar(7), codeval varchar(1));
GO
insert into gen_pop_tools.cc_agegroup_codevals (sex, codeval) 
values ('Female','1'),('Male','2'),('Unknown','3');
GO

IF object_id('gen_pop_tools.cc_race_ethnicity_codevals', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_race_ethnicity_codevals;
GO
Create table gen_pop_tools.cc_race_ethnicity_codevals (race_ethnicity varchar(45), codeval varchar(1));
GO
insert into gen_pop_tools.cc_cr_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('Asian','1'),('Black_or_African_American','2'),('White','3'),('American_Indian_or_Alaska_Native','4'),('Native_Hawaiian_or_Other_Pacific_Islander','5'),('Unspecified','6');

GO

--now write out to JSON
IF object_id('gen_pop_tools.cc_cr_json', 'U') IS NOT NULL drop table gen_pop_tools.cc_cr_json;
GO
select jsonrow, rownum 
into gen_pop_tools.cc_cr_json
from (
select Convert(varchar(max), '{"tablename": "Continutiy of Care",') as jsonrow, 1 as rownum
union
select convert(varchar(max), '"filters": ["year","age_group","sex","race_ethnicity"],') as jsonrow, 2 as rownum
union 
select convert(varchar(max), '"rowmeta": {"a":{"name":"Individuals with at least one encounter in the chosen year(s)","description":"Total population observed","nest":"0","pcalc":"a"},"b":{"name":"≥1 hemoglobin A1C test in the index year","description":"Count of individuals with ≥1 hemoglobin A1C test in the index year","nest":"1","pcalc":"a"},"c":{"name":"≥1 hemoglobin A1C test within the index or 2 preceding years","description":"Count of individuals with ≥1 hemoglobin A1C test within the index or 2 prior years","nest":"1","pcalc":"a"},"d":{"name":"≥1 hemoglobin A1C test ever","description":"Count of individuals with ≥1 hemoglobin A1C test ever","nest":"1","pcalc":"a"},"e":{"name":"Count of individuals with diabetes","description":"Count of patients with diabetes","nest":"1","pcalc":"a"},"f":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients with hemoglobin A1C <7.0 in the preceding 2 years","nest":"2","pcalc":"e"},"g":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients with hemoglobin A1C 7-9 in the preceding 2 years","nest":"2","pcalc":"e"},"h":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients with hemoglobin A1C >9 in the preceding 2 years","nest":"2","pcalc":"e"},"@i":{"name":"Hemoglobin A1C not checked","description":"Count of patients in whom hemoglobin A1C not checked in preceding 2 years","nest":"2","pcalc":"e"},"j":{"name":"Count of individuals with diabetes NOT on diabetes meds ","description":"Count of diabetes patients not receiving diabetes meds in the preceding 2 years","nest":"2","pcalc":"e"},"k":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"j"},"l":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C 7-9 in the  preceding 2 years","nest":"3","pcalc":"j"},"m":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients not receiving diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"j"},"n":{"name":"Hemoglobin A1C not checked","description":"Count of patients not receiving diabetes meds in whom hemoglobin A1C was not checked in the preceding 2 years","nest":"3","pcalc":"j"},"o":{"name":"Count of individuals with diabetes on meds","description":"Count of diabetes patients prescribed diabetes meds in preceding 2 years","nest":"2","pcalc":"e"},"p":{"name":"Last hemoglobin A1C <7.0","description":"Count of diabetes patients receiving diabetes meds whose most recent hemoglobin A1C <7.0 in the preceding 2 years","nest":"3","pcalc":"o"},"q":{"name":"Last hemoglobin A1C 7-9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C 7-9 in preceding 2 years","nest":"3","pcalc":"o"},"r":{"name":"Last hemoglobin A1C >9","description":"Count of diabetes patients prescribed diabetes meds whose most recent hemoglobin A1C >9 in the preceding 2 years","nest":"3","pcalc":"o"},"s":{"name":"Hemoglobin A1C not checked","description":"Count of patients prescribed diabetes meds whose hemoglobin A1C not checked in preceding 2 years","nest":"3","pcalc":"o"}},') as jsonrow, 3 as rownum
union
select convert(varchar(max), '"rowdata": {') as jsonrow, 4 as rownum
union 
(select convert(varchar(max), '"' + case when index_enc_yr = 'x' then 'YEAR' else index_enc_yr end +
          case when agegroup <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end +
          case when sex <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end +
          case when race_ethnicity <> 'x' 
            then (select codeval FROM gen_pop_tools.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end + '":{"a":'+ total
		  + ',"b":' + a1ctest
		  + ',"c":' + a1ctest3
		  + ',"d":' + a1ctestever
		  + ',"e":' + diabetic
		  + ',"f":' + all7
		  + ',"g":' + all79
		  + ',"h":' + all9
		  + ',"i":' + all_noa1c
		  + ',"j":' + norx
		  + ',"k":' + norx7
		  + ',"l":' + norx79 
		  + ',"m":' + norx9
		  + ',"n":' + norx_noa1c		  
		  + ',"o":' + rx 
		  + ',"p":' + rx7 
		  + ',"q":' + rx79 
		  + ',"r":' + rx9 
		  + ',"s":' + rx_noa1c 
		  + case when id = (select max(id) from gen_pop_tools.cc_diab_summaries) then '}' else '},' end) as jsonrow, id + 4 as rownum
FROM gen_pop_tools.cc_diab_summaries t0 ) 
union 
select convert(varchar(max),'}}') as jsonrow, (select max(id) from gen_pop_tools.cc_cr_risk_summaries) + 5 as rownum) t00;
GO




  

