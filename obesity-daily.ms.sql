-- CCDA Build #1.0.2303 (1014) Updated: Wednesday January 6, 2016 at 8:00am POSTGRESQL
-- Analysis name: Obesity Reporting
-- Analysis description: Catalyst Obesity Reporting
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--

IF object_id('dbo.obesity_a_100033_s_100751', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100751;

IF object_id('dbo.obesity_a_100033_s_100752', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100752;

IF object_id('dbo.obesity_a_100033_s_100760', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100760;

IF object_id('dbo.obesity_a_100033_s_100753', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100753;
IF object_id('dbo.obesity_a_100033_s_100753', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100753;

IF object_id('dbo.obesity_a_100033_s_100754', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100754;
IF object_id('dbo.obesity_a_100033_s_100754', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100754;

IF object_id('dbo.obesity_a_100033_s_100756', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100756;
IF object_id('dbo.obesity_a_100033_s_100756', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100756;

IF object_id('dbo.obesity_a_100033_s_100757', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100757;
IF object_id('dbo.obesity_a_100033_s_100757', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100757;

IF object_id('dbo.obesity_a_100033_s_100761', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100761;
IF object_id('dbo.obesity_a_100033_s_100761', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100761;

IF object_id('dbo.obesity_a_100033_s_100762', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100762;
IF object_id('dbo.obesity_a_100033_s_100762', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100762;

IF object_id('dbo.obesity_a_100033_s_100766', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100766;
IF object_id('dbo.obesity_a_100033_s_100766', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100766;

IF object_id('dbo.obesity_a_100033_s_100768', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100768;
IF object_id('dbo.obesity_a_100033_s_100768', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100768;

IF object_id('dbo.obesity_a_100033_s_100763', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100763;
IF object_id('dbo.obesity_a_100033_s_100763', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100763;

IF object_id('dbo.obesity_a_100033_s_100758', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100758;
IF object_id('dbo.obesity_a_100033_s_100758;', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100758;

IF object_id('dbo.obesity_a_100033_s_100967', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100967;
IF object_id('dbo.obesity_a_100033_s_100967', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100967;

IF object_id('dbo.obesity_a_all_encounters', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_all_encounters;
IF object_id('dbo.obesity_a_all_encounters', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_all_encounters;

IF object_id('dbo.obesity_a_unk_bmi_all', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_unk_bmi_all;
IF object_id('dbo.obesity_a_unk_bmi_all', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_unk_bmi_all;

IF object_id('dbo.obesity_a_unk_bmi_dates', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_unk_bmi_dates;
IF object_id('dbo.obesity_a_unk_bmi_dates', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_unk_bmi_dates;

IF object_id('dbo.obesity_a_bmi_to_process', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_to_process;
IF object_id('dbo.obesity_a_bmi_to_process', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_bmi_to_process;

IF object_id('dbo.obesity_a_bmi_new_unmeasured', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_new_unmeasured;
IF object_id('dbo.obesity_a_bmi_new_unmeasured', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_bmi_new_unmeasured;

IF object_id('dbo.obesity_a_bmi_last2years', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_last2years;
IF object_id('dbo.obesity_a_bmi_last2years', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_bmi_last2years;

IF object_id('dbo.ccda_bmi', 'U') IS NOT NULL DROP TABLE dbo.ccda_bmi;
IF object_id('dbo.ccda_bmi', 'V') IS NOT NULL DROP VIEW dbo.ccda_bmi;
GO
--
-- Script body 
--

-- Step 1: Access - dbo.emr_encounter
CREATE VIEW dbo.obesity_a_100033_s_100751 AS SELECT * FROM dbo.emr_encounter;
GO

-- Step 2: Access - dbo.emr_patient
CREATE VIEW dbo.obesity_a_100033_s_100752 AS SELECT * FROM dbo.emr_patient;
GO

-- Step 3: Access - dbo.esp_condition
CREATE VIEW dbo.obesity_a_100033_s_100760 AS SELECT * FROM dbo.esp_condition;
GO

-- Step 4: Query - Get All BMI Values For Encounters For Date Range. 
SELECT patient_id,bmi,date INTO dbo.obesity_a_all_encounters
FROM dbo.obesity_a_100033_s_100751   
WHERE date >= DATEADD(year, -1, getdate())
GO

-- Step 4: Query - Get Details For Encounters That Have  BMI must be lower than 200
SELECT patient_id,bmi,date INTO dbo.obesity_a_100033_s_100753
FROM  dbo.obesity_a_all_encounters 
WHERE bmi is not null and bmi <= 200;
GO

-- Step 5: Query - Get the most recent date for the BMI patients
SELECT DISTINCT * INTO dbo.obesity_a_100033_s_100754 
FROM (SELECT patient_id,max(date) max_date 
  FROM  dbo.obesity_a_100033_s_100753  
  GROUP BY patient_id)  AS DUMMYALIAS302 ;
GO

-- Step 6: SQL - Get the BMI for the most recent encounter for the patient
SELECT DISTINCT patient_id, date, bmi INTO dbo.obesity_a_100033_s_100756
FROM (SELECT DISTINCT T1.patient_id, date, bmi, ROW_NUMBER() OVER (PARTITION BY T1.patient_id ORDER BY T1.patient_id) AS rownumber
    FROM dbo.obesity_a_100033_s_100753 T1, dbo.obesity_a_100033_s_100754 T2
    WHERE T1.patient_id = T2.patient_id 
    AND date = T2.max_date)  AS DUMMYALIAS303 WHERE rownumber=1;
GO

-- Gather Details for Patients With No BMI In the Time Period
SELECT patient_id INTO dbo.obesity_a_unk_bmi_all
FROM dbo.obesity_a_all_encounters
WHERE bmi is null 
GROUP BY patient_id
EXCEPT
SELECT patient_id
FROM dbo.obesity_a_all_encounters
WHERE bmi is NOT null 
group by patient_id;
GO

-- Get the most recent date for No Measured BMI patients
SELECT DISTINCT * INTO dbo.obesity_a_unk_bmi_dates
FROM (SELECT T1.patient_id,max(date) date, T2.bmi 
  FROM  dbo.obesity_a_unk_bmi_all T1, dbo.obesity_a_all_encounters T2
  WHERE T1.patient_id = T2.patient_id   
  GROUP BY T1.patient_id, T2.bmi)  AS DUMMYALIASt302 ;
GO

-- Union together all the BMI data (measured and not measured)
SELECT patient_id,date,bmi INTO dbo.obesity_a_bmi_to_process  
FROM (SELECT patient_id, date, bmi FROM dbo.obesity_a_100033_s_100756
UNION SELECT patient_id, date, bmi FROM dbo.obesity_a_unk_bmi_dates) AS DUMMYALIASu302;
GO

-- Step 7: Query - Get fields from patient table for population in to esp_condition. Derive condition for patient.
SELECT DISTINCT * INTO dbo.obesity_a_100033_s_100757   
FROM (SELECT case when T2.center_id is null or T2.center_id = '' then CAST('1' AS varchar(1)) else T2.center_id end centerid,
T2.natural_key patid,
case when bmi < 25 then 'BMI <25'  
when bmi >= 25 and bmi <30 then 'BMI >=25 and <30'
when bmi >= 30 then 'BMI >= 30' 
when bmi is null then 'No Measured BMI' 
end condition,
datediff(day, '1960-01-01', date) date,
--this is really age at detect date. Due to age restrictions on BMI, we need to be more specific
datediff(day, date_of_birth, date)/365.25 age_at_detect_year,
--datepart(year, date) - datepart(year, date_of_birth) age_at_detect_year,
date date_orig,patient_id 
FROM dbo.obesity_a_bmi_to_process
INNER JOIN dbo.obesity_a_100033_s_100752 T2 
ON ((patient_id = T2.id) AND (patient_id = T2.id)) )  AS DUMMYALIAS304 ;
GO

-- Step 8: Query - Patients Who Do Not Already Have This Condition 
-- Exclude "No Measured BMI Entires"
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year INTO dbo.obesity_a_100033_s_100761   
FROM dbo.obesity_a_100033_s_100757 T1
LEFT OUTER JOIN dbo.obesity_a_100033_s_100760 T2 ON ((T1.patid = T2.patid) AND (T1.condition = T2.condition))  
WHERE  T2.patid is null
AND T1.condition != 'No Measured BMI';
GO

-- Step 9: Query - Get New Data For Patients Who Already Have This Condition But The Data Is Over A Year Old 
-- Exclude "No Measured BMI Entires"
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year INTO dbo.obesity_a_100033_s_100762   
FROM dbo.obesity_a_100033_s_100757 T1
INNER JOIN dbo.obesity_a_100033_s_100760 T2 ON ((T1.patid = T2.patid) AND (T1.condition = T2.condition)) 
WHERE T1.condition != 'No Measured BMI'  
GROUP BY T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year HAVING T1.date > (max(T2.date) + 365);
GO

-- Step 10: SQL - Derive max_date for existing BMI conditions in the esp_condition table 
SELECT  *, max(date) OVER (PARTITION BY patid) as max_date INTO dbo.obesity_a_100033_s_100766
FROM dbo.obesity_a_100033_s_100760
where lower(condition) like '%bmi%';
GO

-- Step 11: Query - Patients Who Have Had This Condition But the Most Recent Condition Is Different
-- Exclude "No Measured BMI Entires"
SELECT T1.centerid,T1.patid,T1.condition,T1.date,T1.age_at_detect_year INTO dbo.obesity_a_100033_s_100768   
FROM dbo.obesity_a_100033_s_100757 T1
INNER JOIN dbo.obesity_a_100033_s_100766 T2 ON ((T1.patid = T2.patid))  
WHERE  T2.date =  T2.max_date and  T1.condition <>  T2.condition 
AND T1.condition != 'No Measured BMI'
and T2.patid not in (
	select T1.patid 
	from dbo.esp_condition T1, dbo.obesity_a_100033_s_100766 T2
	where T2.date =  T2.max_date and  T1.condition =  T2.condition
	AND T1.patid = T2.patid) ;
GO
	
-- For efficiency of next query create a table that contains current query patients
-- who have a max bmi in the last 2 years
SELECT T2.patid INTO dbo.obesity_a_bmi_last2years 
FROM dbo.obesity_a_100033_s_100766 T1, dbo.obesity_a_100033_s_100757 T2 
WHERE T1.patid = T2.patid
AND lower(T1.condition) like '%bmi%' 
AND max_date > T2.date - 730;
GO
	
-- We only want to assign the "No Measured BMI" condition to patients 
-- who have not had a BMI condition in the last 2 years. 
-- Otherwise, each null BMI entry will overwrite a possible recent good entry
SELECT centerid,patid,condition,date,age_at_detect_year INTO dbo.obesity_a_bmi_new_unmeasured 
FROM dbo.obesity_a_100033_s_100757
WHERE condition = 'No Measured BMI'
AND patid not in (SELECT patid 
                  FROM dbo.obesity_a_bmi_last2years); 
GO

-- Step 12: Union - Union Together Both The New and Updated Entries (Also eliminates duplicates between new and diff conditions)
SELECT centerid,patid,condition,date,age_at_detect_year INTO dbo.obesity_a_100033_s_100763   
FROM dbo.obesity_a_100033_s_100761 
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM dbo.obesity_a_100033_s_100762) 
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM dbo.obesity_a_100033_s_100768)
UNION (SELECT centerid,patid,condition,date,age_at_detect_year FROM dbo.obesity_a_bmi_new_unmeasured);
GO

-- Step 13: Query - Derive Age Groups & Critera
SELECT DISTINCT * INTO dbo.obesity_a_100033_s_100758 
FROM 
(SELECT centerid,patid,condition,date,age_at_detect_year,
CASE when (age_at_detect_year <= 9) then '0-9'when (age_at_detect_year <=19) then '10-19' 
when (age_at_detect_year <=29) then '20-29' when (age_at_detect_year <=39) then '30-39' 
when (age_at_detect_year <=49) then '40-49' when (age_at_detect_year <=59) then '50-59' 
when (age_at_detect_year <=69) then '60-69' when (age_at_detect_year <=79) then '70-79' 
when (age_at_detect_year <=89) then '80-89' when (age_at_detect_year <=99) then '90-99' 
else'100+'
end age_group_10yr,
CASE when  (age_at_detect_year) <= 4  THEN  '0-4' 
when age_at_detect_year <= 9  THEN '5-9' 
when age_at_detect_year <= 14 THEN '10-14' 
when age_at_detect_year <= 19 THEN '15-19' 
when age_at_detect_year <= 24 THEN '20-24' 
when age_at_detect_year <= 29 THEN '25-29' 
when age_at_detect_year <= 34 THEN '30-34' 
when age_at_detect_year <= 39 THEN '35-39' 
when age_at_detect_year <= 44 THEN '40-44' 
when age_at_detect_year <= 49 THEN '45-49' 
when age_at_detect_year <= 54 THEN '50-54' 
when age_at_detect_year <= 59 THEN '55-59' 
when age_at_detect_year <= 64 THEN '60-64' 
when age_at_detect_year <= 69 THEN '65-69' 
when age_at_detect_year <= 74 THEN '70-74' 
when age_at_detect_year <= 79 THEN '75-79' 
when age_at_detect_year <= 84 THEN '80-84' 
when age_at_detect_year <= 89 THEN '85-89' 
when age_at_detect_year <= 94 THEN '90-94' 
when age_at_detect_year <= 99 THEN '95-99' 
ELSE '100+' 
END age_group_5yr,
CASE when age_at_detect_year <= 1  THEN '0-1' 
when age_at_detect_year <= 4 THEN '2-4' 
when age_at_detect_year <= 9 THEN '5-9' 
when age_at_detect_year <= 14 THEN '10-14' 
when age_at_detect_year <= 18 THEN '15-18' 
when age_at_detect_year <= 21 THEN '19-21' 
when age_at_detect_year <= 44 THEN '22-44' 
when age_at_detect_year <= 64 THEN '45-64' 
when age_at_detect_year <= 74 THEN '65-74' 
ELSE '75+' 
END  age_group_ms,
case when condition =  'BMI <25' then  'BMI Less Than 25' 
when condition =  'BMI >= 30' then  'BMI Greater Than or Equal To 30' 
when condition =  'BMI >=25 and <30' then  'BMI Greater Than or Equal To 25 and Less Than 30' 
when condition = 'No Measured BMI' then 'BMI Value Has Never Been Recorded'
end criteria,
'NO' status,
cast(null as varchar(max)) notes 
FROM  dbo.obesity_a_100033_s_100763)  AS DUMMYALIAS305 ;
GO

-- Step 14: Query - Filter Out Patients That Are Under the Age of 20 & Overwrite Center ID For Atrius & CHA. Needs to be removed for MLCHC.
SELECT '1' centerid,
patid,
condition,
date,
age_at_detect_year,
age_group_10yr,
age_group_5yr,
age_group_ms,
criteria,
status,
notes 
INTO dbo.obesity_a_100033_s_100967
FROM  dbo.obesity_a_100033_s_100758;   
-- Going to create for all age groups and then limit population to MDPHnet
-- and other data locations as required
--WHERE  age_at_detect_year >= 20;
GO

-- Step 15: Save - ccda_bmi
SELECT * INTO dbo.ccda_bmi
FROM dbo.obesity_a_100033_s_100967;
GO

-- Populate the ESP Condition Table

INSERT INTO dbo.esp_condition SELECT *, null, null from dbo.ccda_bmi;
GO

--
-- Script shutdown section 
--

IF object_id('dbo.obesity_a_100033_s_100751', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100751;

IF object_id('dbo.obesity_a_100033_s_100752', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100752;

IF object_id('dbo.obesity_a_100033_s_100760', 'V') IS NOT NULL DROP VIEW dbo.obesity_a_100033_s_100760;

IF object_id('dbo.obesity_a_100033_s_100753', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100753;

IF object_id('dbo.obesity_a_100033_s_100754', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100754;

IF object_id('dbo.obesity_a_100033_s_100756', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100756;

IF object_id('dbo.obesity_a_100033_s_100757', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100757;

IF object_id('dbo.obesity_a_100033_s_100761', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100761;

IF object_id('dbo.obesity_a_100033_s_100762', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100762;

IF object_id('dbo.obesity_a_100033_s_100766', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100766;

IF object_id('dbo.obesity_a_100033_s_100768', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100768;

IF object_id('dbo.obesity_a_100033_s_100763', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100763;

IF object_id('dbo.obesity_a_100033_s_100758', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100758;

IF object_id('dbo.obesity_a_100033_s_100967', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_100033_s_100967;

IF object_id('dbo.obesity_a_all_encounters', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_all_encounters;

IF object_id('dbo.obesity_a_unk_bmi_all', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_unk_bmi_all;

IF object_id('dbo.obesity_a_unk_bmi_dates', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_unk_bmi_dates;

IF object_id('dbo.obesity_a_bmi_to_process', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_to_process;

IF object_id('dbo.obesity_a_bmi_last2years', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_last2years;

IF object_id('dbo.obesity_a_bmi_new_unmeasured', 'U') IS NOT NULL DROP TABLE dbo.obesity_a_bmi_new_unmeasured;
GO
