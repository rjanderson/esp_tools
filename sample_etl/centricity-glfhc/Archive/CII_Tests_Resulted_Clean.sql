/* Fenway notes to GLFHC: Fenway pulls test data from the OBS table where results are entered. Information on the type of test comes from the test name.
** Depending how GLFHC's system is set up, you may have more data available on the lab order, rather than just the result. Comments in the code indicate certain limitations to this code's approach.
** No data is reported here for the last 13 columns. These default to null in ESP.*/

SET NOCOUNT ON
DECLARE @StartDt DATE
DECLARE @EndDt DATE
SET @StartDt = getdate() - 30
SET @EndDt = getdate()

SELECT pp.pid 'PatientId',
	   pp.patientid 'MRN',
	   obs.obsid 'LabOrderId',
	  --The obsdate represents the date on which the specimen was collected, usually but not always the day the test was ordered
	  convert(nvarchar,year(obs.obsdate)) + right('0' + convert(nvarchar,month(obs.obsdate)), 2) + '' + right('0' + convert(nvarchar,day(obs.obsdate)), 2) as order_date,
	  --Since results are most often input into the obs table automatically, the db_updated_date usually represents the day Fenway received the results
	  convert(nvarchar,year(obs.db_updated_date)) + right('0' + convert(nvarchar,month(obs.db_updated_date)), 2) + '' + right('0' + convert(nvarchar,day(obs.db_updated_date)), 2) as result_date,
	  ISNULL(CAST(u.doctorfacilityid AS VARCHAR),'') 'OrderProvId',
	  null as order_type,
	  substring(oh.mlcode,5, len(oh.mlcode)) as code, /*LOINC and MLI codes*/
	  oh.mlcode as component,
	  oh.description as component_name,
	  ISNULL(obs.obsvalue,'') 'TestResults',
	  ISNULL(obs.Abnormal,'') 'Abnormal',
	  CASE WHEN SUBSTRING(obs.range,1,1) = '<' THEN ''
			WHEN SUBSTRING(obs.range,1,1) = '>' THEN obs.range
			WHEN obs.range LIKE '%-%' THEN SUBSTRING(range, 1, CHARINDEX('-', range)-1)
			WHEN obs.range IS NULL THEN ''
			ELSE obs.range
	   END 'RefLowValue',
	  CASE WHEN SUBSTRING(obs.range,1,1) = '>' THEN ''
			WHEN SUBSTRING(obs.range,1,1) = '<' THEN obs.range
			WHEN obs.range LIKE '%-%' THEN SUBSTRING(range, CHARINDEX('-', range)+1,LEN(range))
			WHEN obs.range IS NULL THEN ''
			ELSE ''
	   END 'RefHighValue',
	   ISNULL(oh.unit,'') 'Unit',
	   CASE WHEN obs.state is NULL 
				OR obs.state = '' THEN 'F'
			ELSE obs.state 
	   END 'ResultStatus',
	  ISNULL(obs.description,'') 'ResultComments',
	  null as specimen_num,
	  null as impression,
	  CASE WHEN oh.MLCODE IN ('MLI-14722','LOI-0691-6' ) THEN 'Urine' /*Chlamydia AND Gonorrhoeae urine*/
			WHEN oh.MLCODE IN ('MLI-314329', 'MLI-359460') THEN 'Throat' /*Chlamydia AND Gonorrhoeae throat*/
			WHEN oh.MLCODE IN ('MLI-117631', 'MLI-359459') THEN 'Rectum'/*Chlamydia, Gonorrhoeae rectum*/
			WHEN oh.MLCODE IN ('LOI-7918-6', 'MLI-144815', 'MLI-498.244', 'MLI-90469','MLI-70594') THEN 'Blood' /*HIVand Syphillis*/
			WHEN oh.MLCODE IN ('LOI-5193-8', 'LOI-5195-3') THEN 'Blood' /*Hep B*/
			WHEN oh.MLCODE IN ('LOI-5199-5', 'MLI-203162', 'MLI-4314') THEN 'Blood' /*Hep C*/
			WHEN oh.MLCODE IN ('MLI-7010') THEN 'Skin Test'
			WHEN oh.MLCODE IN ('MLI-226928') THEN 'Blood'
			WHEN oh.MLCODE IS NULL THEN ''
			ELSE oh.MLCODE 
	   END 'SpecimenSource',
	   ISNULL(CONVERT(VARCHAR(8), obs.db_updated_date, 112),'') 'StatusChgDt'
  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.obs obs ON pp.PId = obs.PID
  LEFT JOIN centricityps.dbo.usr u ON obs.pubuser = u.pvid
  LEFT JOIN centricityps.dbo.obshead oh ON obs.hdid = oh.hdid

WHERE obs.db_updated_date BETWEEN @StartDt AND @EndDt
   AND obs.xid = 1000000000000000000
   AND obs.change NOT IN (0,4,10,11,12) /*Excluded deleted data.*/
--and (obs.hdid in (2719, 2746, 2772, 3552, 3553, 5556, 7293, 45361, 76052, 80012, 110694, 117632, 153580, 240141, 274149, 274150, 274151, 274152, 12700004, 537507, 587011) /*gonorrhea*/
--or obs.hdid in (274156, 274153, 274154, 11209, 117631, 274155, 2100025, 6857, 2679, 2716, 240140, 249524, 587012)) /*chlamydia*/
--/*Exclude recently updated entries for results over 45 days old*/
--and obs.obsdate >= dateadd(dd,-30,@startdate)
   AND pp.PatientStatusMId <> '-903'
   /*Exclude test patients*/            
   AND pp.PatientId NOT LIKE '9999%'
   AND obs.OBSVALUE NOT LIKE  'Pending'
   AND obs.HDID <> -1