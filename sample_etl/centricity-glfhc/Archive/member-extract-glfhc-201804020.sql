
SET NOCOUNT ON

SELECT pp.PId 'PatientId'
	  ,pp.PatientId 'MRN'
	  ,pp.Last 'PatLastNm'
	  ,pp.First 'PatFirstNm'
	  ,ISNULL(pp.Middle,'') 'PatMidNm'
	  ,pp.Address1 'AddrLine1'
	  ,ISNULL (pp.Address2,'') 'AddrLine2'
	  ,pp.City
	  ,pp.State
      ,pp.Zip
	  ,ISNULL (pp.Country,'') 'Country'
	  ,left(pp.phone1,3) 'HomePhoneAreaCode'
	  ,substring(pp.phone1,4,3) + '-' + substring(pp.phone1,7,4) 'HomePhoneNbr'
	  ,'' 'Extension'
	  ,CONVERT(VARCHAR(8), pp.birthdate, 112) 'Encounter_Date'
	  ,pp.Sex 'Gender'
	  ,re.Race 'Race'
	  ,re.PatientPrefLanguage 'Home Language'
	  ,ISNULL (substring(pp.ssn,1,3) + '-' + substring(pp.ssn,4,2) + '-' + substring(pp.ssn,6,4),'') 'SSN'
	  ,pp.DoctorId 'PCP_ID'
	  ,ISNULL (ml.Description,'') 'MaritalStatus'
	  ,'' 'Religion'
	  ,pp.Nickname 'Aliases'
	  ,'' 'MotherMRN'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.DeathDate, 112),'') 'DeathDate'
	  ,pp.FacilityId as 'CenterID'
	  ,CASE WHEN re.UDSEthnicityCode = 'H' THEN 'Hispanic'
            WHEN re.UDSEthnicityCode = 'N' THEN 'Non Hispanic'
            WHEN re.UDSEthnicityCode = 'NR' THEN 'Not Reported'
       END 'Ethnicity'
	  ,ISNULL (pp.MothersMaiden, '')  'MotherMaidenNm'
	  ,CONVERT(VARCHAR(8), pp.lastmodified, 112) 'LastUpdtDt'
      ,pp.FacilityId 'LastUpdtSiteId'
	  ,ISNULL (pp.Suffix,'') 'NameSuffix'
	  ,ISNULL (pp.Prefix,'') 'Titles'
	  ,'' 'remark'
	  ,'' 'Income Level'
	  ,'' 'HousingStatus'
	  ,'' 'InsuranceStatus'
	  ,'' 'BirthCountry'

  FROM centricityps.dbo.PatientProfile pp
  JOIN centricityps.dbo.Appointments a ON pp.PatientProfileId = a.OwnerId
        AND CAST(ApptStart AS DATE) BETWEEN DATEADD(MONTH,-12,GETDATE()) AND GETDATE()
        AND a.Canceled is null
        AND a.ApptStatusMId = 314
        AND a.ApptStart = (SELECT MAX(b.ApptStart)
                             FROM centricityps.dbo.Appointments b
                            WHERE a.OwnerId = b.OwnerId
                              AND CAST(b.ApptStart AS DATE) BETWEEN DATEADD(MONTH,-24,GETDATE()) AND GETDATE()
                              AND b.Canceled is null
                              AND b.ApptStatusMId = 314  -- 'Checked Out'
                           GROUP BY b.OwnerId)
  LEFT JOIN centricityps.dbo.cusUDSPatientRaceEthnicity_2017 re ON pp.PatientProfileId = re.PatientProfileID
  LEFT OUTER JOIN centricityps.dbo.MedLists ml ON pp.MaritalStatusMId = ml.MedListsId
  --LEFT JOIN centricityps.dbo.cusCRIInterview cpi ON pp.PatientProfileId = cpi.PatientProfileID
  --LEFT JOIN centricityps.dbo.cusCRIMedLists ml2 ON cpi.HomelessStatusID = ml2.MedListsId
  --LEFT JOIN centricityps.GLFHC.cusFPLG fplg ON cpi.FamilySize = fplg.FamilySize
 WHERE (pp.PatientStatusMId = '-900'
   AND pp.DoctorId <> 569
   AND pp.PatientId NOT LIKE '9999%')

/*
   --AND (cpi.Homeless IS NOT NULL AND cpi.Homeless <> 0)
note: if mulitple records in cuspatientslidingfeehst table, then take max(CUSPATIENTSLIDINGFEEHSTID)"
*/