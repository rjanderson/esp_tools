#!/bin/bash
d="2017-01-01"
until [[ $d > 2023-01-01 ]]; do
  echo "$d"
  mstart=$(date -d "$d" +%Y%m%d)
  mend=$(date -d "$d + 1 month - 1 day" +%Y%m%d)
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f '/srv/esp/scripts/alt_load_scripts/load_labresult.pg.sql' -v startdt=$mstart -v enddt=$mend
  d=$(date -I -d "$d + 1 month")
done
