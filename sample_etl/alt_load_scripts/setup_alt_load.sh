#!/bin/bash
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f /srv/esp/scripts/alt_load_scripts/move_esp_to_prior.pg.sql
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f /srv/esp/scripts/alt_load_scripts/create_tmp_load_tables.pg.sql
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f /srv/esp/scripts/alt_load_scripts/truncate_esp_tables.pg.sql
