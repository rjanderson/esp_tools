update emr_patient set date_of_birth=null::date where date_of_birth<'1900-01-01'::timestamp;

delete from emr_encounter where date<'2017-01-01'::date;
update emr_encounter 
set date_closed=null::date, hosp_admit_dt=null::date, hosp_dschrg_dt=null::date;

