drop table if exists ccda_bmi;
drop table if exists ccda_opibenzo;
drop table if exists clin_enc_counts;
--caseactivehistory
drop table if exists esp_prior.nodis_caseactivehistory;
select * 
into esp_prior.nodis_caseactivehistory
from nodis_caseactivehistory;
truncate table nodis_caseactivehistory restart identity;
--case events
drop table if exists esp_prior.nodis_case_events;
select * 
into esp_prior.nodis_case_events
from nodis_case_events;
truncate table nodis_case_events restart identity;
--case timespans
drop table if exists esp_prior.nodis_case_timespans;
select * 
into esp_prior.nodis_case_timespans
from nodis_case_timespans;
truncate table nodis_case_timespans restart identity;
--cases
drop table if exists esp_prior.nodis_case;
select * 
into esp_prior.nodis_case
from nodis_case;
truncate table nodis_case restart identity cascade;
--timespan events
drop table if exists esp_prior.hef_timespan_events;
select * 
into esp_prior.hef_timespan_events
from hef_timespan_events;
truncate table hef_timespan_events restart identity;
--timespans
drop table if exists esp_prior.hef_timespan;
select * 
into esp_prior.hef_timespan
from hef_timespan;
truncate table hef_timespan restart identity cascade;
--events
drop table if exists esp_prior.hef_event;
select * 
into esp_prior.hef_event
from hef_event;
create index hef_content_type_idx 
  on esp_prior.hef_event (content_type_id);
analyse esp_prior.hef_event;
truncate table hef_event restart identity cascade;
select distinct he.object_id as id
into temporary hef_encs
from esp_prior.hef_event he
join django_content_type ct on he.content_type_id=ct.id
where ct.model='encounter';
--emr_encounter
drop table if exists emr_prior.emr_encounter;
select enc.id, enc.patient_id, date, bp_systolic, bp_diastolic, weight, height, bmi, 
       raw_encounter_type, primary_payer
into esp_prior.emr_encounter
from emr_encounter enc
join hef_encs he on he.id=enc.id;
drop table if exists esp_prior.emr_encounter_dx_codes;
select dx.*
into esp_prior.emr_encounter_dx_codes 
from emr_encounter_dx_codes dx
join hef_encs he on he.id=dx.encounter_id;
truncate table emr_encounter_dx_codes restart identity;
truncate table emr_encounter restart identity cascade;
drop table hef_encs;
--hef res
select distinct he.object_id as id
into temporary hef_res
from esp_prior.hef_event he
join django_content_type ct on he.content_type_id=ct.id
where ct.model='labresult';
--emr_labresults
drop table if exists emr_prior.emr_labresult;
select lx.id, patient_id, date, native_code, result_date, ref_high_float, 
       ref_low_float, ref_text, ref_unit, result_string, result_float, specimen_source
into esp_prior.emr_labresult
from emr_labresult lx
join hef_res hr on hr.id=lx.id;
truncate table emr_labresult restart identity cascade;
drop table hef_res;
--hef med
select distinct he.object_id as id
into temporary hef_med
from esp_prior.hef_event he
join django_content_type ct on he.content_type_id=ct.id
where ct.model='prescription';
--emr_prescription
drop table if exists emr_prior.emr_prescription;
select rx.id, patient_id, date, name, start_date, end_date
into esp_prior.emr_prescription
from emr_prescription rx
join hef_med hm on hm.id=rx.id;
truncate table emr_prescription restart identity cascade;
drop table hef_med;
--soc hist
drop table if exists esp_prior.emr_socialhistory;
select id, date, patient_id, tobacco_use 
into esp_prior.emr_socialhistory
from emr_socialhistory;
truncate table emr_socialhistory restart identity;
--imm
drop table if exists esp_prior.emr_immunization;
select id, date, patient_id, imm_type, name
into esp_prior.emr_immunization
from emr_immunization;
truncate table emr_immunization restart identity cascade;
--esp_condition
truncate table esp_condition restart identity;
--emr_patient
select * 
into temporary tmp_patient
from emr_patient
where id=1;
truncate table emr_patient restart identity cascade;
insert into emr_patient select * from tmp_patient;
alter sequence emr_patient_id_seq start 2;
drop table tmp_patient;
--emr_provenance
delete from emr_provenance where provenance_id>2;
select setval('emr_provenance_provenance_id_seq', 2);

truncate table emr_etlerror restart identity cascade;
