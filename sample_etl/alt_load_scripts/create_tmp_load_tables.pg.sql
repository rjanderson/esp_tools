drop table if exists temp_pat;
Create table temp_pat
(natural_key character varying(128),
 mrn character varying(128),
 last_name_null character varying(1),
 first_name_null character varying(1),
 middle_name_null character varying(1),
 address1_null character varying(1),
 address2_null character varying(1),
 city character varying(1),
 state character varying(20),
 zip character varying(20),
 country_null character varying(1),
 areacode_null character varying(1),
 tel_null character varying(1),
 tel_ext_null character varying(1),
 date_of_birth character varying(8), --yyyymmdd
 gender character varying(20),
 race character varying(100),
 home_language_null character varying(1),
 ssn_null character varying(1),
 pcp_id_null character varying(1),
 marital_stat_null character varying(1),
 religion_null character varying(1),
 aliases_null character varying(1),
 mother_mrn_null character varying(1),
 date_of_death  character varying(8),
 center_id_null character varying(1),
 ethnicity character varying(100));
drop table if exists temp_enc;
Create table temp_enc(
  patient_natural_key character varying(128),
  mrn character varying(128),
  natural_key character varying(128),
  date character varying(8),
  is_closed_null character varying(1),
  date_closed_null character varying(1),
  provider_id_null character varying(1),
  site_natural_key_null character varying(1),
  site_name_null character varying(1),
  raw_encounter_type character varying(100),
  edd_null character varying(1),
  raw_temperature character varying(6),
  cpt_null character varying(1),
  raw_weight text,
  raw_height text,
  raw_bp_systolic text,
  raw_bp_diastolic text,
  raw_o2_stat_null character varying(1),
  raw_peak_flow_null character varying(1),
  dx_code_id text,
  raw_bmi text,
  hosp_admit_dt_null character varying(1),
  hosp_dschrg_dt character varying(8),
  primary_payer character varying(100));
drop table if exists temp_res;
create table temp_res(
  patient_natural_key character varying(128),
  mrn character varying(128),
  order_natural_key_null character varying(1),
  date character varying(8),
  result_date character varying(8),
  provider_id_null character varying(1),
  order_type_null character varying(1),
  cpt_native_code_null character varying(1),
  native_code character varying(225),
  native_name character varying(225),
  result_string character varying(2000),
  abnormal_flag character varying(20),
  ref_low_string character varying(100),
  ref_high_string character varying(100),
  unit character varying(20),
  status_null character varying(1),
  note_null character varying(1),
  specimen_num_null character varying(1),
  impression_null character varying(1),
  specimen_source character varying(255),
  collection_date character varying(28),
  procedure_name_null character varying(1),
  natural_key character varying(255));
drop table if exists temp_soc;
create table temp_soc (
  patient_natural_key character varying(128),
  mrn character varying(128),
  tobacco_use character varying(100),
  alcohol_use_null character varying(1),
  date_noted character varying(8),
  natural_key character varying(128));
drop table if exists temp_imm;
create table temp_imm (
  patient_natural_key character varying(128),
  imm_type character varying(200),
  name character varying(300),
  date character varying(8),
  dose_null character varying(1),
  manufacturer_null character varying(1),
  lot_null character varying(1),
  natural_key character varying(128));
drop table if exists temp_med;
create table temp_med (
  patient_natural_key character varying(128),
  mrn character varying(128),
  order_natural_key character varying(128),
  provider_id_null character varying(1),
  date character varying(8),
  status_null character varying(1),
  directions_null character varying(1),
  code character varying(20),
  name character varying(3000),
  quantity character varying(128),
  refills character varying(200),
  start_date character varying(8),
  end_date character varying(8),
  route character varying(200) );
