insert into emr_patient (natural_key, created_timestamp, updated_timestamp, mrn,
    city, state, zip, zip5, date_of_birth, cdate_of_birth, gender, race, ethnicity,
    date_of_death, provenance_id)
select natural_key, now()::timestamp(0), now()::timestamp(0), mrn, city, state, zip, substr(zip,1,5), 
   case when length(trim(date_of_birth))=8 then to_timestamp(date_of_birth,'yyyymmdd') else null end, 
   date_of_birth, gender, race, ethnicity, 
   case when length(trim(date_of_death))=8 then to_date(date_of_death,'yyyymmdd') else null end, 1
   from temp_pat;
update emr_patient set date_of_birth=null::timestamp where date_of_birth<='1900-01-01'::timestamp;
update emr_patient set date_of_death=null::date where date_of_death<='2018-03-01'::date;
