#!/srv/esp/prod/bin/python
"""
Name: esp_sql.py

Purpose:  Container for SQL commands.

Modifications:
    17-Apr-2020 Created.  MC
	29-May-2020 Update queries. MC
"""

import esp_gd

SELECT_ESP_Patient_TABLE="""
SELECT	p.person_id AS natural_key,
		p.person_source_value AS mrn,
		NULL AS last_name,
		NULL AS first_name,
		NULL AS middle_name,
		NULL AS address1,
		NULL AS address2,
		loc.city AS city,
		loc.state AS state,
		loc.zip AS zip,
		NULL AS country,
		NULL AS areacode,
		NULL AS tel,
		NULL AS tel_ext,
        case
            when p.year_of_birth is null then null
            when p.month_of_birth is null then trim(to_char(p.year_of_birth,'9999'))
            when p.day_of_birth is null then trim(to_char(p.year_of_birth,'9999')) || trim(to_char(p.month_of_birth,'fm00'))
            else trim(to_char(p.year_of_birth,'9999')) || trim(to_char(p.month_of_birth,'fm00')) || trim(to_char(p.day_of_birth,'fm00'))
        end date_of_birth,
		gcon.concept_name gender,
		rcon.concept_name race,
		NULL AS home_language,
		NULL AS ssn,
		NULL AS pcp_id,
		NULL AS marital_stat,
		NULL AS religion,
		NULL AS aliases,
		NULL AS mother_mrn,
		to_char(d.death_date,'yyyymmdd') AS date_of_death,
		NULL AS center_id,
		econ.concept_name ethnicity
FROM person p
left join concept gcon on gcon.concept_id=p.gender_concept_id
left join location loc on loc.location_id=p.location_id
left join concept rcon on rcon.concept_id=p.race_concept_id
left join concept econ on econ.concept_id=p.ethnicity_concept_id
left join death d on p.person_id=d.person_id
""".format(esp_gd.today, esp_gd.yesterday
       ,esp_gd.today, esp_gd.yesterday
       ,esp_gd.today, esp_gd.yesterday
       ,esp_gd.today, esp_gd.yesterday)

ESP_Patient_DICT= {'SELECT':SELECT_ESP_Patient_TABLE}

CREATE_ESP_TMP_ICD_TABLE="""
create temporary table tmp_dx_arrays as
select array_agg('icd10:'||icd_code) as dx_list,
           visit_occurrence_id, person_id,
           condition_start_date
           from (select distinct
                        case
                          when split_part(co.condition_source_value,'^', 3) is not null
                               then split_part(co.condition_source_value,'^', 3)
                          else target.concept_code end as icd_code,
                        case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                          else array[co.person_id::bigint, 
                            to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else co.condition_start_date end,'yyyymmdd')::bigint]
                        end visit_occurrence_id, 
                        case when v.person_id is not null then v.person_id else co.person_id end person_id,
                        case when v.visit_occurrence_id is not null then v.visit_start_date else co.condition_start_date end condition_start_date,
                 min(length(target.concept_code)) over (partition by case when co.visit_occurrence_id is not null then array[co.visit_occurrence_id::bigint]
                          else array[co.person_id::bigint, to_char(co.condition_start_date,'yyyymmdd')::bigint]
                        end, source.concept_id) minlen
                 from condition_occurrence co
                 join concept source on source.concept_id=co.condition_concept_id
                 join concept_relationship rel on rel.concept_id_2 = source.concept_id and rel.relationship_id = 'Maps to'
                 join concept target on target.concept_id = rel.concept_id_1
                 left join visit_occurrence v on v.visit_occurrence_id=co.visit_occurrence_id
                 where source.vocabulary_id='SNOMED'
                    and target.vocabulary_id ='ICD10CM'
                    and co.condition_start_date between {} and {} ) t0
where minlen=length(icd_code)
group by t0.visit_occurrence_id, t0.person_id, condition_start_date
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_ICD_TABLE="""
create index tmp_dx_arrays_idx on tmp_dx_arrays using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_ICD_TABLE="""
analyse tmp_dx_arrays
"""

CREATE_ESP_TMP_WT_TABLE="""
create temporary table tmp_wt as
select case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
       case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end measurement_date, 
       case when v.person_id is not null then v.person_id else m.person_id end person_id,
  max(case 
    when unit_concept_id=9374 then to_char(floor(m.value_as_number/16),'9999') || ' lb ' ||
           to_char(mod(m.value_as_number,16),'99') || ' oz'  --ounce
    when unit_concept_id in (9529, 3197927, 3023166) then to_char(floor(m.value_as_number*2.20462),'9999') || ' lb ' ||
           to_char(floor((m.value_as_number*2.20462-floor(m.value_as_number*2.20462))*16),'99') || ' oz'  --kilogram
    when unit_concept_id = 8739 then to_char(floor(m.value_as_number),'9999') || ' lb ' ||  
           to_char(floor((m.value_as_number-floor(m.value_as_number))*16),'99') || ' oz' --pound
  end) weight 
from measurement m
left join visit_occurrence v on v.visit_occurrence_id=m.visit_occurrence_id
where m.measurement_concept_id in (3013762, 3025315, 3023166) and m.value_as_number is not null and m.unit_concept_id is not null
  and m.measurement_date between {} and {} 
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end,
           case when v.person_id is not null then v.person_id else m.person_id end,
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_WT_TABLE="""
create index tmp_wt_idx on tmp_wt using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_WT_TABLE="""
analyse tmp_wt
"""

CREATE_ESP_TMP_HT_TABLE="""
create temporary table tmp_ht as
select case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end measurement_date, 
           case when v.person_id is not null then v.person_id else m.person_id end person_id,
  max(case
    when unit_concept_id in (8582,3195806) then trim(to_char(floor(floor(m.value_as_number*.393701)/12),'9')) || ''' ' ||
           trim(to_char(mod(floor(m.value_as_number*.393701),12),'99')) || '"'
    when unit_concept_id = 9330 then trim(to_char(floor(value_as_number/12),'99')) || ''' ' ||
           trim(to_char(mod(floor(value_as_number),12),'99')) || '"' 
  end) height
from measurement m 
left join visit_occurrence v on v.visit_occurrence_id=m.visit_occurrence_id
where m.measurement_concept_id in (3036277, 3019171) and m.value_as_number is not null and m.unit_concept_id is not null
  and m.measurement_date between {} and {} 
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end,
           case when v.person_id is not null then v.person_id else m.person_id end,
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_HT_TABLE="""
create index tmp_ht_idx on tmp_ht using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_HT_TABLE="""
analyse tmp_ht
"""

CREATE_ESP_TMP_TMP_TABLE="""
create temporary table tmp_tmp as
select case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end measurement_date, 
           case when v.person_id is not null then v.person_id else m.person_id end person_id,
  max(case 
    when unit_concept_id in (8653, 3192389, 586323) or value_as_number between 30 and 42 
         then to_char((m.value_as_number*1.8+32),'999.9')
    when unit_concept_id in (9289,3191526,41990844) or value_as_number between 86 and 108 
         then to_char(m.value_as_number,'999.9')
  end) as temp
from measurement m 
left join visit_occurrence v on v.visit_occurrence_id=m.visit_occurrence_id
where m.measurement_concept_id = 3020891
  and m.measurement_date between {} and {}
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end,
           case when v.person_id is not null then v.person_id else m.person_id end,
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_TMP_TABLE="""
create index tmp_tmp_idx on tmp_tmp using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_TMP_TABLE="""
analyse tmp_tmp
"""

CREATE_ESP_TMP_SYS_TABLE="""
create temporary table tmp_sys as
select avg(m.value_as_number) as bp_systolic, 
    case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end measurement_date, 
           case when v.person_id is not null then v.person_id else m.person_id end person_id
from measurement m
left join visit_occurrence v on v.visit_occurrence_id=m.visit_occurrence_id
where m.measurement_concept_id in (3004249,3009395,3035856)
  and m.measurement_date between {} and {} 
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end,
           case when v.person_id is not null then v.person_id else m.person_id end,
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_SYS_TABLE="""
create index tmp_sys_idx on tmp_sys using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_SYS_TABLE="""
analyse tmp_sys
"""

CREATE_ESP_TMP_DIA_TABLE="""
create temporary table tmp_dia as
select avg(value_as_number) bp_diastolic, 
    case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end measurement_date, 
           case when v.person_id is not null then v.person_id else m.person_id end person_id
from measurement m 
left join visit_occurrence v on v.visit_occurrence_id=m.visit_occurrence_id
where m.measurement_concept_id in (3012888,3013940,3019962)
  and m.measurement_date between {} and {}
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[m.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end,'yyyymmdd')::bigint]
           end,
           case when v.person_id is not null then v.person_id else m.person_id end,
           case when v.visit_occurrence_id is not null then v.visit_start_date else m.measurement_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_DIA_TABLE="""
create index tmp_dia_idx on tmp_dia using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_DIA_TABLE="""
analyse tmp_dia
"""

CREATE_ESP_TMP_BP_TABLE="""
create temporary table tmp_bp as
select m1.visit_occurrence_id, m1.bp_systolic, m2.bp_diastolic, m1.measurement_date, m1.person_id
from tmp_sys m1
join tmp_dia m2 on m2.visit_occurrence_id=m1.visit_occurrence_id 
"""
INDEX_ESP_TMP_BP_TABLE="""
create index tmp_bp_idx on tmp_bp using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_BP_TABLE="""
analyse tmp_bp
"""

CREATE_ESP_TMP_BMI_TABLE="""
create temporary table tmp_bmi as
select case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[o.person_id::bigint, 
                to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else o.observation_date end,'yyyymmdd')::bigint]
           end visit_occurrence_id, 
           case when v.visit_occurrence_id is not null then v.visit_start_date else o.observation_date end observation_date, o.person_id,
       max(case when c.concept_name='Body mass index 20-24 - normal' then 23.9
            when c.concept_name='Body mass index 25-29 - overweight' then 28.9
            when c.concept_name='Body mass index 30+ - obesity' then 30.1
            when c.concept_name='Body mass index 40+ - severely obese' then 40.1
            when c.concept_name='Body mass index less than 20' then 19.9
       end) as bmi
from observation o
join concept c on c.concept_id=o.observation_concept_id 
left join visit_occurrence v on v.visit_occurrence_id=o.visit_occurrence_id
where o.observation_concept_id in (4135421, 4060705, 4060985, 4256640, 4147565)
  and observation_date between {} and {} 
group by case when v.visit_occurrence_id is not null then array[v.visit_occurrence_id::bigint]
                else array[o.person_id::bigint, 
                    to_char(case when v.visit_occurrence_id is not null then v.visit_start_date else o.observation_date end,'yyyymmdd')::bigint]
           end,
           o.person_id,
           case when v.visit_occurrence_id is not null then v.visit_start_date else o.observation_date end
""".format(esp_gd.yesterday, esp_gd.today)
INDEX_ESP_TMP_BMI_TABLE="""
create index tmp_bmi_idx on tmp_bmi using GIN (visit_occurrence_id)
"""
ANALYSE_ESP_TMP_BMI_TABLE="""
analyse tmp_bmi
"""

CREATE_ESP_TMP_PAT_DT_TABLE="""
create temporary table tmp_pat_dt as
select t0.visit_occurrence_id, t0.person_id, 
  case when t1.visit_start_date is not null then t1.visit_start_date else t0.odt end as odt, t1.visit_concept_id
from 
(select visit_occurrence_id, person_id, measurement_date odt from tmp_bp
 union select visit_occurrence_id, person_id, measurement_date odt from tmp_tmp
 union select visit_occurrence_id, person_id, measurement_date odt from tmp_ht
 union select visit_occurrence_id, person_id, measurement_date odt from tmp_wt 
 union select visit_occurrence_id, person_id, observation_date odt from tmp_bmi
 union select visit_occurrence_id, person_id, condition_start_date odt from tmp_dx_arrays) t0
left join visit_occurrence t1 on t1.visit_occurrence_id=t0.visit_occurrence_id[1] and array_length(t0.visit_occurrence_id, 1)=1
"""
INDEX_ESP_TMP_PAT_DT_1_TABLE="""
create index tmp_pt_dt_1_idx on tmp_pat_dt using GIN (visit_occurrence_id)
"""
INDEX_ESP_TMP_PAT_DT_2_TABLE="""
create index tmp_pt_dt_2_idx on tmp_pat_dt (person_id)
"""
ANALYSE_ESP_TMP_PAT_DT_TABLE="""
analyse tmp_pat_dt
"""

CREATE_ESP_TMP_PP_TABLE="""
create temporary table tmp_pp as
select distinct on (p.person_id, d.odt) p.person_id, d.odt, p.payer_concept_id, c.concept_name
from payer_plan_period p 
join tmp_pat_dt d on d.person_id=p.person_id and d.odt between p.payer_plan_period_start_date and p.payer_plan_period_end_date
join concept c on c.concept_id=p.payer_concept_id
order by p.person_id, d.odt, p.payer_concept_id
"""
INDEX_ESP_TMP_PP_TABLE="""
create index tmp_pp_idx on tmp_pp (person_id, odt)
"""
ANALYSE_ESP_TMP_PP_TABLE="""
analyse tmp_pp
"""

SELECT_ESP_Visit_TABLE="""
SELECT  distinct v.person_id patient_id,
                p.person_source_value AS mrn,
                v.visit_occurrence_id AS natural_key,
                to_char(v.odt, 'yyyyMMdd') AS date,
                NULL AS is_closed,
                NULL AS date_closed,
                NULL AS provider_id,
                NULL AS site_natural_key,
                NULL AS site_name,
                case
                  when vcon.concept_name is not null then vcon.concept_name
                  else 'Unknown Encounter' 
                end AS raw_encounter_type,
                NULL AS edd, 
                m7.temp AS raw_temperature,
                NULL AS cpt,
                m2.weight AS raw_weight,
                m3.height AS raw_height,
                m4.bp_systolic AS raw_bp_systolic,
                m4.bp_diastolic AS raw_bp_diastolic,
                NULL AS raw_o2_stat,
                NULL AS raw_peak_flow,
                array_to_string(t00.dx_list,';') as dx_list, 
                m5.bmi AS raw_bmi,
                NULL AS hosp_admit_dt,
                NULL AS hosp_dschrg_dt,
                m6.concept_name as primary_payer
FROM person p 
join tmp_pat_dt v on v.person_id=p.person_id
left join concept vcon on vcon.concept_id=v.visit_concept_id
left join tmp_wt m2 on m2.visit_occurrence_id=v.visit_occurrence_id 
left join tmp_ht m3 on m3.visit_occurrence_id=v.visit_occurrence_id
left join tmp_bp m4 on m4.visit_occurrence_id=v.visit_occurrence_id
left join tmp_bmi m5 on m5.visit_occurrence_id=v.visit_occurrence_id
left join tmp_pp m6 on m6.odt=v.odt and m6.person_id=v.person_id
left join tmp_tmp m7 on m7.visit_occurrence_id=v.visit_occurrence_id
left join tmp_dx_arrays t00 on  t00.visit_occurrence_id=v.visit_occurrence_id
"""

ESP_Visit_DICT= {'SELECT':SELECT_ESP_Visit_TABLE,
                 'CREATE':[CREATE_ESP_TMP_ICD_TABLE,INDEX_ESP_TMP_ICD_TABLE,ANALYSE_ESP_TMP_ICD_TABLE,
                           CREATE_ESP_TMP_WT_TABLE,INDEX_ESP_TMP_WT_TABLE,ANALYSE_ESP_TMP_WT_TABLE,
                           CREATE_ESP_TMP_HT_TABLE,INDEX_ESP_TMP_HT_TABLE,ANALYSE_ESP_TMP_HT_TABLE,
                           CREATE_ESP_TMP_TMP_TABLE,INDEX_ESP_TMP_TMP_TABLE,ANALYSE_ESP_TMP_TMP_TABLE,
                           CREATE_ESP_TMP_SYS_TABLE,INDEX_ESP_TMP_SYS_TABLE,ANALYSE_ESP_TMP_SYS_TABLE,
                           CREATE_ESP_TMP_DIA_TABLE,INDEX_ESP_TMP_DIA_TABLE,ANALYSE_ESP_TMP_DIA_TABLE,
                           CREATE_ESP_TMP_BP_TABLE,INDEX_ESP_TMP_BP_TABLE,ANALYSE_ESP_TMP_BP_TABLE,
                           CREATE_ESP_TMP_BMI_TABLE,INDEX_ESP_TMP_BMI_TABLE,ANALYSE_ESP_TMP_BMI_TABLE,
                           CREATE_ESP_TMP_PAT_DT_TABLE,INDEX_ESP_TMP_PAT_DT_1_TABLE,INDEX_ESP_TMP_PAT_DT_2_TABLE,ANALYSE_ESP_TMP_PAT_DT_TABLE,
                           CREATE_ESP_TMP_PP_TABLE,INDEX_ESP_TMP_PP_TABLE,ANALYSE_ESP_TMP_PP_TABLE]}

SELECT_ESP_Immune_TABLE="""
with vax_id as (
  select distinct c.concept_id
  from concept c
  join concept_ancestor cvx_anc on c.concept_id=cvx_anc.descendant_concept_id
  join concept c_cvx on c_cvx.concept_id=cvx_anc.ancestor_concept_id and c_cvx.vocabulary_id='CVX'
  union
  select distinct c.concept_id
  from concept c 
  join concept_ancestor v_anc on c.concept_id=v_anc.descendant_concept_id and v_anc.ancestor_concept_id=21601278)
select distinct d.person_id, source.concept_code, source.concept_name, to_char(d.drug_exposure_start_date, 'yyyyMMdd'),
	null as Dose, 
	null as MFG_Name, 
	null as Lot, 
	concat(d.person_id,'-',source.concept_code,'-',to_char(d.drug_exposure_start_date, 'yyyyMMdd')) as natural_key
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id
join vax_id on vax_id.concept_id=d.drug_concept_id
and d.drug_exposure_start_date < {} AND d.drug_exposure_start_date >= {} 
""".format(esp_gd.today, esp_gd.yesterday)

ESP_Immune_DICT= {'SELECT':SELECT_ESP_Immune_TABLE}


SELECT_ESP_Social_Hx_TABLE="""
select distinct o.person_id, null as MRN, substr(c.concept_name,1,200) as concept_name, null as alcohol_use, 
 to_char(o.observation_date, 'yyyyMMdd') AS date_noted, o.person_id ||'_'|| c.concept_code ||'-'||to_char(o.observation_date, 'yyyyMMdd')
from observation o join concept c on c.concept_id=o.observation_concept_id
where (c.concept_name ilike '%smoke%' or c.concept_name ilike '%tobacco%' or c.concept_name ilike '%cigarette%') 
and c.concept_code not in ('81703003','4004F','72166-2')
and o.observation_date < {} AND o.observation_date >= {}
""".format(esp_gd.today, esp_gd.yesterday)

ESP_Social_Hx_DICT= {'SELECT':SELECT_ESP_Social_Hx_TABLE}


SELECT_ESP_Test_Results_TABLE="""
select distinct m.person_id, null as mrn, null as order_natural_key,
  to_char(m.measurement_date, 'yyyyMMdd') as ldate,
  null as result_date,
  null as provider_id, null as order_type, null as cpt_native_code,
  c.concept_code, c.concept_name,
  case when qual.concept_name is null or qual.concept_name='No matching concept' then rtrim(to_char(m.value_as_number,'FM9999999999.9999'),'.')
       else qual.concept_name end value_string, null as abnormal_flag,
  m.range_low, m.range_high, unit.concept_name,
  null as ph1, null as ph2, null as ph3, null as ph4, null as ph5, null as ph6, null as ph7,
  concat(m.person_id,'-',to_char(m.measurement_date, 'yyyyMMdd'),'-',
       md5(concat(c.concept_code,case
         when qual.concept_name is null or qual.concept_name='No matching concept' then rtrim(to_char(m.value_as_number,'FM9999999999.9999'),'.')
         else qual.concept_name
       end))) natural_key
from measurement m
join concept c on c.concept_id=m.measurement_concept_id
left join concept qual on qual.concept_id=m.value_as_concept_id
left join concept unit on unit.concept_id=m.unit_concept_id
where c.vocabulary_id='LOINC'
and case
         when qual.concept_name is null or qual.concept_name='No matching concept' then rtrim(to_char(m.value_as_number,'FM9999999999.9999'),'.')
         else qual.concept_name
       end is not null
and m.measurement_date < {} AND m.measurement_date >= {}
and c.concept_code not in ('10834-0','14959-1','1759-0','19993-5','2028-9','2524-7','3024-7','30394-1','3097-3','33037-3','3425-6','38250-7','4544-3',
'48642-3','50557-8','5767-9','5802-4','5821-4','5905-5','6301-6','6690-2','718-7','741-9','768-2','786-4','787-2','882-1','14979-9','1927-3',
'20407-3','2075-0','2075-0','2276-4','26449-9','26474-7','2713-6','2777-1','2823-3','3016-3','30250-5','32200-8','32623-1','32623-1','34714-6',
'4544-3','50957-0','53115-2','5643-2','5803-2','58413-6','58766-7','5902-2','59408-5','6742-1','704-7','704-7','713-8','7790-9','785-6','788-0',
'789-8','86919-8','1003-3','10378-8','11555-0','11579-0','14627-4','17861-6','20407-3','30395-8','31160-5','33256-9','48643-1','49497-1','50560-2',
'5803-2','5902-2','630-4','736-9','742-7','753-4','770-8','777-3','7791-7','785-6','787-2','1751-7','1751-7','19123-9','19161-9','2028-9','2075-0',
'24348-5','27297-1','2777-1','2885-2','3107-0','32623-1','45390-2','5769-5','5778-6','5796-8','6301-6','704-7','731-0','768-2','774-0','933-2',
'9796-4','10338-2','11274-8','13945-1','1752-5','1988-5','2028-9','2514-8','26485-3','2823-3','38518-7','38892-6','48643-1','731-0','764-1',
'786-4','786-4','787-2','788-0','788-0','94309-2','10839-9','11282-1','13654-9','1752-5','17861-6','2028-9','21000-5','2276-4','2498-4','2885-2',
'2951-2','32623-1','33914-3','5821-4','5821-4','5905-5','5905-5','702-1','711-2','713-8','742-7','770-8','785-6','789-8','13945-1','19568-5',
'1959-6','20454-5','20570-8','20627-6','2336-6','2498-4','26449-9','2823-3','2857-1','2965-2','3034-6','3349-8','38518-7','5797-6','5799-2',
'58413-6','66746-9','706-2','713-8','718-7','718-7','736-9','751-8','777-3','789-8','798-9','11282-1','17861-6','19123-9','2028-9','20409-9',
'20627-6','2161-8','2500-7','2502-3','26444-0','26449-9','2885-2','2885-2','2888-6','2888-6','2951-2','33037-3','4537-7','5799-2','5821-4',
'5902-2','6690-2','713-8','71695-1','742-7','742-7','769-0','777-3','786-4','787-2','788-0','788-0','12209-3','13359-5','13945-1','1988-5',
'26505-8','2777-1','2951-2','2965-2','3107-0','33037-3','4544-3','53061-8','5803-2','6301-6','6690-2','6690-2','6690-2','704-7','728-6','736-9',
'751-8','770-8','788-0','789-8','1989-3','26052-1','26478-8','2947-0','30180-4','30341-2','3040-3','3040-3','3094-0','48642-3','49024-3','5778-6',
'5796-8','5802-4','6768-6','6768-6','706-2','731-0','736-9','736-9','742-7','779-9','785-6','8190-1','11557-6','11558-4','12179-8','1759-0',
'1759-0','17861-6','19123-9','19161-9','1994-3','20454-5','2106-3','2161-8','26485-3','2823-3','2823-3','2885-2','30934-4','30934-4','3097-3',
'3299-5','34728-6','5811-5','5811-5','5905-5','5905-5','61152-5','6299-2','706-2','711-2','731-0','742-7','768-2','774-0','785-6','10839-9',
'10839-9','1751-7','19123-9','1978-6','21000-5','2132-9','2500-7','26478-8','2885-2','3084-1','3094-0','3094-0','3173-2','4092-3','5769-5',
'6768-6','713-8','751-8','779-9','786-4','789-8','8247-9')
""".format(esp_gd.today, esp_gd.yesterday)

ESP_Test_Results_DICT= {'SELECT':SELECT_ESP_Test_Results_TABLE}


SELECT_ESP_Meds_TABLE="""
select distinct d.person_id, null as mrn, 
  concat(d.person_id,'-',to_char(d.drug_exposure_start_date, 'yyyyMMdd'),'-',
         md5(concat(d.sig,source.concept_code,to_char(d.quantity,'9999999999'),to_char(d.refills,'999999999'),
                    to_char(d.drug_exposure_end_date, 'yyyyMMdd'),route.concept_name))) as natural_key,
  null as provider_id,
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') as ddate,
  null as status, d.sig, source.concept_code,
  source.concept_name, d.quantity, d.refills, 
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') AS start_date, to_char(d.drug_exposure_end_date, 'yyyyMMdd') AS end_date,
  route.concept_name
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
left join concept route on d.route_concept_id = route.concept_id
WHERE d.drug_exposure_start_date < {} AND d.drug_exposure_start_date >= {}
""".format(esp_gd.today, esp_gd.yesterday)

ESP_Meds_DICT= {'SELECT':SELECT_ESP_Meds_TABLE}


if __name__ == '__main__':
    pass

