#!/srv/esp/prod/bin/python

"""
Name: esp_etl.py

Purpose: ETL utilities to support ESP project.

Modifications:
    17-Apr-2020 Created.  MC
	14-May-2020 Updated for python3. MC
"""

import os, sys, time, pymssql, pdb
import esp_sql as esp_sql, esp_gd
import csv
from subprocess import Popen, PIPE, STDOUT
import optparse
from importlib import reload

ESP_SRC_DIR=r'/srv/esp/prod/ESP'
#ESPDATA=r'/srv/esp/data/epic/incoming/'
ESPDATA=r'/srv/esp/data/epic/pending/'


#MSSQL_HOST="serverip.domain.name"
MSSQL_HOST="datawarehouse.ad.mchd.org"
#MSSQL_USER="????"
MSSQL_USER="esp"
#MSSQL_PASSWORD="????"
MSSQL_PASSWORD="cfar4ADMIN1$"
#MSSQL_DB_NAME="????"
MSSQL_DB_NAME="DataView"


def check_date(year,mon,day):
    """Check if date is real"""
    tup_date = (year, mon, day, 0, 0, 0, -1, -1, -1)
    try:
        date_in_secs = time.mktime(tup_date)
        date_from_secs = time.localtime(date_in_secs)
        if tup_date[:3] == date_from_secs[:3]:
            return True
        else:
            return False
    except:
        return False

def get_datestamp(t=time.gmtime()):
    """return datestamp in format MMDDYYYY.  Example: Oct 1,2011 is '10012011'. Default is today's date."""
    # List is used to convert the numeric mon (1-12) to a string based two digit month ('01'-'12')
    MON=['00','01','02','03','04','05','06','07','08','09','10','11','12']
    DD=['00','01','02','03','04','05','06','07','08','09', \
         '10','11','12','13','14','15','16','17','18','19', \
         '20','21','22','23','24','25','26','27','28','29','30','31']
    # Default is today's date
    return MON[t.tm_mon]+DD[t.tm_mday]+str(t.tm_year)

def describe(option, opt_str, value, parser):
    """Describe pymssql"""
    print(value)
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    sql = 'select top 1 * from %s' % value
    cur.execute(sql)
    desc = cur.description
    for col in desc:
        print(col)
    cur.close()
    db.close()
    sys.exit()

def run_dcl(sql):
    """Run sql that doesn't return results"""
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    cur.execute(sql)
    db.commit()

def query(sql):
    """Run sql query and return the result set"""
    db = pymssql.connect(host=MSSQL_HOST, user=MSSQL_USER, password=MSSQL_PASSWORD, database=MSSQL_DB_NAME)
    cur = db.cursor()
    cur.execute(sql)
    return cur.fetchall()

def create_esploadfile(fileandpath,someiterable):
    """Create esp member load file"""
    csv.register_dialect('epicout', delimiter='^', escapechar="\\", quotechar = '', doublequote = False, lineterminator = '\r\n', quoting=csv.QUOTE_NONE)
    writer = csv.writer(open(fileandpath, "w"), dialect='epicout')
    for outrow in someiterable:
        try:
            writer.writerow(outrow)
        except Exception as e:
           print(str(e))

def create_esploadfiles(t=time.gmtime()):
    """Create all of the esp load files"""
    
    # Default is today's date
    datestamp = get_datestamp(t)

    # Create esp load files
    create_esploadfile(ESPDATA+r'epicvis.esp.'+datestamp, query (esp_sql.ESP_Visit_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicmem.esp.'+datestamp, query (esp_sql.ESP_Patient_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicpro.esp.'+datestamp, query (esp_sql.ESP_Provider_DICT['SELECT']))
    #create_esploadfile(ESPDATA+r'epicrkf.esp.'+datestamp, query (esp_sql.ESP_Risk_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicsoc.esp.'+datestamp, query (esp_sql.ESP_Social_Hx_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicres.esp.'+datestamp, query (esp_sql.ESP_Test_Results_DICT['SELECT']))
    create_esploadfile(ESPDATA+r'epicmed.esp.'+datestamp, query (esp_sql.ESP_Meds_DICT['SELECT']))


def historic_data_load(start_yr, start_mon, start_day, end_yr, end_mon, end_day):
    """Run historic data load with start < date < end"""


    # Set the current working directory to $ESP_HOME/src/ESP
    os.chdir(ESP_SRC_DIR)

    #
    # Original historical data load
    #
    # Start date (oldest date)
    #start = time.mktime((2019, 9, 1, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    #end = time.mktime((2019, 10, 1, 0, 0, 0, -1, -1, -1))

    # Start date (oldest date)
    start = time.mktime((start_yr, start_mon, start_day, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    end = time.mktime((end_yr, end_mon, end_day, 0, 0, 0, -1, -1, -1))
    #icd cutover
    icd_dt = time.mktime((2015,10,1,0,0, 0, -1, -1, -1))


    # Set the dates
    t = time.gmtime(start)
    esp_gd.yesterday = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
    print("esp_gd.yesterday is: "+esp_gd.yesterday)
    t = time.gmtime(end)
    esp_gd.today = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
    print("esp_gd.today is: "+esp_gd.today)
    if start >= icd_dt:
        esp_gd.icd='icd10:'
    else: 
        esp_gd.icd='icd9:'
    print("esp_gd.icd is: "+esp_gd.icd)
    # Reload to change SQL.  This will update the SQL with the new dates and icd value.
    reload(esp_sql)
    # Create the tables
    # create_esp_tables()
    # Create the loadfiles with the end date.
    create_esploadfiles(time.gmtime(end))
    # Run the loads with the end date or esp_gd.today which happens to be start at this point.
    #run_esploads(time.gmtime(start))


def historic_data_load_1dayatatime(start_yr, start_mon, start_day, end_yr, end_mon, end_day):
    """Run historic data load with start < date < end"""


    # Set the current working directory to $ESP_HOME/src/ESP
    os.chdir(ESP_SRC_DIR)

    #
    # Original historical data load
    #
    # Start date (oldest date)
    #start = time.mktime((2009, 5, 9, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    #end = time.mktime((2010, 6, 29, 0, 0, 0, -1, -1, -1))

    # Start date (oldest date)
    start = time.mktime((start_yr, start_mon, start_day, 0, 0, 0, -1, -1, -1))
    # End date (more recent date)
    end = time.mktime((end_yr, end_mon, end_day, 0, 0, 0, -1, -1, -1))

    # Loop until end date is reached
    while start < end:
        # Set the dates
        t = time.gmtime(start)
        esp_gd.yesterday = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
        print("esp_gd.yesterday is: "+esp_gd.yesterday)
        start = start+(60*60*24)
        t = time.gmtime(start)
        esp_gd.today = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
        print("esp_gd.today is: "+esp_gd.today)
        # Reload to change SQL.  This will update the SQL with the new dates.
        reload(esp_sql)
        # Create the tables
        # create_esp_tables()
        # Create the loadfiles with the end date or esp_gd.today which happens to be start at this point.
        create_esploadfiles(time.gmtime(start))
        # Run the loads with the end date or esp_gd.today which happens to be start at this point.
        #run_esploads(time.gmtime(start))

def run_daily_load():
    """Run the daily data load"""
    create_esploadfiles()
    # The ESP batch command in cron will also load the files.  I've decided to load them here as well.
    #run_esploads()

def main():

    # Some examples:
    #create_esp_table(esp_sql.ESP_Patient_DICT,DROP=True)
    #run_dcl(esp_sql.ALTER_ESP_Patient)
    #print query("select count(*) from X_ESP_Patient")
    #describe('PAT_ENC')

    parser = optparse.OptionParser()
    parser.add_option("-d", "--describe", dest="tablename", type="string",
                  action="callback", callback=describe,
                  help="describe MS SQL <TABLENAME>")
    parser.add_option("-t", "--truncate_initial_load_table", action="store_true",dest="truncate_flag",
                  help="Warning! This command deletes all data in the X_ESP_INITIAL_LOAD_DONE table.")
    group = optparse.OptionGroup(parser, "Historic Data Load Options")
    group.add_option("--historic_load", action="store_true", dest="historic_data_load_flag",
                  help="Perform ESP historic load for date range.  Requires START and END dates.")
    group.add_option("-e", "--end_date", action="store", dest="end", type="string",
                  help="Format: MM/DD/YYYY, <END> date parameter for historic load")
    group.add_option("-s", "--start_date", action="store", dest="start", type="string",
                  help="Format: MM/DD/YYYY. <START> date parameter for historic load")
    group.add_option("-o", "--one_file_per_day", action="store_true", dest="one_file_flag",
                  help="Default is large file.  This option creates etl files for each day")
    parser.add_option_group(group)
    (opts, args) = parser.parse_args()

    if parser.values.truncate_flag:
        truncate_initial_load_done()
        sys.exit()

    elif parser.values.historic_data_load_flag:
        if parser.values.end and parser.values.start:
            start_yyyy,start_dd,start_mm = int(parser.values.start.split('/')[2]), \
                                           int(parser.values.start.split('/')[1]), \
                                           int(parser.values.start.split('/')[0])
            end_yyyy,end_dd,end_mm = int(parser.values.end.split('/')[2]), \
                                     int(parser.values.end.split('/')[1]), \
                                     int(parser.values.end.split('/')[0])
            start_tup = (start_yyyy, start_mm, start_dd,0, 0, 0, -1, -1, -1)
            end_tup = (end_yyyy, end_mm, end_dd,0, 0, 0, -1, -1, -1)
            # Check if valid dates
            if check_date(start_yyyy,start_mm,start_dd) != True:
                raise parser.error("Invalid START date")
            if check_date(end_yyyy,end_mm,end_dd) != True:
                raise parser.error("Invalid END date")
            # Check start_date < end_date
            if time.mktime(start_tup) > time.mktime(end_tup):
                #raise optparse.OptionValueError("START date isn't older than END date.") 
                raise parser.error("START date isn't older than END date.") 
            # Call historic load
            if parser.values.one_file_flag:
                 historic_data_load_1dayatatime(start_yyyy, start_mm, start_dd, end_yyyy, end_mm, end_dd)
            else:
                 historic_data_load(start_yyyy, start_mm, start_dd, end_yyyy, end_mm, end_dd)
            sys.exit()
        else:
           #raise optparse.OptionValueError("START and END dates required for historic load.") 
           raise parser.error("START and END dates required for historic load.") 

    # Default is to run ETL daily load
    run_daily_load()
    sys.exit()

if __name__ == '__main__':
    main()
