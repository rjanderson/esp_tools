#!/usr/bin/python
"""
Name: esp_gd.py

Purpose:  Container for global dates.

Modifications:
    29-Jun-2010 Created.  JL

"""

import time
# Set today to defualt.  This only happens the first time the module is imported.
t = time.gmtime()
today = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"
# Set yesterday to defualt.  This only happens the first time the module is imported.
t = time.gmtime( time.time()-(60*60*24) )
yesterday = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"

# You can override date ranges by settting today and yesterday here
#today="'6/9/2009'"
#yesterday="'5/9/2009'"
t = time.gmtime( time.time()-(60*60*48) )
twodaysago = "'"+str(t.tm_mon)+"/"+str(t.tm_mday)+"/"+str(t.tm_year)+"'"


