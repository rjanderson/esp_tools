set search_path=gen_pop_tools;

drop table if exists tt_pat_bp_seq_enc;
create table tt_pat_bp_seq_enc as
select seq.patient_id, seq.year_month,
        count(bp.patient_id) over (partition by seq.patient_id order by seq.year_month range between unbounded preceding and current row) as allpriorbp,
        count(bp.patient_id) over (partition by seq.patient_id order by seq.year_month rows between 23 preceding and current row) as prior2bp,
        count(bp.patient_id) over (partition by seq.patient_id order by seq.year_month rows between 11 preceding and current row) as prior1bp
from tt_pat_seq seq 
left join tt_bp_encs_amb bp on seq.patient_id=bp.patient_id and seq.year_month=bp.year_month;

drop table if exists gen_pop_tools.tt_out2;
select     aa, ab, ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar,
    "as", "at", au, av, aw, ax, ay, az, ba, bb, bc, bd, be, bf, bg, bh, bi, bj,
    case
        when bk=0 then 3
	when bk=1 then 1
	when bk=2 then 2
	else 0
    end bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx, 
    case
	when "by"=4 then 0 
	else "by"
    end "by", bz, ca, cb,
case
    when pse.age<=4 then 1
    when pse.age>=5 and pse.age <=9 then 2
    when pse.age>=10 and pse.age <=14 then 3
    when pse.age>=15 and pse.age <=19 then 4
    when pse.age>=20 and pse.age <=24 then 5
    when pse.age>=25 and pse.age <=29 then 6
    when pse.age>=30 and pse.age <=34 then 7
    when pse.age>=35 and pse.age <=44 then 8
    when pse.age>=45 and pse.age <=54 then 9
    when pse.age>=55 and pse.age <=64 then 10
    when pse.age>=65 and pse.age <=74 then 11
    when pse.age>=75 and pse.age <=84 then 12
    when pse.age>=85 then 13
end as ah_alt,
row_number() over (partition by tto.aa order by tto.af, tto.ag) aa_seq,
bpseq.allpriorbp, bpseq.prior2bp, bpseq.prior1bp
into gen_pop_tools.tt_out2
from tt_out tto
join tt_pat_seq_enc pse on pse.patient_id=tto.aa and pse.year_month=tto.af || '_' || tto.ag
join tt_pat_bp_seq_enc bpseq on bpseq.patient_id=tto.aa and bpseq.year_month=tto.af || '_' || tto.ag;
copy (select aa_seq, ab, ac, ad, ae, af, ag, ah, ai, aj, ak, 
      am, an, ao, aq, "as", at, au, av, aw, ax, ay, az, 
      ba, bb, bc, bk, bs, bx, "by", bz, ca, cb, ah_alt, allpriorbp, prior2bp, prior1bp
      from gen_pop_tools.tt_out2) 
 to :'pathToFile' with CSV delimiter ',' header;
--the copy command requires filesystem access to the DB server, and the location must be writable by the postgres user
--if you need to use \COPY, (so as to get the file off a remote DB),  variable substitution (pathToFile) gets complicated.
--See https://stackoverflow.com/questions/24671177/variable-substitution-in-psql-copy

