﻿DROP TABLE IF EXISTS mgtp_all_gon_labs_ctr;
DROP TABLE IF EXISTS mgtp_num_tests_per_year_male_ctr;
DROP TABLE IF EXISTS mgtp_num_tests_per_year_female_ctr;
DROP TABLE IF EXISTS mgtp_num_uniq_pats_tested_male_ctr; 
DROP TABLE IF EXISTS mgtp_num_uniq_pats_tested_female_ctr; 
DROP TABLE IF EXISTS mgtp_all_pos_gon_test_ctr;
DROP TABLE IF EXISTS mgtp_num_pos_tests_per_year_male_ctr;
DROP TABLE IF EXISTS mgtp_num_pos_tests_per_year_female_ctr;
DROP TABLE IF EXISTS mgtp_all_gon_labs_ctr_filtered;
DROP TABLE IF EXISTS mgtp_all_pos_gon_test_ctr_filtered;
DROP TABLE IF EXISTS mgtp_num_uniq_pats_pos_male_ctr;
DROP TABLE IF EXISTS mgtp_num_uniq_pats_pos_female_ctr;
DROP TABLE IF EXISTS mgtp_pats_with_encounters_ctr;
DROP TABLE IF EXISTS mgtp_pats_with_encounters_ctr_filtered;
DROP TABLE IF EXISTS mgtp_num_uniq_encs_male_ctr;
DROP TABLE IF EXISTS mgtp_num_uniq_encs_female_ctr;
DROP TABLE IF EXISTS mgtp_all_years_centers;
DROP TABLE IF EXISTS mgtp_output_ctr;


-- Get the details for all gonnorrhea labs 
-- Here we will also apply center filtering for MLCHC to the labs
CREATE TABLE mgtp_all_gon_labs_ctr AS 
SELECT T1.id as lab_id, T1.patient_id, T1.native_name, T1.native_code, T1.date, T1.specimen_source, EXTRACT(YEAR FROM T1.date) rpt_year, T3.gender, T3.center_id,
date_part('year', age(T1.date, T3.date_of_birth))
FROM emr_labresult T1,
conf_labtestmap T2,
emr_patient T3 
WHERE T1.native_code = T2.native_code
--AND T3.center_id not in ('1')
AND date_part('year', age(T1.date, T3.date_of_birth)) BETWEEN 15 AND 39
AND gender IN ('FEMALE', 'F', 'MALE', 'M')
AND T1.patient_id = T3.id
AND T2.test_name = 'gonorrhea'
AND T1.date >= '01-01-2006'
AND T1.date < '01-01-2018';

-- Get the counts for total tests per year for males
CREATE TABLE mgtp_num_tests_per_year_male_ctr AS
select rpt_year, count(*) as total_num_tests_male, center_id
from mgtp_all_gon_labs_ctr
WHERE gender IN ('MALE', 'M')
group by rpt_year, center_id
order by rpt_year;

-- Get the counts for total tests per year for females
CREATE TABLE mgtp_num_tests_per_year_female_ctr AS
select rpt_year, count(*) as total_num_tests_female, center_id
from mgtp_all_gon_labs_ctr
WHERE gender IN ('FEMALE', 'F')
group by rpt_year, center_id
order by rpt_year;

-- Only count patients that were tested in multiple centers in the same year once
CREATE TABLE mgtp_all_gon_labs_ctr_filtered AS
SELECT DISTINCT ON (patient_id, center_id, rpt_year) patient_id, gender, rpt_year, center_id, native_name, native_code, date, specimen_source
FROM mgtp_all_gon_labs_ctr;

-- Get the counts for number of unique male patients per year
CREATE TABLE mgtp_num_uniq_pats_tested_male_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_tested_male, center_id
from mgtp_all_gon_labs_ctr_filtered
WHERE gender IN ('MALE', 'M')
group by rpt_year, center_id
order by rpt_year;

-- Get the counts for number of unique female patients per year
CREATE TABLE mgtp_num_uniq_pats_tested_female_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_tested_female, center_id
from mgtp_all_gon_labs_ctr_filtered
WHERE gender IN ('FEMALE', 'F')
group by rpt_year, center_id
order by rpt_year;

-- Get all positive tests
CREATE TABLE mgtp_all_pos_gon_test_ctr AS 
SELECT T1.patient_id, T1.rpt_year, T1.gender, T1.lab_id, T2.name, T1.center_id
FROM mgtp_all_gon_labs_ctr T1,
hef_event T2
WHERE T1.patient_id = T2.patient_id
AND T1.lab_id = T2.object_id
AND T2.name = 'lx:gonorrhea:positive';

-- Only count patients that tested postive in multiple centers in the same year once
CREATE TABLE mgtp_all_pos_gon_test_ctr_filtered AS
SELECT DISTINCT ON (patient_id, center_id, rpt_year) patient_id, rpt_year, gender, lab_id, name, center_id
FROM mgtp_all_pos_gon_test_ctr;


-- Get the counts for total pos tests per year for males
CREATE TABLE mgtp_num_pos_tests_per_year_male_ctr AS
select rpt_year, count(*) as total_num_pos_tests_male, center_id
from mgtp_all_pos_gon_test_ctr
WHERE gender IN ('MALE', 'M')
group by rpt_year, center_id
order by rpt_year;

-- Get the counts for total pos tests per year for females
CREATE TABLE mgtp_num_pos_tests_per_year_female_ctr AS
select rpt_year, count(*) as total_num_pos_tests_female, center_id
from mgtp_all_pos_gon_test_ctr
WHERE gender IN ('FEMALE', 'F')
group by rpt_year, center_id
order by rpt_year;


-- Get the counts for number of unique male positive patients per year
CREATE TABLE mgtp_num_uniq_pats_pos_male_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_pos_male, center_id
from mgtp_all_pos_gon_test_ctr_filtered
WHERE gender IN ('MALE', 'M')
group by rpt_year, center_id
order by rpt_year;

-- Get the counts for number of unique female positive patients per year
CREATE TABLE mgtp_num_uniq_pats_pos_female_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_pos_female, center_id
from mgtp_all_pos_gon_test_ctr_filtered
WHERE gender IN ('FEMALE', 'F')
group by rpt_year, center_id
order by rpt_year;

-- Number of unique patients with at least one encounter
-- Here we will apply center filtering and encounter type filtering
CREATE TABLE mgtp_pats_with_encounters_ctr AS
SELECT patient_id, gender, EXTRACT(YEAR FROM T1.date) rpt_year, T2.center_id
from emr_encounter T1,
emr_patient T2,
static_enc_type_lookup T3
WHERE T1.patient_id = T2.id
AND T1.raw_encounter_type = T3.raw_encounter_type 
AND (T1.raw_encounter_type is NULL or T3.rs_mdphnet = 1)
--AND T2.center_id not in ('1')
AND date_part('year', age(T1.date, T2.date_of_birth)) BETWEEN 15 AND 39
AND gender IN ('FEMALE', 'F', 'MALE', 'M')
AND T1.date >= '01-01-2006'
AND T1.date < '01-01-2018'
--GROUP BY patient_id, gender, rpt_year, center_id
ORDER BY rpt_year;

-- Only count patients that visited multiple centers in the same year once
CREATE TABLE mgtp_pats_with_encounters_ctr_filtered AS
SELECT DISTINCT ON (patient_id, center_id, rpt_year) patient_id, gender, rpt_year, center_id
FROM mgtp_pats_with_encounters_ctr;

-- Get the counts for number of unique male encounters per year
CREATE TABLE mgtp_num_uniq_encs_male_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_encs_male, center_id
from mgtp_pats_with_encounters_ctr_filtered
WHERE gender IN ('MALE', 'M')
group by rpt_year, center_id
order by rpt_year;

-- Get the counts for number of unique male encounters per year
CREATE TABLE mgtp_num_uniq_encs_female_ctr AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_encs_female, center_id
from mgtp_pats_with_encounters_ctr_filtered
WHERE gender IN ('FEMALE', 'F')
group by rpt_year, center_id
order by rpt_year;

-- All years and centers
CREATE TABLE mgtp_all_years_centers AS
SELECT rpt_year, center_id from  mgtp_all_gon_labs_ctr group by rpt_year, center_id
UNION
SELECT rpt_year, center_id from mgtp_pats_with_encounters_ctr_filtered group by rpt_year, center_id;

-- Sample of how to bring it all together
CREATE TABLE mgtp_output_ctr AS
select coalesce(T1.rpt_year, T2.rpt_year, T3.rpt_year, T4.rpt_year, T5.rpt_year, T6.rpt_year, T7.rpt_year, T8.rpt_year, T9.rpt_year, T10.rpt_year) report_year, 
coalesce(T1.center_id, T2.center_id, T3.center_id, T4.center_id, T5.center_id, T6.center_id, T7.center_id, T8.center_id, T9.center_id, T10.center_id) ctr_id,
max(num_uniq_encs_male) num_uniq_encs_male, 
max(total_num_tests_male) total_num_tests_male, 
max(num_uniq_pats_tested_male) num_uniq_pats_tested_male, 
max(total_num_pos_tests_male) total_num_pos_tests_male, 
max (num_uniq_pats_pos_male) num_uniq_pats_pos_male,
max(num_uniq_encs_female) num_uniq_encs_female, 
max(total_num_tests_female) total_num_tests_female, 
max(num_uniq_pats_tested_female) num_uniq_pats_tested_female, 
max(total_num_pos_tests_female) total_num_pos_tests_female, 
max(num_uniq_pats_pos_female) num_uniq_pats_pos_female
from mgtp_all_years_centers T11
LEFT JOIN mgtp_num_tests_per_year_male_ctr T1 ON (T11.rpt_year = T1.rpt_year and T11.center_id = T1.center_id)
LEFT JOIN mgtp_num_tests_per_year_female_ctr T2 ON (T11.rpt_year = T2.rpt_year and T11.center_id = T2.center_id)
LEFT JOIN mgtp_num_uniq_pats_tested_male_ctr T3 ON (T11.rpt_year = T3.rpt_year and T11.center_id = T3.center_id)
LEFT JOIN mgtp_num_uniq_pats_tested_female_ctr T4 ON (T11.rpt_year = T4.rpt_year and T11.center_id = T4.center_id)
LEFT JOIN mgtp_num_pos_tests_per_year_male_ctr T5 ON (T11.rpt_year = T5.rpt_year and T11.center_id = T5.center_id)
LEFT JOIN mgtp_num_pos_tests_per_year_female_ctr T6 ON (T11.rpt_year = T6.rpt_year and T11.center_id = T6.center_id)
LEFT JOIN mgtp_num_uniq_pats_pos_male_ctr T7 ON (T11.rpt_year = T7.rpt_year and T11.center_id = T7.center_id)
LEFT JOIN mgtp_num_uniq_pats_pos_female_ctr T8 ON (T11.rpt_year = T8.rpt_year and T11.center_id = T8.center_id)
LEFT JOIN mgtp_num_uniq_encs_male_ctr T9 ON (T11.rpt_year = T9.rpt_year and T11.center_id = T9.center_id)
LEFT JOIN mgtp_num_uniq_encs_female_ctr T10 ON (T11.rpt_year = T10.rpt_year and T11.center_id = T10.center_id)
group by report_year, ctr_id
ORDER BY report_year;

--select * from mgtp_output_ctr;