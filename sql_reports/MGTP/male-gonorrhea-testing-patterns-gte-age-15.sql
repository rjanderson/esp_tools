﻿DROP TABLE IF EXISTS mgtp_all_gon_labs;
DROP TABLE IF EXISTS mgtp_num_tests_per_year_male;
DROP TABLE IF EXISTS mgtp_num_tests_per_year_female;
DROP TABLE IF EXISTS num_uniq_pats_tested_male; 
DROP TABLE IF EXISTS num_uniq_pats_tested_female; 
DROP TABLE IF EXISTS mgtp_all_pos_gon_test;
DROP TABLE IF EXISTS mgtp_num_pos_tests_per_year_male;
DROP TABLE IF EXISTS mgtp_num_pos_tests_per_year_female;
DROP TABLE IF EXISTS mgtp_num_uniq_pats_pos_male;
DROP TABLE IF EXISTS mgtp_num_uniq_pats_pos_female;
DROP TABLE IF EXISTS mgtp_pats_with_encounters;
DROP TABLE IF EXISTS mgtp_num_uniq_encs_male;
DROP TABLE IF EXISTS mgtp_num_uniq_encs_female;
DROP TABLE IF EXISTS mgtp_all_years;
DROP TABLE IF EXISTS mgtp_output;


-- Get the details for all gonnorrhea labs 
-- Here we will also apply center filtering for MLCHC to the labs
CREATE TABLE mgtp_all_gon_labs AS 
SELECT T1.id as lab_id, T1.patient_id, T1.native_name, T1.native_code, T1.date, T1.specimen_source, EXTRACT(YEAR FROM T1.date) rpt_year, T3.gender, T3.center_id,
date_part('year', age(T1.date, T3.date_of_birth))
FROM emr_labresult T1,
conf_labtestmap T2,
emr_patient T3 
WHERE T1.native_code = T2.native_code
--AND T3.center_id not in ('1')
AND T1.patient_id = T3.id
AND T2.test_name = 'gonorrhea'
AND date_part('year', age(T1.date, T3.date_of_birth)) >= 15
AND gender IN ('FEMALE', 'F', 'MALE', 'M')
AND T1.date >= '01-01-2006'
AND T1.date < '01-01-2018';


-- Get the counts for total tests per year for males
CREATE TABLE mgtp_num_tests_per_year_male AS
select rpt_year, count(*) as total_num_tests_male
from mgtp_all_gon_labs
WHERE gender IN ('MALE', 'M')
group by rpt_year
order by rpt_year;

-- Get the counts for total tests per year for females
CREATE TABLE mgtp_num_tests_per_year_female AS
select rpt_year, count(*) as total_num_tests_female
from mgtp_all_gon_labs
WHERE gender IN ('FEMALE', 'F')
group by rpt_year
order by rpt_year;

-- Get the counts for number of unique male patients per year
CREATE TABLE num_uniq_pats_tested_male AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_tested_male
from mgtp_all_gon_labs
WHERE gender IN ('MALE', 'M')
group by rpt_year
order by rpt_year;

-- Get the counts for number of unique male patients per year
CREATE TABLE num_uniq_pats_tested_female AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_tested_female
from mgtp_all_gon_labs
WHERE gender IN ('FEMALE', 'F')
group by rpt_year
order by rpt_year;

-- Get all positive tests
CREATE TABLE mgtp_all_pos_gon_test AS 
SELECT T1.patient_id, T1.rpt_year, T1.gender, T1.lab_id, T2.name
FROM mgtp_all_gon_labs T1,
hef_event T2
WHERE T1.patient_id = T2.patient_id
AND T1.lab_id = T2.object_id
AND T2.name = 'lx:gonorrhea:positive';


-- Get the counts for total pos tests per year for males
CREATE TABLE mgtp_num_pos_tests_per_year_male AS
select rpt_year, count(*) as total_num_pos_tests_male
from mgtp_all_pos_gon_test
WHERE gender IN ('MALE', 'M')
group by rpt_year
order by rpt_year;

-- Get the counts for total pos tests per year for females
CREATE TABLE mgtp_num_pos_tests_per_year_female AS
select rpt_year, count(*) as total_num_pos_tests_female
from mgtp_all_pos_gon_test
WHERE gender IN ('FEMALE', 'F')
group by rpt_year
order by rpt_year;


-- Get the counts for number of unique male positive patients per year
CREATE TABLE mgtp_num_uniq_pats_pos_male AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_pos_male
from mgtp_all_pos_gon_test
WHERE gender IN ('MALE', 'M')
group by rpt_year
order by rpt_year;

-- Get the counts for number of unique female positive patients per year
CREATE TABLE mgtp_num_uniq_pats_pos_female AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_pats_pos_female
from mgtp_all_pos_gon_test
WHERE gender IN ('FEMALE', 'F')
group by rpt_year
order by rpt_year;

-- Number of unique patients with at least one encounter
-- Here we will apply center filtering and encounter type filtering
CREATE TABLE mgtp_pats_with_encounters AS
SELECT patient_id, gender, EXTRACT(YEAR FROM T1.date) rpt_year
from emr_encounter T1,
emr_patient T2,
static_enc_type_lookup T3
WHERE T1.patient_id = T2.id
AND T1.raw_encounter_type = T3.raw_encounter_type 
AND (T1.raw_encounter_type is NULL or T3.rs_mdphnet = 1)
--AND T2.center_id not in ('1')
AND T1.date >= '01-01-2006'
AND T1.date < '01-01-2018'
AND date_part('year', age(T1.date, T2.date_of_birth)) >= 15
AND gender IN ('FEMALE', 'F', 'MALE', 'M')
--GROUP BY patient_id, gender, rpt_year
ORDER BY rpt_year;

-- Get the counts for number of unique male encounters per year
CREATE TABLE mgtp_num_uniq_encs_male AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_encs_male
from mgtp_pats_with_encounters
WHERE gender IN ('MALE', 'M')
group by rpt_year
order by rpt_year;

-- Get the counts for number of unique male encounters per year
CREATE TABLE mgtp_num_uniq_encs_female AS
SELECT rpt_year, count(distinct(patient_id)) as num_uniq_encs_female
from mgtp_pats_with_encounters
WHERE gender IN ('FEMALE', 'F')
group by rpt_year
order by rpt_year;

-- All years and centers
CREATE TABLE mgtp_all_years AS
SELECT rpt_year from  mgtp_all_gon_labs group by rpt_year
UNION
SELECT rpt_year from mgtp_pats_with_encounters group by rpt_year;


-- Sample of how to bring it all together
CREATE TABLE mgtp_output AS
select coalesce(T1.rpt_year, T2.rpt_year, T3.rpt_year, T4.rpt_year, T5.rpt_year, T6.rpt_year, T7.rpt_year, T8.rpt_year, T9.rpt_year, T10.rpt_year) report_year, 
max(num_uniq_encs_male) num_uniq_encs_male, 
max(total_num_tests_male) total_num_tests_male, 
max(num_uniq_pats_tested_male) num_uniq_pats_tested_male, 
max(total_num_pos_tests_male) total_num_pos_tests_male, 
max (num_uniq_pats_pos_male) num_uniq_pats_pos_male,
max(num_uniq_encs_female) num_uniq_encs_female, 
max(total_num_tests_female) total_num_tests_female, 
max(num_uniq_pats_tested_female) num_uniq_pats_tested_female, 
max(total_num_pos_tests_female) total_num_pos_tests_female, 
max(num_uniq_pats_pos_female) num_uniq_pats_pos_female
from mgtp_all_years T11
LEFT JOIN mgtp_num_tests_per_year_male T1 ON (T11.rpt_year = T1.rpt_year)
LEFT JOIN mgtp_num_tests_per_year_female T2 ON (T11.rpt_year = T2.rpt_year)
LEFT JOIN num_uniq_pats_tested_male T3 ON (T11.rpt_year = T3.rpt_year)
LEFT JOIN num_uniq_pats_tested_female T4 ON (T11.rpt_year = T4.rpt_year)
LEFT JOIN mgtp_num_pos_tests_per_year_male T5 ON (T11.rpt_year = T5.rpt_year)
LEFT JOIN mgtp_num_pos_tests_per_year_female T6 ON (T11.rpt_year = T6.rpt_year)
LEFT JOIN mgtp_num_uniq_pats_pos_male T7 ON (T11.rpt_year = T7.rpt_year)
LEFT JOIN mgtp_num_uniq_pats_pos_female T8 ON (T11.rpt_year = T8.rpt_year)
LEFT JOIN mgtp_num_uniq_encs_male T9 ON (T11.rpt_year = T9.rpt_year)
LEFT JOIN mgtp_num_uniq_encs_female T10 ON (T11.rpt_year = T10.rpt_year)
group by report_year
ORDER BY report_year;

--select * from mgtp_output;



