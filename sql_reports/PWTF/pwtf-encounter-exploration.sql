﻿DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_diabetes;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_obesity;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_obesity;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_depression;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_depression;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_smokers;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_smokers;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_diabetes_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_obesity_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_obesity_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_depression_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_depression_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_smokers_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_smokers_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_diabetes_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_obesity_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_obesity_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_depression_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_depression_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_smokers_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_smokers_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_diabetes_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_obesity_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_obesity_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_depression_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_depression_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_smokers_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_smokers_encs_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_diabetes_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_obesity_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_obesity_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_depression_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_depression_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_smokers_stats_l2y;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_no_smokers_stats_l2y;

DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_output;

-- Patients with T2 Diabetes
create table esp_mdphnet.pwtf_dd_diabetes AS
select patient_id, condition, quarter_of_interest 
from esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
where condition = 'diabetes_t2_act'
and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients without T2 Diabetes
create table esp_mdphnet.pwtf_dd_no_diabetes AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'no_diabetes' and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients with Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_obesity AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'BMI >= 30' AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients without Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_no_obesity AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition in ('BMI <25', 'No Measured BMI', 'BMI >=25 and <30') AND age_group_10_yr != '0-9' and age_group_10_yr != '10-19' AND quarter_of_interest = 'apr_17' 
group by patient_id, condition, quarter_of_interest;

-- Patients with Depression
CREATE TABLE esp_mdphnet.pwtf_dd_depression AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'depression_act' and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients without Depression
CREATE TABLE esp_mdphnet.pwtf_dd_no_depression AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'no_depression' and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients who are current smokers
CREATE TABLE esp_mdphnet.pwtf_dd_smokers AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'current_smoker' and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;

-- Patients who are not current smokers
CREATE TABLE esp_mdphnet.pwtf_dd_no_smokers AS
select patient_id, condition, quarter_of_interest
from 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
WHERE condition = 'non_smoker' and quarter_of_interest = 'apr_17'
group by patient_id, condition, quarter_of_interest;


-- Encounter Counts For T2 Diabetes
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_diabetes T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encouner Counts For No T2 Diabetes
create table esp_mdphnet.pwtf_dd_no_diabetes_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_diabetes T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encouner Counts For Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_obesity_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_obesity T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encouner Counts For No Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_no_obesity_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_obesity T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encouner Counts For Depression
CREATE TABLE esp_mdphnet.pwtf_dd_depression_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_depression T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encouner Counts For No Depression
CREATE TABLE esp_mdphnet.pwtf_dd_no_depression_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_depression T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encounter Counts for Smokers
CREATE TABLE esp_mdphnet.pwtf_dd_smokers_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_smokers T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- Encounter Counts for Non-Smokers
CREATE TABLE esp_mdphnet.pwtf_dd_no_smokers_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_smokers T2
WHERE T1.patient_id = T2.patient_id
and date < '04-01-2017'
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For T2 Diabetes
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_diabetes T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For No T2 Diabetes
create table esp_mdphnet.pwtf_dd_no_diabetes_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_diabetes T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_obesity_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_obesity T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For No Obesity
CREATE TABLE esp_mdphnet.pwtf_dd_no_obesity_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_obesity T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For Depression
CREATE TABLE esp_mdphnet.pwtf_dd_depression_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_depression T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts For No Depression
CREATE TABLE esp_mdphnet.pwtf_dd_no_depression_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_depression T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts for Smokers
CREATE TABLE esp_mdphnet.pwtf_dd_smokers_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_smokers T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- L2Y Encounter Counts for Non-Smokers
CREATE TABLE esp_mdphnet.pwtf_dd_no_smokers_encs_l2y AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_no_smokers T2
WHERE T1.patient_id = T2.patient_id
--AND center_id not in ('1', '6', '7', '29', '34') 
AND raw_encounter_type not in ('HISTORY')
AND date > '04-01-2017'::date - interval '2 years' and date < '04-01-2017'
GROUP BY T2.patient_id;

-- Diabetes Stats
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_stats AS
select min(count) as min_diabetes, max(count) as max_diabetes, round(avg(count)) as avg_diabetes, max(median_diabetes) median_diabetes
from esp_mdphnet.pwtf_dd_diabetes_encs, 
(SELECT  MAX(count) as median_diabetes FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_diabetes_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- No Diabetes Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_diabetes_stats AS
select min(count) as min_no_diabetes, max(count) as max_no_diabetes, round(avg(count)) as avg_no_diabetes, max(median_no_diabetes) median_no_diabetes
from esp_mdphnet.pwtf_dd_no_diabetes_encs, 
(SELECT  MAX(count) as median_no_diabetes FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_diabetes_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- Obesity Stats
CREATE TABLE esp_mdphnet.pwtf_dd_obesity_stats AS
select min(count) as min_obesity, max(count) as max_obesity, round(avg(count)) as avg_obesity, max(median_obesity) median_obesity
from esp_mdphnet.pwtf_dd_obesity_encs, 
(SELECT  MAX(count) as median_obesity FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_obesity_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- No Obesity Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_obesity_stats AS
select min(count) as min_no_obesity, max(count) as max_no_obesity, round(avg(count)) as avg_no_obesity, max(median_no_obesity) median_no_obesity
from esp_mdphnet.pwtf_dd_no_obesity_encs, 
(SELECT  MAX(count) as median_no_obesity FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_obesity_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- Depression Stats
CREATE TABLE esp_mdphnet.pwtf_dd_depression_stats AS
select min(count) as min_depression, max(count) as max_depression, round(avg(count)) as avg_depression, max(median_depression) median_depression
from esp_mdphnet.pwtf_dd_depression_encs, 
(SELECT  MAX(count) as median_depression FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_depression_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- No Depression Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_depression_stats AS
select min(count) as min_no_depression, max(count) as max_no_depression, round(avg(count)) as avg_no_depression, max(median_no_depression) median_no_depression
from esp_mdphnet.pwtf_dd_no_depression_encs, 
(SELECT  MAX(count) as median_no_depression FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_depression_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- Smoker Stats
CREATE TABLE esp_mdphnet.pwtf_dd_smokers_stats AS
select min(count) as min_smokers, max(count) as max_smokers, round(avg(count)) as avg_smokers, max(median_smokers) median_smokers
from esp_mdphnet.pwtf_dd_smokers_encs, 
(SELECT  MAX(count) as median_smokers FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_smokers_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- No Smoker Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_smokers_stats AS
select min(count) as min_no_smokers, max(count) as max_no_smokers, round(avg(count)) as avg_no_smokers, max(median_no_smokers) median_no_smokers
from esp_mdphnet.pwtf_dd_no_smokers_encs, 
(SELECT  MAX(count) as median_no_smokers FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_smokers_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y Diabetes Stats
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_stats_l2y AS
select min(count) as min_diabetes_l2y, max(count) as max_diabetes_l2y, round(avg(count)) as avg_diabetes_l2y, max(median_diabetes) median_diabetes_l2y
from esp_mdphnet.pwtf_dd_diabetes_encs_l2y, 
(SELECT  MAX(count) as median_diabetes FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_diabetes_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y No Diabetes Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_diabetes_stats_l2y AS
select min(count) as min_no_diabetes_l2y, max(count) as max_no_diabetes_l2y, round(avg(count)) as avg_no_diabetes_l2y, max(median_no_diabetes) median_no_diabetes_l2y
from esp_mdphnet.pwtf_dd_no_diabetes_encs_l2y, 
(SELECT  MAX(count) as median_no_diabetes FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_diabetes_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y Obesity Stats
CREATE TABLE esp_mdphnet.pwtf_dd_obesity_stats_l2y AS
select min(count) as min_obesity_l2y, max(count) as max_obesity_l2y, round(avg(count)) as avg_obesity_l2y, max(median_obesity) median_obesity_l2y
from esp_mdphnet.pwtf_dd_obesity_encs_l2y, 
(SELECT  MAX(count) as median_obesity FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_obesity_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y No Obesity Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_obesity_stats_l2y AS
select min(count) as min_no_obesity_l2y, max(count) as max_no_obesity_l2y, round(avg(count)) as avg_no_obesity_l2y, max(median_no_obesity) median_no_obesity_l2y
from esp_mdphnet.pwtf_dd_no_obesity_encs_l2y, 
(SELECT  MAX(count) as median_no_obesity FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_obesity_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y Depression Stats
CREATE TABLE esp_mdphnet.pwtf_dd_depression_stats_l2y AS
select min(count) as min_depression_l2y, max(count) as max_depression_l2y, round(avg(count)) as avg_depression_l2y, max(median_depression) median_depression_l2y
from esp_mdphnet.pwtf_dd_depression_encs_l2y, 
(SELECT  MAX(count) as median_depression FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_depression_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y No Depression Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_depression_stats_l2y AS
select min(count) as min_no_depression_l2y, max(count) as max_no_depression_l2y, round(avg(count)) as avg_no_depression_l2y, max(median_no_depression) median_no_depression_l2y
from esp_mdphnet.pwtf_dd_no_depression_encs_l2y, 
(SELECT  MAX(count) as median_no_depression FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_depression_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y Smoker Stats
CREATE TABLE esp_mdphnet.pwtf_dd_smokers_stats_l2y AS
select min(count) as min_smokers_l2y, max(count) as max_smokers_l2y, round(avg(count)) as avg_smokers_l2y, max(median_smokers) median_smokers_l2y
from esp_mdphnet.pwtf_dd_smokers_encs_l2y, 
(SELECT  MAX(count) as median_smokers FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_smokers_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;

-- L2Y No Smoker Stats
CREATE TABLE esp_mdphnet.pwtf_dd_no_smokers_stats_l2y AS
select min(count) as min_no_smokers_l2y, max(count) as max_no_smokers_l2y, round(avg(count)) as avg_no_smokers_l2y, max(median_no_smokers) median_no_smokers_l2y
from esp_mdphnet.pwtf_dd_no_smokers_encs_l2y, 
(SELECT  MAX(count) as median_no_smokers FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_no_smokers_encs_l2y) as t WHERE bucket = 1 GROUP BY bucket) T2;


-- Print Output
CREATE TABLE esp_mdphnet.pwtf_dd_output AS
select * from
esp_mdphnet.pwtf_dd_diabetes_stats, 
esp_mdphnet.pwtf_dd_diabetes_stats_l2y, 
esp_mdphnet.pwtf_dd_no_diabetes_stats,
esp_mdphnet.pwtf_dd_no_diabetes_stats_l2y,
esp_mdphnet.pwtf_dd_obesity_stats,
esp_mdphnet.pwtf_dd_obesity_stats_l2y,
esp_mdphnet.pwtf_dd_no_obesity_stats,
esp_mdphnet.pwtf_dd_no_obesity_stats_l2y,
esp_mdphnet.pwtf_dd_depression_stats,
esp_mdphnet.pwtf_dd_depression_stats_l2y,
esp_mdphnet.pwtf_dd_no_depression_stats,
esp_mdphnet.pwtf_dd_no_depression_stats_l2y,
esp_mdphnet.pwtf_dd_smokers_stats,
esp_mdphnet.pwtf_dd_smokers_stats_l2y,
esp_mdphnet.pwtf_dd_no_smokers_stats,
esp_mdphnet.pwtf_dd_no_smokers_stats_l2y;

SELECT * from esp_mdphnet.pwtf_dd_output;


