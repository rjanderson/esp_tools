﻿DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_encounters CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_t2diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.ptwf_comm_bmi_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_hypertension_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_hypertension_max CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_asthma_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_asthma_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_opioid_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_bmi_ow_ob CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_hypertension_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_asthma_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_mapped CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_current CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_passive CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.ptwf_comm_conditions_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_valid_conditions CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_denom CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_full_strat CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_summary_output CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_town_output CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_full_state CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_all_denom_and_num CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_t1diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_active CASCADE;


-- ENCOUNTERS Patients with encounter for any reason in the 2 years preceding the index date 
CREATE TABLE esp_mdphnet.pwtf_comm_encounters AS
SELECT DISTINCT(patient_id), 'encounter'::text condition
FROM  public.emr_encounter  T1,   
public.emr_patient T2
WHERE T1.patient_id = T2.id 
--AND center_id not in ('1', '7', '15', '19', '20') 
AND raw_encounter_type not in ('HISTORY')
AND date > ('01-01-2015'::date - interval  '2 years') and date < '01-01-2015';  

-- T1 DIABETES As Of Index Date
CREATE TABLE esp_mdphnet.pwtf_comm_t1diabetes AS
SELECT patient_id, condition
FROM public.nodis_case 
WHERE date < '01-01-2015' 
AND condition = 'diabetes:type-1';

-- T2 DIABETES As Of Index Date
CREATE TABLE esp_mdphnet.pwtf_comm_t2diabetes AS
SELECT patient_id, condition
FROM public.nodis_case 
WHERE date < '01-01-2015' 
AND condition = 'diabetes:type-2';

-- ACTIVE DIABETES case and events prior to index date
CREATE  TABLE esp_mdphnet.pwtf_comm_diabetes_t2_all AS
select T1.patient_id, T1.id case_id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from public.nodis_case T1, public.nodis_caseactivehistory T2
WHERE T1.date < '01-01-2015' 
AND T2.date < '01-01-2015'
and condition = 'diabetes:type-2'
ANd T1.id = T2.case_id;

-- ACTIVE DIABETES most recent case event. 
CREATE  TABLE esp_mdphnet.pwtf_comm_diabetes_t2_max AS
SELECT T1.patient_id, case_id, condition, case_date, history_date, status, isactive, max_date
FROM esp_mdphnet.pwtf_comm_diabetes_t2_all T1,
(SELECT patient_id, max(history_date) max_date from esp_mdphnet.pwtf_comm_diabetes_t2_all group by patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.history_date = T2.max_date;

-- ACTIVE DIABETES only get active cases as of index date
CREATE  TABLE esp_mdphnet.pwtf_comm_diabetes_t2_active AS
SELECT patient_id, 'Active Diabetes'::TEXT as condition 
FROM esp_mdphnet.pwtf_comm_diabetes_t2_max
WHERE status in ('I', 'R');


-- DEPRESSION case and events prior to index date
CREATE  TABLE esp_mdphnet.pwtf_comm_depression_all AS
select T1.patient_id, T1.id case_id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from public.nodis_case T1, public.nodis_caseactivehistory T2
WHERE T1.date < '01-01-2015' 
AND T2.date < '01-01-2015'
and condition = 'depression'
ANd T1.id = T2.case_id;

-- DEPRESSION most recent case event. 
CREATE  TABLE esp_mdphnet.pwtf_comm_depression_max AS
SELECT T1.patient_id, case_id, condition, case_date, history_date, status, isactive, max_date
FROM esp_mdphnet.pwtf_comm_depression_all T1,
(SELECT patient_id, max(history_date) max_date from esp_mdphnet.pwtf_comm_depression_all group by patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.history_date = T2.max_date;

-- DEPRESSION only get active cases as of index date
CREATE  TABLE esp_mdphnet.pwtf_comm_depression_active AS
SELECT patient_id, 'depression'::TEXT as condition 
FROM esp_mdphnet.pwtf_comm_depression_max
WHERE status in ('I', 'R');

-- OVERWEIGHT & OBESE Get most recent BMI condition prior to the index date  
CREATE TABLE esp_mdphnet.ptwf_comm_bmi_all AS 
select T1.patid, T1.condition, T2.max_date
FROM 
public.esp_condition T1,
(SELECT patid, max(date) max_date from esp_condition where condition ilike '%bmi%' AND DATE < ('01-01-2015'::date - '01-01-1960'::date) GROUP BY patid) T2
where T1.patid = T2.patid
AND T1.date = T2.max_date
AND T1.condition ilike '%bmi%';

-- OVERWEIGHT & OBESE Assign condition for those overweight or obese  
CREATE TABLE esp_mdphnet.pwtf_comm_bmi_ow_ob AS
SELECT T2.id patient_id,
case when condition = 'BMI >= 30' then 'obese'
when condition = 'BMI >=25 and <30' then 'overweight'
ELSE 'OOPS!' 
end condition
FROM esp_mdphnet.ptwf_comm_bmi_all T1,
public.emr_patient T2
WHERE condition in ('BMI >= 30', 'BMI >=25 and <30')
AND T1.patid = T2.natural_key;

-- HYPERTENSION case and events prior to index date
CREATE  TABLE esp_mdphnet.pwtf_comm_hypertension_all AS
select T1.patient_id, T1.id case_id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from public.nodis_case T1, public.nodis_caseactivehistory T2
WHERE T1.date < '01-01-2015' 
AND T2.date < '01-01-2015'
and condition = 'hypertension'
ANd T1.id = T2.case_id;

-- HYPERTENSION most recent case event. 
CREATE  TABLE esp_mdphnet.pwtf_comm_hypertension_max AS
select T1.patient_id, case_id, condition, case_date, history_date, status, isactive, max_date
FROM esp_mdphnet.pwtf_comm_hypertension_all T1,
(select patient_id, max(history_date) max_date from esp_mdphnet.pwtf_comm_hypertension_all group by patient_id) T2
where T1.patient_id = T2.patient_id
AND T1.history_date = T2.max_date;

-- HYPERTENSION only get active cases as of index date
CREATE  TABLE esp_mdphnet.pwtf_comm_hypertension_active AS
SELECT patient_id, condition 
FROM esp_mdphnet.pwtf_comm_hypertension_max
WHERE status in ('I', 'R');

-- ASTHMA case and events prior to index date
CREATE  TABLE esp_mdphnet.pwtf_comm_asthma_all AS
select T1.patient_id, T1.id case_id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive
from public.nodis_case T1, public.nodis_caseactivehistory T2
WHERE T1.date < '01-01-2015' 
AND T2.date < '01-01-2015'
and condition = 'asthma'
ANd T1.id = T2.case_id;

-- ASTHMA most recent case event. 
CREATE  TABLE esp_mdphnet.pwtf_comm_asthma_max AS
select T1.patient_id, case_id, condition, case_date, history_date, status, isactive, max_date
FROM esp_mdphnet.pwtf_comm_asthma_all T1,
(select patient_id, max(history_date) max_date from esp_mdphnet.pwtf_comm_asthma_all group by patient_id) T2
where T1.patient_id = T2.patient_id
AND T1.history_date = T2.max_date;

-- ASTHMA only get active cases as of index date
CREATE  TABLE esp_mdphnet.pwtf_comm_asthma_active AS
SELECT patient_id, condition 
FROM esp_mdphnet.pwtf_comm_asthma_max
WHERE status in ('I', 'R');


-- SMOKING - Get the most recent smoking status on max(date) prior to the index date. 
CREATE TABLE esp_mdphnet.pwtf_comm_smoking_all  AS 
SELECT distinct on (T1.patient_id) T1.patient_id, T1.tobacco_use, T2.max_smoking_date 
FROM emr_socialhistory T1,
(select patient_id, max(date) as max_smoking_date from emr_socialhistory where tobacco_use is not null and tobacco_use != '' AND date < '01-01-2015' group by patient_id) T2
WHERE  T1.date < '01-01-2015' 
AND T1.tobacco_use is not null 
and T1.tobacco_use != '' 
AND T1.date = T2.max_smoking_date
AND T1.patient_id = T2.patient_id;


-- SMOKING -- Map Smoking values
CREATE  TABLE esp_mdphnet.pwtf_comm_smoking_mapped AS 
SELECT patient_id,
case when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 'Passive'      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 'Never'  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 'Former'  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 'Current'                      
else 'Not available' end  smoking 
FROM esp_mdphnet.pwtf_comm_smoking_all;

-- SMOKING: Get current smokers
CREATE  TABLE esp_mdphnet.pwtf_comm_smoking_current AS 
select patient_id, 'smoker'::text condition 
FROM esp_mdphnet.pwtf_comm_smoking_mapped
WHERE smoking = 'Current';

-- SMOKING: Get passive smokers
CREATE  TABLE esp_mdphnet.pwtf_comm_smoking_passive AS 
select patient_id, 'passive_smoker'::text condition 
FROM esp_mdphnet.pwtf_comm_smoking_mapped
WHERE smoking = 'Passive';

-- OPIOID
CREATE TABLE esp_mdphnet.pwtf_comm_opioid_all AS
SELECT distinct(T1.patient_id), 'opioid'::TEXT condition
FROM emr_prescription T1,
static_rx_lookup T2
WHERE T1.name = T2.name
AND T2.type = 1
AND T1.date BETWEEN '01-01-2014' and '12-31-2014';

-- UNION together all of the patients and conditions
CREATE TABLE esp_mdphnet.ptwf_comm_conditions_all AS
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_t1diabetes
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_t2diabetes
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_diabetes_t2_active
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_depression_active
UNION 
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_bmi_ow_ob
UNION 
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_hypertension_active
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_asthma_active
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_smoking_current
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_smoking_passive 
UNION 
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_opioid_all;


-- JOIN TO ENCOUNTERS to ensure we only get patients who've 
-- had an encounter in the 2 years prior to the index date
CREATE TABLE esp_mdphnet.pwtf_comm_valid_conditions AS 
SELECT T1.patient_id, T2.condition 
FROM esp_mdphnet.pwtf_comm_encounters T1,
esp_mdphnet.ptwf_comm_conditions_all T2
WHERE T1.patient_id = T2.patient_id;

-- -- Add in Denominator Encounter Data
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_denom AS 
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_valid_conditions
UNION
SELECT patient_id, condition FROM esp_mdphnet.pwtf_comm_encounters;

-- PATIENT -- Determine age on index date and gather patient details from patient table and esp_demogrpahic table
-- Limit to patients with a zip in a PWTF community
CREATE  TABLE esp_mdphnet.pwtf_comm_conditions_patient  WITHOUT OIDS  AS 
SELECT T1.patient_id,T1.condition,
date_part('year', age('01-01-2015', date_of_birth)) age,
CASE   
when date_part('year', age('01-01-2015', date_of_birth)) <= 9 then '0-9'    
when date_part('year', age('01-01-2015', date_of_birth)) <= 19 then '10-19'  
when date_part('year', age('01-01-2015', date_of_birth)) <= 29 then '20-29' 
when date_part('year', age('01-01-2015', date_of_birth)) <= 39 then '30-39' 
when date_part('year', age('01-01-2015', date_of_birth)) <= 49 then '40-49'   
when date_part('year', age('01-01-2015', date_of_birth)) <= 59 then '50-59' 
when date_part('year', age('01-01-2015', date_of_birth)) <= 69 then '60-69' 
when date_part('year', age('01-01-2015', date_of_birth)) <= 79 then '70-79'   
when date_of_birth is null then 'UNKNOWN AGE'
else '>= 80' end age_group_10_yr,
T2.natural_key,
T2.zip5,
T3.sex,
CASE when T3.race_ethnicity = 6 then 'hispanic'  
when T3.race_ethnicity = 5 then 'white' 
when T3.race_ethnicity = 3 then 'black' 
when T3.race_ethnicity = 2 then 'asian' 
when T3.race_ethnicity = 1 then 'native_american' 
when T3.race_ethnicity = 0 then 'unknown' 
end race,
coalesce(T4.city, 'NOT MAPPED'::text) town,
COALESCE(T4.region, 'NOT MAPPED'::TEXT) region
FROM esp_mdphnet.pwtf_comm_conditions_denom T1
INNER JOIN public.emr_patient T2 ON (T1.patient_id = T2.id )
INNER JOIN esp_mdphnet.esp_demographic T3 on (T2.natural_key = T3.patid)
FULL OUTER JOIN static_eohhs_region_mappings T4 ON (T2.zip5 = T4.zip5);

-- Create rows for the full state
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_patient_full_state AS
select distinct(patient_id), condition, age, age_group_10_yr, natural_key, zip5, sex, race, 'FULL STATE' as town, 'FULL STATE' as region from esp_mdphnet.pwtf_comm_conditions_patient;

-- Union together the data for counts
CREATE TABLE esp_mdphnet.pwtf_comm_conditions_all_denom_and_num AS
SELECT * from esp_mdphnet.pwtf_comm_conditions_patient 
UNION 
SELECT * FROM esp_mdphnet.pwtf_comm_conditions_patient_full_state;


-- Full Stratified output
CREATE TABLE esp_mdphnet.pwtf_comm_output_full_strat AS 
select
age_group_10_yr, race, sex, town,
count(CASE WHEN condition = 'encounter' THEN 1 END) encounters,
count(CASE WHEN condition = 'diabetes:type-1' then 1 END) t1_diabetes,
count(CASE WHEN condition = 'diabetes:type-2' AND age >= 20 then 1 END) t2_diabetes_gte20,
count(CASE WHEN condition = 'Active Diabetes' AND age >= 20 then 1 END) t2_active_diabetes_gte20,
count(CASE WHEN condition = 'overweight' AND age >= 20 then 1 END) overweight_gte20,
count(CASE WHEN condition = 'obese' AND age >= 20 then 1 END) obese_gte20,
count(CASE WHEN condition = 'hypertension' AND age >= 20 then 1 END) hypertension_gte20,
count(case when condition = 'asthma' and age <= 9 then 1 end) asthma_lte9,
count(case when condition = 'asthma' and age >= 10 AND age <= 19 then 1 end) asthma_gte10_lte19,
count(case when condition = 'asthma' and age >= 20 then 1 end) asthma_gte20,
count(case when condition = 'depression' and age >= 20 then 1 end) depression_gte20,
count(case when condition = 'smoker' and age >= 10 and age <= 19 then 1 end) smoker_gte10_lte19,
count(case when condition = 'smoker' and age >= 20 then 1 end) smoker_gte20,
count(case when condition = 'passive_smoker' and age >= 10 and age <= 19 then 1 end) passive_smoker_gte10_lte19,
count(case when condition = 'passive_smoker' and age >= 20 then 1 end) passive_smoker_gte20,
count(case when condition = 'opioid' then 1 end) opioid
FROM 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
GROUP BY age_group_10_yr,race,sex, town
order by town, sex, race, age_group_10_yr;

-- Town Output
CREATE TABLE esp_mdphnet.pwtf_comm_output_town_output AS 
select
town,
count(CASE WHEN condition = 'encounter' THEN 1 END) encounters,
count(CASE WHEN condition = 'diabetes:type-1' then 1 END) t1_diabetes,
count(CASE WHEN condition = 'diabetes:type-2' AND age >= 20 then 1 END) t2_diabetes_gte20,
count(CASE WHEN condition = 'Active Diabetes' AND age >= 20 then 1 END) t2_active_diabetes_gte20,
count(CASE WHEN condition = 'overweight' AND age >= 20 then 1 END) overweight_gte20,
count(CASE WHEN condition = 'obese' AND age >= 20 then 1 END) obese_gte20,
count(CASE WHEN condition = 'hypertension' AND age >= 20 then 1 END) hypertension_gte20,
count(case when condition = 'asthma' and age <= 9 then 1 end) asthma_lte9,
count(case when condition = 'asthma' and age >= 10 AND age <= 19 then 1 end) asthma_gte10_lte19,
count(case when condition = 'asthma' and age >= 20 then 1 end) asthma_gte20,
count(case when condition = 'depression' and age >= 20 then 1 end) depression_gte20,
count(case when condition = 'smoker' and age >= 10 and age <= 19 then 1 end) smoker_gte10_lte19,
count(case when condition = 'smoker' and age >= 20 then 1 end) smoker_gte20,
count(case when condition = 'passive_smoker' and age >= 10 and age <= 19 then 1 end) passive_smoker_gte10_lte19,
count(case when condition = 'passive_smoker' and age >= 20 then 1 end) passive_smoker_gte20,
count(case when condition = 'opioid' then 1 end) opioid
FROM 
esp_mdphnet.pwtf_comm_conditions_all_denom_and_num
group by town
order by town;

--Print output to screen
select * from esp_mdphnet.pwtf_comm_output_full_strat;
select * from esp_mdphnet.pwtf_comm_output_town_output;



DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_encounters CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_t2diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.ptwf_comm_bmi_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_hypertension_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_hypertension_max CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_asthma_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_asthma_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_all CASCADE;
drop table IF EXISTS esp_mdphnet.pwtf_comm_opioid_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_bmi_ow_ob CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_hypertension_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_asthma_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_mapped CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_current CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_smoking_passive CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.ptwf_comm_conditions_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_valid_conditions CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_denom CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_patient_full_state CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_conditions_all_denom_and_num CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_t1diabetes CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_diabetes_t2_active CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_all CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_max CASCADE;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_depression_active CASCADE;

--DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_full_strat CASCADE;
--DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_summary_output CASCADE;
--DROP TABLE IF EXISTS esp_mdphnet.pwtf_comm_output_town_output CASCADE;

