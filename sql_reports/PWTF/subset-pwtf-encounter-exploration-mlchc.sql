DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_encs;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_diabetes_stats;
DROP TABLE IF EXISTS esp_mdphnet.pwtf_dd_output;


-- Encounter Counts For T2 Diabetes
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_encs AS
select count(*), T2.patient_id
from emr_encounter T1, esp_mdphnet.pwtf_dd_diabetes T2, emr_patient T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.id
AND T2.patient_id = T3.id
and date < '04-01-2017'
AND center_id not in ('1', '6', '7', '29', '34') 
-- AND raw_encounter_type not in ('HISTORY')
GROUP BY T2.patient_id;


-- Diabetes Stats
CREATE TABLE esp_mdphnet.pwtf_dd_diabetes_stats AS
select min(count) as min_diabetes, max(count) as max_diabetes, round(avg(count)) as avg_diabetes, max(median_diabetes) median_diabetes
from esp_mdphnet.pwtf_dd_diabetes_encs, 
(SELECT  MAX(count) as median_diabetes FROM  (SELECT count, ntile(2) OVER (ORDER BY count) AS bucket FROM esp_mdphnet.pwtf_dd_diabetes_encs) as t WHERE bucket = 1 GROUP BY bucket) T2;


-- Print Output
CREATE TABLE esp_mdphnet.pwtf_dd_output AS
select * from
esp_mdphnet.pwtf_dd_diabetes_stats, 
esp_mdphnet.pwtf_dd_diabetes_stats_l2y, 
esp_mdphnet.pwtf_dd_no_diabetes_stats,
esp_mdphnet.pwtf_dd_no_diabetes_stats_l2y,
esp_mdphnet.pwtf_dd_obesity_stats,
esp_mdphnet.pwtf_dd_obesity_stats_l2y,
esp_mdphnet.pwtf_dd_no_obesity_stats,
esp_mdphnet.pwtf_dd_no_obesity_stats_l2y,
esp_mdphnet.pwtf_dd_depression_stats,
esp_mdphnet.pwtf_dd_depression_stats_l2y,
esp_mdphnet.pwtf_dd_no_depression_stats,
esp_mdphnet.pwtf_dd_no_depression_stats_l2y,
esp_mdphnet.pwtf_dd_smokers_stats,
esp_mdphnet.pwtf_dd_smokers_stats_l2y,
esp_mdphnet.pwtf_dd_no_smokers_stats,
esp_mdphnet.pwtf_dd_no_smokers_stats_l2y;

SELECT * from esp_mdphnet.pwtf_dd_output;
