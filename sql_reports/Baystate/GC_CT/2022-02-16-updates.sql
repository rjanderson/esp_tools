
-- UPDATES TO LOINC/SNOMEDS 01/25/2022
-- gonorrhea
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 18;
update conf_labtestmap set output_code = '24111-7', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 19;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 20;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 21;
update conf_labtestmap set output_code = 'MDPH-304', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R182' WHERE id = 25;
update conf_labtestmap set output_code = '45072-6', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R182' WHERE id = 26;
update conf_labtestmap set output_code = '24111-7', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 27;
update conf_labtestmap set output_code = '698-1', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 28;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 29;

-- gon 
-- MDPH wanted 24111-7 but no Indeterminate SNOMED exists
-- Reverting to 5026-6 LOINC for now per DPH. They will advise when the other one is available.
update conf_labtestmap set output_code = '5028-6', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 22;
update conf_labtestmap set output_code = '5028-6', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 24;

-- chlamydia
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 6;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 7;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 8;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 9;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 10;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 12;
update conf_labtestmap set output_code = 'MDPH-304', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = '42425007' WHERE id = 13;
update conf_labtestmap set output_code = '45072-6', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = '42425007' WHERE id = 14;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 15;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 17;

--chlam
-- MDPH wanted 11475-1 but it does not have a negative SNOMED.
-- Changed it to 6349-5 until DPH advises differently.
update conf_labtestmap set output_code = '6349-5', snomed_pos = '63938009', snomed_neg = '260385009', snomed_ind = null WHERE id = 16;