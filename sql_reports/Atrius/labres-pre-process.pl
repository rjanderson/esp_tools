#!/usr/bin/perl
## Author: keberhardt@commoninf.com

use strict;
use warnings;
use Data::Dumper;
use File::Copy;

#############################
### Set Your Variables Here ##
##############################

my ($res_file) = @ARGV;
my $archive_dir = "/srv/esp30/data/epic/archive";
my @source_labs = ('SOURCE REPORTABLE', 'SOURCE', 'A SOURCE');
my @genital_native_names = ('N GONORRHOEAE GENITAL');
my @throat_native_names  = ('N. GONORRHOEAE RNA, TMA, THROAT', 'CHLAMYDIA TRACHOMATIS RNA TMA THROAT');
my @rectal_native_names  = ('CULTURE, NEISSERIA GONORRHOEAE, ANAL', 'CHLAMYDIA TRACHOMATIS RNA TMA RECTAL', 'NEISSERIA GONORRHOEAE RNA TMA RECTAL');
my @genital_proc_names   = ('CHLAMYDIA / GC GENITAL IN PAP VIAL', 'CHLAMYDIA / GC GENITAL RRNA', 'CHLAMYDIA / GC GENITAL SWAB', 'CHLAMYDIA / GC GENITAL SWAB DNA',
                            'CHLAMYDIA DNA, GENITAL', 'CHLAMYDIA GENITAL IN PAP VIAL', 'CHLAMYDIA GENITAL RRNA', 'CHLAMYDIA GENITAL SWAB', 'CHLAMYDIA GENITAL SWAB DNA',
                            'CHLAMYDIA GENTIAL IN PAP VIAL', 'CHLAMYDIA/GC GENITAL RRNA', 'GC DNA, GENITAL', 'GC GENITAL IN PAP VIAL', 'GC GENITAL RNA TMA', 'GC GENITAL SWAB',
                            'GC GENITAL SWAB DNA');
my @throat_proc_names    = ('CHLAMYDIA / N. GONORRHOEAE RNA TMA THROAT', 'CHLAMYDIA TRACHOMATIS RNA TMA THROAT', 'NEISSERIA GONORRHOEAE RNA, TMA, THROAT');
my @rectal_proc_names    = ('CHLAMYDIA / GC PCR THINPREP VIAL (ANAL)', 'CHLAMYDIA PCR THINPREP VIAL (ANAL)', 'CHLAMYDIA TRACHOMATIS RNA TMA RECTAL', 'GC CULTURE, ANAL',
                            'NEISSERIA GONORRHOEAE RNA TMA RECTAL');
my @urine_proc_names     = ('CHLAMYDIA / GC URINE DNA', 'CHLAMYDIA / GC URINE RRNA', 'CHLAMYDIA APTIMA URINE OR SWAB', 'CHLAMYDIA DNA URINE', 'CHLAMYDIA DNA, URINE',
                            'CHLAMYDIA URINE DNA', 'CHLAMYDIA URINE RRNA', 'CHLAMYDIA/GC URINE RRNA', 'GC DNA, URINE', 'GC URINE', 'GC URINE DNA', 'GC URINE RNA TMA');


############################
### End Variable Set       ##
#############################

#Sort out the filenames so we archive and make backups

my $res_file_name = ( split '/', $res_file )[ -1 ];
my $archive_res_path = $archive_dir . "/" . $res_file_name . ".orig";

#Make a backup of the results file before we overwrite it
copy($res_file,$archive_res_path) or die "ERROR: The move of $res_file to $archive_res_path failed: $!";

#Process the res file and create a hash with the values
open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my %resdata;

while(<RESDATA>) {
    chomp;
    my ($patient_id, $mrn, $order_natural_key, $order_date, $result_date, $provider_id, $order_type, $cpt, $component, $component_name, $result_string) = split(/\^/);

    #For identified component names create a hash based on order natural key 
    #that contains the result string which is really the specimen source
    if (defined $component_name && $component_name ne '' && $component_name ~~ @source_labs) {
        #print "$component_name has $result_string\n";
        $resdata{$order_natural_key} .= $result_string;
    }
    
}
    
#print Dumper(\%resdata);
    
for my $order_natural_key ( keys %resdata ) {
    my $found_specimen_source = $resdata{$order_natural_key};
}
    
    
close(RESDATA);


#Process the results file again to create the updated rows 

open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my @row_final;
while ( my $res_line = <RESDATA> ) {
    my @row = split( /\^/, $res_line );
    my $res_to_match = $row[2];
    if( exists($resdata{$res_to_match}) ){    
        #Only update if the specimen source field is blank
        if ($row[19] eq '') { 
            $row[19] = $resdata{$res_to_match};
            @row =  join( '^', @row );
            push @row_final, @row;
        } else {
            @row = join( '^', @row );
            push @row_final, @row;
        }
    } else {
       @row = join( '^', @row );
       push @row_final, @row;
    }
}

close(RESDATA);


open(RESDATA, ">$res_file") || die "ERROR: Can't open $res_file: $!\n";

#Overwrtite the file with the new data

print RESDATA @row_final;

close(RESDATA);


#If specimen source is still not set, use a specimen source based on native name

open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my %resdata_nn;

while(<RESDATA>) {
    chomp;
    my ($patient_id, $mrn, $order_natural_key, $order_date, $result_date, $provider_id, $order_type, $cpt, $component, $component_name, $result_string) = split(/\^/);

    #For identified procedure names create a hash based on order natural key and component value
	#as the same natural key can have different components
    if (defined $component && defined $component_name && $component_name ne '' && $component_name ~~ @genital_native_names) {
        #print "$component_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_nn{$res_unique_key} .= "GENITAL";
    } elsif (defined $component && defined $component_name && $component_name ne '' && $component_name ~~ @throat_native_names) {
	    #print "$component_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_nn{$res_unique_key} .= "THROAT";
    } elsif (defined $component && defined $component_name && $component_name ne '' && $component_name ~~ @rectal_native_names) {
	    #print "$component_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_nn{$res_unique_key} .= "RECTAL";	
    }
    
}
    
print Dumper(\%resdata_nn);
    
for my $res_unique_key ( keys %resdata_nn ) {
    my $nn_specimen_source = $resdata_nn{$res_unique_key};
}
        
close(RESDATA);

#Process the results file again to create the updated rows 

open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my @row_final_nn;
while ( my $res_line = <RESDATA> ) {
    my @row = split( /\^/, $res_line );
	my $res_to_match;
    if (defined $row[2] && defined $row[8]) {
        $res_to_match = "$row[2]-$row[8]";
	} else {
	    $res_to_match = $row[2];
	}
    if( exists($resdata_nn{$res_to_match}) ){    
        #Only update if the specimen source field is blank
        if ($row[19] eq '') { 
            $row[19] = $resdata_nn{$res_to_match};
            @row =  join( '^', @row );
            push @row_final_nn, @row;
        } else {
            @row = join( '^', @row );
            push @row_final_nn, @row;
        }
    } else {
       @row = join( '^', @row );
       push @row_final_nn, @row;
    }
}

close(RESDATA);


open(RESDATA, ">$res_file") || die "ERROR: Can't open $res_file: $!\n";

#Overwrtite the file with the new data

print RESDATA @row_final_nn;

close(RESDATA);

#If specimen source is still not set, use a specimen source based on procedure name

open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my %resdata_pn;

while(<RESDATA>) {
    chomp;
    my ($patient_id, $mrn, $order_natural_key, $order_date, $result_date, $provider_id, $order_type, $cpt, $component, $component_name, $result_string, $normal_flag, $ref_low, $ref_high, $unit, $status, $note, $specimen_num, $impression, $specimen_source, $collection_date, $procedure_name ) = split(/\^/);
	
    #For identified procedure names create a hash based on order natural key and component value
	#as the same natural key can have different components
    if (defined $component && defined $procedure_name && $procedure_name ne '' && $procedure_name ~~ @genital_proc_names) {
        #print "$procedure_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_pn{$res_unique_key} .= "GENITAL";
    } elsif (defined $component && defined $procedure_name && $procedure_name ne '' && $procedure_name ~~ @throat_proc_names) {
	    #print "$procedure_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_pn{$res_unique_key} .= "THROAT";
    } elsif (defined $component && defined $procedure_name && $procedure_name ne '' && $procedure_name ~~ @rectal_proc_names) {
	    #print "$procedure_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_pn{$res_unique_key} .= "RECTAL";
    } elsif (defined $component && defined $procedure_name && $procedure_name ne '' && $procedure_name ~~ @urine_proc_names) {
	    #print "$procedure_name\n";
		my $res_unique_key = "$order_natural_key-$component";
        $resdata_pn{$res_unique_key} .= "URINE";     		
    }
    
}
    
#print Dumper(\%resdata_pn);
    
for my $res_unique_key ( keys %resdata_pn ) {
    my $pn_specimen_source = $resdata_pn{$res_unique_key};
}
        
close(RESDATA);

#Process the results file again to create the updated rows 

open(RESDATA, $res_file) || die "ERROR: Can't open $res_file: $!\n";

my @row_final_pn;
while ( my $res_line = <RESDATA> ) {
    my @row = split( /\^/, $res_line );
	my $res_to_match;
	if (defined $row[2] && defined $row[8]) {
        $res_to_match = "$row[2]-$row[8]";
	} else {
	    $res_to_match = $row[2];
	}
    if( exists($resdata_pn{$res_to_match}) ){    
        #Only update if the specimen source field is blank
        if ($row[19] eq '') { 
            $row[19] = $resdata_pn{$res_to_match};
            @row =  join( '^', @row );
            push @row_final_pn, @row;
        } else {
            @row = join( '^', @row );
            push @row_final_pn, @row;
        }
    } else {
       @row = join( '^', @row );
       push @row_final_pn, @row;
    }
}

close(RESDATA);


open(RESDATA, ">$res_file") || die "ERROR: Can't open $res_file: $!\n";

#Overwrtite the file with the new data

print RESDATA @row_final_pn;

close(RESDATA);



