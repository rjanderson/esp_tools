set client_min_messages to error;
--Get the single encounter id that matches us with the STI natural key
drop table if exists temp_ssun_encid;
create table temp_ssun_encid as
select enc.id, enc.natural_key as encnaturalkey, sti.natural_key as stinaturalkey
from 
emr_stiencounterextended sti, emr_encounter enc
where sti.natural_key = enc.natural_key;


--Get all the dxs that exist for set of encounters that matches to sti encounter key 
drop table if exists temp_ssun_visits_diags;
create table temp_ssun_visits_diags as 
select sti.patient_id as stipatient, sti.natural_key as stinatural, enc.natural_key as encnatural, enc.id as encounter_id, sti.date as encounter_date, dx.dx_code_id as dx_code 
from emr_stiencounterextended sti, emr_encounter enc, emr_encounter_dx_codes dx
where 
(
--- matches exactly
sti.natural_key = enc.natural_key
-- matches when you add a B to sti key
or sti.natural_key ||'B'= enc.natural_key
-- matches when you add a B to enc key)
or sti.natural_key = enc.natural_key || 'B'
-- matches when you add a A to sti key
or sti.natural_key ||'A'= enc.natural_key
-- matches when you add an A to enc key)
or sti.natural_key = enc.natural_key || 'A'
)
and enc.date >= :start_date 
and enc.date < :end_date 
and enc.id = dx.encounter_id;


--Match dx's back up with one single encounter id again

drop table if exists temp_ssun_predxreport;
create table temp_ssun_predxreport as
select tsvd.stipatient, tse.id as encounter_id, tsvd.encounter_date, tsvd.dx_code
from temp_ssun_visits_diags tsvd, temp_ssun_encid tse
where tse.stinaturalkey= tsvd.stinatural;


drop table if exists temp_ssun_diags_report;
create table temp_ssun_diags_report as
	select stipatient as "F2_PatientID", encounter_id as "F2_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F2_Visdate", 
	case 
	when dx_code like 'icd9:091.0%' or dx_code like 'icd9:091.1%' or dx_code = 'icd9:091.2' or dx_code = 'icd10:A51.0' 
	or dx_code = 'icd10:A51.1' or dx_code = 'icd10:A51.2' then 'SY01'
      
        when dx_code = 'icd9:091.3' or dx_code = 'icd9:091.4' or dx_code='icd9:091.69' or dx_code = 'icd9:091.7' or 
	dx_code = 'icd9:091.89' or dx_code = 'icd9:091.9' or dx_code = 'icd10:A51.39' or dx_code = 'icd10:A51.49' then 'SY02'

        when dx_code = 'icd9:091.2' or dx_code = 'icd9:091.9' or dx_code = 'icd10:51.5' then 'SY03'

	when dx_code = 'icd9:096' or dx_code like 'icd9:097.0%' or dx_code='icd9:097.1' or dx_code='icd9:097.9' or dx_code = 'icd10:A52.8' 
        or dx_code = 'icd10:A52.9' or dx_code = 'icd10:A53.0' or dx_code = 'icd10:A53.0' then 'SY04'

	when dx_code like 'icd9:094.%' or dx_code = 'icd10:A52.11' or dx_code = 'icd10:A52.17' or dx_code = 'icd10:A52.13' 
        or dx_code = 'icd10:A52.14' or dx_code = 'icd10:A52.15' or dx_code = 'icd10:A52.19' or dx_code = 'icd10:A52.2' or dx_code = 			'icd10:A52.3' then 'SY05'

	when dx_code = 'icd9:97.9' or dx_code = 'icd10:A53.9' then 'SY06'

	when dx_code like 'icd9:098.%' or dx_code = 'icd9:647.1' or dx_code = 'icd9:647.11' or dx_code = 'icd9:647.12' or dx_code = 			'icd9:647.13' or dx_code = 'icd9:647.14' or dx_code = 'icd9:V02.7' or dx_code = 'icd9:647.1' or dx_code = 'icd10:A54.00'
        or dx_code = 'icd10:A54.09' or dx_code = 'icd10:A54.01' or dx_code = 'icd10:A54.02' or dx_code = 'icd10:A54.03' 
	or dx_code = 'icd10:A54.1' 
	or dx_code= 'icd10:A54.21' or dx_code = 'icd10:A54.22' or dx_code = 'icd10:A54.23' or dx_code = 'icd10:A54.24' 
	or dx_code = 'icd10:A54.29' or dx_code = 'icd10:A54.30' or dx_code = 'icd10:A54.31' or dx_code = 'icd10:A54.32'or dx_code = 			'icd10:A54.33' or dx_code = 'icd10:A54.39' or dx_code = 'icd10:A54.40' or dx_code = 'icd10:A54.41' or dx_code = 'icd10:A54.42'
	or dx_code = 'icd10:A54.43' or dx_code = 'icd10:A54.49' or dx_code = 'icd10:A54.5' or dx_code = 'icd10:A54.6' or dx_code = 			'icd10:A54.81' or dx_code = 'icd10:A54.82' or dx_code = 'icd10:A54.83' or dx_code = 'icd10:A54.84' or dx_code = 'icd10:A54.85'
        or dx_code = 'icd10:A54.86' or dx_code = 'icd10:A54.89' or dx_code = 'icd10:A54.9' or dx_code = 'icd10:A54.00' or dx_code = 			'icd10:O98.211' or dx_code = 'icd10:O98.212' or dx_code = 'icd10:O98.213' or dx_code = 	'icd10:O98.219' or dx_code = 				'icd10:O98.22' or dx_code = 'icd10:O98.23' then 'GC01'

	when dx_code = 'icd9:077.98' or dx_code ='icd9:078.88' or dx_code ='icd9:079.88' or dx_code = 'icd9:079.98' or 					dx_code='icd9:099.41' or dx_code = 'icd9:099.50' or dx_code = 'icd9:099.51' or dx_code = 'icd9:099.52' or dx_code = 'icd9:099.53'
	or dx_code = 'icd9:099.54' or dx_code = 'icd9:099.55' or dx_code = 'icd9:099.56' or dx_code = 'icd9:099.59' or dx_code = 			'icd9:483.1' or dx_code = 'icd9:V73.88' or dx_code = 'icd9:V73.98' or dx_code = 'icd10:A56.00' or dx_code = 'icd10:A56.01' 
	or dx_code = 'icd10:A56.02' or dx_code = 'icd10:A56.09' or dx_code = 'icd10:A56.11' or dx_code = 'icd10:A56.19' or dx_code = 			'icd10:A56.2' or dx_code = 'icd10:A56.3' or dx_code = 'icd10:A56.4' or dx_code = 'icd10:A56.8' or dx_code = 'icd10:A74.0'  
	or dx_code = 'icd10:A74.81' or dx_code = 'icd10:A74.89' or dx_code = 'icd10:A74.9' or dx_code = 'icd10:J16.0'  then 'CT01'

	when dx_code = 'icd9:078.10' or dx_code = 'icd9:078.11' or dx_code = 'icd9:078.19' or dx_code = 'icd9:079.4' then 'GW01'

	when dx_code = 'icd9:042' or dx_code = 'icd9:79.53' or dx_code = 'icd9:795.71' or dx_code = 'icd9:V08' or dx_code = 'icd9:V65.44' 
	or dx_code = 'icd10:B20' or dx_code = 'icd10:B97.35' or dx_code = 'icd10:R75'or dx_code = 'icd10:Z21' then 'HI01' 

	when dx_code = 'icd9:616.1' or dx_code = 'icd10:N76.0' or dx_code = 'icd10:N76.1' or dx_code = 'icd10:N76.2' or dx_code = 			'icd10:N76.3' then 'BV01'
	
	when dx_code = 'icd9:007.3' or dx_code='icd9:131.00' or dx_code='icd9:131.01' or dx_code='icd9:131.02' or dx_code='icd9:131.09'
	or dx_code='icd9:131.8' or dx_code='icd9:131.9' or dx_code = 'icd10:A59.00' or dx_code = 'icd10:A59.01' or dx_code = 				'icd10:A59.02' or dx_code = 'icd10:A59.03' or dx_code = 'icd10:A59.09' or dx_code = 'icd10:A59.8' or dx_code = 'icd10:A59.9'
	then 'TR01'

	when dx_code = 'icd9:054.10' or dx_code ='icd9:054.11' or dx_code ='icd9:054.12' or dx_code = 'icd9:054.13' or dx_code = 			'icd9:054.19' or dx_code = 'icd10:A60.00' or dx_code = 'icd10:A60.01' or dx_code = 'icd10:A60.02'  or dx_code = 'icd10:A60.03' 
	or dx_code = 'icd10:A60.04' or dx_code = 'icd10:A60.09' or dx_code = 'icd10:A60.1' or dx_code = 'icd10:A60.9' then 'GH01'
	
	when dx_code = 'icd9:99.4' or dx_code = 'icd9:99.41' or dx_code = 'icd9:99.49' or dx_code='icd10:N34.1' then 'NU01'

	when dx_code = 'icd9:616' or dx_code = 'icd10:N72'   then 'MC01'

	when dx_code like 'icd9:614.0%' or dx_code like 'icd9:614.1%' or dx_code = 'icd9:614.2' or dx_code='icd9:098.17'
	or dx_code= 'icd9:098.37' or dx_code like 'icd9:614.3%' or dx_code like 'icd9:614.4%' or dx_code = 'icd9:614.5'
	or dx_code = 'icd9:098.86' or dx_code like 'icd9:614.7%' or dx_code like 'icd9:614.8%' or dx_code = 'icd9:614.9'
	or dx_code = 'icd9:098.10'or dx_code = 'icd9:098.30' or dx_code = 'icd9:098.39' then 'PI01'

	when dx_code = 'icd9:604.9' or dx_code = 'icd10:N45.1' or dx_code = 'icd10:N45.2' or dx_code = 'icd10:N45.3' 
	or dx_code = 'icd10:N45.4' then 'EP01'

	when dx_code = 'icd9:99' or dx_code =  'icd10:A57' then 'CC01'

	when dx_code = 'icd9:99.1' or dx_code = 'icd10:A55' then 'LV01'

	when dx_code = 'icd9:99.2' or dx_code = 'icd10:A58' then 'GI01'

	when dx_code = 'icd9:112' or dx_code = 'icd9:112.1' or dx_code = 'icd9:112.2' or dx_code = 'icd10:B37.0' 
	or dx_code = 'icd10:B37.83' or dx_code = 'icd10:B37.3' or dx_code = 'icd10:B37.42' or dx_code = 'icd10:B37.049' 
	then 'CD01'
 
 	when dx_code = 'icd9:133' or dx_code = 'icd10:B86' then 'SC01'

	when dx_code like 'icd9:132.1%' or dx_code='icd9:132.2' or dx_code = 'icd10:B85.1'
	or dx_code = 'icd10:B85.2' or dx_code = 'icd10:B85.3' or dx_code = 'icd10:B85.4' then 'PD01' 

	when dx_code = 'icd9:V01.6' or dx_code = 'icd10:Z20.2' or dx_code = 'icd10:Z20.5' or dx_code = 'icd10:Z20.6'  then 'CS01'
 
	when dx_code like 'icd9:V22.0%' or dx_code like 'icd9:V22.1%' or dx_code = 'icd9:V22.2' then 'PG01'

	when dx_code = 'icd9:V65.5' or dx_code = 'icd10:Z71.1' then 'NE01'

        else 'OT01' 
       	end as "F2_DXCODE"

	from temp_ssun_predxreport;
\copy temp_ssun_diags_report to '/tmp/SSUN_diags.csv' csv header
