

-- -- SETUP STEPS
-- -- ONLY RUN ONCE


-- -- Table: kre_report.ravioli_rpt_dx_codes

-- -- DROP TABLE IF EXISTS kre_report.ravioli_rpt_dx_codes;

-- CREATE TABLE IF NOT EXISTS kre_report.ravioli_rpt_dx_codes
-- (
    -- dx_code character varying(25) COLLATE pg_catalog."default" NOT NULL,
    -- condition character varying(50) COLLATE pg_catalog."default" NOT NULL,
    -- CONSTRAINT ravioli_rpt_dx_codes_pk PRIMARY KEY (dx_code, condition)
-- );

-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:A08.2', 'adenovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'adenovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B97.0', 'adenovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.0', 'adenovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'coronavirus_non19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B97.29', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.82', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.89', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J80', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R05.1', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R48.1', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:U07.1', 'covid19');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B97.81', 'h_metapneumovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.3', 'h_metapneumovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J21.1', 'h_metapneumovirus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J09.X1', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J09.X2', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J10.00', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J10.1', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J11.00', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J11.1', 'influenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B96.0', 'm_pneumoniae');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B33.8', 'parainfluenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'parainfluenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J20.4', 'parainfluenza');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'rhino_entero_virus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'rhino_entero_virus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B97.10', 'rhino_entero_virus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J20.6', 'rhino_entero_virus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J45.902', 'rhino_entero_virus');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:B97.4', 'rsv');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.1', 'rsv');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J20.5', 'rsv');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J21.0', 'rsv');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J21.8', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R06.03', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:P81.9', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J12.9', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R50.81', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J96.90', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R05.9', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J96.91', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:J96.92', 'ravioli_other');
-- INSERT INTO kre_report.ravioli_rpt_dx_codes VALUES('icd10:R57.9', 'ravioli_other');


-- -- Table: kre_report.ravioli_rpt_full_output

-- -- DROP TABLE IF EXISTS kre_report.ravioli_rpt_full_output;

-- CREATE TABLE IF NOT EXISTS kre_report.ravioli_rpt_full_output
-- (
    -- week_start_date date NOT NULL,
    -- total_encs bigint,
    -- total_ravioli_encs bigint,
    -- adenovirus_encs bigint,
    -- c_pneumoniae_encs bigint,
    -- coronavirus_non19_encs bigint,
    -- covid19_encs bigint,
    -- h_metapneumovirus_encs bigint,
    -- influenza_encs bigint,
    -- m_pneumoniae_encs bigint,
    -- parainfluenza_encs bigint,
    -- parapertussis_encs bigint,
    -- rhino_entero_encs bigint,
    -- rsv_encs bigint,
    -- ravioli_other_encs bigint,
    -- CONSTRAINT ravioli_rpt_full_output_pkey PRIMARY KEY (week_start_date)
-- )
-- WITH (
    -- OIDS = FALSE
-- );


---------------------------------------------------------------------------------------------------------------------



DROP TABLE IF EXISTS kre_report.ravioli_rpt_pos_tests;
create table kre_report.ravioli_rpt_pos_tests AS
select DISTINCT patient_id, date as index_test_date, 
CASE WHEN name = 'lx:rsv:positive' THEN 'rsv'
     WHEN name = 'lx:parainfluenza:positive' THEN 'parainfluenza'
	 WHEN name = 'lx:adenovirus:positive' THEN 'adenovirus'
	 WHEN name = 'lx:rhino_entero_virus:positive' THEN 'rhino_entero_virus'
	 WHEN name = 'lx:coronavirus_non19:positive' THEN 'coronavirus_non19'
	 WHEN name in ('lx:influenza:positive', 'lx:influenza_culture:positive', 'lx:rapid_flu:positive') THEN 'influenza'
	 WHEN name = 'lx:h_metapneumovirus:positive' THEN 'h_metapneumovirus'
	 WHEN name in ('lx:c_pneumoniae_igm:positive', 'lx:c_pneumoniae_pcr:positive') THEN 'c_pneumoniae'
	 WHEN name in ('lx:m_pneumoniae_igm:positive', 'lx:m_pneumoniae_pcr:positive') THEN 'm_pneumoniae'
	 WHEN name in ('lx:parapertussis:positive') THEN 'parapertussis'
	 WHEN name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive') THEN 'covid19'
ELSE name END as condition
from hef_event
where name in ('lx:rsv:positive',
			   'lx:parainfluenza:positive',
			   'lx:adenovirus:positive',
			   'lx:rhino_entero_virus:positive',
			   'lx:coronavirus_non19:positive',
			   'lx:influenza:positive', 
			   'lx:influenza_culture:positive', 
			   'lx:rapid_flu:positive',
			   'lx:h_metapneumovirus:positive',
			   'lx:c_pneumoniae_igm:positive', 
			   'lx:c_pneumoniae_pcr:positive',
			   'lx:parapertussis:positive',
			   'lx:m_pneumoniae_igm:positive', 
			   'lx:m_pneumoniae_pcr:positive',
			   'lx:covid19_pcr:positive', 
			   'lx:covid19_ag:positive'
			   )
and date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;

-- need to unique by condition so may need to eliminate the dates from tests and encounters 
-- when doing the counts

drop table if exists kre_report.ravioli_rpt_wk_all_dx;
create table kre_report.ravioli_rpt_wk_all_dx AS
select DISTINCT T2.patient_id, T2.date as encounter_date, T3.dx_code, T3.condition
from emr_encounter_dx_codes T1
INNER JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
INNER JOIN kre_report.ravioli_rpt_dx_codes T3 ON (T1.dx_code_id = T3.dx_code)
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and T1.dx_code_id != 'icd9:799.9';

DROP TABLE IF EXISTS kre_report.ravioli_rpt_pathogen_spec;
CREATE TABLE kre_report.ravioli_rpt_pathogen_spec AS
select patient_id, condition
from kre_report.ravioli_rpt_pos_tests
UNION
select patient_id, condition
from kre_report.ravioli_rpt_wk_all_dx
where condition != 'ravioli_other'
ORDER BY patient_id;

DROP TABLE IF EXISTS kre_report.ravioli_rpt_meas_fever;
CREATE TABLE kre_report.ravioli_rpt_meas_fever AS
select DISTINCT patient_id, date as encounter_date, 'MEASURED_FEVER' as dx_code, 'ravioli_other' as condition
from hef_event
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and name = 'enc:fever';

DROP TABLE IF EXISTS kre_report.ravioli_rpt_wk_all_dx_and_fever;
CREATE TABLE kre_report.ravioli_rpt_wk_all_dx_and_fever AS
SELECT * from kre_report.ravioli_rpt_wk_all_dx
UNION
SELECT * from kre_report.ravioli_rpt_meas_fever;

-- don't count pats in other if already in pathogen specific
DROP TABLE IF EXISTS kre_report.ravioli_rpt_other_ravioli;
CREATE TABLE kre_report.ravioli_rpt_other_ravioli AS
select DISTINCT patient_id, condition
from kre_report.ravioli_rpt_wk_all_dx_and_fever
where condition = 'ravioli_other'
and patient_id not in (select patient_id from kre_report.ravioli_rpt_pathogen_spec);

DROP TABLE IF EXISTS kre_report.ravioli_rpt_pathogen_other_combo;
CREATE TABLE kre_report.ravioli_rpt_pathogen_other_combo AS
SELECT patient_id, condition 
FROM kre_report.ravioli_rpt_pathogen_spec
UNION
SELECT patient_id, condition
FROM kre_report.ravioli_rpt_other_ravioli;


DROP TABLE IF EXISTS kre_report.ravioli_rpt_pathogen_other_combo_counts;
CREATE TABLE kre_report.ravioli_rpt_pathogen_other_combo_counts AS
select count(distinct(patient_id)) enc_count, condition
from kre_report.ravioli_rpt_pathogen_other_combo
group by condition;


DROP TABLE IF EXISTS kre_report.ravioli_rpt_total_encs;
CREATE TABLE kre_report.ravioli_rpt_total_encs AS
SELECT count(distinct(patient_id)) total_encounters
FROM gen_pop_tools.clin_enc T1
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;



DROP TABLE IF EXISTS kre_report.ravioli_rpt_single_week_output;
CREATE TABLE kre_report.ravioli_rpt_single_week_output AS
SELECT 
week_start_date,
max(T4.total_encounters) as total_encs,
count(distinct(T2.patient_id)) as total_ravioli_encs,
max(case when T1.condition = 'adenovirus' then enc_count else 0 end) as adenovirus_encs,
max(case when T1.condition = 'c_pneumoniae' then enc_count else 0 end) as c_pneumoniae_encs,
max(case when T1.condition = 'coronavirus_non19' then enc_count else 0 end) as coronavirus_non19_encs,
max(case when T1.condition = 'covid19' then enc_count else 0 end) as covid19_encs,
max(case when T1.condition = 'h_metapneumovirus' then enc_count else 0 end) as h_metapneumovirus_encs,
max(case when T1.condition = 'influenza' then enc_count else 0 end) as influenza_encs,
max(case when T1.condition = 'm_pneumoniae' then enc_count else 0 end) as m_pneumoniae_encs,
max(case when T1.condition = 'parainfluenza' then enc_count else 0 end) as parainfluenza_encs,
max(case when T1.condition = 'parapertussis' then enc_count else 0 end) as parapertussis_encs,
max(case when T1.condition = 'rhino_entero_virus' then enc_count else 0 end) as rhino_entero_encs,
max(case when T1.condition = 'rsv' then enc_count else 0 end) as rsv_encs,
max(case when T1.condition = 'ravioli_other' then enc_count else 0 end) as ravioli_other_encs
from  kre_report.ravioli_rpt_pathogen_other_combo_counts T1
LEFT JOIN kre_report.ravioli_rpt_pathogen_other_combo T2 on (1=1)
JOIN (select (:week_end_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
LEFT JOIN kre_report.ravioli_rpt_total_encs T4 on (1=1)
GROUP BY week_start_date;


-- Clear out any results for this week from the full table
DELETE FROM kre_report.ravioli_rpt_full_output where week_start_date = (select week_start_date from kre_report.ravioli_rpt_single_week_output);

-- Populate the table with the details from this week's run
INSERT INTO kre_report.ravioli_rpt_full_output
SELECT * from kre_report.ravioli_rpt_single_week_output;






