1. Create the directories

sudo mkdir -p /srv/esp/data/ravioli_reports
sudo mkdir -p /srv/esp/data/ravioli_reports/historical

2. Create the tables & do the inserts
* public.ravioli_rpt_dx_codes
* public.ravioli_rpt_full_output_w_ageagroup
( See ravioli-db-table-setup.sql for the SQL to run)

3. Prepare the shell script for normal weekly run
cp ravioli-weekly-report-run-stratify.sh.template ravioli-weekly-report-run-stratify.sh
Fix up the site specific variables
chmod 755 ravioli-weekly-report-run-stratify.sh

4. Run the weekly report (NOT HISTORICAL)
nohup ./ravioli-weekly-report-run-stratify.sh &

5. Prepare the shell script for historical run
cp ravioli-weekly-report-run-stratify-historical.sh.template ravioli-weekly-report-run-stratify-historical.sh
Fix up the site specific variables
Set the date range that you want
chmod 755 ravioli-weekly-report-run-stratify-historical.sh




