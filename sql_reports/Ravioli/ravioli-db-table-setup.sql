-- SETUP STEPS
-- ONLY RUN ONCE


-- Table: public.ravioli_rpt_dx_codes

-- DROP TABLE IF EXISTS public.ravioli_rpt_dx_codes;

CREATE TABLE IF NOT EXISTS public.ravioli_rpt_dx_codes
(
    dx_code character varying(25) COLLATE pg_catalog."default" NOT NULL,
    condition character varying(50) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ravioli_rpt_dx_codes_pk PRIMARY KEY (dx_code, condition)
);

INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:A08.2', 'adenovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'adenovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.0', 'adenovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.0', 'adenovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'coronavirus_non19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.29', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.82', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.89', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J80', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R05.1', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R48.1', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:U07.1', 'covid19');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.81', 'h_metapneumovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.3', 'h_metapneumovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.1', 'h_metapneumovirus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J09.X1', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J09.X2', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J10.00', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J10.1', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J11.00', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J11.1', 'influenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B33.8', 'parainfluenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'parainfluenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.4', 'parainfluenza');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'rhino_entero_virus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'rhino_entero_virus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.10', 'rhino_entero_virus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.6', 'rhino_entero_virus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J45.902', 'rhino_entero_virus');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.4', 'rsv');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.1', 'rsv');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.5', 'rsv');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.0', 'rsv');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.8', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R06.03', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:P81.9', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.9', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R50.81', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.90', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R05.9', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.91', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.92', 'ravioli_other');
INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R57.9', 'ravioli_other');



-- Table: public.ravioli_rpt_full_output_w_ageagroup

-- DROP TABLE IF EXISTS public.ravioli_rpt_full_output_w_ageagroup;

CREATE TABLE IF NOT EXISTS public.ravioli_rpt_full_output_w_ageagroup
(
    week_start_date date NOT NULL,
    age_group character varying(20) COLLATE pg_catalog."default" NOT NULL,
	clin_enc_pats_l2yr bigint,
	clin_enc_pats_week bigint,
	ravioli_enc_pats_week bigint,
    adenovirus_enc_pats bigint,
    coronavirus_non19_enc_pats bigint,
    covid19_enc_pats bigint,
    h_metapneumovirus_enc_pats bigint,
    influenza_enc_pats bigint,
    parainfluenza_enc_pats bigint,
    rhino_entero_enc_pats bigint,
    rsv_enc_pats bigint,
    ravioli_other_enc_pats bigint,
	ili_cases bigint,
    CONSTRAINT ravioli_rpt_full_output_w_ageagro_week_start_date_age_group_key UNIQUE (week_start_date, age_group)
)