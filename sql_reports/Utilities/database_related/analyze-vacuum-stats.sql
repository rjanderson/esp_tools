select schemaname, relname,last_vacuum, last_autovacuum, last_analyze, last_autoanalyze, n_mod_since_analyze, n_dead_tup 
from pg_stat_user_tables
order by n_dead_tup desc;