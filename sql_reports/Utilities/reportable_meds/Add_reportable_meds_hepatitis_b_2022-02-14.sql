-- conf_reportablemedication
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'adefovir', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'entecavir', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'lamivudine', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'tenofovir', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'interferon alfa-2b', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'peginterferon alfa-2a', null, 'hepatitis_b') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'telbivudine', null, 'hepatitis_b') ON CONFLICT DO NOTHING;

-- static drug_synonym
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Adefovir', 'Adefovir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Adefovir', 'Hepsera', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Entecavir', 'Entecavir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Entecavir', 'Baraclude', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Lamivudine', 'Lamivudine', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Lamivudine', 'Epivir', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Lamivudine', 'Zeffix', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Lamivudine', 'Heptodin', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tenofovir', 'Tenofovir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tenofovir', 'Vemlidy', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tenofovir', 'Viread', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2b', 'Interferon alfa-2b', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2b', 'Intron A', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2a', 'Peginterferon alfa-2a', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2a', 'Pegasys', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Telbivudine', 'Telbivudine', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Telbivudine', 'Tyzeka', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Telbivudine', 'Sebivo', null) ON CONFLICT DO NOTHING;