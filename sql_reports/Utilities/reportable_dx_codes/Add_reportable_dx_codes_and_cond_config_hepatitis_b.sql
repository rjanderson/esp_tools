--this will be added to the .3.5.2 release in a migration

-- Hep B condition configs 
insert into conf_conditionconfig (name, initial_status, lab_days_before, lab_days_after, dx_code_days_before, dx_code_days_after, med_days_before, med_days_after, ext_var_days_before, ext_var_days_after, reinfection_days) SELECT 'hepatitis_b', 'AR', 14, 10000, 14, 30, 0, 10000, 0, 0, 0 WHERE NOT EXISTS (select * from conf_conditionconfig where name = 'hepatitis_b');

-- To update existing sites
UPDATE conf_conditionconfig 
	SET lab_days_before = 14,
	lab_days_after = 10000,
	dx_code_days_before = 14,
	dx_code_days_after = 30,
	med_days_before = 0,
	med_days_after = 10000
WHERE name = 'hepatitis_b';


--HEP B all ICD 10 codes
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.0' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.0');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.1' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.1');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.2' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.2');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.3' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.3');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.8' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.8');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.9' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.9');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.10' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.10');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.11' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.11');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.12' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.12');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.13' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.13');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.30' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.30');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.31' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.31');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.32' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.32');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.33' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.33');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.81' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.81');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.811' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.811');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.812' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.812');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.813' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.813');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.814' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.814');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.815' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.815');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.816' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.816');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.817' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.817');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.819' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.819');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.821' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.821');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.823' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.823');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.825' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.825');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.83' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.83');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.84' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.84');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R10.82' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R10.82');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R11.0' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R11.0');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R11.10' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R11.10');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R11.11' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R11.11');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R11.2' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R11.2');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R17' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R17');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R19.7' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R19.7');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:R50.9' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:R50.9');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:B18.0' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:B18.0');
insert into conf_reportabledx_code (condition_id, dx_code_id) SELECT 'hepatitis_b', 'icd10:B18.1' WHERE NOT EXISTS (SELECT * from conf_reportabledx_code WHERE condition_id = 'hepatitis_b' and dx_code_id = 'icd10:B18.1');

