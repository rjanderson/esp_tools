drop table if exists temp_unmapped;

--use this query for DPH reportable conditions only
-- gonorrhea, chlamydia, syphilis, lyme, tuberculosis, hepatitis (all flavors)
-- anaplasmosis, babesiosis, hiv
-- platelets, white blood cells, genotype tests, red blood cells, alt, ast, cd4, creatinine
create table temp_unmapped as
select e.count, e.native_name, e.native_code, procedure_name
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
-- UNCOMMENT THIS FOR WIDE RANGE OF STRINGS
--and e.native_name ~* '(alt|spgt|ast|aminotrans|sgot|hep|anaplasma|phago|hge|bili|tbil|hb|hbv|cd4|chlam|trac|cr|creat|fta|gc|gon|hcv|genotype|hiv|immuno|pcr|lyme|borr|burg|plt|rbc|red bl|rpr|rapid plasma reagin|tb|tuber|afb|cult|igra|feron|t-spot|mycob|tp|trep|syph|pallidium|particle|vdrl|wbc|white bl|qft|sgpt|platelet|helper|hematocrit|hct)'
-- THIS LINE IS FOR STANDARD SEARCH STRINGS ONLY. SEE LINE ABOVE TO EXPAND IF DESIRED.
and e.native_name ~* '(chlam|trac|gon|gc)'
group by e.count, e.native_name, e.native_code, procedure_name;


-- **NOTE**
-- use this query for a wider range of strings
-- needs to be reviewed and updated
--
-- drop table if exists temp_unmapped;
-- create table temp_unmapped as
-- select count(*), e.native_name, e.native_code, procedure_name
-- from emr_labtestconcordance e,
-- emr_labresult l
-- where e.native_code = l.native_code
-- and e.native_code not in (select native_code from conf_labtestmap)
-- and e.native_code not in (select native_code from conf_ignoredcode)
-- and e.native_name ~* '(a1c|alk|alp|alt|aminotrans|ast|bili|bilirubin|borr|burg|ca|cd4|chlam|chol|ck|cpk|cr|creat|diab|eo|fta|gad65|gc|
-- genotype|gluc|glyc|gon|haemog|hb|hbv|hcv|hdl|hemog|hep|hg|hiv|ica512|immuno|insulin|islet|k|kinase|ldl|leuc|leuk|lipoprotein|lyme|
-- lymph|na|neut|ogtt|pallidum|pcr|peptide|pert|plastin|plat|plt|pmn|poly|pot|ptt|rapid plasma reagin|rpr|serol|sgot|sgpt|sod|syph|
-- tbil|thromboc|tp|trac|trep|trig|vdrl|wbc|white bl)'
-- group by e.native_name, e.native_code, procedure_name;

DROP TABLE IF EXISTS temp_unmapped_with_result_strings;
CREATE TABLE temp_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_unmapped S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name 
  from emr_labresult T1,
  temp_unmapped T2
  where T1.native_code = T2.native_code 
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;


\COPY temp_unmapped_with_result_strings  TO '/tmp/unmapped_labs_with_result_strings.csv' DELIMITER ',' CSV HEADER;


--drop table temp_unmapped;
--drop table temp_unmapped_with_result_strings;