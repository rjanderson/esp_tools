-- How to run this report"
-- 		psql -d esp -q -f /srv/esp/esp_tools/sql_reports/Utilities/extract-population-validation.sql -o /tmp/SITE-NAME-date-extract-validation.txt
-- 		For example:
-- 		psql -d esp -q -f /srv/esp/esp_tools/sql_reports/Utilities/extract-population-validation.sql -o /tmp/BMC-2021-10-06-extract-validation.txt

-- NOTE: When reviewing results turn off word wrap in Notepad++ or Notepad


DROP TABLE IF EXISTS kre_report.val_emr_patient_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_patient_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_races;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_genders;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_ethnicity;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_vital_status;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_gender_identity;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sex_orientation;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_birth_sex;
DROP TABLE IF EXISTS kre_report.val_emr_provider_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_provider_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_encounter_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_encounter_dx_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_encounter_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_encounter_counts_by_year;
DROP TABLE IF EXISTS kre_report.val_emr_rx_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_rx_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_rx_counts_by_year;
DROP TABLE IF EXISTS kre_report.val_emr_labres_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_labres_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_labres_counts_by_year;
DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_counts_by_year;
DROP TABLE IF EXISTS kre_report.val_emr_sochist_col_pop_counts;
DROP TABLE IF EXISTS kre_report.val_emr_sochist_col_pop_percents;
DROP TABLE IF EXISTS kre_report.val_emr_socialhistory_counts_by_year;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_tobacco_use;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_alcohol_use;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sex_partner_gender;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_ill_drug_use;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sexually_active;
DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_birth_control_method;

--
-- PATIENTS
--

DROP TABLE IF EXISTS kre_report.val_emr_patient_col_pop_counts;
CREATE TABLE kre_report.val_emr_patient_col_pop_counts AS
SELECT 
count(pat.id) as total_patients,
count(case when mrn is null or mrn = '' then 1 end) as mrn_is_empty,
count(case when date_of_birth is null or date_of_birth <= '1910-01-01' then 1 end) as date_of_birth_is_empty_or_invalid,
count(case when gender is null or gender = '' then 1 end) as gender_is_empty,
count(case when race is null or race = '' then 1 end) as race_is_empty,
count(case when ethnicity is null or ethnicity = '' then 1 end) as ethnicity_is_empty,
count(case when home_language is null or home_language = '' then 1 end) as home_language_is_empty,
count(case when ssn is null or ssn = '' then 1 end) as ssn_is_empty,
count(case when pcp.last_name = 'UNKNOWN' or pcp.last_name is null or pcp.last_name = '' then 1 end) pcp_unknown_or_no_last_name,
count(case when marital_stat is null or marital_stat = '' then 1 end) as marital_stat_is_empty,
count(case when date_of_death is null or date_of_death <= '1910-01-01' then 1 end) as date_of_death_is_empty_or_invalid,
count(case when pat.center_id is null or pat.center_id = '' then 1 end) as center_id_is_empty,
count(case when income_level is null or income_level = '' then 1 end) as income_level_is_empty,
count(case when housing_status is null or housing_status = '' then 1 end) as housing_status_is_empty,
count(case when insurance_status is null or insurance_status = '' then 1 end) as insurance_status_is_empty,
count(case when birth_country is null or birth_country = '' then 1 end) as birth_country_is_empty,
count(case when vital_status is null or vital_status = '' then 1 end) as vital_status_is_empty,
count(case when next_prov.last_name = 'UNKNOWN' or next_prov.last_name is null or next_prov.last_name = '' then 1 end) next_provider_unknown_or_no_last_name,
count(case when next_appt_date is null or next_appt_date <= '1910-01-01' then 1 end) as next_appt_date_is_empty_or_invalid,
count(case when next_fac_prov.dept = 'UNKNOWN' or next_fac_prov.dept is null or next_fac_prov.dept = '' then 1 end) next_facility_prov_dept_unknown_or_no_dept,
count(case when gender_identity is null or gender_identity = '' then 1 end) as gender_identity_is_empty,
count(case when sex_orientation is null or sex_orientation = '' then 1 end) as sex_orientation_is_empty,
count(case when birth_sex is null or birth_sex = '' then 1 end) as birth_sex_is_empty
FROM emr_patient pat
LEFT join emr_provider pcp on (pat.pcp_id = pcp.id)
LEFT join emr_provider next_prov on (pat.next_appt_provider_id = next_prov.id)
LEFT join emr_provider next_fac_prov on (pat.next_appt_fac_provider_id = next_fac_prov.id);

DROP TABLE IF EXISTS kre_report.val_emr_patient_col_pop_percents;
CREATE TABLE kre_report.val_emr_patient_col_pop_percents AS
SELECT total_patients,
round((mrn_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS mrn_is_empty_pct,
round((date_of_birth_is_empty_or_invalid * 100)::numeric / NULLIF(total_patients, 0), 2) AS date_of_birth_is_empty_or_invalid_pct,
round((gender_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS gender_is_empty_pct,
round((race_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS race_is_empty_pct,
round((ethnicity_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS ethnicity_is_empty_pct,
round((home_language_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS home_language_is_empty_pct,
round((ssn_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS ssn_is_empty_pct,
round((pcp_unknown_or_no_last_name * 100)::numeric / NULLIF(total_patients, 0), 2) AS pcp_unknown_or_no_last_name_pct,
round((marital_stat_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS marital_stat_is_empty_pct,
round((date_of_death_is_empty_or_invalid * 100)::numeric / NULLIF(total_patients, 0), 2) AS date_of_death_is_empty_or_invalid_pct,
round((center_id_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS center_id_is_empty_pct,
round((income_level_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS income_level_is_empty_pct,
round((housing_status_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS housing_status_is_empty_pct,
round((insurance_status_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS insurance_status_is_empty_pct,
round((birth_country_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS birth_country_is_empty_pct,
round((vital_status_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS vital_status_is_empty_pct,
round((next_provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_patients, 0), 2) AS next_provider_unknown_or_no_last_name_pct,
round((next_appt_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_patients, 0), 2) AS next_appt_date_is_empty_or_invalid_pct,
round((next_facility_prov_dept_unknown_or_no_dept * 100)::numeric / NULLIF(total_patients, 0), 2) AS next_facility_prov_dept_unknown_or_no_dept_pct,
round((gender_identity_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS gender_identity_is_empty_pct,
round((sex_orientation_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS sex_orientation_is_empty_pct,
round((birth_sex_is_empty * 100)::numeric / NULLIF(total_patients, 0), 2) AS birth_sex_is_empty_pct
FROM kre_report.val_emr_patient_col_pop_counts;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_races;
CREATE TABLE kre_report.val_emr_patient_non_std_races AS
SELECT count(*) non_std_races_count, race
FROM emr_patient
where upper(race) not in (
'ALASKAN',
'AM INDIAN',
'AMERICAN INDIAN/ALASKAN NATIVE',
'ASIAN',
'BLACK',
'CAUCASIAN',
'DECLINED/REFUSED',
'DECLINED TO ANSWER',
'HISPANIC',
'INDIAN',
'MULTIRACIAL',
'NAT AMERICAN',
'NATIVE HAWAI',
'OTHER',
'PAC ISLANDER',
'PACIFIC ISLANDER/HAWAIIAN',
'PATIENT DECL',
'UNKNOWN',
'WHITE',
'A',
'B',
'I',
'O',
'P'
'W',
'')
and race is not null
group by race;

DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_genders;
CREATE TABLE kre_report.val_emr_patient_non_std_genders AS
SELECT count(*) non_std_genders_count, gender
FROM emr_patient
where upper(gender) not in (
'F',
'FEMALE',
'M',
'MALE',
'T',
'U',
'UNKNOWN',
'')
and gender is not null
group by gender;

DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_ethnicity;
CREATE TABLE kre_report.val_emr_patient_non_std_ethnicity AS
SELECT count(*) non_std_ethnicity_count, ethnicity
FROM emr_patient
where upper(ethnicity) not in (
'NOT HISPANIC OR LATINO',
'NH',
'NON HISPANIC',
'NH',
'HISPANIC OR LATINO',
'H',
'HISPANIC',
'NOT REPORTED',
'U',
'OTHER OR UNDETERMINED',
'UNKNOWN',
'')
and ethnicity is not null
group by ethnicity;

DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_vital_status;
CREATE TABLE kre_report.val_emr_patient_non_std_vital_status AS
SELECT count(*) non_std_vital_status_count, vital_status
FROM emr_patient
WHERE upper(vital_status) not in (
'DECEASED', 
'ALIVE',
''
)
and vital_status is not null
group by vital_status;

DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_gender_identity;
CREATE TABLE kre_report.val_emr_patient_non_std_gender_identity AS
SELECT count(*) non_std_gender_identity_count, gender_identity
FROM emr_patient
WHERE upper(gender_identity) not in (
'FEMALE',
'MALE',
'TRANSGENDER FEMALE / MALE-TO-FEMALE',
'TRANSGENDER MALE / FEMALE-TO-MALE',
'OTHER',
'CHOOSE NOT TO DISCLOSE',
'NON-BINARY',
''
)
and gender_identity is not null
group by gender_identity;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sex_orientation;
CREATE TABLE kre_report.val_emr_patient_non_std_sex_orientation AS
SELECT count(*) non_std_sex_orientation_count, sex_orientation
FROM emr_patient
WHERE upper(sex_orientation) not in (
'STRAIGHT (NOT LESBIAN OR GAY)',
'BISEXUAL',
'SOMETHING ELSE',
'DON''T KNOW',
'CHOOSE NOT TO DISCLOSE',
'GAY',
'LESBIAN',
'ASEXUAL',
'PANSEXUAL',
'LESBIAN OR GAY',
''
)
and sex_orientation is not null
group by sex_orientation;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_birth_sex;
CREATE TABLE kre_report.val_emr_patient_non_std_birth_sex AS
SELECT count(*) non_std_birth_sex_count, birth_sex
FROM emr_patient
WHERE upper(birth_sex) not in (
'FEMALE',
'MALE',
'UNKNOWN',
'NOT RECORDED ON BIRTH CERTIFICATE',
'CHOOSE NOT TO DISCLOSE',
'UNCERTAIN',
''
)
and birth_sex is not null
group by birth_sex;

--
-- PROVIDERS
--

DROP TABLE IF EXISTS kre_report.val_emr_provider_col_pop_counts;
CREATE TABLE kre_report.val_emr_provider_col_pop_counts AS
SELECT 
count(id) as total_providers,
count(case when provider_type = 1 then 1 end) as total_people_providers,
count(case when provider_type = 2 then 1 end) as total_facility_providers,
count(case when provider_type is null or provider_type not in (1, 2) then 1 end) as provider_type_empty,
count(case when provider_type = 1 and (npi is null or npi = '') then 1 end) as people_provider_npi_empty,
count(case when provider_type = 2 and (dept is null or dept = '') then 1 end) as facilty_provider_dept_empty
FROM emr_provider prov;

DROP TABLE IF EXISTS kre_report.val_emr_provider_col_pop_percents;
CREATE TABLE kre_report.val_emr_provider_col_pop_percents AS
SELECT total_providers,
total_people_providers,
round((total_people_providers * 100)::numeric / NULLIF(total_providers, 0), 2) AS total_people_providers_pct,
total_facility_providers,
round((total_facility_providers * 100)::numeric / NULLIF(total_providers, 0), 2) AS total_facility_providers_pct,
round((provider_type_empty * 100)::numeric / NULLIF(total_providers, 0), 2) AS provider_type_empty_pct,
round((people_provider_npi_empty * 100)::numeric / NULLIF(total_people_providers, 0), 2) AS people_provider_npi_empty_pct,
round((facilty_provider_dept_empty * 100)::numeric / NULLIF(total_facility_providers, 0), 2) AS facilty_provider_dept_empty_pct
FROM kre_report.val_emr_provider_col_pop_counts;


--
-- ENCOUNTERS
--

DROP TABLE IF EXISTS kre_report.val_emr_encounter_col_pop_counts;
CREATE TABLE kre_report.val_emr_encounter_col_pop_counts AS
SELECT
count(enc.id) as total_encounters_l2y,
count(case when prov.last_name = 'UNKNOWN' or prov.last_name is null or prov.last_name = '' then 1 end) provider_unknown_or_no_last_name,
count(case when site_name is null or site_name = '' then 1 end) site_name_empty,
count(case when raw_encounter_type is null or raw_encounter_type = '' then 1 end) raw_encounter_type_empty,
count(case when edd is not null then 1 end) encs_with_edd,
count(case when pregnant is true then 1 end) pregnant_encs,
count(case when raw_temperature is null or raw_temperature = '' then 1 end) temperature_empty,
count(case when raw_weight is null or raw_weight = '' then 1 end) raw_weight_empty,
count(case when raw_weight !~ '^\d+ lb$'
      and raw_weight !~ '^\d+ lb \d+.?\d o$'
      and raw_weight !~ '^\d+ lb \d+.?\d? oz$'
      and raw_weight !~ '^\d+.? lb \d+.?\d+? oz$'
      and raw_weight !~ '^\d+ lb \d+.?\d$'
      -- bad one just to see if there are others
      and raw_weight !~ '^\d+.?\d? lb \d+.?\d+? oz$'
      and raw_weight is not null then 1 end ) as raw_weight_wrong_format,
count(case when raw_height is null or raw_height = '' then 1 end) raw_height_empty,
count(case when raw_height !~ '^\d'' \d+"?$' 
      and raw_height !~ '^\d'' \d+.?\d+?"?$' 
      and raw_height !~ '^\d'' .\d+"?$' 
      and raw_height is not null then 1 end) as raw_height_wrong_format,
count(case when raw_bp_systolic is null or raw_bp_systolic = '' then 1 end) raw_bp_systolic_empty,
count(case when raw_bp_diastolic is null or raw_bp_diastolic = '' then 1 end) raw_bp_diastolic_empty,
count(case when raw_o2_sat is null or raw_o2_sat = '' then 1 end) raw_o2_sat_empty,
count(case when raw_peak_flow is null or raw_peak_flow = '' then 1 end) raw_peak_flow_empty,
count(case when raw_bmi is null or raw_bmi = '' then 1 end) raw_bmi_empty,
count(case when hosp_admit_dt is null or hosp_admit_dt <= '1910-01-01' then 1 end) as hosp_admit_dt_is_empty_or_invalid,
count(case when hosp_dschrg_dt is null or hosp_dschrg_dt <= '1910-01-01' then 1 end) as hosp_dschrg_dt_is_empty_or_invalid
FROM emr_encounter enc
LEFT JOIN emr_provider prov on (enc.provider_id = prov.id)
WHERE date >= now() - INTERVAL '2 years';


-- Dive in to dx code stats
DROP TABLE IF EXISTS kre_report.val_emr_encounter_dx_pop_counts;
CREATE TABLE kre_report.val_emr_encounter_dx_pop_counts AS
SELECT sum(encs_with_dx_codes) as encs_with_dx_codes,
dx_code_wrong_format
FROM (
	SELECT
	max(case when dx_code_id is not null and dx_code_id != 'icd9:799.9' then 1 end) as encs_with_dx_codes,
	count(case when dx_code_id !~ '^icd10:*.*$' 
		  and dx_code_id !~ '^icd9:*.*$' then 1 end) as dx_code_wrong_format
	FROM emr_encounter enc
	LEFT JOIN emr_encounter_dx_codes dx on (enc.id = dx.encounter_id)
	WHERE date >= now() - INTERVAL '2 years'
	GROUP BY enc.id) T1
GROUP BY dx_code_wrong_format;



DROP TABLE IF EXISTS kre_report.val_emr_encounter_col_pop_percents;
CREATE TABLE kre_report.val_emr_encounter_col_pop_percents AS
SELECT
total_encounters_l2y,
round((provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS provider_unknown_or_no_last_name_pct,
round((site_name_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS site_name_empty_pct,
round((raw_encounter_type_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_encounter_type_empty_pct,
round((encs_with_edd * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS encs_with_edd_pct,
round((pregnant_encs * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS pregnant_encs_pct,
round((temperature_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS temperature_empty_pct,
round((raw_weight_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_weight_empty_pct,
round((raw_weight_wrong_format * 100)::numeric / NULLIF((total_encounters_l2y - raw_weight_empty), 0), 2) AS raw_weight_wrong_format_pct,
round((raw_height_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_height_empty_pct,
round((raw_height_wrong_format * 100)::numeric / NULLIF((total_encounters_l2y - raw_height_empty), 0), 2) AS raw_height_wrong_format_pct,
round((raw_bp_systolic_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_bp_systolic_empty_pct,
round((raw_bp_diastolic_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_bp_diastolic_empty_pct,
round((raw_o2_sat_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_o2_sat_empty_pct,
round((raw_peak_flow_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_peak_flow_empty_pct,
round((raw_bmi_empty * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS raw_bmi_empty_pct,
round((hosp_admit_dt_is_empty_or_invalid * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS hosp_admit_dt_is_empty_or_invalid_pct,
round((hosp_dschrg_dt_is_empty_or_invalid * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS hosp_dschrg_dt_is_empty_or_invalid_pct,
round((encs_with_dx_codes * 100)::numeric / NULLIF(total_encounters_l2y, 0), 2) AS encs_with_dx_codes_pct,
round((dx_code_wrong_format * 100)::numeric / NULLIF(encs_with_dx_codes, 0), 2) AS dx_code_wrong_format_pct
FROM kre_report.val_emr_encounter_col_pop_counts
LEFT JOIN kre_report.val_emr_encounter_dx_pop_counts ON (1=1) ;


-- Enc counts by year
DROP TABLE IF EXISTS kre_report.val_emr_encounter_counts_by_year;
CREATE TABLE kre_report.val_emr_encounter_counts_by_year AS 
SELECT count(*) enc_count, extract(year from date) enc_year
from emr_encounter
group by enc_year
order by enc_year;


/* 

outliers (need to identify ranges and add to validation)
========
raw_temperature
weight
height
systolic
diastolic
bmi

ask about limiting of admit and discharge to inpatient only */



--
-- PRESCRIPTIONS
--

DROP TABLE IF EXISTS kre_report.val_emr_rx_col_pop_counts;
CREATE TABLE kre_report.val_emr_rx_col_pop_counts AS
SELECT
count(rx.id) as total_rx_l2y,
count(case when prov.last_name = 'UNKNOWN' or prov.last_name is null or prov.last_name = '' then 1 end) provider_unknown_or_no_last_name,
count(case when status is null or status = '' then 1 end) status_empty,
count(case when directions is null or directions = '' then 1 end) directions_empty,
count(case when code is null or code = '' then 1 end) code_empty,
count(case when name is null or name = '' then 1 end) name_empty,
count(case when quantity is null or quantity = '' then 1 end) quantity_empty,
count(case when refills is null or refills = '' then 1 end) refills_empty,
count(case when start_date is null or start_date <= '1910-01-01' then 1 end) as start_date_is_empty_or_invalid,
count(case when end_date is null or end_date <= '1910-01-01' then 1 end) as end_date_is_empty_or_invalid,
count(case when route is null or route = '' then 1 end) route_empty,
count(case when dose is null or dose = '' then 1 end) dose_empty,
count(case when man_prov.last_name = 'UNKNOWN' or man_prov.last_name is null or man_prov.last_name = '' then 1 end) managing_provider_unknown_or_no_last_name,
count(case when fac_prov.dept = 'UNKNOWN' or fac_prov.dept is null or fac_prov.dept = '' then 1 end) facility_prov_dept_unknown_or_no_dept
FROM emr_prescription rx
LEFT JOIN emr_provider prov on (rx.provider_id = prov.id)
LEFT JOIN emr_provider man_prov on (rx.managing_provider_id = man_prov.id)
LEFT JOIN emr_provider fac_prov on (rx.facility_provider_id = fac_prov.id)
WHERE date >= now() - INTERVAL '2 years';


DROP TABLE IF EXISTS kre_report.val_emr_rx_col_pop_percents;
CREATE TABLE kre_report.val_emr_rx_col_pop_percents AS
SELECT
total_rx_l2y,
round((provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS provider_unknown_or_no_last_name_pct,
round((status_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS status_empty_pct,
round((directions_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS directions_empty_pct,
round((code_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS code_empty_pct,
round((name_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS name_empty_pct,
round((quantity_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS quantity_empty_pct,
round((refills_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS refills_empty_pct,
round((start_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS start_date_is_empty_or_invalid_pct,
round((end_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS end_date_is_empty_or_invalid_pct,
round((route_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS route_empty_pct,
round((dose_empty * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS dose_empty_pct,
round((managing_provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS managing_provider_unknown_or_no_last_name_pct,
round((facility_prov_dept_unknown_or_no_dept * 100)::numeric / NULLIF(total_rx_l2y, 0), 2) AS facility_prov_dept_unknown_or_no_dept_pct
FROM kre_report.val_emr_rx_col_pop_counts;


-- Rx counts by year
DROP TABLE IF EXISTS kre_report.val_emr_rx_counts_by_year;
CREATE TABLE kre_report.val_emr_rx_counts_by_year AS 
SELECT count(*) rx_count, extract(year from date) rx_year
from emr_prescription
group by rx_year
order by rx_year;



/* 
--DRILL IN TO VALUES FOR THESE:

status
quantity types??
quantity not numeric?
 */

--
-- LAB RESULTS
--


DROP TABLE IF EXISTS kre_report.val_emr_labres_col_pop_counts;
CREATE TABLE kre_report.val_emr_labres_col_pop_counts AS
SELECT
count(labres.id) as total_labresults_l2y,
count(case when date is null or date <= '1910-01-01' then 1 end) as date_is_empty_or_invalid,
count(case when result_date is null or result_date <= '1910-01-01' then 1 end) as result_date_is_empty_or_invalid,
count(case when prov.last_name = 'UNKNOWN' or prov.last_name is null or prov.last_name = '' then 1 end) provider_unknown_or_no_last_name,
count(case when native_code is null or native_code = '' then 1 end) native_code_empty,
count(case when native_name is null or native_name = '' then 1 end) native_name_empty,
count(case when result_string is null or result_string = '' then 1 end) result_string_empty,
count(case when abnormal_flag is null or abnormal_flag = '' then 1 end) abnormal_flag_empty,
count(case when ref_low_string is null or ref_low_string = '' then 1 end) ref_low_string_empty,
count(case when ref_high_string is null or ref_high_string = '' then 1 end) ref_high_string_empty,
count(case when ref_unit is null or ref_unit = '' then 1 end) ref_unit_empty,
count(case when status is null or status = '' then 1 end) status_empty,
count(case when comment is null or comment = '' then 1 end) comment_empty,
count(case when specimen_num is null or specimen_num = '' then 1 end) specimen_num_empty,
count(case when specimen_source is null or specimen_source = '' then 1 end) specimen_source_empty,
count(case when collection_date is null or collection_date <= '1910-01-01' then 1 end) as collection_date_is_empty_or_invalid,
count(case when procedure_name is null or procedure_name = '' then 1 end) procedure_name_empty,
count(case when "CLIA_ID_id" is null or "CLIA_ID_id" = '' then 1 end) clia_empty,
count(case when fac_prov.dept = 'UNKNOWN' or fac_prov.dept is null or fac_prov.dept = '' then 1 end) facility_prov_dept_unknown_or_no_dept
FROM emr_labresult labres
LEFT JOIN emr_provider prov on (labres.provider_id = prov.id)
LEFT JOIN emr_provider fac_prov on (labres.facility_provider_id = fac_prov.id)
WHERE date >= now() - INTERVAL '2 years';


DROP TABLE IF EXISTS kre_report.val_emr_labres_col_pop_percents;
CREATE TABLE kre_report.val_emr_labres_col_pop_percents AS
SELECT
total_labresults_l2y,
round((date_is_empty_or_invalid * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS date_is_empty_or_invalid_pct,
round((result_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS result_date_is_empty_or_invalid_pct,
round((provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS provider_unknown_or_no_last_name_pct,
round((native_code_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS native_code_empty_pct,
round((native_name_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS native_name_empty_pct,
round((result_string_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS result_string_empty_pct,
round((abnormal_flag_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS abnormal_flag_empty_pct,
round((ref_low_string_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS ref_low_string_empty_pct,
round((ref_high_string_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS ref_high_string_empty_pct,
round((ref_unit_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS ref_unit_empty_pct,
round((status_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS status_empty_pct,
round((comment_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS comment_empty_pct,
round((specimen_num_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS specimen_num_empty_pct,
round((specimen_source_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS specimen_source_empty_pct,
round((collection_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS collection_date_is_empty_or_invalid_pct,
round((procedure_name_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS procedure_name_empty_pct,
round((clia_empty * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS clia_empty_pct,
round((facility_prov_dept_unknown_or_no_dept * 100)::numeric / NULLIF(total_labresults_l2y, 0), 2) AS facility_prov_dept_unknown_or_no_dept_pct
FROM kre_report.val_emr_labres_col_pop_counts;



-- Lab result counts by year
DROP TABLE IF EXISTS kre_report.val_emr_labres_counts_by_year;
CREATE TABLE kre_report.val_emr_labres_counts_by_year AS 
SELECT count(*) labres_count, extract(year from date) labres_year
from emr_labresult
group by labres_year
order by labres_year;


/* DRILL IN TO VALUES FOR THESE:

status
specimen_source

 */


--
--  ORDERS/PROCEDURES
--

DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_col_pop_counts;
CREATE TABLE kre_report.val_emr_orderprocs_col_pop_counts AS
SELECT
count(orderproc.id) as total_orderprocs_l2y,
count(case when procedure_code is null or procedure_code = '' then 1 end) procedure_code_empty,
count(case when specimen_id is null or specimen_id = '' then 1 end) specimen_id_empty,
count(case when date is null or date <= '1910-01-01' then 1 end) as date_is_empty_or_invalid,
count(case when order_type is null or order_type = '' then 1 end) order_type_empty,
count(case when prov.last_name = 'UNKNOWN' or prov.last_name is null or prov.last_name = '' then 1 end) provider_unknown_or_no_last_name,
count(case when procedure_name is null or procedure_name = '' then 1 end) procedure_name_empty,
count(case when specimen_source is null or specimen_source = '' then 1 end) specimen_source_empty,
count(case when test_status is null or test_status = '' then 1 end) test_status_empty,
count(case when group_id is null or group_id = '' then 1 end) group_id_empty,
count(case when obs_start_date is null or obs_start_date <= '1910-01-01' then 1 end) as obs_start_date_is_empty_or_invalid,
count(case when obs_end_date is null or obs_end_date <= '1910-01-01' then 1 end) as obs_end_date_is_empty_or_invalid,
count(case when fac_prov.dept = 'UNKNOWN' or fac_prov.dept is null or fac_prov.dept = '' then 1 end) facility_prov_dept_unknown_or_no_dept
FROM emr_laborder orderproc
LEFT JOIN emr_provider prov on (orderproc.provider_id = prov.id)
LEFT JOIN emr_provider fac_prov on (orderproc.facility_provider_id = fac_prov.id)
WHERE date >= now() - INTERVAL '2 years';

DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_col_pop_percents;
CREATE TABLE kre_report.val_emr_orderprocs_col_pop_percents AS
SELECT
total_orderprocs_l2y,
round((procedure_code_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS procedure_code_empty_pct,
round((specimen_id_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS specimen_id_empty_pct,
round((date_is_empty_or_invalid * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS date_is_empty_or_invalid_pct,
round((order_type_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS order_type_empty_pct,
round((provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS provider_unknown_or_no_last_name_pct,
round((procedure_name_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS procedure_name_empty_pct,
round((specimen_source_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS specimen_source_empty_pct,
round((test_status_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS test_status_empty_pct,
round((group_id_empty * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS group_id_empty_pct,
round((obs_start_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS obs_start_date_is_empty_or_invalid_pct,
round((obs_end_date_is_empty_or_invalid * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS obs_end_date_is_empty_or_invalid_pct,
round((facility_prov_dept_unknown_or_no_dept * 100)::numeric / NULLIF(total_orderprocs_l2y, 0), 2) AS facility_prov_dept_unknown_or_no_dept
FROM kre_report.val_emr_orderprocs_col_pop_counts;


-- Orderprocs result counts by year
DROP TABLE IF EXISTS kre_report.val_emr_orderprocs_counts_by_year;
CREATE TABLE kre_report.val_emr_orderprocs_counts_by_year AS 
SELECT count(*) orderprocs_count, extract(year from date) orderprocs_year
from emr_laborder
group by orderprocs_year
order by orderprocs_year;


/* DRILL IN TO VALUES FOR THESE:

order_type
test_status

*/


--
-- SOCIAL HISTORY
--


DROP TABLE IF EXISTS kre_report.val_emr_sochist_col_pop_counts;
CREATE TABLE kre_report.val_emr_sochist_col_pop_counts AS
SELECT
count(sochist.id) as total_socialhistory_l2y,
count(distinct(patient_id)) as distinct_pat_socialhistory_l2y,
count(case when mrn is null or mrn = '' then 1 end) mrn_empty,
count(case when tobacco_use is null or tobacco_use = '' then 1 end) tobacco_use_empty,
count(case when alcohol_use is null or alcohol_use = '' then 1 end) alcohol_use_empty,
count(case when date is null or date <= '1910-01-01' then 1 end) as date_is_empty_or_invalid,
count(case when prov.last_name = 'UNKNOWN' or prov.last_name is null or prov.last_name = '' then 1 end) provider_unknown_or_no_last_name,
count(case when sex_partner_gender is null or sex_partner_gender = '' then 1 end) sex_partner_gender_empty,
count(case when ill_drug_use is null or ill_drug_use = '' then 1 end) ill_drug_use_empty,
count(case when sexually_active is null or sexually_active = '' then 1 end) sexually_active_empty,
count(case when birth_control_method is null or birth_control_method = '' then 1 end) birth_control_method_empty--,
--count(case when housing is null or housing = '' then 1 end) housing_sec_empty,
--count(case when food is null or food = '' then 1 end) food_sec_empty,
--count(case when utilities is null or utilities = '' then 1 end) utilities_sec_empty,
--count(case when medical_transportation is null or medical_transportation = '' then 1 end) medical_transportation_sec_empty,
--count(case when transportation is null or transportation = '' then 1 end) transportation_sec_empty
FROM emr_socialhistory sochist
LEFT JOIN emr_provider prov on (sochist.provider_id = prov.id)
WHERE date >= now() - INTERVAL '2 years';


DROP TABLE IF EXISTS kre_report.val_emr_sochist_col_pop_percents;
CREATE TABLE kre_report.val_emr_sochist_col_pop_percents AS
SELECT
total_socialhistory_l2y,
distinct_pat_socialhistory_l2y,
round((mrn_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS mrn_empty_pct,
round((tobacco_use_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS tobacco_use_empty_pct,
round((alcohol_use_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS alcohol_use_empty_pct,
round((date_is_empty_or_invalid * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS date_is_empty_or_invalid_pct,
round((provider_unknown_or_no_last_name * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS provider_unknown_or_no_last_name_pct,
round((sex_partner_gender_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS sex_partner_gender_empty_pct,
round((ill_drug_use_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS ill_drug_use_empty_pct,
round((sexually_active_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS sexually_active_empty_pct,
round((birth_control_method_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS birth_control_method_empty_pct--,
-- round((housing_sec_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS housing_sec_empty_pct,
-- round((food_sec_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS food_sec_empty_pct,
-- round((utilities_sec_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS utilities_sec_empty_pct,
-- round((medical_transportation_sec_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS medical_transportation_sec_empty_pct,
-- round((transportation_sec_empty * 100)::numeric / NULLIF(total_socialhistory_l2y, 0), 2) AS transportation_sec_empty_pct
FROM kre_report.val_emr_sochist_col_pop_counts;



-- Social History counts by year
DROP TABLE IF EXISTS kre_report.val_emr_socialhistory_counts_by_year;
CREATE TABLE kre_report.val_emr_socialhistory_counts_by_year AS 
SELECT count(*) socialhistory_count, extract(year from date) socialhistory_year
from emr_socialhistory
group by socialhistory_year
order by socialhistory_year;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_tobacco_use;
CREATE TABLE kre_report.val_emr_patient_non_std_tobacco_use AS
SELECT count(*) non_std_tobacco_use_count, tobacco_use
FROM emr_socialhistory
WHERE upper(tobacco_use) not in (
'NEVER',
'NOT ASKED', 
'PASSIVE', 
'QUIT',
'YES', 
''
)
and tobacco_use is not null
group by tobacco_use;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_alcohol_use;
CREATE TABLE kre_report.val_emr_patient_non_std_alcohol_use AS
SELECT count(*) non_std_alcohol_use_count, alcohol_use
FROM emr_socialhistory
WHERE upper(alcohol_use) not in (
'NEVER',
'NOT CURRENTLY',
'DEFER',
'NO', 
'NOT ASKED', 
'YES', 
''
)
and alcohol_use is not null
group by alcohol_use;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sex_partner_gender;
CREATE TABLE kre_report.val_emr_patient_non_std_sex_partner_gender AS
SELECT count(*) non_std_sex_partner_gender_count, sex_partner_gender
FROM emr_socialhistory
WHERE upper(sex_partner_gender) not in (
'FEMALE', 
'MALE', 
'FEMALE + MALE', 
'MALE + FEMALE',
''
)
and sex_partner_gender is not null
group by sex_partner_gender;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_ill_drug_use;
CREATE TABLE kre_report.val_emr_patient_non_std_ill_drug_use AS
SELECT count(*) non_std_ill_drug_use_count, ill_drug_use
FROM emr_socialhistory
WHERE upper(ill_drug_use) not in (
'YES', 
'NO',
'NOT CURRENTLY', 
'NEVER', 
'DEFER',
'NOT ASKED',
''
)
and ill_drug_use is not null
group by ill_drug_use;

DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_sexually_active;
CREATE TABLE kre_report.val_emr_patient_non_std_sexually_active AS
SELECT count(*) non_std_sexually_active_count, sexually_active
FROM emr_socialhistory
WHERE upper(sexually_active) not in (
'YES', 
'NO',
'NOT CURRENTLY', 
'NEVER', 
'DEFER',
'NOT ASKED',
''
)
and sexually_active is not null
group by sexually_active;


DROP TABLE IF EXISTS kre_report.val_emr_patient_non_std_birth_control_method;
CREATE TABLE kre_report.val_emr_patient_non_std_birth_control_method AS
SELECT count(*) non_std_birth_ctrl_method, birth_control_method
FROM emr_socialhistory
WHERE upper(birth_control_method) NOT ILIKE ALL(ARRAY['%ABSTINENCE%', '%CONDOM%', '%PILL%', '%DIAPHRAGM%', '%IUD%','%SURGICAL%', '%SPERMICIDE%', '%IMPLANT%', '%RHYTHM%', '%INJECTION%', '%SPONGE%', '%INSERTS%']) 
AND birth_control_method is not NULL
AND birth_control_method != ''
group by birth_control_method;



/* DRILL IN TO VALUES FOR THESE:

housing
food
utilities
medical_transportation
transportation

 */


SELECT * FROM kre_report.val_emr_patient_col_pop_percents;
SELECT * FROM kre_report.val_emr_patient_non_std_races;
SELECT * FROM kre_report.val_emr_patient_non_std_genders;
SELECT * FROM kre_report.val_emr_patient_non_std_ethnicity;
SELECT * FROM kre_report.val_emr_patient_non_std_vital_status;
SELECT * FROM kre_report.val_emr_patient_non_std_gender_identity;
SELECT * FROM kre_report.val_emr_patient_non_std_sex_orientation;
SELECT * FROM kre_report.val_emr_patient_non_std_birth_sex;
SELECT * FROM kre_report.val_emr_provider_col_pop_percents;
SELECT * FROM kre_report.val_emr_encounter_col_pop_percents;
SELECT * FROM kre_report.val_emr_encounter_counts_by_year;
SELECT * FROM kre_report.val_emr_rx_col_pop_percents;
SELECT * FROM kre_report.val_emr_rx_counts_by_year;
SELECT * FROM kre_report.val_emr_labres_col_pop_percents;
SELECT * FROM kre_report.val_emr_labres_counts_by_year;
SELECT * FROM kre_report.val_emr_orderprocs_col_pop_percents;
SELECT * FROM kre_report.val_emr_sochist_col_pop_percents;
SELECT * FROM kre_report.val_emr_socialhistory_counts_by_year;
SELECT * FROM kre_report.val_emr_patient_non_std_tobacco_use;
SELECT * FROM kre_report.val_emr_patient_non_std_alcohol_use;
SELECT * FROM kre_report.val_emr_patient_non_std_sex_partner_gender;
SELECT * FROM kre_report.val_emr_patient_non_std_ill_drug_use;
SELECT * FROM kre_report.val_emr_patient_non_std_sexually_active;
SELECT * FROM kre_report.val_emr_patient_non_std_birth_control_method;






