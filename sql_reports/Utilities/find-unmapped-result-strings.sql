﻿SET client_min_messages TO WARNING;

DROP TABLE IF EXISTS kre_report.tmp_unmapped_gonorrhea;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_chlamydia;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_a;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_b;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hiv;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_lyme;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_rpr;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tp;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tppa;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_vdrl;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_pertussis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tuberculosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_a1c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_alt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ast;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_bilirubin;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ogtt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_fta;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_islet_cell_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_c_peptide;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_cd4;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_gad65;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_glucose;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ica512;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_insulin_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid19;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_influenza;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid_suspect;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_anaplasmosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_babesiosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_all;

DROP TABLE IF EXISTS kre_report.unmapped_result_strings_report;


DROP TABLE IF EXISTS kre_report.tmp_unmapped_filter;
CREATE TABLE kre_report.tmp_unmapped_filter
(
  filter_result text
)
WITH (
  OIDS=FALSE
);


-- "starts with" matches
INSERT INTO kre_report.tmp_unmapped_filter(filter_result)
VALUES 
( '--'),
( '? INTERFERING SUBSTANCE'),
( '?NOT COLLECTED'),
( '?not collected'),
( '0.00 Comment'),
( '(617)-983-6600'),
( 'Abbott BinaxNOW COVID-19 Antigen Self Test'),
( 'Access Bio CareStart COVID-19 Antigen Home Test'),
( 'ACON/Flowflex COVID-19 Antigen Home Test Kit'),
( 'ADDED IN ERROR'), 
( 'Added Test'),
( 'Additional Testing Ordered'),
( 'Admission Surveillance'),
( 'Anal: Unsatisfactory'),
( 'Analyzer down. Changed to alternate analyzer.'),
( 'ADD ON STABILITY'),
( '(Added) SEE TEXT'),
( 'ALTERNATE TEST PERFORMED.'),
( 'Alternative testing done at Quest Diagnostics reference lab 06-2023'),
( 'AN SWAB'),
( 'Analyzer down. Converted to alternate testing platform to expedite for ER patient.'), 
( 'ANALYZER DOWN, SWITCHED TO ALTRENATE INSTRUMENT'), 
( 'ARS COV 2 RNA PANEL, PROBE, RESPIRATORY SPECIMEN ('),
( 'ARTERIAL'),
( 'Bad derived test component'),
( 'BATCHING ERROR. REORDERED.'),
( 'broad institute'),
( 'Broad test not performed. Sent to in-house PCR pla'),
( 'Cancel'),
( 'Cancelled'),
( 'CANCELED'),
( 'CAP'),
( 'CERVIX'),
( 'CHANGED TO CCVD19'),
( 'CHANGE PER CR'),
( 'CODE CHANGED'),
( 'Collection method not acceptable.'), 
( 'COLONIES TOO SMALL TO IDENTIFY. REINCUBATED'), 
( 'COMBINED TESTS'),
( 'Comment'),
( 'Complete'),
( 'Container leaked during transport; no specimen remains for testing.'), 
( 'Container leaked during transport; no specimen rem'),
( 'CONTAMINANT'),
( 'CONTAMINATED'), 
( 'CONTAMINATED SPECIMEN'), 
( 'Contaminated specimen. Please disregard result and draw new specimen.'),
( 'Contamination suspected'), 
( 'CORONAVIRUS DISEASE 2019 (COVID-19)- EXT'),
( 'COV2 BROAD INSTITUTE 9/28/2021'),
( 'Covid PCR result - Broad Lab'),
( 'covid 19 result'), 
( 'covid result'),
( 'CRDUP'),
( 'Credit'), 
( 'CREDITED'), 
( 'CREDIT.'),
( 'CRHEMO'),
( 'CRORD'), 
( 'CRRX'), 
( 'CRYPTOSPORIDIU...'),
( 'CT GC NAAT'),
( 'CX PER CR'),
( 'Culture in Process'),
( 'Culture in progress.'),
( 'Declined'),
( 'DELETED'),
( 'Direct bilirubin greater than total bilirubin, suspect assay'), 
( 'Direct bilirubin is less than the measureable limit. Therefore, indirect'), 
( 'DISREGARD RESULTS'),
( 'DISREGARD RESULTS, contaminated specimen.'),
( 'Disregard results,  contaminated specimen.'), 
( 'Disregard results. Incorrect patient registered.'),
( 'DNR'), 
( 'DOUP'), 
( 'DRUG SUSCEPTIBILITY'),
( 'DRUG SUSCEPTIBILITY TESTING WILL NOT BE DONE ON'),
( 'DRUG SUSCEPTIBILITY TESTING WILL NOT BE'),
( 'DUPLICATE'), 
( 'DUPLICATE SPECIMEN. ONLY ONE ACCEPTED PER DAY.'), 
( 'Duplicate Order'),
( 'DUPLICATE REQUEST'),
( 'DUPO'), 
( 'Ellume COVID-19 Home Test Kit'),
( 'Endocervical'),
( 'ENDOCX'), 
( 'ENTERED IN ERROR - WRONG PATIENT'), 
( 'endo cx'),
( 'Err code 6'), 
( 'error'), 
( 'Erroneous Encounter'), 
( 'FIA antigen test not performed. Sent for PCR test in-house, result pending'),
( 'FINAL'),
( 'FIXED TO CLCVD'), 
( 'fixed to clcvd'),
( 'fixed to pcvd19'), 
( 'FOR RESULTS, PLEASE SEE OUTSIDE LAB IN EPIC MEDIA'),
( 'GEL CLUMPS PRESENT'),
( 'GROSS HEMOLYSIS PRESENT'),
( 'GROSSLY HEMOLYZED'), 
( 'HEALTH_MAINTENANCE_COMPLETED'),
( 'HEM'),
( 'HEMOLY'),
( 'HEMOLYSIS PRESENT IN SAMPLE'), 
( 'HEMOLYTIC'), 
( 'HEMOLYZED SPECIMEN'), 
( 'HepC Ab CMIA'),
( 'hbsui'),
( 'hbsu1'),
( 'hbsul'),
( 'HIDE'), 
( 'HIV Ag/Ab CMIA'),
( 'HIV COUNSEL - DONE'),
( 'https://wihlaboratory.testcatalog.org'),
( 'iHealth COVID-19 Antigen Rapid Test'),
( 'IMPROPER TRANSPORT MEDIA'), 
( 'In-house testing being performed'),
( 'INCORRECT ORDER CODE'),
( 'Instrument malfunction, QNS for retesting.'),
( 'INTERFERING SUBSTANCE'),
( 'INTERFERING SUBSTANCE, NO INTERPRETATION.'), 
( 'INTERPRET WITH CAUTION, SPECIMEN HEMOLYZED'), 
( 'INSTRUMENT DOWN'),
( 'INSTRUMENT ERROR FLAG'),
( 'Invalid'),
( 'Inhibited'),
( 'Lab Accident at Reference Lab, Test not done.'), 
( 'LAB ACCIDENT: Unable to test. Resubmission requested.'),
( 'Lab Cancel'),
( 'Lab Error'),
( 'LABORATORY LOG IN ERROR'),
( 'Lipemic Specimen'), 
( 'LIPEMIC, GROSSLY'),
( 'Long wait for analyzer. Converted to alternate testing platform to expedite for ER patient.'),
( 'Long wait time. Converted to alternate platform to expedite for ER patient.'),
( 'LOST IN TRANSIT TO REF LAB'),
( 'LOST SPECIMEN. DENISE ANGERANE, ED CHARGE RN NOTIFIED.'), 
( 'Luminex PCR not performed. PCR sent to Broad Institute, result pending'),
( 'MACHINE DOWN'),
( 'MD AND/OR CARE UNIT NOTIFIED'), 
( 'MGLAB'),
( 'MIC'), 
( 'MICROSCOPY NOT DONE'),
( 'MISLABELED'), 
( 'MIXED ORGANISMS RESEMBLING OROPHARYNGEAL FLORA'),
( 'neh'),
( 'N/A'),
( 'NASAL'), 
( 'Nasopharyngeal'),
( 'NASOPHARYNGEAL SWAB'),
( 'Nasopharynx'),
( 'ND'), 
( 'Neisseria species'),
( 'NEISSERIA GONORRHOEAE NEGATIVE FOR PENICILLINASE PRODUCTION'), 
( 'NEISSERIA MENINGITIDIS'), 
( 'Nina Lowery'),
( 'NO ADD'),
( 'NOADD'), 
( 'NO CHARGE'), 
( 'NO CHARGE CREDITED: DUPLICATE SPECIMEN/ORDER.'),
( 'NO CHARGE CREDITED: NO SPECIMEN RECEIVED'), 
( 'NO CHARGE CREDITED: WRONG ORDER CODE USED.'), 
( 'NO KIT, REORDER AS CCVD19'),
( 'NO RESULT'), 
( 'No result (prediction failure)'),
( 'NO SPECIMEN RECEIVED'), 
( 'NO SPECIMEN RECEIVED,CALLED TO U6 AMANDA'), 
( 'NORMAL BACTERIAL VAGINAL FLORA'), 
( 'NORMOCHROMIC NORMOCYTIC ANEMIA WITHOUT SCHISTOCYTES, SPHEROCYTES OR BITE CELLS.'),
( 'NOT AVAIL.'),
( 'Not Applicable'),
( 'NOT CALCULATED'), 
( 'Not Done'),
( 'Not Drawn. Patient Refused.'),
( 'NOT INDICATED'),
( 'NOT NEEDED'),
( 'Not Perf'),
( 'Not performed'),
( 'NOT_PERFORMED'),
( 'Not tested'),
( '(NOTE)'), 
( 'NOT VALID'),
( 'NOTIFIED RN'),
( 'NP'),
( 'NULL'),
( 'O.4'), 
( 'OLAB'), 
( 'OLD'), 
( 'OLD SPECIMEN'), 
( 'ORDER ENTRY ERROR'),
( 'OPERATOR ERROR'),
( 'Orasure InteliSwab COVID-19 Rapid Test'),
( 'order error'),
( 'ORDER ERROR'),
( 'order sent to lab'),
( 'ORDERED AND RESULTED ON WRONG PATIENT'), 
( 'ordered in error'),
( 'ORDERED INCORRECTLY'),
( 'Ordered on new accession.'), 
( 'ORDERED BY LAB'),
( 'Other (see comments)'),
( 'PATIENT ID IN QUESTION'), 
( 'PATIENT DID NOT ARRIVE FOR SCHEDULED TESTING.'), 
( 'PCR sent to in-house platform'),
( 'PCR test not performed. Sent for FIA antigen test in house, result pending'),
( 'Pending'),
( 'PND'),
( 'pt declined'),
( 'PATIENT DISCHARGED'),
( 'PT TO RETURN WITH URINE'),
( 'PATIENT REFUSED'),
( 'PCR for B. burgdorferi is only available on synovial fluid, synovial tissue specimens, or skin biopsies.  The sensitivity of this assay, when applied to other specimen types, is unacceptably low.'), 
( 'Performed'),
( 'Performed at MAYO MEDICAL LABORATORY, 200 First Street Southwest, Rochester, MN 55905'),
( 'Performed at NORTHWESTERN REPRODUCTIVE GENETICS, INC., 680 North Lake Shore Drive, Suite 1230, Chicago, IL 60611'),
( 'Performed at NSMC'),
( 'PLASMODIUM SPECIES IDENTIFIED. DEFINITIVE SPECIATION REQUIRES FURTHER STUDIES.'),
( 'PLASMODIUM FALCIPARUM'),
( 'PLEASE DISREGARD CANCELLATION'), 
( 'PLEASE DISREGARD RESULT.'),
( 'PLEASE DISREGARD RESULTS'), 
( 'PLEASE DISREGARD RESULTS, PATIENT ID IN QUESTION'), 
( 'Please refer to COVID-19, NAA result displayed on the All Results Tab'),
( 'PLEASE REFER TO SPECIMEN NUMBER:'), 
( 'PLEASE SEE OUTSIDE LAB IN EPIC MEDIA.'), 
( 'PLEASE SEE OUTSIDE LAB IN EPIC MEDIA'),
( 'PLEASE SEE HARDCOPY/SCANNED LAB REPORT'),
( 'PLEASE SEE REPORT FOR ADENOVIRUS'), 
( 'PLEASE SEE REPORT FOR RSV RNA, QL PCR'),
( 'PLEASE SEE REPORT FOR SARS CoV 2 IgG'), 
( 'PLEASE SEE REPORT IN EPIC MEDIA'),
( 'PLEASE SEE REPORT FOR HEPATITIS C AB'), 
( 'Please see results in Media Manager 9/20/2022'), 
( 'Please see results in Media Manager 9/21/2022'),
( 'Please see results in Media Manager 9/16/2022'),
( 'POSSIBLE CONTAMINATION'), 
( 'PREF'),
( 'pt ref'),
( 'QNS'),
( 'QUANTITY NOT SUFFICIENT'),
( 'QUESTION REACTIVE'),
( 'Quidel Quickvue At-Home OTC COVID-19 Test'), 
( 'R'),
( 'RAPID MIC METHOD'), 
( 'RAPID SARS COV + SARS COV 2 AG, QL IA, RESPIRATORY'),
( 'REACCESSIONED FOR RAPID IN HOUSE TESTING.'), 
( 'Re-accessioned due to Billing Error. Verified by'),
( 'REFER TO CCVD19'),
( 'REFER TO FOLLOWING FREE TEXT COMMENT'),
( 'Reflexed to PCR Assay'),
( 'Refused'),
( 'REORDERED'),
( 'REORDERD'), 
( 'reordered with pcvd19'), 
( 'Reordered as MISC. SEND-OUT TEST and sent to Quest Diagnostics for molecular testing.'),
( 'Replaced'),
( 'REQUEST CREDITED'), 
( 'Result Invalid. The presence or absence of COVID-19 Viral RNAs cannot be determined. At a low frequency, clinical samples can contain inhibitors that may generate invalid results.'), 
( 'Results do not belong to this patient. Laboratory'),
( 'Result not cal... mg/dL (calc)'), 
( 'Results not reliable with moderate/severe specimen hemolysis. A repeat'),
( 'RESULT NOT REPORTED, HEMOLYSIS'), 
( 'RESULT NOT REPORTED, ICTERUS'),
( 'RESULT NOT REPORTED, LIPEMIA'), 
( 'Result NOT VALID. Recommend collection of a new specimen.'), 
( 'Result Scanned'),
( '(Results below)'),
( 'Results Scanned'),
( 'RESULT NOT CALCULATED'),
( 'Results Viewable in Attached Link:'),
( 'RETEST'),
( '(Revised) SEE TEXT'),
( '(Revised) TNP'),
( 'RPR: RAECTIVE'),
( 'SAMPLE CONTAMINATED'),
( 'Sample not labeled according to JCAHO standards.'),
( 'Sample sent to reference lab. Test results to be sent to ordering provider by reference lab.'),
( 'Sample sent to reference lab. Results communicated to provider directly.'), 
( 'sars cov 2 Broad Institute 9/27/2021'), 
( 'SARS COV 2 9/27/2021 BROAD INSTITUTE'),
( 'SARS COV 2 9/28/2021 BROAD INSTITUTE'),
( 'SARS COV 2 RNA PANEL, PROBE, RESPIRATORY SPECIMEN'),
( 'SCANNED'),
( 'See '),
( '(see '),
( 'seenote'),
( '**SEE NOTE**'), 
( 'SEND OUT TO CAPE COD HOSPITAL'), 
( 'SENDING TO BCH'),
( 'SENDING TO MGH UNDER CODE SCVGM'), 
( 'SENDOUT TO MAYO MEDICAL LABORATORY ID PERFORMED BY MAYO MEDICAL LABORATORIES'), 
( 'Sent for HIV Confirm'),
( 'SENT TO'),
( 'Spec. integrity issue, redraw suggested'),
( 'SPECIMEN APPEARS GROSSLY LIPEMIC'), 
( 'Specimen collected and sent to Broad Institute for testing. Results to follow.'), 
( 'Specimen hemolyzed.  Please resubmit a fresh speci'),
( 'Specimen hemolyzed.  Please resubmit a fresh specimen.'), 
( 'SPECIMEN LEAKED IN TRANSIT TO REFERENCE LAB'), 
( 'SPECIMEN MISLABELED, RESULTS DO NOT BELONG TO THIS PATIENT'), 
( 'SPECIMEN NOT COLLECTED'), 
( 'Specimen Not Received'),
( 'SPECIMEN NOT RECEIVED'),
( 'SPECIMEN NOT RECEIVED IN LAB, ELECTRONIC ORDER CANCELLED.'),
( 'SPECIMEN NOT RECEIVED IN LAB, ELECTRONIC ORDER CAN'),
( 'SPECIMEN RECEIVED'),
( 'SPECIMEN RELOGGED'),
( 'Specimen sent to State lab.'),
( 'Specimen slightly hemolyzed.  Result may be falsely elevated.'), 
( 'Specimen slightly hemolyzed.  Result may be affected, interpret with caution.'),
( 'Specimen testing being performed in-house.'),
( 'Specimen too hemolyzed to test.'), 
( 'Specimen too old to process'), 
( 'SPECIMEN TYPE NOT APPROPRIATE FOR TEST REQUESTED'), 
( 'SPECIMEN TYPE HAS NOT BEEN VALIDATED FOR TEST REQUESTED'),
( 'SPECIMEN UNACCEPTABLE'),
( 'Specimen unsatisfactory for evaluation'),
( 'Specimen was received in laboratory with incorrect patient identification. The requested testing will not be performed. Healthcare provider notified:'), 
( 'Specimen was received in the Microbiology Laboratory and being processed to send out to the Massachusetts State Lab, results to follow.'),
( 'SST TUBE LEFT UNSPUN AT OFFICE OVERNIGHT, CANNOT BE USED FOR TESTING'),
( 'STABILITY LIMIT EXCEEDED'),
( 'SUBMIT NEW SPEC/ORIGINAL UNSATIS'),
( 'SUSPECTED I.V. CONTAMINATION'),
( 'SWAB COLLECTED IN MEDIA NWH PLATFORMS ARE NOT VALIDATED FOR. ON CALL PHYSICIAN PAGED AND NOTIFIED.'), 
( 'Swab Not Received'), 
( 'Symptomatic'),
( 'Syphilis Ab CMIA'),
( 'SYPHILIS ANTIBODY CASCADING REFLEX'),
( 'TEST'),
( 'Test cancelled'),
( 'Test component not applicable or not reported.'), 
( 'TEST CREDITED DUE TO REAGENT UNAVAILABILITY'), 
( 'Test currently unavailable due to manufacturer reagent shortage.'), 
( 'TEST INCORRECTLY'), 
( 'Test not performed'),
( 'TEST ORDERED IN ERROR'), 
( 'TEST PERFORMED AT MASSACHUSETTS GENERAL HOSPITAL - BOSTON,MA'),
( 'Testing performed in NWH Microbiology'),
( 'TEST PERFORMED ON ALTERNATE METHOD.'), 
( 'Test received-See reflex to IDDL test SARS CoV2 (COVID-19) Virus RT-PCR'),
( 'Test replaced'),
( 'Testing sent to Quest'),
( 'Test sent to reference laboratory.'),
( 'Test(s) cancelled by Care Unit.'), 
( 'Test(s) cancelled by M.D.'), 
( 'Testing CANCELLED per Microbiology Director due to COVID 19 workflow management.'), 
( 'TESTING CREDIT'), 
( 'TESTING DONE IN HOUSE'),
( 'TESTING INCOMPLETE DUE TO LABORATORY ACCIDENT'), 
( 'testing moved to inhouse test'), 
( 'Testing performed at Quest'),
( 'Testing platform: Abbott ID NOW SARS-CoV-2(isothermal nucleic acid amplification)Test'),
( 'Testing platform: Abbott ID NOW SARS-CoV-2 Test'),
( 'Testing platform: Cepheid Xpert?? Xpress'),
( 'Testing platform: Roche cobas Liat RT-PCR'),
( 'The BioFire Diagnostics respiratory panel is targeted  for Coronavirus 229E, HKU1, NL63, and OC43. BioFire Diagnostics has indicated that reactivity in this assay does NOT predict cross-reactivity with the 2019-nCoV Coronavirus.'),
( 'THE BUFFER TO PREVERVE'),
( 'This specimen was received without adequate identification and cannot be replaced. After consultation with the responsible provider, processing was deemed to be in the best interest of patient care, in accordance with hospital policy.'),
( 'There is non-specific interference by antibody scr'),
( 'THIS CULTURE.'),
( 'THIS SPECIMEN WAS RECEIVED UNLABELLED. TEST(S) CANCELLED. NOTIFIED:'),
( 'This test has not been reviewed by the FDA.'),
( 'THIS RESULT IS INCORRECT FOR THIS PATIENT'),
( 'TND'),
( 'TNP'),
( 'TO OLD'),
( 'TOO OLD'),
( 'TOO OLD RECOLLECT'),
( 'Total bilirubin is less than the measureable limit. Therefore, indirect'),
( 'Totally hemolyzed specimen. Unable to perform test'), 
( 'Too Young to Interpret, Final Result to Follow.'), 
( 'TP-PA Testing not performed. This patient has been tested by TP-PA at least twice in the past.'), 
( 'TRANSPORT DELAY TO REFERENCE LAB'), 
( 'TUBE WAS MISLABELLED'), 
( 'UNABLE TO ASSAY BY NSMC METHOD. SENT OUT TO REFERENCE LAB.'),
( 'Unable to assay,specimen was collected in an expired sampling container,please recollect'), 
( 'UNABLE TO ADD'),
( 'Unable to be tested: Laboratory error at MA State Lab. Sample integrity was compromised.'), 
( 'Unable to calculate'), 
( 'UNABLE TO DISCERN IF SAMPLE IS LABELLED CORRECTLY. OFFICE NOTIFIED.'), 
( 'Unable to measure.'), 
( 'UNABLE TO OBTAIN RESULT VIA NSMC METHOD.'), 
( 'UNABLE TO OBTAIN RESULT.'),
( 'UNABLE TO PERFORM BY NSMC METHOD.'), 
( 'UNABLE TO PERFORM BY NSMC METHOD. SAMPLE SENT TO REFERENCE LAB.'), 
( 'UNABLE TO PERFORM BY NSMC METHOD'),
( 'Unable to perform test.'),
( 'UNABLE TO PERFORM TEST BY NSMC METHOD.'), 
( 'UNABLE TO PERFORM TEST DUE TO LIPEMIA.'), 
( 'UNABLE TO PERFORM TESTING FOR DBILI DUE TO HEMOLYSIS'),
( 'UNABLE TO PERFORM TESTING ON DBIL DUE TO HEMOLYSIS'),
( 'UNABLE TO PERFORM TESTING ON DBILI DUE TO HEMOLYSIS'),
( 'Unable to process'),
( 'Unable to report due to hemolysis'), 
( 'UNABLE TO REPORT DUE TO INTERFERENCE FROM HEMOLYSIS'), 
( 'Unable to report, likely IV contamination confirmed by:'),
( 'UNABLE TO RESULT'), 
( 'Unable to result due to lipemic interference'), 
( 'UNABLE TO RESULT DUE TO LIPEMIC INTERFERENCE'), 
( 'UNABLE TO TEST FOR DIRECT BILI DUE TO HEMOLYSIS'), 
( 'UNABLE TO TEST DBILI DUE TO HEMOLYSIS'),
( 'UNINTERPRETABLE'), 
( 'Unknown'), 
( 'Unlabeled specimen'),
( 'Unsatisfactory'),
( 'URINE'), 
( 'VAGINAL SPECIMEN'),
( 'Vaginal: Unsatisfactory'),
( 'Valid'),
( 'VOID'),
( 'WARNING: This nonreactive result:'),
( 'When testing is completed, results will be available in Epic Chart Review under the Media tab.'), 
( 'WRONG CODE'), 
( 'wrong code'), 
( 'wrong code used'), 
( 'WRONG CODE USED'), 
( 'WRONG ORDER CODE'), 
( 'Wrong order code used, credit issued'),
( 'WRONG PATIENT'), 
( 'Wrong patient, disregard result'),
( 'WRONG PATIENT ID USED, PLEASE DISREGARD PREVIOUS RESULT.'),
( 'WRONG PATIENT. PLEASE DISREGARD PREVIOUS REPORT.'), 
( 'Wrong specimen type received.'), 
( 'WRONG TEST CODE'), 
( 'WRONG TEST ORDERED'),
( 'WRONG TEST ORDERED, CORRECT TEST HAS BEEN REORDERED.'),
( 'x')
;


create table kre_report.tmp_unmapped_gonorrhea AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name = 'gonorrhea'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:gonorrhea%'; 



CREATE TABLE kre_report.tmp_unmapped_chlamydia AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name = 'chlamydia'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')


	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:chlamydia%';

CREATE TABLE kre_report.tmp_unmapped_hepatitis_a AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_a%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_a%';


CREATE TABLE kre_report.tmp_unmapped_hepatitis_b AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_b%'
	AND test_name not in ('hepatitis_b_core_antigen_general_antibody', 'hepatitis_b_core_ab')
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_b%';

CREATE TABLE kre_report.tmp_unmapped_hepatitis_c AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_c%'
	AND c.test_name != 'hepatitis_c_genotype'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_c%';

CREATE TABLE kre_report.tmp_unmapped_hiv AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hiv%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	AND c.native_code not in ('MDPH-293')
	AND result_string not in ('<30 Detected', '< 20', '< 1.301', 'HIV-1 RNA detected, <20 cp/mL', '<30 DETECTED', '<40')
	AND result_string not ilike '%copies/mL%'
	AND result_string not ilike '<20%'
	AND result_string not ilike '<1.30%'

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hiv%' 
	AND c.native_code not in ('MDPH-293');

-- FOR LYME - UPDATING TO 30 DAYS FOR NOW SINCE IT IS NOT BEING REPORTED YET
CREATE TABLE kre_report.tmp_unmapped_lyme AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'lyme%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '30 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	-- only check if hef events for lyme have been created in the last 2 years
	-- this is so result strings won't be triggered for sites that have labs mapped but are not running hef for the condition
	and exists (select * from hef_event where name ilike 'lx:lyme%' and date >= (CURRENT_DATE - INTERVAL '1 years')::date)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:lyme%';

CREATE TABLE kre_report.tmp_unmapped_rpr AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'rpr%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	-- rpr excluded positive strings
	AND result_string not in ('REACTIVE', 'WEAKLY REACTIVE', 'POSITIVE', 'Positive', 'Reactivre', 'Reactive', 'reactive', 'Weakly Reactive', 'RPR: REACTIVE', 'Immune', 'TP-PA: REACTIVE', 'TPPA: Reactive', 'laboratory evidence of syphilis infection. Resubmit in 2-4 weeks if clinically indicated.')

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:rpr%';

CREATE TABLE kre_report.tmp_unmapped_tp AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tp-%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tp-%';

CREATE TABLE kre_report.tmp_unmapped_tppa AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tppa%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tppa%';

CREATE TABLE kre_report.tmp_unmapped_vdrl AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'vdrl%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:vdrl%';


CREATE TABLE kre_report.tmp_unmapped_tuberculosis AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tb_%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tb_%';

CREATE TABLE kre_report.tmp_unmapped_alt AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'alt%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	-- ignore those beginning with a less than symbol as these are expected to not be interpreted
	AND result_string !~ ('^<*')
	AND result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:alt%';

CREATE TABLE kre_report.tmp_unmapped_ast AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'ast%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	AND result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	AND ((result_float >= 100 and ref_high_float is null) or (result_float >= ref_high_float*2) or result_float is null)
	-- no provision for handling non-numeric results for ast
	AND result_string !~ '^(<|>)'
	AND result_string not in ('Above linear range')

	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:ast%';

CREATE TABLE kre_report.tmp_unmapped_bilirubin AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE 
	   (
	   (c.test_name ilike 'bilirubin_direct' and result_string not ilike '>%' and result_string not ilike '<%')
	   or (c.test_name = 'bilirubin_indirect' and result_string not ilike '>%' and result_string not ilike '<%')
  		--bilirubin_total does not create negative_events so these need to be filtered out
	   or (c.test_name = 'bilirubin_total'
			and result_string not ilike '<%'
			and result_string not ilike '>%'
			and upper(result_string) not in ('BELOW LINEAR RANGE','UL', 'DL', 'NEGATIVE','NEG','N/A')
			and result_string not ilike '%below assay range%'
			and (result_float > 1.5 or result_float is null)) 
	   )
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:bilirubin%';


CREATE TABLE kre_report.tmp_unmapped_fta AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'fta%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:fta%';

CREATE TABLE kre_report.tmp_unmapped_cd4 AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'cd4%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:cd4%';



CREATE TABLE kre_report.tmp_unmapped_covid19 AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE ( c.test_name = 'covid19_pcr' 
		   -- rogue PCR result
		   AND result_string not in ('2')
	        OR c.test_name IN ('covid19_igg', 'covid19_igm', 'covid19_iga', 'covid19_ab_total', 'covid19_ag')
	      )
        AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT', 'NASOPHARYNX')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:covid%';
	
	
	
CREATE TABLE kre_report.tmp_unmapped_influenza AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
        WHERE c.test_name in  ('influenza', 'rapid_flu', 'influenza_culture')
        AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:influenza%' or name ilike 'lx:rapid_flu%');
	
	
	
CREATE TABLE kre_report.tmp_unmapped_covid_suspect AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
        WHERE c.test_name in  ('h_metapneumovirus', 'parainfluenza', 'adenovirus', 'rhino_entero_virus', 'coronavirus_non19', 'm_pneumoniae_igm',
		      'm_pneumoniae_pcr', 'c_pneumoniae_igm', 'c_pneumoniae_pcr', 'rsv', 'parapertussis')
        AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:h_metapneumovirus%' or name ilike 'lx:parainfluenza%' or name ilike 'lx:adenovirus%' or name ilike 'lx:rhino_entero_virus%' or name ilike 'lx:coronavirus_non19%' or name ilike 'lx:m_pneumoniae_igm%'
	       or name ilike 'lx:m_pneumoniae_pcr%' or name ilike 'lx:c_pneumoniae_igm%' or name ilike 'lx:c_pneumoniae_pcr%' or name ilike 'lx:rsv%' or name ilike 'lx:parapertussis%');	
		   
		   
create table kre_report.tmp_unmapped_anaplasmosis AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'anaplasmosis%'
	AND c.test_name != 'anaplasmosis not a test'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:anaplasmosis%'; 
	
create table kre_report.tmp_unmapped_babesiosis AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE (c.test_name ilike 'babesiosis%' or c.test_name = 'tick_bloodsmear')
	AND c.test_name != 'babesiosis not a test'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:babesiosis%' or name ilike 'lx:tick_bloodsmear%'); 
	
	

CREATE TABLE kre_report.tmp_unmapped_all AS
SELECT * from kre_report.tmp_unmapped_gonorrhea
UNION
SELECT * from kre_report.tmp_unmapped_chlamydia
UNION 
SELECT * from kre_report.tmp_unmapped_hepatitis_a
UNION 
SELECT * from kre_report.tmp_unmapped_hepatitis_b
UNION
SELECT * FROM kre_report.tmp_unmapped_hepatitis_c
UNION
SELECT * FROM kre_report.tmp_unmapped_hiv
UNION
SELECT * FROM kre_report.tmp_unmapped_lyme
UNION
SELECT * FROM kre_report.tmp_unmapped_rpr
UNION
SELECT * FROM kre_report.tmp_unmapped_tp
UNION
SELECT * FROM kre_report.tmp_unmapped_tppa
UNION
SELECT * FROM kre_report.tmp_unmapped_vdrl
UNION
SELECT * FROM kre_report.tmp_unmapped_tuberculosis
UNION
SELECT * FROM kre_report.tmp_unmapped_alt
UNION
SELECT * FROM kre_report.tmp_unmapped_ast
UNION
SELECT * FROM kre_report.tmp_unmapped_bilirubin
UNION
SELECT * FROM kre_report.tmp_unmapped_fta
UNION
SELECT * FROM kre_report.tmp_unmapped_cd4
UNION
SELECT * FROM kre_report.tmp_unmapped_covid19
UNION
SELECT * FROM kre_report.tmp_unmapped_influenza
UNION
SELECT * FROM kre_report.tmp_unmapped_covid_suspect
UNION
SELECT * FROM kre_report.tmp_unmapped_anaplasmosis
UNION
SELECT * FROM kre_report.tmp_unmapped_babesiosis;


CREATE TABLE kre_report.unmapped_result_strings_report AS
select count(*), native_code, result_string, test_name
from kre_report.tmp_unmapped_all
group by result_string, test_name, native_code
having count(*) > 1
ORDER BY test_name, native_code, count(*) desc;

SELECT * FROM kre_report.unmapped_result_strings_report;


\COPY kre_report.unmapped_result_strings_report  TO '/tmp/unmapped_result_strings_report.csv' DELIMITER ',' CSV HEADER;


DROP TABLE IF EXISTS kre_report.tmp_unmapped_gonorrhea;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_chlamydia;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_a;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_b;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hiv;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_lyme;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_rpr;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tp;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tppa;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_vdrl;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_pertussis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tuberculosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_a1c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_alt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ast;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_bilirubin;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ogtt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_fta;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_islet_cell_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_c_peptide;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_cd4;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_gad65;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_glucose;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ica512;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_insulin_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid19;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_influenza;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid_suspect;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_anaplasmosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_babesiosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_all;

	


