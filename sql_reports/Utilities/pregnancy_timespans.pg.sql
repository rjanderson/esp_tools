---Assumes no pregnancy timespans currently exist

Select distinct patient_id, date, id as event_id 
into temporary tmp_preg_onset
from hef_event where name='dx:pregnancy:onset';

Select distinct patient_id, date, id as event_id 
into temporary tmp_preg_complications
from hef_event where name='dx:pregnancy:complications';

Select distinct patient_id, date, id as event_id 
into temporary tmp_preg_end_provis
from hef_event where name='dx:pregnancy:end-of-pregnancy-provisional-icd10';

Select distinct patient_id, date, id as event_id 
into temporary tmp_preg_end
from hef_event where name='dx:pregnancy:end-of-pregnancy-icd10';

Select distinct e.patient_id, e.id as event_id, 
  case when split_part(dx.dx_code_id,'.',2) ~ '^[0-9]+$' then e.date - interval '1 week' * split_part(dx.dx_code_id,'.',2)::integer
       else null
  end as date
into temporary tmp_preg_gest_age
from hef_event e
join emr_encounter enc on enc.id=e.object_id and e.name='dx:pregnancy:gestational-age'
join emr_encounter_dx_codes dx on dx.encounter_id=enc.id and dx.dx_code_id like 'icd10:Z3A%';


select distinct patient_id, date, event_id, 'start'::varchar(5) as which
into temporary tmp_onset_grp
from
(select patient_id, date - interval '30 days' date, event_id from tmp_preg_onset
 union 
 select patient_id, date - interval '30 days' date, event_id from tmp_preg_complications 
 union 
 select patient_id, date, event_id from tmp_preg_gest_age) t0;
 
select distinct patient_id, date, event_id, 'end'::varchar(5) as which
into temporary tmp_eop_grp
from
(select patient_id, date, event_id from tmp_preg_end_provis
 union 
 select patient_id, date, event_id from tmp_preg_end) t0;
 
select *, lag(date) over (partition by patient_id order by patient_id, date, which) as prior_dt,
          lead(date) over (partition by patient_id order by patient_id, date, which) as next_dt,
          lag(which) over (partition by patient_id order by patient_id, date, which) as prior_which,
          lead(which) over (partition by patient_id order by patient_id, date, which) as next_which
into temporary tmp_preg_seq1a
from (
 select * from tmp_onset_grp
 union
 select * from tmp_eop_grp
 ) t0;
 
--need to create fake end records for pregnancies that have no end codes within 300 days of start.
select patient_id, date + interval '300 days' as date, event_id, 'fend'::varchar(5) as which
into temporary tmp_fake_end
from tmp_preg_seq1a t0
where ((which='start' and prior_which='end' and 
             DATE_PART('year', AGE(date, prior_dt)) * 12 + DATE_PART('month', AGE(date, prior_dt)) > 0)
             or (which='start' and prior_which is null))
         and not exists (select null from tmp_preg_seq1a t1 
                         where t0.patient_id=t1.patient_id and t1.which='end'
                            and t1.date <= t0.date + interval '300 days');

--need to create fake start records for pregnancies that have no start codes within 300 days of end.
select patient_id, date - interval '30 days' as date, event_id, 'fstrt'::varchar(5) as which
into temporary tmp_fake_start
from tmp_preg_seq1a t0
where ((which='end' and prior_which='start' and date-prior_dt > interval '300 days')
             or (which='end' and prior_which is null));

                            
select patient_id, date, event_id, which,
          lag(date) over (partition by patient_id order by patient_id, date, which) as prior_dt, 
          lead(date) over (partition by patient_id order by patient_id, date, which) as next_dt,
          lag(which) over (partition by patient_id order by patient_id, date, which) as prior_which,
          lead(which) over (partition by patient_id order by patient_id, date, which) as next_which,
          row_number() over (partition by patient_id order by patient_id, date, which) as i
into temporary tmp_preg_seq1b
from (
select patient_id, date, event_id, which from tmp_preg_seq1a
union
select patient_id, date, event_id, which from tmp_fake_end
union
select patient_id, date, event_id, which from tmp_fake_start
) t0;

drop table if exists tmp_preg_seq2;
select *,
   case when which='start' and prior_which in ('end','fend') and 
             DATE_PART('year', AGE(date, prior_dt)) * 12 + DATE_PART('month', AGE(date, prior_dt)) > 0 then 'sop'::varchar(3)
        when which='start' and prior_which is null then 'sop'::varchar(3)
        when which='end' and next_which in ('start','fstrt') and 
             DATE_PART('year', AGE(next_dt, date)) * 12 + DATE_PART('month', AGE(next_dt, date)) > 0 then 'eop'::varchar(3)
        when which='end' and next_which is null then 'eop'::varchar(3)
        when which='fend' then 'eop'::varchar(3)
        when which='fstrt' then 'sop'::varchar(3)
   end as preg        
into temporary tmp_preg_seq2
from tmp_preg_seq1b;

drop table if exists tmp_prg_seq3;
select *,
    sum(case when preg='sop' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as timespan_grps
into temporary tmp_prg_seq3
from tmp_preg_seq2;

drop table if exists tmp_prg_spans;
select patient_id, min(date) start_date, max(case when preg='eop' then date else null end) end_date, timespan_grps
into temporary tmp_prg_spans
from tmp_prg_seq3
group by patient_id, timespan_grps;

delete from hef_timespan_events te where exists (select null from hef_timespan t where te.timespan_id=t.id and t.name='pregnancy');
delete from hef_timespan where name='pregnancy';
insert into hef_timespan (name, source, patient_id, start_date, end_date, timestamp, pattern)
select 'pregnancy' as name, 'urn:x-esphealth:heuristic:commoninf:timespan:pregnancy:v1' as source, patient_id, start_date, end_date,
       current_timestamp as timestamp, 'onset:dx_code eop:dx_code' as pattern
from tmp_prg_spans;

drop table if exists tmp_prg_spans_w_id;
select id, patient_id, start_date, end_date, pattern
into temporary tmp_prg_spans_w_id
from hef_timespan
where name='pregnancy';

insert into hef_timespan_events (event_id, timespan_id)
select distinct t0.event_id, t1.id as timespan_id
from tmp_prg_seq3 t0
join tmp_prg_spans_w_id t1 on t0.patient_id=t1.patient_id 
  and t0.date>=t1.start_date and t0.date<=t1.end_date
  where not exists (select null from hef_timespan_events te where te.timespan_id=t1.id and te.event_id=t0.event_id);
 
 

