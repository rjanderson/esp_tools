--SQL to Create a listing of cases for a specific condition
--update the condition name in the line 
-- AND nodis.condition = 'chlamydia'

-- chlamydia
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'chlamydia' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/ESP-chlamydia_case_details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'chlamydia' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-chlamydia-count.csv' with csv header;


--gonorrhea
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'gonorrhea' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/ESP-gonorrhea_case_details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'gonorrhea' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-gonorrhea-count.csv' with csv header;

--syphilis
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'syphilis' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-02-10-syhphilis-case-details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'syphilis' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/2021-02-10-ESP-syphilis-count.csv' with csv header;

--hepatitis_a:acute
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'hepatitis_a:acute' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-02-10-hepA-case-details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'hepatitis_a:acute' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/2021-02-10-ESP-hepA-count.csv' with csv header;

-- hepatitis_b (new algorithm that includes chronic)
-- added nodis_caseactivehistory details
-- only includes cases since '2018-01-01'
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events, array_agg(distinct('status: '||nch.status||' date: '||nch.date||' change reason: '||nch.change_reason||' ')) case_active_history FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat, nodis_caseactivehistory nch WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.id = nch.case_id AND nodis.condition = 'hepatitis_b' AND nodis.date >= '2018-01-01' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-10-22-hepB-acute-chronic-case-details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'hepatitis_b' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/2021-10-22-ESP-hepB-acute-chronic-count.csv' with csv header;


--hepatitis_b:acute
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'hepatitis_b:acute' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-02-10-hepB-case-details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'hepatitis_b:acute' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/2021-02-10-ESP-hepBcount.csv' with csv header;

--tuberculosis
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'tuberculosis' AND nodis.date >= '2018-01-01' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-02-07-tuberculosis-case-details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'tuberculosis' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/2021-02-07-ESP-tuberculosis-count.csv' with csv header;

-- hiv
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select concat(name,date) from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select concat(native_name, native_code, procedure_name, date::date) from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select concat(dx_code_id, date) as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'hiv' and nodis.date > '01-01-2018' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2021-06-01-ESP-hiv-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'hiv' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-hiv-count.csv' with csv header;


-- babesiosis
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select concat(name,date) from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select concat(native_name, native_code, procedure_name, date::date) from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select concat(dx_code_id, date) as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'babesiosis' and nodis.date > '01-01-2016' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2022-03-30-ESP-babesiosis-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'babesiosis' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-babesiosis-count.csv' with csv header;

-- anaplasmosis
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select concat(name,date) from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select concat(native_name, native_code, procedure_name, date::date) from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select concat(dx_code_id, date) as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'anaplasmosis' and nodis.date > '01-01-2016' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/2022-03-30-ESP-anaplasmosis-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'anaplasmosis' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-anaplasmosis-count.csv' with csv header;


--hepatitis_c
\COPY (SELECT nodis.id as esp_case_id, nodis.date as case_date, pat.id as esp_patient_id, mrn, first_name, last_name, date_of_birth::date, nodis.condition, criteria, array_agg(name) as event_names, array_agg(hef.date) as event_dates, array_agg(CASE WHEN hef.name ilike 'rx:%' then (select name||date from emr_prescription where id = hef.object_id) WHEN hef.name ilike 'lx:%' then (select native_name|| ' - ' ||native_code|| ' - ' ||procedure_name || ' ' || date::date from emr_labresult where id = hef.object_id) WHEN hef.name ilike 'dx:%' then (select string_agg (dx_code_id, ',') from (select dx_code_id || ' ' || date as dx_code_id from emr_encounter, emr_encounter_dx_codes where emr_encounter.id = hef.object_id and emr_encounter.id = emr_encounter_dx_codes.encounter_id) T1) ELSE 'NOT FOUND' END) source_events FROM nodis_case nodis, hef_event hef, nodis_case_events nce, emr_patient pat WHERE hef.id = nce.event_id AND nodis.id = nce.case_id AND pat.id = hef.patient_id AND nodis.patient_id = pat.id AND nodis.condition = 'hepatitis_c' GROUP BY pat.id, nodis.criteria, nodis.id, nodis.condition, nodis.date ORDER BY nodis.date) TO '/tmp/ESP-hepc_case_details-PHI.csv' with csv header;

\COPY (select count(*), condition, date_part('month', date) as datemon, date_part('year', date) as dateyear from nodis_case where condition = 'hepatitis_c' group by condition, dateyear, datemon order by condition, dateyear, datemon) TO '/tmp/ESP-hepc-count.csv' with csv header;
