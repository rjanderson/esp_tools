--long running dx events via sql

--asthma
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:asthma';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like  'icd10:J45%'
       or dx_code_id like 'icd9:493.%'
       or dx_code_id like 'icd10:J46%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);  
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:asthma'::varchar(128) as name, 'urn:x-esphealth:heuristic:commoninf:asthma:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--depression
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:depression';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like 'icd9:296.2%'
       or dx_code_id like 'icd9:296.3%'
       or dx_code_id like 'icd9:300.4%'
       or dx_code_id like 'icd9:308.0%'
       or dx_code_id like 'icd9:309.0%'
       or dx_code_id like 'icd9:309.1%' 
       or dx_code_id like 'icd9:309.28%'
       or dx_code_id like 'icd9:311%'
       or dx_code_id like 'icd9:V79.0%'
       or dx_code_id like 'icd10:F32.%'
       or dx_code_id like 'icd10:F33.%'
       or dx_code_id like 'icd10:F34.1%'
       or dx_code_id like 'icd10:F43.21%'
       or dx_code_id like 'icd10:F43.23%'
       or dx_code_id like 'icd10:F06.31%'
       or dx_code_id like 'icd10:F06.32%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id); 
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:depression'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:depression:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--diabetes:all-types
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:diabetes:all-types';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like 'icd9:250.%'
       or dx_code_id like 'icd10:E10%'
       or dx_code_id like 'icd10:E11%'
       or dx_code_id like 'icd10:E12%'
       or dx_code_id like 'icd10:E13%'
       or dx_code_id like 'icd10:E14%'
)
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id); 
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:diabetes:all-types'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:diabetes:all-types:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--need type-1-not-stated-icd10
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:diabetes:type-1-not-stated-icd10';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like  'icd10:E10%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:diabetes:type-1-not-stated-icd10'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:diabetes:type-1-not-stated-icd10:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--diabetes:type-2-not-stated-icd10
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:diabetes:type-2-not-stated-icd10';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like  'icd10:E11%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:diabetes:type-2-not-stated-icd10'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:diabetes:type-2-not-stated-icd10:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--pregnancy:complications
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:pregnancy:complications';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id = 'icd10:O10'
       or dx_code_id like 'icd10:O10.%1'
       or dx_code_id = 'icd10:O11.4'
       or dx_code_id = 'icd10:O11.5'
       or dx_code_id like 'icd10:O12.%4'
       or dx_code_id like 'icd10:O12.%5'
       or dx_code_id = 'icd10:O13.4'
       or dx_code_id = 'icd10:O13.5'
       or dx_code_id like 'icd10:O14.%4'
       or dx_code_id like 'icd10:O14.%5'
       or dx_code_id = 'icd10:O15'
       or dx_code_id like 'icd10:O15.%'
       or dx_code_id = 'icd10:O15.9'
       or dx_code_id = 'icd10:O16.4'
       or dx_code_id = 'icd10:O16.5'
       or dx_code_id like 'icd10:O20.%'
       or dx_code_id like 'icd10:O21.%'
       or dx_code_id like 'icd10:O22.%'
       or dx_code_id like 'icd10:O23.%'
       or dx_code_id like 'icd10:O24.%1'
       or dx_code_id like 'icd10:O25.1%'
       or dx_code_id like 'icd10:O26.0%'
       or dx_code_id like 'icd10:O26.1%'
       or dx_code_id like 'icd10:O26.3%'
       or dx_code_id like 'icd10:O26.4%'
       or dx_code_id like 'icd10:O26.5%'
       or dx_code_id like 'icd10:O26.60%'
       or dx_code_id like 'icd10:O26.61%'
       or dx_code_id like 'icd10:O26.64%'
       or dx_code_id like 'icd10:O26.65%'
       or dx_code_id like 'icd10:O26.66%'
       or dx_code_id like 'icd10:O26.67%'
       or dx_code_id like 'icd10:O26.68%'
       or dx_code_id like 'icd10:O26.69%'
       or dx_code_id like 'icd10:O26.70%'
       or dx_code_id like 'icd10:O26.71%'
       or dx_code_id like 'icd10:O26.74%'
       or dx_code_id like 'icd10:O26.75%'
       or dx_code_id like 'icd10:O26.76%'
       or dx_code_id like 'icd10:O26.77%'
       or dx_code_id like 'icd10:O26.78%'
       or dx_code_id like 'icd10:O26.79%'
       or dx_code_id like 'icd10:O26.8%'
       or dx_code_id like 'icd10:O26.9%'
       or dx_code_id like 'icd10:O28.%'
       or dx_code_id like 'icd10:O29.%'
       or dx_code_id like 'icd10:O31.8%'
       or dx_code_id like 'icd10:O34.%'
       or dx_code_id like 'icd10:O35.%'
       or dx_code_id like 'icd10:O36.1%'
       or dx_code_id like 'icd10:O36.2%'
       or dx_code_id like 'icd10:O36.3%'
       or dx_code_id like 'icd10:O36.5%'
       or dx_code_id like 'icd10:O36.6%'
       or dx_code_id like 'icd10:O36.81%'
       or dx_code_id like 'icd10:O36.82%'
       or dx_code_id like 'icd10:O36.83%'
       or dx_code_id like 'icd10:O36.84%'
       or dx_code_id like 'icd10:O36.85%'
       or dx_code_id like 'icd10:O36.86%'
       or dx_code_id like 'icd10:O36.88%'
       or dx_code_id like 'icd10:O36.89%'
       or dx_code_id like 'icd10:O37.%'
       or dx_code_id like 'icd10:O38.%'
       or dx_code_id like 'icd10:O39.%'
       or dx_code_id like 'icd10:O40.%'
       or dx_code_id like 'icd10:O41.%'
       or dx_code_id like 'icd10:O43.%'
       or dx_code_id like 'icd10:O44.%'
       or dx_code_id like 'icd10:O45.%'
       or dx_code_id like 'icd10:O46.%'
       or dx_code_id like 'icd10:O47.%'
       or dx_code_id like 'icd10:O48.%'
       or dx_code_id like 'icd10:O60.0%'
       or dx_code_id like 'icd10:O99.01%'
       or dx_code_id like 'icd10:O99.11%'
       or dx_code_id = 'icd10:O99.05'
       or dx_code_id = 'icd10:O99.15'
       or dx_code_id like 'icd10:O99.2%4'
       or dx_code_id like 'icd10:O99.2%5'
       or dx_code_id like 'icd10:O99.3%4'
       or dx_code_id like 'icd10:O99.3%5'
       or dx_code_id = 'icd10:O99.41'
       or dx_code_id = 'icd10:O99.51'
       or dx_code_id = 'icd10:O99.61'
       or dx_code_id = 'icd10:O99.71'
       or dx_code_id like 'icd10:O99.8%4'
       or dx_code_id like 'icd10:O99.8%5'
       or dx_code_id like 'icd10:O99.9%4'
       or dx_code_id like 'icd10:O99.9%5'
       or dx_code_id = 'icd10:O9A.11'
       or dx_code_id = 'icd10:O9A.21'
       or dx_code_id = 'icd10:O9A.31'
       or dx_code_id = 'icd10:O9A.41'
       or dx_code_id = 'icd10:O9A.51'
)
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);   
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:pregnancy:complications'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:pregnancy:complications:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--pregnancy:end-of-pregnancy-icd10
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:pregnancy:end-of-pregnancy-icd10';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like 'icd10:O10.%2'
       or dx_code_id like 'icd10:O10.%3'
       or dx_code_id like 'icd10:O11.%4'
       or dx_code_id like 'icd10:O11.%5'
       or dx_code_id like 'icd10:O12.%4'
       or dx_code_id like 'icd10:O12.%5'
       or dx_code_id like 'icd10:O13.%4'
       or dx_code_id like 'icd10:O13.%5'
       or dx_code_id like 'icd10:O14.%4'
       or dx_code_id like 'icd10:O14.%5'
       or dx_code_id = 'icd10:O15.1'
       or dx_code_id = 'icd10:O15.2'
       or dx_code_id like 'icd10:O16.%4'
       or dx_code_id like 'icd10:O16.%5'
       or dx_code_id like 'icd10:O24.%2'
       or dx_code_id like 'icd10:O24.%3'
       or dx_code_id like 'icd10:O25.%2'
       or dx_code_id like 'icd10:O25.%3'
       or dx_code_id = 'icd10:O26.62'
       or dx_code_id = 'icd10:O26.63'
       or dx_code_id = 'icd10:O26.72'
       or dx_code_id = 'icd10:O26.73'
       or dx_code_id like 'icd10:O42.%'
       or dx_code_id like 'icd10:O60.1%'
       or dx_code_id like 'icd10:O60.2%'
       or dx_code_id like 'icd10:O67.%'
       or dx_code_id like 'icd10:O68.%'
       or dx_code_id like 'icd10:O69.%'
       or dx_code_id like 'icd10:O70.%'
       or dx_code_id like 'icd10:O71.%'
       or dx_code_id like 'icd10:O72.%'
       or dx_code_id like 'icd10:O73.%'
       or dx_code_id like 'icd10:O74.%'
       or dx_code_id like 'icd10:O75.%'
       or dx_code_id like 'icd10:O76.%'
       or dx_code_id like 'icd10:O77.%'
       or dx_code_id like 'icd10:O78.%'
       or dx_code_id like 'icd10:O79.%'
       or dx_code_id like 'icd10:O80.%'
       or dx_code_id like 'icd10:O81.%'
       or dx_code_id like 'icd10:O82.%'
       or dx_code_id like 'icd10:O83.%'
       or dx_code_id like 'icd10:O84.%'
       or dx_code_id like 'icd10:O85.%'
       or dx_code_id like 'icd10:O86.%'
       or dx_code_id like 'icd10:O87.%'
       or dx_code_id like 'icd10:O88.%'
       or dx_code_id like 'icd10:O89.%'
       or dx_code_id like 'icd10:O90.%'
       or dx_code_id = 'icd10:O99.02'
       or dx_code_id = 'icd10:O99.03'
       or dx_code_id = 'icd10:O99.12'
       or dx_code_id = 'icd10:O99.13'
       or dx_code_id like 'icd10:O99.2%4'
       or dx_code_id like 'icd10:O99.2%5'
       or dx_code_id like 'icd10:O99.3%4'
       or dx_code_id like 'icd10:O99.3%5'
       or dx_code_id = 'icd10:O99.42'
       or dx_code_id = 'icd10:O99.43'
       or dx_code_id = 'icd10:O99.52'
       or dx_code_id = 'icd10:O99.53'
       or dx_code_id = 'icd10:O99.62'
       or dx_code_id = 'icd10:O99.63'
       or dx_code_id = 'icd10:O99.72'
       or dx_code_id = 'icd10:O99.73'
       or dx_code_id like 'icd10:O99.8%4'
       or dx_code_id like 'icd10:O99.8%5'
       or dx_code_id like 'icd10:O99.9%4'
       or dx_code_id like 'icd10:O99.9%5'
       or dx_code_id = 'icd10:O9A.12'
       or dx_code_id = 'icd10:O9A.13'
       or dx_code_id = 'icd10:O9A.22'
       or dx_code_id = 'icd10:O9A.23'
       or dx_code_id = 'icd10:O9A.32'
       or dx_code_id = 'icd10:O9A.33'
       or dx_code_id = 'icd10:O9A.42'
       or dx_code_id = 'icd10:O9A.43'
       or dx_code_id = 'icd10:O9A.52'
       or dx_code_id = 'icd10:O9A.53'
       or dx_code_id like 'icd10:Z37.%'
)
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);   
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:pregnancy:end-of-pregnancy-icd10'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:pregnancy:end-of-pregnancy-icd10:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--pregnancy:gestational-age
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:pregnancy:gestational-age';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id = 'icd10:Z3A.08'
       or dx_code_id = 'icd10:Z3A.09'
       or dx_code_id like 'icd10:Z3A.1%'
       or dx_code_id like 'icd10:Z3A.2%'
       or dx_code_id like 'icd10:Z3A.3%'
       or dx_code_id like 'icd10:Z3A.4%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:pregnancy:gestational-age'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:pregnancy:gestational-age:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';

--pregnancy:onset
drop table if exists temp_known_dx_evnt;
Select object_id 
into temporary temp_known_dx_evnt
from hef_event
where name = 'dx:pregnancy:onset';
drop table if exists temp_new_dx;
select distinct encounter_id 
into temporary temp_new_dx
from emr_encounter_dx_codes dx
where (dx_code_id like 'icd9:V22.%'
       or dx_code_id like 'icd9:V23.%'
       or dx_code_id like 'icd10:O09.%'
       or dx_code_id like 'icd10:O26.2%'
       or dx_code_id like 'icd10:O30.%'
       or dx_code_id = 'icd10:O31'
       or dx_code_id like 'icd10:O31.1%'
       or dx_code_id like 'icd10:O31.2%'
       or dx_code_id like 'icd10:O31.3%'
       or dx_code_id like 'icd10:O32.%'
       or dx_code_id like 'icd10:O33.%'
       or dx_code_id like 'icd10:O36.7%'
       or dx_code_id = 'icd10:Z33.1'
       or dx_code_id = 'icd10:Z33.3'
       or dx_code_id like 'icd10:Z34.%')
    and not exists (select null from temp_known_dx_evnt kdx where kdx.object_id=dx.encounter_id);   
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'dx:pregnancy:onset'::varchar(128) as name, 'urn:x-esphealth:disease:commoninf:pregnancy:onset:v1'::varchar(250) as source,
  e.date, current_timestamp as "timestamp", 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id
from emr_encounter e 
join temp_new_dx dx on e.id=dx.encounter_id
join django_content_type ct on app_label='emr' and model='encounter';


