--
-- Script setup section 
--

DROP TABLE IF EXISTS hiv_rpt.prep_hef_w_lab_details CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_cases_of_interest CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_gon_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_chlam_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_syph_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_wbpos_elisaposneg CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_gon_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_chlam_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_syph_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_syph_cases CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_cpts_of_interest CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_cases CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hebp_cases CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_cases CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_new_diag CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_meds CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_truvada_array CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_truvada_2mogap CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_truvada_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_all_details CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_dx_codes CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_diag_fields CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_rx_of_interest CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_rx_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_output_with_patient CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_output_pat_and_enc CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_ouput_pat_and_enc_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_diags CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_diags_2 CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_problem_list CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_ouput_pat_and_hiv_enc_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_combo_distinct CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_3_diff CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_3_diff_count CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_truvada_rx_and_pills CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hiv_meds_distinct CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_truvada_rx_and_pills_peryear CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_bicillin_all CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_bicillin_subset CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepb_labs_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_case_history CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_hepc_labs_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_herpes_direct_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_herpes_serolgy_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_herpes_direct_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_herpes_serology_counts CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_syph_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_syph_labs_events CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_amph CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_cocaine CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_fentanyl CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_labs CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_methadone CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_opiate CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_tox_med_asst_trmt CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_output_part_2 CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_sex_part_gend CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_alcohol_use CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_alcohol_oz_per_week CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_ill_drug_use CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_sexually_active CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_birth_control_method CASCADE;



DROP TABLE IF EXISTS hiv_rpt.prep_report_final_output CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_report_final_output_agetestpat_filter CASCADE;
DROP TABLE IF EXISTS hiv_rpt.prep_report_masked_patient_id CASCADE;

-- old report had 511864 patients

--
-- Script body 
--

-- Join - BASE Join the hef events with the lab results
-- OK
CREATE TABLE hiv_rpt.prep_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code 
FROM emr_labresult T1 
INNER JOIN hef_event T2 
ON ((T1.id = T2.object_id) AND (T1.patient_id = T2.patient_id))
WHERE T2.date >= '01-01-2006';


-- Query - BASE - Get the cases we are interested in 
-- OK
CREATE TABLE hiv_rpt.prep_cases_of_interest AS 
SELECT T1.condition, T1.date, T1.patient_id, EXTRACT(YEAR FROM date) rpt_year 
FROM nodis_case T1 
WHERE date >= '01-01-2006' 
AND date < '01-01-2017'
AND condition in ('hepatitis_c', 'hepatitis_b:acute', 'syphilis');

-- SQL - GONORRHEA - Gather up gonorrhea events
-- OK
-- Having multiple results on the same date is acceptable here as tests are done for multiple specimen sources and we want to capture both
CREATE TABLE hiv_rpt.prep_gon_events AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, specimen_source, native_code 
FROM hiv_rpt.prep_hef_w_lab_details 
WHERE name like 'lx:gonorrhea%'
AND date >= '01-01-2006';


-- SQL - CHLAMYDIA - Gather up chlamydia events
-- OK
-- Having multiple results on the same date is acceptable here as tests are done for multiple specimen sources and we want to capture both
CREATE TABLE hiv_rpt.prep_chlam_events AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, specimen_source, native_code 
FROM hiv_rpt.prep_hef_w_lab_details
WHERE name like 'lx:chlamydia%'
AND date >= '01-01-2006';


-- SYPH LABS
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
-- We want to limit to one test type per day
-- OK
CREATE TABLE hiv_rpt.prep_syph_labs AS 
SELECT T1.patient_id, T2.test_name, null::text as name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, collection_date
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm')
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND result_string is not null
AND date >= '01-01-2006'
GROUP BY test_name, patient_id, rpt_year, rpt_year_new, date, collection_date;

-- SYPH HEF EVENTS - Gather up HEF Events
-- We want to limit to one test type per day
-- Although, some tests do create a pos and neg on the same date. It's unfortunate but as good as gets for right now.
-- OK
CREATE TABLE hiv_rpt.prep_syph_events AS 
SELECT T1.patient_id, T2.test_name, T1.name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date
FROM hiv_rpt.prep_hef_w_lab_details T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm')
GROUP BY patient_id, rpt_year, rpt_year_new, name, date, test_name;

-- SYPH LABS & HEF TOGETHER
-- BRING HEPC LABS AND TESTS TOGETHER
-- AS FOR ONLY COUNTING ONE TEST PER DATE WHILE STILL COUNTING LABS THAT DIDN'T RESULT IN POS/NEG DETERMINATION, THIS IS AS GOOD AS WE GET FOR NOW
-- Need to match on date or collection_date since some hef events are tied to lab date and others to collection date
-- OK
CREATE TABLE hiv_rpt.prep_syph_labs_events AS 
select T1.patient_id, T1.test_name, coalesce(T2.name, T1.name) as name, coalesce(T2.rpt_year, T1.rpt_year) rpt_year, coalesce(T2.rpt_year_new, T1.rpt_year_new) rpt_year_new, coalesce(T2.date, T1.date) date
FROM hiv_rpt.prep_syph_labs T1
LEFT JOIN hiv_rpt.prep_syph_events T2 ON (T1.patient_id = T2.patient_id and T1.test_name = T2.test_name and ((T1.date = T2.date) or T1.collection_date = T2.date))
GROUP BY T1.patient_id, T1.test_name, coalesce(T2.name, T1.name), coalesce(T2.rpt_year, T1.rpt_year), coalesce(T2.rpt_year_new, T1.rpt_year_new), coalesce(T2.date, T1.date);


-- HEPC LABS
-- We want to limit to one test type per day
-- OK
CREATE TABLE hiv_rpt.prep_hepc_labs AS
SELECT T1.patient_id, T2.test_name, null::text as name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, collection_date
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna') 
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND result_string is not null
AND date >= '01-01-2006'
GROUP BY test_name, patient_id, rpt_year, rpt_year_new, date, collection_date;

-- HEPC HEF EVENTS
-- OK
CREATE TABLE hiv_rpt.prep_hepc_events AS 
SELECT T1.patient_id, T2.test_name, T1.name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date
FROM hiv_rpt.prep_hef_w_lab_details T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna') 
GROUP BY patient_id, rpt_year, rpt_year_new, name, date, test_name;

-- HEPC LABS AND HEF TOGETHER
-- AS FOR ONLY COUNTING ONE TEST PER DATE WHILE STILL COUNTING LABS THAT DIDN'T RESULT IN POS/NEG DETERMINATION, THIS IS AS GOOD AS WE GET FOR NOW
-- OK
CREATE TABLE hiv_rpt.prep_hepc_labs_events AS 
select T1.patient_id, T1.test_name, coalesce(T2.name, T1.name) as name, coalesce(T2.rpt_year, T1.rpt_year) rpt_year, coalesce(T2.rpt_year_new, T1.rpt_year_new) rpt_year_new, coalesce(T2.date, T1.date) date
FROM hiv_rpt.prep_hepc_labs T1
LEFT JOIN hiv_rpt.prep_hepc_events T2 ON (T1.patient_id = T2.patient_id and T1.test_name = T2.test_name and ((T1.date = T2.date) or T1.collection_date = T2.date))
GROUP BY T1.patient_id, T1.test_name, coalesce(T2.name, T1.name), coalesce(T2.rpt_year, T1.rpt_year), coalesce(T2.rpt_year_new, T1.rpt_year_new), coalesce(T2.date, T1.date);

-- HEPB LABS
-- We want to limit to one test type per day
-- OK
CREATE TABLE hiv_rpt.prep_hepb_labs AS
SELECT T1.patient_id, T2.test_name, null::text as name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, collection_date
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name in ('hepatitis_b_viral_dna', 'hepatitis_b_surface_antigen') 
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND result_string is not null
AND date >= '01-01-2006'
GROUP BY test_name, patient_id, rpt_year, rpt_year_new, date, collection_date;

-- HEPB HEF EVENTS
-- OK
CREATE TABLE hiv_rpt.prep_hepb_events AS 
SELECT T1.patient_id, T2.test_name, T1.name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date
FROM hiv_rpt.prep_hef_w_lab_details T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name in ('hepatitis_b_viral_dna', 'hepatitis_b_surface_antigen') 
GROUP BY patient_id, rpt_year, rpt_year_new, name, date, test_name;

-- HEPB LABS AND HEF TOGETHER
-- OK
CREATE TABLE hiv_rpt.prep_hepb_labs_events AS 
select T1.patient_id, T1.test_name, coalesce(T2.name, T1.name) as name, coalesce(T2.rpt_year, T1.rpt_year) rpt_year, coalesce(T2.rpt_year_new, T1.rpt_year_new) rpt_year_new, coalesce(T2.date, T1.date) date
FROM hiv_rpt.prep_hepb_labs T1
LEFT JOIN hiv_rpt.prep_hepb_events T2 ON (T1.patient_id = T2.patient_id and T1.test_name = T2.test_name and ((T1.date = T2.date) or T1.collection_date = T2.date))
GROUP BY T1.patient_id, T1.test_name, coalesce(T2.name, T1.name), coalesce(T2.rpt_year, T1.rpt_year), coalesce(T2.rpt_year_new, T1.rpt_year_new), coalesce(T2.date, T1.date);


-- HIV LABS
-- OK
CREATE TABLE hiv_rpt.prep_hiv_labs AS
SELECT T1.patient_id, T2.test_name, null::text as name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, collection_date
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
WHERE test_name ilike 'hiv_%' 
AND test_name != 'hiv not a test'
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND result_string is not null
AND date >= '01-01-2006'
GROUP BY test_name, patient_id, rpt_year, rpt_year_new, date, collection_date;


-- HIV - Gather up lab positive western blot and positive/negative elisa tests
-- OK
CREATE TABLE hiv_rpt.prep_wbpos_elisaposneg AS 
SELECT T1.patient_id,T1.name,T1.date 
FROM hiv_rpt.prep_hef_w_lab_details T1 
WHERE name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_elisa:negative');


-- GONORRHEA - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_gon_counts AS 
SELECT T1.patient_id,count(*) t_gon_tests, count(CASE WHEN rpt_year = '2006' THEN 1 END) t_gon_tests_06,
count(CASE WHEN rpt_year = '2007' THEN 1 END) t_gon_tests_07, count(CASE WHEN rpt_year = '2008' THEN 1 END) t_gon_tests_08,
count(CASE WHEN rpt_year = '2009' THEN 1 END) t_gon_tests_09, count(CASE WHEN rpt_year = '2010' THEN 1 END) t_gon_tests_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END) t_gon_tests_11, count(CASE WHEN rpt_year = '2012' THEN 1 END) t_gon_tests_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END) t_gon_tests_13, count(CASE WHEN rpt_year = '2014' THEN 1 END) t_gon_tests_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END) t_gon_tests_15, count(CASE WHEN rpt_year = '2016' THEN 1 END) t_gon_tests_16, 
count(CASE WHEN rpt_year = '2017' THEN 1 END) t_gon_tests_17, count(CASE WHEN rpt_year = '2018' THEN 1 END) t_gon_tests_18, 
count(CASE WHEN rpt_year = '2019' THEN 1 END) t_gon_tests_19,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_06,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_gon_tests_07, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_08,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_09, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_10,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_11, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_12,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_13, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_14, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_15, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_16, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_17, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_18, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_gon_tests_19,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2006' THEN 1 END) t_gon_tests_throat_06, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2007' THEN 1 END) t_gon_tests_throat_07, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2008' THEN 1 END) t_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2009' THEN 1 END) t_gon_tests_throat_09, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2010' THEN 1 END) t_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2011' THEN 1 END) t_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2012' THEN 1 END) t_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2013' THEN 1 END) t_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2014' THEN 1 END) t_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2015' THEN 1 END) t_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2016' THEN 1 END) t_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2017' THEN 1 END) t_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2018' THEN 1 END) t_gon_tests_throat_18, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2019' THEN 1 END) t_gon_tests_throat_19, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_throat_06, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_gon_tests_throat_07, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_throat_09, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_throat_18, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_gon_tests_throat_19, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2006' THEN 1 END) t_gon_tests_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2007' THEN 1 END) t_gon_tests_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2008' THEN 1 END) t_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2009' THEN 1 END) t_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2010' THEN 1 END) t_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2011' THEN 1 END) t_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2012' THEN 1 END) t_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2013' THEN 1 END) t_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2014' THEN 1 END) t_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2015' THEN 1 END) t_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2016' THEN 1 END) t_gon_tests_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2017' THEN 1 END) t_gon_tests_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2018' THEN 1 END) t_gon_tests_rectal_18, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2019' THEN 1 END) t_gon_tests_rectal_19, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_gon_tests_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_rectal_18, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_gon_tests_rectal_19
FROM hiv_rpt.prep_gon_events T1 
GROUP BY T1.patient_id;

-- CHLAMYDIA - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_chlam_counts AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END) t_chlam_06, count(CASE WHEN rpt_year = '2007' THEN 1 END) t_chlam_07, 
count(CASE WHEN rpt_year = '2008' THEN 1 END) t_chlam_08, count(CASE WHEN rpt_year = '2009' THEN 1 END) t_chlam_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END) t_chlam_10, count(CASE WHEN rpt_year = '2011' THEN 1 END) t_chlam_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END) t_chlam_12, count(CASE WHEN rpt_year = '2013' THEN 1 END) t_chlam_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END) t_chlam_14,count(CASE WHEN rpt_year = '2015' THEN 1 END) t_chlam_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END) t_chlam_16, count(CASE WHEN rpt_year = '2017' THEN 1 END) t_chlam_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END) t_chlam_18,count(CASE WHEN rpt_year = '2019' THEN 1 END) t_chlam_19, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_06, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_07,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_08, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_09,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_11,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_13,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_15,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_17,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_chlam_18, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_chlam_19,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2006' THEN 1 END) t_chlam_throat_06,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2007' THEN 1 END) t_chlam_throat_07,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2008' THEN 1 END) t_chlam_throat_08,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2009' THEN 1 END) t_chlam_throat_09,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2010' THEN 1 END) t_chlam_throat_10,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2011' THEN 1 END) t_chlam_throat_11,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2012' THEN 1 END) t_chlam_throat_12,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2013' THEN 1 END) t_chlam_throat_13,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2014' THEN 1 END) t_chlam_throat_14,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2015' THEN 1 END) t_chlam_throat_15, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2016' THEN 1 END) t_chlam_throat_16, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2017' THEN 1 END) t_chlam_throat_17, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2018' THEN 1 END) t_chlam_throat_18, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and rpt_year = '2019' THEN 1 END) t_chlam_throat_19, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_throat_06, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_throat_07, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_throat_08, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_throat_09, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_throat_10, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_throat_11, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_throat_12, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_throat_13, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_throat_14, 
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_throat_15,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_throat_16,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_throat_17,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_chlam_throat_18,
count(CASE WHEN T1.specimen_source ~* '^(THROAT)' and T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_chlam_throat_19,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2006' THEN 1 END) t_chlam_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2007' THEN 1 END) t_chlam_rectal_07,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2008' THEN 1 END) t_chlam_rectal_08,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2009' THEN 1 END) t_chlam_rectal_09,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2010' THEN 1 END) t_chlam_rectal_10,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2011' THEN 1 END) t_chlam_rectal_11,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2012' THEN 1 END) t_chlam_rectal_12,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2013' THEN 1 END) t_chlam_rectal_13,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2014' THEN 1 END) t_chlam_rectal_14,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2015' THEN 1 END) t_chlam_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2016' THEN 1 END) t_chlam_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2017' THEN 1 END) t_chlam_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2018' THEN 1 END) t_chlam_rectal_18, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and rpt_year = '2019' THEN 1 END) t_chlam_rectal_19, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)'and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)'and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_rectal_17,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_chlam_rectal_18,
count(CASE WHEN T1.specimen_source ~* '^(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_chlam_rectal_19
FROM hiv_rpt.prep_chlam_events T1 
GROUP BY T1.patient_id;

-- SYPHILLIS - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_syph_counts AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END) t_syph_06, 
count(CASE WHEN rpt_year = '2007' THEN 1 END) t_syph_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END) t_syph_08, 
count(CASE WHEN rpt_year = '2009' THEN 1 END) t_syph_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END) t_syph_10, 
count(CASE WHEN rpt_year = '2011' THEN 1 END) t_syph_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END) t_syph_12, 
count(CASE WHEN rpt_year = '2013' THEN 1 END) t_syph_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END) t_syph_14, 
count(CASE WHEN rpt_year = '2015' THEN 1 END) t_syph_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END) t_syph_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END) t_syph_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END) t_syph_18,
count(CASE WHEN rpt_year = '2019' THEN 1 END) t_syph_19,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_syph_06,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_syph_07,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_syph_08,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_syph_09,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_syph_10,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_syph_11,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_syph_12,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_syph_13,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_syph_14,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_syph_15, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_syph_16,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_syph_17, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_syph_18, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2019' THEN 1 END) p_syph_19  
FROM hiv_rpt.prep_syph_labs_events T1 
GROUP BY T1.patient_id;

-- SYPHILIS - Cases
-- OK
CREATE TABLE hiv_rpt.prep_syph_cases AS 
SELECT T1.patient_id, 
max(CASE WHEN T1.rpt_year = '2006' then 1 end) syphilis_06, 
max(CASE WHEN T1.rpt_year = '2007' then 1 end) syphilis_07,
max(CASE WHEN T1.rpt_year = '2008' then 1 end) syphilis_08, 
max(CASE WHEN T1.rpt_year = '2009' then 1 end) syphilis_09,
max(CASE WHEN T1.rpt_year = '2010' then 1 end) syphilis_10,
max(CASE WHEN T1.rpt_year = '2011' then 1 end) syphilis_11,
max(CASE WHEN T1.rpt_year = '2012' then 1 end) syphilis_12, 
max(CASE WHEN T1.rpt_year = '2013' then 1 end) syphilis_13,
max(CASE WHEN T1.rpt_year = '2014' then 1 end) syphilis_14, 
max(CASE WHEN T1.rpt_year = '2015' then 1 end) syphilis_15, 
max(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_16,
max(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_17,
max(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_18,
max(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_19
FROM hiv_rpt.prep_cases_of_interest T1 
WHERE condition = 'syphilis' GROUP BY T1.patient_id;

-- CPT CODES - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_cpts_of_interest AS 
SELECT T1.patient_id, 
max(CASE WHEN T1.native_code like '86631%' then 1 end) ser_test_for_lgv_ever,
max(CASE WHEN T1.native_code like '88160%' then 1 end) anal_cytology_test_ever 
FROM emr_labresult T1 
WHERE native_code like '86631%' 
OR native_code like '88160%' 
GROUP BY T1.patient_id;

-- HEP C - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_hepc_counts AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2006' THEN 1 END) t_hcv_anti_06,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2007' THEN 1 END) t_hcv_anti_07,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2008' THEN 1 END) t_hcv_anti_08,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2009' THEN 1 END) t_hcv_anti_09,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2010' THEN 1 END) t_hcv_anti_10,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2011' THEN 1 END) t_hcv_anti_11,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2012' THEN 1 END) t_hcv_anti_12,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2013' THEN 1 END) t_hcv_anti_13,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2014' THEN 1 END) t_hcv_anti_14,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2015' THEN 1 END) t_hcv_anti_15,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2016' THEN 1 END) t_hcv_anti_16,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2017' THEN 1 END) t_hcv_anti_17,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2018' THEN 1 END) t_hcv_anti_18,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2019' THEN 1 END) t_hcv_anti_19,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2006' THEN 1 END) t_hcv_rna_06,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2007' THEN 1 END) t_hcv_rna_07,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2008' THEN 1 END) t_hcv_rna_08,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2009' THEN 1 END) t_hcv_rna_09,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2010' THEN 1 END) t_hcv_rna_10,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2011' THEN 1 END) t_hcv_rna_11,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2012' THEN 1 END) t_hcv_rna_12,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2013' THEN 1 END) t_hcv_rna_13,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2014' THEN 1 END) t_hcv_rna_14,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2015' THEN 1 END) t_hcv_rna_15,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2016' THEN 1 END) t_hcv_rna_16,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2017' THEN 1 END) t_hcv_rna_17,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2018' THEN 1 END) t_hcv_rna_18,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2019' THEN 1 END) t_hcv_rna_19,
max (CASE WHEN T1.name like '%elisa:positive%' or name like '%rna:positive%' THEN 1 END) hcv_anti_or_rna_pos,
min(CASE WHEN T1.name like '%elisa:positive%' THEN T1.rpt_year END) hcv_anti_pos_date,
min(CASE WHEN T1.name like '%rna:positive%' THEN T1.rpt_year END) hcv_rna_pos_date 
FROM hiv_rpt.prep_hepc_labs_events T1
GROUP BY T1.patient_id;


-- HEP C CASES
-- Get all of the history events and details for all types of HepC cases
-- Identify the last history event date for the case
-- OK
CREATE TABLE hiv_rpt.prep_hepc_case_history as
SELECT T1.patient_id, T1.id, T1.condition, T1.date as case_date, T2.date as history_date, T2.status, T1.isactive, max_case_event_date 
FROM nodis_case T1, 
nodis_caseactivehistory T2,
(SELECT case_id, max(date) max_case_event_date 
FROM nodis_caseactivehistory WHERE  date < '01-01-2020' and status ilike 'HEP_C%' group by case_id) T3
WHERE T1.date < '01-01-2020' 
AND condition = 'hepatitis_c'
AND T1.id = T2.case_id 
AND T1.id = T3.case_id
order by T2.case_id;

-- HEP C ACUTE CASES
-- Get details only for cases that are and remain HepC Acute
-- OK
CREATE  TABLE hiv_rpt.prep_hepc_cases AS 
SELECT T1.patient_id, T1.id as case_id, T1.status, T1.condition, EXTRACT(YEAR FROM T1.history_date) acute_hepc_per_esp_date, 1 acute_hepc_per_esp
FROM hiv_rpt.prep_hepc_case_history T1
WHERE max_case_event_date = history_date
AND status in ('HEP_C-A');


-- HEP B - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_hepb_counts AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2006' THEN 1 END) t_hepb_antigen_06,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2007' THEN 1 END) t_hepb_antigen_07,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2008' THEN 1 END) t_hepb_antigen_08,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2009' THEN 1 END) t_hepb_antigen_09,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2010' THEN 1 END) t_hepb_antigen_10,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2011' THEN 1 END) t_hepb_antigen_11,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2012' THEN 1 END) t_hepb_antigen_12,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2013' THEN 1 END) t_hepb_antigen_13,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2014' THEN 1 END) t_hepb_antigen_14,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2015' THEN 1 END) t_hepb_antigen_15,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2016' THEN 1 END) t_hepb_antigen_16,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2017' THEN 1 END) t_hepb_antigen_17,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2018' THEN 1 END) t_hepb_antigen_18,
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2019' THEN 1 END) t_hepb_antigen_19,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2006' THEN 1 END) t_hepb_dna_06,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2007' THEN 1 END) t_hepb_dna_07,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2008' THEN 1 END) t_hepb_dna_08,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2009' THEN 1 END) t_hepb_dna_09,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2010' THEN 1 END) t_hepb_dna_10,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2011' THEN 1 END) t_hepb_dna_11,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2012' THEN 1 END) t_hepb_dna_12,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2013' THEN 1 END) t_hepb_dna_13,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2014' THEN 1 END) t_hepb_dna_14,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2015' THEN 1 END) t_hepb_dna_15,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2016' THEN 1 END) t_hepb_dna_16,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2017' THEN 1 END) t_hepb_dna_17,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2018' THEN 1 END) t_hepb_dna_18,
count(CASE WHEN T1.test_name like '%dna%' and T1.rpt_year = '2019' THEN 1 END) t_hepb_dna_19,
max(CASE WHEN T1.name like '%surface_antigen:positive%' or name like '%dna:positive%' THEN 1 END) hepb_pos_antigen_or_dna,
min(CASE WHEN T1.name like '%surface_antigen:positive%' or name like '%dna:positive%' THEN T1.rpt_year END) hepb_pos_antigen_or_dna_date 
FROM hiv_rpt.prep_hepb_labs_events T1
GROUP BY T1.patient_id;

-- HEP B - Cases
-- OK
CREATE TABLE hiv_rpt.prep_hebp_cases AS 
SELECT T1.patient_id, 1 acute_hepb_per_esp, min(rpt_year) acute_hepb_per_esp_date 
FROM hiv_rpt.prep_cases_of_interest T1 
WHERE condition = 'hepatitis_b:acute' 
GROUP BY T1.patient_id;

-- HEP B Diagnosis Codes
-- OK
CREATE TABLE hiv_rpt.prep_hepb_diags AS 
SELECT T1.id, T1.encounter_id, T1.dx_code_id 
FROM emr_encounter_dx_codes T1
WHERE 
(dx_code_id ~ '^(icd9:070.2)' or dx_code_id ~ '^(icd9:070.3)' or dx_code_id ~ '^(icd10:B18.0)'
or dx_code_id ~ '^(icd10:B18.1)' or dx_code_id ~ '^(icd10:B19.1)');

-- HEP B Diagnosis Codes - Faster as 2 queries
-- OK
CREATE TABLE hiv_rpt.prep_hepb_diags_2 AS
select patient_id, 1::int hist_of_hep_b
from hiv_rpt.prep_hepb_diags T1,
emr_encounter T2
where T1.encounter_id = T2.id
and T2.date < '01-01-2020'
group by patient_id;

-- BASE Gather up dx_codes 
-- OK
CREATE TABLE hiv_rpt.prep_dx_codes AS 
SELECT * FROM emr_encounter_dx_codes 
WHERE dx_code_id ~'^(icd9:796.7|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.|icd9:099.4|icd9:054.1|icd10:A63.|icd9:304.0|icd10:F11.|icd9:304.1|icd10:F13.|icd9:304.2|icd10:F14.|icd9:304.4|icd10:F15.|icd10:F19.|icd9:304.9|icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd9:647.2|icd10:O98.3|icd10:Z04.4|icd10:T74.21|icd10:T76.21|icd10:T74.11|icd10:T76.11|icd10:T74.31|icd10:T74.22|icd9:305.0|icd10:F10.1|icd9:305.5|icd10:F11.1|icd9:305.2|icd10:F12.1|icd9:305.4|icd10:F13.1|icd9:305.6|icd10:F14.1|icd9:305.7|icd10:F15.1|icd9:305.3|icd10:F16.1|icd9:305.9|icd10:F18.1|icd9:292.8|icd10:F19.1|icd10:A60.0|icd10:T74.91|icd10:T74.22|icd10:Y04.8|icd10:R85.61|icd10:N70.|icd10:F64.|icd10:N73.|icd10:O98.7|icd10:Z72.5)' 
OR dx_code_id in (
	'icd10:D01.3',
	'icd9:569.44',
	'icd9:230.5',
	'icd9:230.6',
	'icd9:097.9',
	'icd10:A52,7',
	'icd9:091.1',
	'icd9:098.7',
	'icd10:A54.6',
	'icd9:098.6',
	'icd10:A54.5', 
	'icd9:099.52', 
	'icd10:A56.3', 
	'icd9:099.51', 
	'icd10:A56.4', 
	'icd9:099.1', 
	'icd10:A55', 
	'icd9:099.0', 
	'icd10:A57', 
	'icd9:099.2', 
	'icd10:A58', 
	'icd10:N34.1', 
	'icd9:054.8', 
	'icd9:054.9', 
	'icd9:054.79', 
	'icd10:A60.1', 
	'icd10:A60.9', 
	'icd10:A60.0', 
	'icd9:078.11', 
	'icd10:A63.0', 
	'icd9:569.41', 
	'icd10:A64', 
	'icd9:614', 
	'icd9:614.1', 
	'icd9:614.2', 
	'icd9:614.3', 
	'icd9:614.5', 
	'icd9:614.9', 
	'icd10:N72', 
	'icd9:V01.6', 
	'icd10:Z20.2', 
	'icd10:Z20.6', 
	'icd9:V69.2', 
	'icd9:V65.44', 
	'icd10:Z71.7', 
	'icd9:307.1', 
	'icd10:F50.0', 
	'icd9:307.51', 
	'icd10:F50.2', 
	'icd9:307.50', 
	'icd10:F50.9', 
	'icd9:302.0', 
	'icd10:F66.0', 
	'icd9:302.5', 
	'icd9:302.50', 
	'icd9:302.51', 
	'icd9:302.52', 
	'icd9:302.53', 
	'icd9:V61.21', 
	'icd9:937', 
	'icd10:T18.5', 
	'icd9:305.00', 
	'icd9:305.01', 
	'icd9:305.02', 
	'icd9:303.90', 
	'icd9:303.91', 
	'icd9:303.92', 
	'icd10:F10.2', 
	'icd10:Z61.4', 
	'icd10:Z61.5',
	'icd9:V62.5',
	'icd10:Z65.1',
	'icd10:Z65.2',
	'icd9:V71.5',
	'icd9:995.83',
	'icd9:909.9',
	'icd9:V58.89',
	'icd9:V71.81',
	'icd9:995.80',
	'icd9:995.81',
	'icd9:995.82',
	'icd9:E960.1',
	'icd10:B00.9',
	'icd9:635.90',
	'icd10:Z33.2',
	'icd9:V25.03',
	'icd10:Z30.012',
	'icd9:995.53',
	'icd10:Z87.890',
	'icd9:079.53',
	'icd10:Z11.4',
	'icd10:Z20.5',
	'icd10:Z77.21',
	'icd10:K62.82',
	'icd10:R85.81',
	'icd10:R85.82',
	'icd9:099.8',
	'icd9:099.9',
	'icd10:F65.1',
	'icd9:302.6',
	'icd9:302.85',
	'icd10:Z69.010',
	'icd10:T76.22',
	'icd10:A53.0',
	'icd10:A53.9',
	'icd9:097.1',
	'icd10:K62.6'
);

set enable_nestloop=off;


-- MODIFIED - DIAGNOSES - Create fields
-- OK
CREATE TABLE hiv_rpt.prep_diag_fields AS 
SELECT T1.patient_id, 
min(case when T2.dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' or T2.dx_code_id in ('icd9:569.44','icd10:K62.82','icd9:230.5','icd9:230.6','icd10:D01.3','icd10:R85.81','icd10:R85.82','icd10:A51.1','icd9:091.1') then to_char(T1.date, 'yyyy') end) anal_cyt_dysp_carci_hpv_syph,
min(case when T2.dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
	or T2.dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then to_char(T1.date, 'yyyy') end) syphillis_of_any_site_or_stage_except_late,
min(case when T2.dx_code_id in ('icd9:098.7','icd10:A54.6') then to_char(T1.date, 'yyyy') end) gonococcal_infection_of_anus_and_rectum,
min(case when T2.dx_code_id in ('icd9:098.6','icd10:A54.5') then to_char(T1.date, 'yyyy') end) gonococcal_pharyngitis,
min(case when T2.dx_code_id in ('icd9:099.52','icd10:A56.3') then to_char(T1.date, 'yyyy') end) chlamydial_infection_of_anus_and_recturm,
min(case when T2.dx_code_id in ('icd9:099.51','icd10:A56.4') then to_char(T1.date, 'yyyy') end) chlamydial_infection_of_pharynx,
min(case when T2.dx_code_id in ('icd9:099.1','icd10:A55') then to_char(T1.date, 'yyyy') end) lymphgranuloma_venereum,
min(case when T2.dx_code_id in ('icd9:099.0','icd10:A57') then to_char(T1.date, 'yyyy') end) chancroid,
min(case when T2.dx_code_id in ('icd9:099.2', 'icd10:A58') then to_char(T1.date, 'yyyy') end) granuloma_inguinale,
min(case when T2.dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1') then to_char(T1.date, 'yyyy') end) nongonococcal_urethritis,
min(case when T2.dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') then to_char(T1.date, 'yyyy') end) herpes_simplex_w_complications,
min(case when T2.dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' then to_char(T1.date, 'yyyy') end) genital_herpes,
min(case when T2.dx_code_id in ('icd9:078.11', 'icd10:A63.0') then to_char(T1.date, 'yyyy') end) anogenital_warts,
min(case when T2.dx_code_id in ('icd9:569.41', 'icd10:K62.6') then to_char(T1.date, 'yyyy') end) anorectal_ulcer,
min(case when T2.dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9') then to_char(T1.date, 'yyyy') end) unspecified_std,
min(case when T2.dx_code_id ~ '^(icd10:N70.|icd10:N73.)' or T2.dx_code_id in ('icd9:614', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9','icd10:N72') then to_char(T1.date, 'yyyy') end) pelvic_inflammatory_disease,
min(case when T2.dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then to_char(T1.date, 'yyyy') end) contact_with_or_exposure_to_venereal_disease,
min(case when T2.dx_code_id ~ '^(icd10:Z72.5)' or T2.dx_code_id in ('icd9:V69.2') then to_char(T1.date, 'yyyy') end) high_risk_sexual_behavior,
min(case when T2.dx_code_id in ('icd9:V65.44','icd10:Z71.7') then to_char(T1.date, 'yyyy') end) hiv_counseling,
min(case when T2.dx_code_id in ('icd9:307.1','icd10:F50.0') then to_char(T1.date, 'yyyy') end) anorexia_nervosa,
min(case when T2.dx_code_id in ('icd9:307.51','icd10:F50.2') then to_char(T1.date, 'yyyy') end) bulimia_nervosa,
min(case when T2.dx_code_id in ('icd9:307.50','icd10:F50.9') then to_char(T1.date, 'yyyy') end) eating_disorder_nos,
min(case when T2.dx_code_id ~ '^(icd10:F64.)' or T2.dx_code_id in ('icd9:302.0', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F65.1', 'icd10:Z87.890', 'icd9:302.6', 'icd9:302.85') then to_char(T1.date, 'yyyy') end) gend_iden_trans_sex_reassign,
min(case when T2.dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z69.010') then to_char(T1.date, 'yyyy') end) counseling_for_child_sexual_abuse,
min(case when T2.dx_code_id in ('icd9:937', 'icd10:T18.5') then to_char(T1.date, 'yyyy') end) foreign_body_in_anus,
min(case when T2.dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or T2.dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2') then to_char(T1.date, 'yyyy') end) alcohol_dependence_abuse,
min(case when T2.dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' then to_char(T1.date, 'yyyy') end) opioid_dependence_abuse,
min(case when T2.dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' then to_char(T1.date, 'yyyy') end) sed_hypn_anxio_depend_abuse,
min(case when T2.dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' then to_char(T1.date, 'yyyy') end) cocaine_dependence_abuse,
min(case when T2.dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' then to_char(T1.date, 'yyyy') end) amphet_stim_dependence_abuse,
min(case when T2.dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' then to_char(T1.date, 'yyyy') end) oth_psycho_or_unspec_subs_depend_abuse,
min(case when T2.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' then to_char(T1.date, 'yyyy') end) hiv_first_icd_year,
min(case when T2.dx_code_id in ('icd9:V62.5', 'icd10:Z65.1') then to_char(T1.date, 'yyyy') end) incarceration,
min(case when T2.dx_code_id in ('icd9:V62.5','icd10:Z65.2') then to_char(T1.date, 'yyyy') end) prison_release_probs,
min(case when T2.dx_code_id ~ '^(icd9:647.2|icd10:O98.3)' then to_char(T1.date, 'yyyy') end) oth_sex_inf_with_preg,
min(case when T2.dx_code_id in ('icd9:V71.5') or T2.dx_code_id ~ '^(icd10:Z04.4)' then to_char(T1.date, 'yyyy') end) enc_after_alleged_r_sa_or_batt,
min(case when T2.dx_code_id in ('icd9:995.83', 'icd10:T74.21XA') then to_char(T1.date, 'yyyy') end) adult_sexual_abuse,
min(case when T2.dx_code_id in ('icd9:909.9', 'icd10:T74.21XS') then to_char(T1.date, 'yyyy') end) adult_sexual_abuse_sequela,
min(case when T2.dx_code_id in ('icd9:V58.89', 'icd10:T74.21XD') then to_char(T1.date, 'yyyy') end) adult_sexual_abuse_subsequent,
min(case when T2.dx_code_id in ('icd9:V71.81') or T2.dx_code_id ~ '^(icd10:T76.21)' then to_char(T1.date, 'yyyy') end) adult_sexual_abuse_suspected,
min(case when T2.dx_code_id in ('icd9:995.80') or T2.dx_code_id ~ '^(icd10:T74.91)' then to_char(T1.date, 'yyyy') end) adult_maltreatment,
min(case when T2.dx_code_id in ('icd9:995.81') or T2.dx_code_id ~ '^(icd10:T74.11|icd10:T76.11)' then to_char(T1.date, 'yyyy') end) adult_physical_abuse,
min(case when T2.dx_code_id in ('icd9:995.82') or T2.dx_code_id ~ '^(icd10:T74.31)' then to_char(T1.date, 'yyyy') end) adult_emotional_abuse,
min(case when T2.dx_code_id in ('icd9:995.53', 'icd10:T76.22') or T2.dx_code_id ~ '^(icd10:T74.22)' then to_char(T1.date, 'yyyy') end) child_sexual_abuse,
min(case when T2.dx_code_id in ('icd9:E960.1') or T2.dx_code_id ~ '^(icd10:Y04.8)' then to_char(T1.date, 'yyyy') end) rape_aslt_bod_force,
min(case when T2.dx_code_id ~ '^(icd9:305.2|icd10:F12.1)' then to_char(T1.date, 'yyyy') end) cannabis_abuse,
min(case when T2.dx_code_id ~ '^(icd9:305.3|icd10:F16.1)' then to_char(T1.date, 'yyyy') end) hallucinogen_abuse,
min(case when T2.dx_code_id ~ '^(icd9:305.9|icd10:F18.1)' then to_char(T1.date, 'yyyy') end) inhalent_abuse,
min(case when T2.dx_code_id in ('icd9:635.90', 'icd10:Z33.2') then to_char(T1.date, 'yyyy') end) elec_abort_in_first_sec_tri,
min(case when T2.dx_code_id in ('icd9:V25.03', 'icd10:Z30.012') then to_char(T1.date, 'yyyy') end) emergency_contracep,
min(case when T2.dx_code_id in ('icd10:Z11.4') then to_char(T1.date, 'yyyy') end) enc_for_hiv_screen,
min(case when T2.dx_code_id in ('icd10:Z20.5') then to_char(T1.date, 'yyyy') end) contact_w_viral_hepatitis,
min(case when T2.dx_code_id in ('icd10:Z77.21') then to_char(T1.date, 'yyyy') end) contact_w_pot_hzrd_bod_fluids
FROM emr_encounter T1 INNER JOIN hiv_rpt.prep_dx_codes T2 ON ((T1.id = T2.encounter_id)) 
WHERE date < '01-01-2020'
GROUP BY T1.patient_id;

set enable_nestloop=on;


-- HIV - Lab Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_hiv_counts AS 
SELECT count(*) total_hiv_tests, 
count(CASE WHEN test_name = 'hiv_rna_viral' then 1 END) total_hiv_rna_tests, T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END) t_hiv_06, count(CASE WHEN rpt_year = '2007' THEN 1 END) t_hiv_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END) t_hiv_08, count(CASE WHEN rpt_year = '2009' THEN 1 END) t_hiv_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END) t_hiv_10, count(CASE WHEN rpt_year = '2011' THEN 1 END) t_hiv_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END) t_hiv_12, count(CASE WHEN rpt_year = '2013' THEN 1 END) t_hiv_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END) t_hiv_14, count(CASE WHEN rpt_year = '2015' THEN 1 END) t_hiv_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END) t_hiv_16, count(CASE WHEN rpt_year = '2017' THEN 1 END) t_hiv_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END) t_hiv_18, count(CASE WHEN rpt_year = '2019' THEN 1 END) t_hiv_19,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2006' THEN 1 END) t_hiv_elisa_06,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2007' THEN 1 END) t_hiv_elisa_07,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2008' THEN 1 END) t_hiv_elisa_08,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2009' THEN 1 END) t_hiv_elisa_09,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2010' THEN 1 END) t_hiv_elisa_10,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2011' THEN 1 END) t_hiv_elisa_11,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2012' THEN 1 END) t_hiv_elisa_12,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2013' THEN 1 END) t_hiv_elisa_13,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2014' THEN 1 END) t_hiv_elisa_14,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2015' THEN 1 END) t_hiv_elisa_15,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2016' THEN 1 END) t_hiv_elisa_16,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2017' THEN 1 END) t_hiv_elisa_17,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2018' THEN 1 END) t_hiv_elisa_18,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2019' THEN 1 END) t_hiv_elisa_19,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2006' THEN 1 END) t_hiv_wb_06,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2007' THEN 1 END) t_hiv_wb_07,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2008' THEN 1 END) t_hiv_wb_08,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2009' THEN 1 END) t_hiv_wb_09,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2010' THEN 1 END) t_hiv_wb_10,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2011' THEN 1 END) t_hiv_wb_11,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2012' THEN 1 END) t_hiv_wb_12,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2013' THEN 1 END) t_hiv_wb_13,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2014' THEN 1 END) t_hiv_wb_14,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2015' THEN 1 END) t_hiv_wb_15,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2016' THEN 1 END) t_hiv_wb_16,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2017' THEN 1 END) t_hiv_wb_17,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2018' THEN 1 END) t_hiv_wb_18,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2019' THEN 1 END) t_hiv_wb_19,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2006' THEN 1 END) t_hiv_rna_06,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2007' THEN 1 END) t_hiv_rna_07,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2008' THEN 1 END) t_hiv_rna_08,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2009' THEN 1 END) t_hiv_rna_09,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2010' THEN 1 END) t_hiv_rna_10,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2011' THEN 1 END) t_hiv_rna_11,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2012' THEN 1 END) t_hiv_rna_12,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2013' THEN 1 END) t_hiv_rna_13,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2014' THEN 1 END) t_hiv_rna_14,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2015' THEN 1 END) t_hiv_rna_15,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2016' THEN 1 END) t_hiv_rna_16, 
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2017' THEN 1 END) t_hiv_rna_17, 
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2018' THEN 1 END) t_hiv_rna_18, 
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2019' THEN 1 END) t_hiv_rna_19, 
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2006' THEN 1 END) t_hiv_agab_06,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2007' THEN 1 END) t_hiv_agab_07,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2008' THEN 1 END) t_hiv_agab_08,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2009' THEN 1 END) t_hiv_agab_09,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2010' THEN 1 END) t_hiv_agab_10,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2011' THEN 1 END) t_hiv_agab_11,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2012' THEN 1 END) t_hiv_agab_12,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2013' THEN 1 END) t_hiv_agab_13,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2014' THEN 1 END) t_hiv_agab_14,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2015' THEN 1 END) t_hiv_agab_15,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2016' THEN 1 END) t_hiv_agab_16,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2017' THEN 1 END) t_hiv_agab_17,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2018' THEN 1 END) t_hiv_agab_18,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2019' THEN 1 END) t_hiv_agab_19
FROM hiv_rpt.prep_hiv_labs T1 
GROUP BY T1.patient_id;

-- HIV - Cases
-- OK
CREATE TABLE hiv_rpt.prep_hiv_cases AS 
SELECT T1.patient_id, 1::int hiv_per_esp, min(EXTRACT(YEAR FROM date)) hiv_per_esp_date, T1.date
FROM nodis_case T1
WHERE condition = 'hiv' 
AND date < '01-01-2020'
GROUP BY T1.patient_id,T1.date;


-- HIV - New Diagnosis Computation
-- OK
CREATE TABLE hiv_rpt.prep_hiv_new_diag AS 
SELECT T2.patient_id, 
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive') and T1.date <= T2.date then 1 
		 when T1.name = 'lx:hiv_elisa:negative' and T1.date >= (T2.date - INTERVAL '2 years') then 1 else 0 end) new_hiv_diagnosis 
FROM hiv_rpt.prep_wbpos_elisaposneg T1 
RIGHT OUTER JOIN hiv_rpt.prep_hiv_cases T2 ON ((T1.patient_id = T2.patient_id)) 
GROUP BY T2.patient_id;

-- HIV - Medications
-- OK
CREATE TABLE hiv_rpt.prep_hiv_meds AS 
SELECT T1.patient_id,
T1.name,
split_part(T1.name, ':', 2) stripped_name,
T1.date,
case when T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end truvada_rx,
case when T1.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1) ELSE 1 END refills_mod,
1::int hiv_meds,
string_to_array(replace(T1.name, 'rx:hiv_',''), '-') test4,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-')::text test5,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-') med_array,
quantity,
quantity_float,
quantity_type,
EXTRACT(YEAR FROM T1.date) rpt_year
FROM hef_event T1 
INNER JOIN emr_prescription T2 
ON ((T1.object_id = T2.id)) 
WHERE T1.name ilike 'rx:hiv_%'; 

-- HIV - Meds - Patient Id's Only
-- OK
CREATE TABLE hiv_rpt.prep_hiv_meds_distinct AS
SELECT T1.patient_id
FROM hiv_rpt.prep_hiv_meds T1
GROUP BY T1.patient_id;

-- HIV - Truvada Array - For those with more than 2 rx (includes refills)
-- OK
CREATE TABLE hiv_rpt.prep_truvada_array AS 
SELECT T1.patient_id, T1.rpt_year, sum(T1.refills_mod) total_truvada_rx,
array_agg(T1.date ORDER BY T1.date) truvada_array 
FROM hiv_rpt.prep_hiv_meds T1 
WHERE truvada_rx = 1 
GROUP BY T1.patient_id,T1.rpt_year 
HAVING sum(T1.refills_mod) >=2;

-- Truvada - Find those that have prescriptions 2 or more months apart in the same year
-- OK
CREATE TABLE hiv_rpt.prep_truvada_2mogap AS 
SELECT T1.patient_id, 
T1.truvada_array[1] first_rx,
truvada_array[array_length(truvada_array, 1)] final_rx,
T1.rpt_year,
truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] two_month_check,
1 truvada_criteria_met 
FROM hiv_rpt.prep_truvada_array T1 
WHERE truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] >= 60;

-- SQL - Break down the combo meds in to distinct individual meds.
-- OK
CREATE TABLE hiv_rpt.prep_hiv_rx_combo_distinct AS
select distinct unnest(med_array) as distinct_med, patient_id, rpt_year 
from hiv_rpt.prep_hiv_meds 
group by patient_id, med_array, rpt_year
order by patient_id;

-- Rx for ≥3 different HIV meds - Part 1
-- OK
CREATE TABLE hiv_rpt.prep_hiv_rx_3_diff AS 
SELECT T1.patient_id,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2006 then 1 else 0 end three_diff_hiv_med_06, 
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2007 then 1 else 0 end three_diff_hiv_med_07,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2008 then 1 else 0 end three_diff_hiv_med_08,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2009 then 1 else 0 end three_diff_hiv_med_09,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2010 then 1 else 0 end three_diff_hiv_med_10,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2011 then 1 else 0 end three_diff_hiv_med_11,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2012 then 1 else 0 end three_diff_hiv_med_12,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2013 then 1 else 0 end three_diff_hiv_med_13,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2014 then 1 else 0 end three_diff_hiv_med_14,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2015 then 1 else 0 end three_diff_hiv_med_15,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2016 then 1 else 0 end three_diff_hiv_med_16,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2017 then 1 else 0 end three_diff_hiv_med_17,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2018 then 1 else 0 end three_diff_hiv_med_18,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2019 then 1 else 0 end three_diff_hiv_med_19
FROM hiv_rpt.prep_hiv_rx_combo_distinct T1 
GROUP BY T1.patient_id, T1.rpt_year;

-- Rx for ≥3 different HIV meds - Part 2
-- OK
CREATE TABLE hiv_rpt.prep_hiv_rx_3_diff_count AS
SELECT T1.patient_id,
MAX(three_diff_hiv_med_06) three_diff_hiv_med_06, 
MAX(three_diff_hiv_med_07) three_diff_hiv_med_07,
MAX(three_diff_hiv_med_08) three_diff_hiv_med_08,
MAX(three_diff_hiv_med_09) three_diff_hiv_med_09,
MAX(three_diff_hiv_med_10) three_diff_hiv_med_10,
MAX(three_diff_hiv_med_11) three_diff_hiv_med_11,
MAX(three_diff_hiv_med_12) three_diff_hiv_med_12,
MAX(three_diff_hiv_med_13) three_diff_hiv_med_13,
MAX(three_diff_hiv_med_14) three_diff_hiv_med_14,
MAX(three_diff_hiv_med_15) three_diff_hiv_med_15,
MAX(three_diff_hiv_med_16) three_diff_hiv_med_16,
MAX(three_diff_hiv_med_17) three_diff_hiv_med_17,
MAX(three_diff_hiv_med_18) three_diff_hiv_med_18,
MAX(three_diff_hiv_med_19) three_diff_hiv_med_19
FROM hiv_rpt.prep_hiv_rx_3_diff T1
GROUP BY T1.patient_id;

-- Number of Truvada Prescriptions & Total Number of Pills
-- OK
CREATE TABLE hiv_rpt.prep_truvada_rx_and_pills AS 
SELECT T1.patient_id,rpt_year,
sum( refills_mod ) truvada_num_rx,
sum(case when quantity_type in ('tab', 'tabs', '', 'tablet') or quantity_type is null then (quantity_float * refills_mod) else 0 end) truvada_num_pills
FROM hiv_rpt.prep_hiv_meds T1 
WHERE truvada_rx = 1 
GROUP BY T1.patient_id, rpt_year;

CREATE TABLE hiv_rpt.prep_truvada_rx_and_pills_peryear AS
SELECT T1.patient_id,
max(coalesce(CASE WHEN rpt_year = 2006 then truvada_num_rx end, 0)) truvada_num_rx_06,
max(coalesce(CASE WHEN rpt_year = 2007 then truvada_num_rx end, 0)) truvada_num_rx_07,
max(coalesce(CASE WHEN rpt_year = 2008 then truvada_num_rx end, 0)) truvada_num_rx_08,
max(coalesce(CASE WHEN rpt_year = 2009 then truvada_num_rx end, 0)) truvada_num_rx_09,
max(coalesce(CASE WHEN rpt_year = 2010 then truvada_num_rx end, 0)) truvada_num_rx_10,
max(coalesce(CASE WHEN rpt_year = 2011 then truvada_num_rx end, 0)) truvada_num_rx_11,
max(coalesce(CASE WHEN rpt_year = 2012 then truvada_num_rx end, 0)) truvada_num_rx_12,
max(coalesce(CASE WHEN rpt_year = 2013 then truvada_num_rx end, 0)) truvada_num_rx_13,
max(coalesce(CASE WHEN rpt_year = 2014 then truvada_num_rx end, 0)) truvada_num_rx_14,
max(coalesce(CASE WHEN rpt_year = 2015 then truvada_num_rx end, 0)) truvada_num_rx_15,
max(coalesce(CASE WHEN rpt_year = 2016 then truvada_num_rx end, 0)) truvada_num_rx_16,
max(coalesce(CASE WHEN rpt_year = 2017 then truvada_num_rx end, 0)) truvada_num_rx_17,
max(coalesce(CASE WHEN rpt_year = 2018 then truvada_num_rx end, 0)) truvada_num_rx_18,
max(coalesce(CASE WHEN rpt_year = 2019 then truvada_num_rx end, 0)) truvada_num_rx_19,
max(coalesce(CASE WHEN rpt_year = 2006 then truvada_num_pills end, 0)) truvada_num_pills_06,
max(coalesce(CASE WHEN rpt_year = 2007 then truvada_num_pills end, 0)) truvada_num_pills_07,
max(coalesce(CASE WHEN rpt_year = 2008 then truvada_num_pills end, 0)) truvada_num_pills_08,
max(coalesce(CASE WHEN rpt_year = 2009 then truvada_num_pills end, 0)) truvada_num_pills_09,
max(coalesce(CASE WHEN rpt_year = 2010 then truvada_num_pills end, 0)) truvada_num_pills_10,
max(coalesce(CASE WHEN rpt_year = 2011 then truvada_num_pills end, 0)) truvada_num_pills_11,
max(coalesce(CASE WHEN rpt_year = 2012 then truvada_num_pills end, 0)) truvada_num_pills_12,
max(coalesce(CASE WHEN rpt_year = 2013 then truvada_num_pills end, 0)) truvada_num_pills_13,
max(coalesce(CASE WHEN rpt_year = 2014 then truvada_num_pills end, 0)) truvada_num_pills_14,
max(coalesce(CASE WHEN rpt_year = 2015 then truvada_num_pills end, 0)) truvada_num_pills_15,
max(coalesce(CASE WHEN rpt_year = 2016 then truvada_num_pills end, 0)) truvada_num_pills_16,
max(coalesce(CASE WHEN rpt_year = 2017 then truvada_num_pills end, 0)) truvada_num_pills_17,
max(coalesce(CASE WHEN rpt_year = 2018 then truvada_num_pills end, 0)) truvada_num_pills_18,
max(coalesce(CASE WHEN rpt_year = 2019 then truvada_num_pills end, 0)) truvada_num_pills_19
FROM hiv_rpt.prep_truvada_rx_and_pills T1
GROUP BY T1.patient_id;


-- HIV - Truvada Counts & Column Creation 
-- OK
CREATE TABLE hiv_rpt.prep_truvada_counts AS 
SELECT T1.patient_id,
max(case when truvada_criteria_met = 1 and rpt_year = 2006 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_06,
max(case when truvada_criteria_met = 1 and rpt_year = 2007 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_07,
max(case when truvada_criteria_met = 1 and rpt_year = 2008 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_08,
max(case when truvada_criteria_met = 1 and rpt_year = 2009 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_09,
max(case when truvada_criteria_met = 1 and rpt_year = 2010 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_10,
max(case when truvada_criteria_met = 1 and rpt_year = 2011 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_11,
max(case when truvada_criteria_met = 1 and rpt_year = 2012 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_12,
max(case when truvada_criteria_met = 1 and rpt_year = 2013 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_13,
max(case when truvada_criteria_met = 1 and rpt_year = 2014 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_14,
max(case when truvada_criteria_met = 1 and rpt_year = 2015 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_15,
max(case when truvada_criteria_met = 1 and rpt_year = 2016 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_16,
max(case when truvada_criteria_met = 1 and rpt_year = 2017 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_17, 
max(case when truvada_criteria_met = 1 and rpt_year = 2018 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_18, 
max(case when truvada_criteria_met = 1 and rpt_year = 2019 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_19  
FROM hiv_rpt.prep_truvada_2mogap T1 
LEFT OUTER JOIN hiv_rpt.prep_hiv_cases T2 ON ((T1.patient_id = T2.patient_id)) 
GROUP BY T1.patient_id;

-- HIV on the Problem List
-- OK
CREATE TABLE hiv_rpt.prep_hiv_problem_list AS 
SELECT DISTINCT ON(patient_id) T1.id,T1.patient_id,T2.first_date hiv_first_prob_date, T1.dx_code_id, 1::int hiv_prob 
FROM emr_problem T1,
	(SELECT patient_id, min(date) as first_date from emr_problem WHERE dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' group by patient_id) T2
WHERE (dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)')
AND T1.date = T2.first_date
AND T1.patient_id = T2.patient_id ;



-- HIV - Full Outer Join HIV Lab Events, Meds, and Cases - Derive Compound Values
-- OK
CREATE TABLE hiv_rpt.prep_hiv_all_details AS SELECT 
coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id, hiv_3_diff.patient_id, hiv_truv_rx.patient_id) patient_id,
(case when total_hiv_tests is null and hiv_per_esp is null then 2
  when total_hiv_tests > 0 and hiv_per_esp is null then 0
  else hiv_per_esp end) as hiv_per_esp_final, 
coalesce(hiv_per_esp_date, null) as hiv_per_esp_date,
(case when hiv_per_esp is null and total_hiv_rna_tests > 0 then 1
  when hiv_per_esp is null and (total_hiv_rna_tests = 0 or total_hiv_rna_tests is null) then 0
  else null end) as hiv_neg_with_rna_test,
(case when meds.patient_id is null and hiv_per_esp is null then 0
  when meds.patient_id is not null and hiv_per_esp is null then 1
  else null end) as hiv_neg_with_meds,
coalesce(hiv_neg_truvada_06,0) as hiv_neg_truvada_06,
coalesce(hiv_neg_truvada_07,0) as hiv_neg_truvada_07,
coalesce(hiv_neg_truvada_08,0) as hiv_neg_truvada_08,
coalesce(hiv_neg_truvada_09,0) as hiv_neg_truvada_09,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_neg_truvada_18,0) as hiv_neg_truvada_18,
coalesce(hiv_neg_truvada_19,0) as hiv_neg_truvada_19,
coalesce(hiv_prob, 0) as hiv_on_problem_list, 
coalesce(hiv_first_prob_date, null) as hiv_first_prob_date,
coalesce(three_diff_hiv_med_06, 0) three_diff_hiv_med_06, 
coalesce(three_diff_hiv_med_07, 0) three_diff_hiv_med_07,
coalesce(three_diff_hiv_med_08, 0) three_diff_hiv_med_08,
coalesce(three_diff_hiv_med_09, 0) three_diff_hiv_med_09,
coalesce(three_diff_hiv_med_10, 0) three_diff_hiv_med_10,
coalesce(three_diff_hiv_med_11, 0) three_diff_hiv_med_11,
coalesce(three_diff_hiv_med_12, 0) three_diff_hiv_med_12,
coalesce(three_diff_hiv_med_13, 0) three_diff_hiv_med_13,
coalesce(three_diff_hiv_med_14, 0) three_diff_hiv_med_14,
coalesce(three_diff_hiv_med_15, 0) three_diff_hiv_med_15,
coalesce(three_diff_hiv_med_16, 0) three_diff_hiv_med_16,
coalesce(three_diff_hiv_med_17, 0) three_diff_hiv_med_17,
coalesce(three_diff_hiv_med_18, 0) three_diff_hiv_med_18,
coalesce(three_diff_hiv_med_19, 0) three_diff_hiv_med_19,
coalesce(truvada_num_rx_06, 0) truvada_num_rx_06,
coalesce(truvada_num_rx_07, 0) truvada_num_rx_07,
coalesce(truvada_num_rx_08, 0) truvada_num_rx_08,
coalesce(truvada_num_rx_09, 0) truvada_num_rx_09,
coalesce(truvada_num_rx_10, 0) truvada_num_rx_10,
coalesce(truvada_num_rx_11, 0) truvada_num_rx_11,
coalesce(truvada_num_rx_12, 0) truvada_num_rx_12,
coalesce(truvada_num_rx_13, 0) truvada_num_rx_13,
coalesce(truvada_num_rx_14, 0) truvada_num_rx_14,
coalesce(truvada_num_rx_15, 0) truvada_num_rx_15,
coalesce(truvada_num_rx_16, 0) truvada_num_rx_16,
coalesce(truvada_num_rx_17, 0) truvada_num_rx_17,
coalesce(truvada_num_rx_18, 0) truvada_num_rx_18,
coalesce(truvada_num_rx_19, 0) truvada_num_rx_19,
coalesce(truvada_num_pills_06, 0) truvada_num_pills_06,
coalesce(truvada_num_pills_07, 0) truvada_num_pills_07,
coalesce(truvada_num_pills_08, 0) truvada_num_pills_08,
coalesce(truvada_num_pills_09, 0) truvada_num_pills_09,
coalesce(truvada_num_pills_10, 0) truvada_num_pills_10,
coalesce(truvada_num_pills_11, 0) truvada_num_pills_11,
coalesce(truvada_num_pills_12, 0) truvada_num_pills_12,
coalesce(truvada_num_pills_13, 0) truvada_num_pills_13,
coalesce(truvada_num_pills_14, 0) truvada_num_pills_14,
coalesce(truvada_num_pills_15, 0) truvada_num_pills_15,
coalesce(truvada_num_pills_16, 0) truvada_num_pills_16,
coalesce(truvada_num_pills_17, 0) truvada_num_pills_17,
coalesce(truvada_num_pills_18, 0) truvada_num_pills_18,
coalesce(truvada_num_pills_19, 0) truvada_num_pills_19
FROM hiv_rpt.prep_hiv_counts lab
FULL OUTER JOIN hiv_rpt.prep_hiv_cases cases
 ON lab.patient_id=cases.patient_id
FULL OUTER JOIN hiv_rpt.prep_hiv_meds_distinct meds
 ON meds.patient_id = coalesce(lab.patient_id, cases.patient_id)
FULL OUTER JOIN hiv_rpt.prep_truvada_counts truvada
 ON truvada.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hiv_problem_list hiv_prob
	ON hiv_prob.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hiv_rx_3_diff_count hiv_3_diff
	ON hiv_3_diff.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id)
FULL OUTER JOIN hiv_rpt.prep_truvada_rx_and_pills_peryear hiv_truv_rx
	ON hiv_truv_rx.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id, hiv_3_diff.patient_id);
	
	
-- BILCILLIN -- gather up all bicillin meds that match criteria
-- OK 	
CREATE TABLE hiv_rpt.prep_bicillin_all AS
SELECT patient_id, name, date, EXTRACT(YEAR FROM date) rpt_year, 1 rx_bicillin
FROM emr_prescription 
WHERE ( name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR')
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
)
AND date >= '01-01-2006' 
AND date < '01-01-2020'
GROUP BY patient_id, name, date;	

-- Exclude bicillin prescriptions that match specific dx codes 
-- Designed to help eliminate bicillin for non syph meds
-- occurring on the same date
-- OK
CREATE TABLE hiv_rpt.prep_bicillin_subset AS
SELECT * FROM  hiv_rpt.prep_bicillin_all
EXCEPT
SELECT T1.*
FROM hiv_rpt.prep_bicillin_all T1, 
emr_encounter T2,
emr_encounter_dx_codes T3
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
and T1.date = T2.date
order by patient_id;


-- GATHER UP PRESCRIPTIONS AND DATES
-- OK
CREATE TABLE hiv_rpt.prep_rx_of_interest AS 
SELECT T1.patient_id,T1.name, EXTRACT(YEAR FROM date) rpt_year,
max(CASE WHEN T1.name in (
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)' ) then 1 end) rx_azithromycin,
max(CASE WHEN T1.name in ( 
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG') then 1 end) rx_ceftriaxone,
max(CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end) rx_methadone,
max(CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike 'buprenorphine%naloxone%' 
	or T1.name ilike '%zubsolv%' 
	or T1.name ilike '%bunavail%' 
	or T1.name ilike '%cassipa%'  then 1 end) rx_suboxone,
max(CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end) rx_viagara_cilais_or_levitra 
FROM emr_prescription T1 
WHERE (name in (
    'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	'CEFTRIAXONE IM INJECTION <=250 MG', 
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	'CEFTRIAXONE-LIDOCAINE 500 MG IM KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)',
	'CEFTRIAXONE-LIDOCAINE IM INJECTION <=250 MG')
OR (name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%'
	or name ilike '%suboxone%' 
	or name ilike 'buprenorphine%naloxone%' 
	or name ilike '%zubsolv%'
	or name ilike '%bunavail%'
	or name ilike '%cassipa%' 	
	or name ilike '%tadalafil%' 
	or name ilike '%cilais%' 
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%'
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%' 
	or name ilike '%viagra%')
AND name not ilike '%revatio%'
	AND name not ilike '%ANTIHYPERTENSIVE%'
	AND name not ilike '%HYPERTENSION%'
	AND name not ilike '%adcirca%'
	AND name not ilike '%alyq%'	
AND date >= '01-01-2006' 
AND date < '01-01-2020')
GROUP BY T1.patient_id, T1.name, T1.date;

-- PRESCRIPTIONS - CREATE FIELDS
-- OK
CREATE TABLE hiv_rpt.prep_rx_counts AS 
SELECT coalesce(T1.patient_id, T2.patient_id) patient_id,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2006 then 1 else 0 end) rx_bicillin_06,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2007 then 1 else 0 end) rx_bicillin_07,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2008 then 1 else 0 end) rx_bicillin_08,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2009 then 1 else 0 end) rx_bicillin_09,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2010 then 1 else 0 end) rx_bicillin_10,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2011 then 1 else 0 end) rx_bicillin_11,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2012 then 1 else 0 end) rx_bicillin_12,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2013 then 1 else 0 end) rx_bicillin_13,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2014 then 1 else 0 end) rx_bicillin_14,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2015 then 1 else 0 end) rx_bicillin_15,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2016 then 1 else 0 end) rx_bicillin_16,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2017 then 1 else 0 end) rx_bicillin_17,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2018 then 1 else 0 end) rx_bicillin_18,
max(CASE WHEN rx_bicillin = 1 and T2.rpt_year = 2019 then 1 else 0 end) rx_bicillin_19,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2006 then 1 else 0 end) rx_azithromycin_06,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2007 then 1 else 0 end) rx_azithromycin_07,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2008 then 1 else 0 end) rx_azithromycin_08,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2009 then 1 else 0 end) rx_azithromycin_09,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2010 then 1 else 0 end) rx_azithromycin_10,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2011 then 1 else 0 end) rx_azithromycin_11,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2012 then 1 else 0 end) rx_azithromycin_12,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2013 then 1 else 0 end) rx_azithromycin_13,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2014 then 1 else 0 end) rx_azithromycin_14,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2015 then 1 else 0 end) rx_azithromycin_15,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2016 then 1 else 0 end) rx_azithromycin_16,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2017 then 1 else 0 end) rx_azithromycin_17,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2018 then 1 else 0 end) rx_azithromycin_18,
max(CASE WHEN rx_azithromycin = 1 and T1.rpt_year = 2019 then 1 else 0 end) rx_azithromycin_19,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2006 then 1 else 0 end) rx_ceftriaxone_06,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2007 then 1 else 0 end) rx_ceftriaxone_07,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2008 then 1 else 0 end) rx_ceftriaxone_08,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2009 then 1 else 0 end) rx_ceftriaxone_09,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2010 then 1 else 0 end) rx_ceftriaxone_10,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2011 then 1 else 0 end) rx_ceftriaxone_11,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2012 then 1 else 0 end) rx_ceftriaxone_12,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2013 then 1 else 0 end) rx_ceftriaxone_13,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2014 then 1 else 0 end) rx_ceftriaxone_14,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2015 then 1 else 0 end) rx_ceftriaxone_15,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2016 then 1 else 0 end) rx_ceftriaxone_16,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2017 then 1 else 0 end) rx_ceftriaxone_17,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2018 then 1 else 0 end) rx_ceftriaxone_18,
max(CASE WHEN rx_ceftriaxone = 1 and T1.rpt_year = 2019 then 1 else 0 end) rx_ceftriaxone_19,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2006 then 1 else 0 end) rx_methadone_06,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2007 then 1 else 0 end) rx_methadone_07,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2008 then 1 else 0 end) rx_methadone_08,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2009 then 1 else 0 end) rx_methadone_09,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2010 then 1 else 0 end) rx_methadone_10,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2011 then 1 else 0 end) rx_methadone_11,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2012 then 1 else 0 end) rx_methadone_12,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2013 then 1 else 0 end) rx_methadone_13,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2014 then 1 else 0 end) rx_methadone_14,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2015 then 1 else 0 end) rx_methadone_15,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2016 then 1 else 0 end) rx_methadone_16,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2017 then 1 else 0 end) rx_methadone_17,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2018 then 1 else 0 end) rx_methadone_18,
max(CASE WHEN rx_methadone = 1 and T1.rpt_year = 2019 then 1 else 0 end) rx_methadone_19,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2006 then 1 else 0 end)  rx_suboxone_06,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2007 then 1 else 0 end) rx_suboxone_07,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2008 then 1 else 0 end) rx_suboxone_08,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2009 then 1 else 0 end) rx_suboxone_09,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2010 then 1 else 0 end) rx_suboxone_10,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2011 then 1 else 0 end) rx_suboxone_11,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2012 then 1 else 0 end) rx_suboxone_12,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2013 then 1 else 0 end) rx_suboxone_13,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2014 then 1 else 0 end) rx_suboxone_14,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2015 then 1 else 0 end) rx_suboxone_15,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2016 then 1 else 0 end) rx_suboxone_16,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2017 then 1 else 0 end) rx_suboxone_17,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2018 then 1 else 0 end) rx_suboxone_18,
max(CASE WHEN rx_suboxone = 1 and T1.rpt_year = 2019 then 1 else 0 end) rx_suboxone_19,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2006 then 1 else 0 end) rx_viagara_cilais_or_levitra_06,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2007 then 1 else 0 end) rx_viagara_cilais_or_levitra_07,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2008 then 1 else 0 end) rx_viagara_cilais_or_levitra_08,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2009 then 1 else 0 end) rx_viagara_cilais_or_levitra_09,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2010 then 1 else 0 end) rx_viagara_cilais_or_levitra_10,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2011 then 1 else 0 end) rx_viagara_cilais_or_levitra_11,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2012 then 1 else 0 end) rx_viagara_cilais_or_levitra_12,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2013 then 1 else 0 end) rx_viagara_cilais_or_levitra_13,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2014 then 1 else 0 end) rx_viagara_cilais_or_levitra_14,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2015 then 1 else 0 end) rx_viagara_cilais_or_levitra_15,
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2016 then 1 else 0 end) rx_viagara_cilais_or_levitra_16, 
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2017 then 1 else 0 end) rx_viagara_cilais_or_levitra_17, 
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2018 then 1 else 0 end) rx_viagara_cilais_or_levitra_18, 
max(CASE WHEN rx_viagara_cilais_or_levitra = 1 and T1.rpt_year = 2019 then 1 else 0 end) rx_viagara_cilais_or_levitra_19 
FROM hiv_rpt.prep_rx_of_interest T1 
LEFT JOIN hiv_rpt.prep_bicillin_subset T2 ON T1.patient_id = T2.patient_id
GROUP BY coalesce(T1.patient_id, T2.patient_id);


-- HERPES Labs - No hef events so just basing on native_name
-- ASK DR.KRAKOWER ABOUT THRESHOLD, TESTS TO INCLUDE
-- DIRECT TESTS
CREATE TABLE hiv_rpt.prep_herpes_direct_labs AS
SELECT T1.patient_id, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, result_string,
case when (result_string ILIKE ANY(ARRAY['pos%', 'det%', 'Antibody to%det%', 'Antibodes to both % detec', 'antibody%detected%', '>%', 'ISOLATED']) and result_string not ilike '%not det%') OR (result_float >= 0.9 and result_string not ilike '%:%')
then 'positive' else null end as positive_test
FROM emr_labresult T1
WHERE (native_name ilike '%hsv%'or native_name ilike '%herpes%')
AND native_code ILIKE ANY(ARRAY['87140--%', '87252--%', '87254--%', '87255--%', '87273--%', '87529--%', '87530--%', 'LAB1351--%', 'LAB573--%', 'LAB574--%', 'LAB575--%'])
AND native_code not in ('LAB574--20910', 'LAB575--19278')
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND date >= '01-01-2006';


-- HERPES Labs - No hef events so just basing on native_name
-- ASK DR.KRAKOWER ABOUT THRESHOLD, TESTS TO INCLUDE
-- SEROLOGY TESTS
CREATE TABLE hiv_rpt.prep_herpes_serolgy_labs AS
SELECT T1.patient_id, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, result_string,
case when (result_string ILIKE ANY(ARRAY['pos%', 'det%', 'Antibody to%det%', 'Antibodes to both % detec', 'antibody%detected%', '>%', 'ISOLATED']) and result_string not ilike '%not det%') OR (result_float >= 0.9 and result_string not ilike '%:%')
then 'positive' else null end as positive_test
FROM emr_labresult T1
WHERE (native_name ilike '%hsv%'or native_name ilike '%herpes%')
AND native_code ILIKE ANY(ARRAY['80090--%', '86694--%', '86695--%', '86696--%', '86777--%', '86790--%', 'LAB571--%', 'LAB572--%', 'MR0001--%'])
AND native_code not in ('86694--4325')
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND date >= '01-01-2006';




-- HERPES DIRECT TESTING COUNTS
-- Only counting if a patient was tested during the year, not counting tests due to not having mapped tests
CREATE TABLE hiv_rpt.prep_herpes_direct_counts as
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) herpes_dir_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) herpes_dir_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) herpes_dir_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) herpes_dir_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) herpes_dir_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) herpes_dir_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) herpes_dir_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) herpes_dir_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) herpes_dir_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) herpes_dir_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) herpes_dir_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) herpes_dir_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) herpes_dir_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) herpes_dir_tested_19,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2006 then 1 else 0 end) herpes_dir_pos_test_06,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2007 then 1 else 0 end) herpes_dir_pos_test_07,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2008 then 1 else 0 end) herpes_dir_pos_test_08,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2009 then 1 else 0 end) herpes_dir_pos_test_09,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2010 then 1 else 0 end) herpes_dir_pos_test_10,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2011 then 1 else 0 end) herpes_dir_pos_test_11,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2012 then 1 else 0 end) herpes_dir_pos_test_12,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2013 then 1 else 0 end) herpes_dir_pos_test_13,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2014 then 1 else 0 end) herpes_dir_pos_test_14,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2015 then 1 else 0 end) herpes_dir_pos_test_15,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2016 then 1 else 0 end) herpes_dir_pos_test_16,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2017 then 1 else 0 end) herpes_dir_pos_test_17,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2018 then 1 else 0 end) herpes_dir_pos_test_18,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2019 then 1 else 0 end) herpes_dir_pos_test_19
FROM hiv_rpt.prep_herpes_direct_labs T1
GROUP BY patient_id;


-- HERPES SEROLOGY TESTING COUNTS
-- Only counting if a patient was tested during the year, not counting tests due to not having mapped tests
CREATE TABLE hiv_rpt.prep_herpes_serology_counts as
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) herpes_ser_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) herpes_ser_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) herpes_ser_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) herpes_ser_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) herpes_ser_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) herpes_ser_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) herpes_ser_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) herpes_ser_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) herpes_ser_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) herpes_ser_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) herpes_ser_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) herpes_ser_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) herpes_ser_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) herpes_ser_tested_19,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2006 then 1 else 0 end) herpes_ser_pos_test_06,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2007 then 1 else 0 end) herpes_ser_pos_test_07,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2008 then 1 else 0 end) herpes_ser_pos_test_08,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2009 then 1 else 0 end) herpes_ser_pos_test_09,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2010 then 1 else 0 end) herpes_ser_pos_test_10,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2011 then 1 else 0 end) herpes_ser_pos_test_11,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2012 then 1 else 0 end) herpes_ser_pos_test_12,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2013 then 1 else 0 end) herpes_ser_pos_test_13,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2014 then 1 else 0 end) herpes_ser_pos_test_14,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2015 then 1 else 0 end) herpes_ser_pos_test_15,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2016 then 1 else 0 end) herpes_ser_pos_test_16,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2017 then 1 else 0 end) herpes_ser_pos_test_17,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2018 then 1 else 0 end) herpes_ser_pos_test_18,
max(CASE WHEN positive_test = 'positive' AND T1.rpt_year = 2019 then 1 else 0 end) herpes_ser_pos_test_19
FROM hiv_rpt.prep_herpes_serolgy_labs T1
GROUP BY patient_id;


-- TOXICOLOGY TESTS

CREATE TABLE hiv_rpt.prep_tox_labs AS
SELECT T1.patient_id, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(date, now()))) rpt_year_new, date, result_float, result_string, native_name, native_code, procedure_name
FROM emr_labresult T1
WHERE native_code ILIKE ANY(ARRAY['LAB377%','80100%','80101%','80102%','80305%','82145%','82520%','83520--7156','83520--7157','83520--7158','83520--7159','83840%','83925%','G0434%','G0477%', 'LAB1103%','LAB374%','LAB375%','LAB431%','LAB715%','N2198%','N2746%'])
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.')
AND native_name not in (
'CHAIN OF CUSTODY', 
'SPECIMEN TYPE', 
'PRESCRIBED DRUG 1 URINE',
'PRESCRIBED DRUG 2 URINE',
'PRESCRIBED DRUG 3 URINE',
'PRESCRIBED DRUG 4 URINE',
'PRESCRIBED DRUG 5 URINE', 
'N. GONORRHEA',
'C. TRACHOMATIS',
'METHYLENETETRAHYDROFOLATE REDUCTASE (MTH',
'COMMENT'
)
AND result_string is not null
and procedure_name  not ilike '%serum%'
and procedure_name not ilike '%plasma'
AND date >= '01-01-2006';


CREATE TABLE hiv_rpt.prep_tox_amph AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) amphet_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) amphet_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) amphet_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) amphet_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) amphet_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) amphet_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) amphet_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) amphet_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) amphet_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) amphet_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) amphet_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) amphet_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) amphet_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) amphet_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
and native_name not in ('TEST NAME') then 1 else 0 end) amphet_pos_19
FROM hiv_rpt.prep_tox_labs T1
WHERE result_string ILIKE ANY(ARRAY['%AMPHETAMINE%', '%ECSTASY%', '%MDA%', '%MDEA%', '%MDMA%'])
or native_name ILIKE ANY(ARRAY['%AMPHETAMINE%', '%ECSTASY%', '%MDA%', '%MDEA%', '%MDMA%'])
or procedure_name ILIKE ANY(ARRAY['%AMPHETAMINE%', '%ECSTASY%', '%MDA%', '%MDEA%', '%MDMA%'])
GROUP BY patient_id;


CREATE TABLE hiv_rpt.prep_tox_cocaine AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) cocaine_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) cocaine_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) cocaine_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) cocaine_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) cocaine_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) cocaine_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) cocaine_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) cocaine_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) cocaine_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) cocaine_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) cocaine_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) cocaine_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) cocaine_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) cocaine_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 50) then 1 else 0 end) cocaine_pos_19
FROM hiv_rpt.prep_tox_labs T1
WHERE result_string ILIKE ANY(ARRAY['%COCAINE%', '%BENZOYLECGONINE%', '%COCAETHYLENE%', 'ECGONINE METHYL ESTER%'])
or native_name ILIKE ANY(ARRAY['%COCAINE%', '%BENZOYLECGONINE%', '%COCAETHYLENE%', 'ECGONINE METHYL ESTER%'])
or procedure_name ILIKE ANY(ARRAY['%COCAINE%', '%BENZOYLECGONINE%', '%COCAETHYLENE%', 'ECGONINE METHYL ESTER%'])
GROUP BY patient_id;


CREATE TABLE hiv_rpt.prep_tox_methadone AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) methadone_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) methadone_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) methadone_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) methadone_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) methadone_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) methadone_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) methadone_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) methadone_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) methadone_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) methadone_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) methadone_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) methadone_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) methadone_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) methadone_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) methadone_pos_19
FROM hiv_rpt.prep_tox_labs T1
WHERE result_string ILIKE ANY(ARRAY['%METHADONE%', '%EDDP%'])
or native_name ILIKE ANY(ARRAY['%METHADONE%', '%EDDP%'])
or procedure_name ILIKE ANY(ARRAY['%METHADONE%', '%EDDP%'])
GROUP BY patient_id;



CREATE TABLE hiv_rpt.prep_tox_fentanyl AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) fentanyl_mam_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) fentanyl_mam_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) fentanyl_mam_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) fentanyl_mam_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) fentanyl_mam_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) fentanyl_mam_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) fentanyl_mam_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) fentanyl_mam_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) fentanyl_mam_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) fentanyl_mam_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) fentanyl_mam_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) fentanyl_mam_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) fentanyl_mam_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) fentanyl_mam_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) fentanyl_mam_pos_19
FROM hiv_rpt.prep_tox_labs T1
WHERE result_string ILIKE ANY(ARRAY['%fentanyl%', '%MAM%'])
or native_name ILIKE ANY(ARRAY['%fentanyl%', '%MAM%'])
or procedure_name ILIKE ANY(ARRAY['%fentanyl%', '%MAM%'])
GROUP BY patient_id;


CREATE TABLE hiv_rpt.prep_tox_opiate AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) opiate_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) opiate_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) opiate_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) opiate_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) opiate_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) opiate_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) opiate_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) opiate_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) opiate_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) opiate_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) opiate_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) opiate_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) opiate_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) opiate_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 200)
AND native_name not in ('ALCOHOL, ETHYL', 'BARBITUATES QN (S)', 'BARBITURATES', 'AMPHETAMINE', 'BARBITURATES', 'COCAINE', 'CONFIRMED TESTING PERFORMED BY', 'MARIJUANA CONFIRM', 'METHADONE', 'NALOXONE QN (UR)', 'NALOXONE UR', 'TEST NAME') then 1 else 0 end) opiate_pos_19
FROM hiv_rpt.prep_tox_labs T1
where result_string ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
or native_name ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
or procedure_name ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
GROUP BY patient_id;

-- MEDICATION-ASSISTED TREATMENT
CREATE TABLE hiv_rpt.prep_tox_med_asst_trmt AS 
SELECT patient_id,
max(CASE WHEN T1.rpt_year = 2006 then 1 else 0 end) med_asst_trmt_tested_06,
max(CASE WHEN T1.rpt_year = 2007 then 1 else 0 end) med_asst_trmt_tested_07,
max(CASE WHEN T1.rpt_year = 2008 then 1 else 0 end) med_asst_trmt_tested_08,
max(CASE WHEN T1.rpt_year = 2009 then 1 else 0 end) med_asst_trmt_tested_09,
max(CASE WHEN T1.rpt_year = 2010 then 1 else 0 end) med_asst_trmt_tested_10,
max(CASE WHEN T1.rpt_year = 2011 then 1 else 0 end) med_asst_trmt_tested_11,
max(CASE WHEN T1.rpt_year = 2012 then 1 else 0 end) med_asst_trmt_tested_12,
max(CASE WHEN T1.rpt_year = 2013 then 1 else 0 end) med_asst_trmt_tested_13,
max(CASE WHEN T1.rpt_year = 2014 then 1 else 0 end) med_asst_trmt_tested_14,
max(CASE WHEN T1.rpt_year = 2015 then 1 else 0 end) med_asst_trmt_tested_15,
max(CASE WHEN T1.rpt_year = 2016 then 1 else 0 end) med_asst_trmt_tested_16,
max(CASE WHEN T1.rpt_year = 2017 then 1 else 0 end) med_asst_trmt_tested_17,
max(CASE WHEN T1.rpt_year = 2018 then 1 else 0 end) med_asst_trmt_tested_18,
max(CASE WHEN T1.rpt_year = 2019 then 1 else 0 end) med_asst_trmt_tested_19,
max(CASE WHEN T1.rpt_year = 2006 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_06,
max(CASE WHEN T1.rpt_year = 2007 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_07,
max(CASE WHEN T1.rpt_year = 2008 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_08,
max(CASE WHEN T1.rpt_year = 2009 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_09,
max(CASE WHEN T1.rpt_year = 2010 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_10,
max(CASE WHEN T1.rpt_year = 2011 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_11,
max(CASE WHEN T1.rpt_year = 2012 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_12,
max(CASE WHEN T1.rpt_year = 2013 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_13,
max(CASE WHEN T1.rpt_year = 2014 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_14,
max(CASE WHEN T1.rpt_year = 2015 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_15,
max(CASE WHEN T1.rpt_year = 2016 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_16,
max(CASE WHEN T1.rpt_year = 2017 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_17,
max(CASE WHEN T1.rpt_year = 2018 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_18,
max(CASE WHEN T1.rpt_year = 2019 and (result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' or result_float > 10) then 1 else 0 end) med_asst_trmt_pos_19
FROM hiv_rpt.prep_tox_labs T1
WHERE result_string ILIKE ANY(ARRAY['%BUPRENORPHINE%','%NALOXONE%', '%NALTREXONE%'])
or native_name ILIKE ANY(ARRAY['%BUPRENORPHINE%','%NALOXONE%', '%NALTREXONE%'])
or procedure_name ILIKE ANY(ARRAY['%BUPRENORPHINE%','%NALOXONE%', '%NALTREXONE%'])
GROUP BY patient_id;



-- FULL OUTER JOIN ALL OF THE DATA TOGETHER
-- OK
CREATE TABLE hiv_rpt.prep_output_part_2 AS SELECT 
coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, herpes_serology.patient_id, amph.patient_id, cocaine.patient_id, methadone.patient_id, fentanyl.patient_id, opiate.patient_id, med_asst.patient_id) as master_patient_id,
coalesce(t_gon_tests_06, 0) as total_gonorrhea_tests_06,
coalesce(t_gon_tests_07, 0) as total_gonorrhea_tests_07,
coalesce(t_gon_tests_08, 0) as total_gonorrhea_tests_08,
coalesce(t_gon_tests_09, 0) as total_gonorrhea_tests_09,
coalesce(t_gon_tests_10, 0) as total_gonorrhea_tests_10,
coalesce(t_gon_tests_11, 0) as total_gonorrhea_tests_11,
coalesce(t_gon_tests_12, 0) as total_gonorrhea_tests_12,
coalesce(t_gon_tests_13, 0) as total_gonorrhea_tests_13,
coalesce(t_gon_tests_14, 0) as total_gonorrhea_tests_14,
coalesce(t_gon_tests_15, 0) as total_gonorrhea_tests_15,
coalesce(t_gon_tests_16, 0) as total_gonorrhea_tests_16,
coalesce(t_gon_tests_17, 0) as total_gonorrhea_tests_17,
coalesce(t_gon_tests_18, 0) as total_gonorrhea_tests_18,
coalesce(t_gon_tests_19, 0) as total_gonorrhea_tests_19,
coalesce(p_gon_tests_06, 0) as positive_gonorrhea_tests_06,
coalesce(p_gon_tests_07, 0) as positive_gonorrhea_tests_07,
coalesce(p_gon_tests_08, 0) as positive_gonorrhea_tests_08,
coalesce(p_gon_tests_09, 0) as positive_gonorrhea_tests_09,
coalesce(p_gon_tests_10, 0) as positive_gonorrhea_tests_10,
coalesce(p_gon_tests_11, 0) as positive_gonorrhea_tests_11,
coalesce(p_gon_tests_12, 0) as positive_gonorrhea_tests_12,
coalesce(p_gon_tests_13, 0) as positive_gonorrhea_tests_13,
coalesce(p_gon_tests_14, 0) as positive_gonorrhea_tests_14,
coalesce(p_gon_tests_15, 0) as positive_gonorrhea_tests_15,
coalesce(p_gon_tests_16, 0) as positive_gonorrhea_tests_16,
coalesce(p_gon_tests_17, 0) as positive_gonorrhea_tests_17,
coalesce(p_gon_tests_18, 0) as positive_gonorrhea_tests_18,
coalesce(p_gon_tests_19, 0) as positive_gonorrhea_tests_19,
coalesce(t_gon_tests_throat_06, 0) as total_gonorrhea_tests_throat_06,
coalesce(t_gon_tests_throat_07, 0) as total_gonorrhea_tests_throat_07,
coalesce(t_gon_tests_throat_08, 0) as total_gonorrhea_tests_throat_08,
coalesce(t_gon_tests_throat_09, 0) as total_gonorrhea_tests_throat_09,
coalesce(t_gon_tests_throat_10, 0) as total_gonorrhea_tests_throat_10,
coalesce(t_gon_tests_throat_11, 0) as total_gonorrhea_tests_throat_11,
coalesce(t_gon_tests_throat_12, 0) as total_gonorrhea_tests_throat_12,
coalesce(t_gon_tests_throat_13, 0) as total_gonorrhea_tests_throat_13,
coalesce(t_gon_tests_throat_14, 0) as total_gonorrhea_tests_throat_14,
coalesce(t_gon_tests_throat_15, 0) as total_gonorrhea_tests_throat_15,
coalesce(t_gon_tests_throat_16, 0) as total_gonorrhea_tests_throat_16,
coalesce(t_gon_tests_throat_17, 0) as total_gonorrhea_tests_throat_17,
coalesce(t_gon_tests_throat_18, 0) as total_gonorrhea_tests_throat_18,
coalesce(t_gon_tests_throat_19, 0) as total_gonorrhea_tests_throat_19,
coalesce(p_gon_tests_throat_06, 0) as positive_gonorrhea_tests_throat_06,
coalesce(p_gon_tests_throat_07, 0) as positive_gonorrhea_tests_throat_07,
coalesce(p_gon_tests_throat_08, 0) as positive_gonorrhea_tests_throat_08,
coalesce(p_gon_tests_throat_09, 0) as positive_gonorrhea_tests_throat_09,
coalesce(p_gon_tests_throat_10, 0) as positive_gonorrhea_tests_throat_10,
coalesce(p_gon_tests_throat_11, 0) as positive_gonorrhea_tests_throat_11,
coalesce(p_gon_tests_throat_12, 0) as positive_gonorrhea_tests_throat_12,
coalesce(p_gon_tests_throat_13, 0) as positive_gonorrhea_tests_throat_13,
coalesce(p_gon_tests_throat_14, 0) as positive_gonorrhea_tests_throat_14,
coalesce(p_gon_tests_throat_15, 0) as positive_gonorrhea_tests_throat_15,
coalesce(p_gon_tests_throat_16, 0) as positive_gonorrhea_tests_throat_16,
coalesce(p_gon_tests_throat_17, 0) as positive_gonorrhea_tests_throat_17,
coalesce(p_gon_tests_throat_18, 0) as positive_gonorrhea_tests_throat_18,
coalesce(p_gon_tests_throat_19, 0) as positive_gonorrhea_tests_throat_19,
coalesce(t_gon_tests_rectal_06, 0) as total_gonorrhea_tests_rectal_06,
coalesce(t_gon_tests_rectal_07, 0) as total_gonorrhea_tests_rectal_07,
coalesce(t_gon_tests_rectal_08, 0) as total_gonorrhea_tests_rectal_08,
coalesce(t_gon_tests_rectal_09, 0) as total_gonorrhea_tests_rectal_09,
coalesce(t_gon_tests_rectal_10, 0) as total_gonorrhea_tests_rectal_10,
coalesce(t_gon_tests_rectal_11, 0) as total_gonorrhea_tests_rectal_11,
coalesce(t_gon_tests_rectal_12, 0) as total_gonorrhea_tests_rectal_12,
coalesce(t_gon_tests_rectal_13, 0) as total_gonorrhea_tests_rectal_13,
coalesce(t_gon_tests_rectal_14, 0) as total_gonorrhea_tests_rectal_14,
coalesce(t_gon_tests_rectal_15, 0) as total_gonorrhea_tests_rectal_15,
coalesce(t_gon_tests_rectal_16, 0) as total_gonorrhea_tests_rectal_16,
coalesce(t_gon_tests_rectal_17, 0) as total_gonorrhea_tests_rectal_17,
coalesce(t_gon_tests_rectal_18, 0) as total_gonorrhea_tests_rectal_18,
coalesce(t_gon_tests_rectal_19, 0) as total_gonorrhea_tests_rectal_19,
coalesce(p_gon_tests_rectal_06, 0) as positive_gonorrhea_tests_rectal_06,
coalesce(p_gon_tests_rectal_07, 0) as positive_gonorrhea_tests_rectal_07,
coalesce(p_gon_tests_rectal_08, 0) as positive_gonorrhea_tests_rectal_08,
coalesce(p_gon_tests_rectal_09, 0) as positive_gonorrhea_tests_rectal_09,
coalesce(p_gon_tests_rectal_10, 0) as positive_gonorrhea_tests_rectal_10,
coalesce(p_gon_tests_rectal_11, 0) as positive_gonorrhea_tests_rectal_11,
coalesce(p_gon_tests_rectal_12, 0) as positive_gonorrhea_tests_rectal_12,
coalesce(p_gon_tests_rectal_13, 0) as positive_gonorrhea_tests_rectal_13,
coalesce(p_gon_tests_rectal_14, 0) as positive_gonorrhea_tests_rectal_14,
coalesce(p_gon_tests_rectal_15, 0) as positive_gonorrhea_tests_rectal_15,
coalesce(p_gon_tests_rectal_16, 0) as positive_gonorrhea_tests_rectal_16,
coalesce(p_gon_tests_rectal_17, 0) as positive_gonorrhea_tests_rectal_17,
coalesce(p_gon_tests_rectal_18, 0) as positive_gonorrhea_tests_rectal_18,
coalesce(p_gon_tests_rectal_19, 0) as positive_gonorrhea_tests_rectal_19,
coalesce(t_chlam_06, 0) as total_chlamydia_tests_06,
coalesce(t_chlam_07, 0) as total_chlamydia_tests_07,
coalesce(t_chlam_08, 0) as total_chlamydia_tests_08,
coalesce(t_chlam_09, 0) as total_chlamydia_tests_09,
coalesce(t_chlam_10, 0) as total_chlamydia_tests_10,
coalesce(t_chlam_11, 0) as total_chlamydia_tests_11,
coalesce(t_chlam_12, 0) as total_chlamydia_tests_12,
coalesce(t_chlam_13, 0) as total_chlamydia_tests_13,
coalesce(t_chlam_14, 0) as total_chlamydia_tests_14,
coalesce(t_chlam_15, 0) as total_chlamydia_tests_15,
coalesce(t_chlam_16, 0) as total_chlamydia_tests_16,
coalesce(t_chlam_17, 0) as total_chlamydia_tests_17,
coalesce(t_chlam_18, 0) as total_chlamydia_tests_18,
coalesce(t_chlam_19, 0) as total_chlamydia_tests_19,
coalesce(p_chlam_06, 0) as positive_chlamydia_tests_06,
coalesce(p_chlam_07, 0) as positive_chlamydia_tests_07,
coalesce(p_chlam_08, 0) as positive_chlamydia_tests_08,
coalesce(p_chlam_09, 0) as positive_chlamydia_tests_09,
coalesce(p_chlam_10, 0) as positive_chlamydia_tests_10,
coalesce(p_chlam_11, 0) as positive_chlamydia_tests_11,
coalesce(p_chlam_12, 0) as positive_chlamydia_tests_12,
coalesce(p_chlam_13, 0) as positive_chlamydia_tests_13,
coalesce(p_chlam_14, 0) as positive_chlamydia_tests_14,
coalesce(p_chlam_15, 0) as positive_chlamydia_tests_15,
coalesce(p_chlam_16, 0) as positive_chlamydia_tests_16,
coalesce(p_chlam_17, 0) as positive_chlamydia_tests_17,
coalesce(p_chlam_18, 0) as positive_chlamydia_tests_18,
coalesce(p_chlam_19, 0) as positive_chlamydia_tests_19,
coalesce(t_chlam_throat_06, 0) as total_chlamydia_tests_throat_06,
coalesce(t_chlam_throat_07, 0) as total_chlamydia_tests_throat_07,
coalesce(t_chlam_throat_08, 0) as total_chlamydia_tests_throat_08,
coalesce(t_chlam_throat_09, 0) as total_chlamydia_tests_throat_09,
coalesce(t_chlam_throat_10, 0) as total_chlamydia_tests_throat_10,
coalesce(t_chlam_throat_11, 0) as total_chlamydia_tests_throat_11,
coalesce(t_chlam_throat_12, 0) as total_chlamydia_tests_throat_12,
coalesce(t_chlam_throat_13, 0) as total_chlamydia_tests_throat_13,
coalesce(t_chlam_throat_14, 0) as total_chlamydia_tests_throat_14,
coalesce(t_chlam_throat_15, 0) as total_chlamydia_tests_throat_15,
coalesce(t_chlam_throat_16, 0) as total_chlamydia_tests_throat_16,
coalesce(t_chlam_throat_17, 0) as total_chlamydia_tests_throat_17,
coalesce(t_chlam_throat_18, 0) as total_chlamydia_tests_throat_18,
coalesce(t_chlam_throat_19, 0) as total_chlamydia_tests_throat_19,
coalesce(p_chlam_throat_06, 0) as positive_chlamydia_tests_throat_06,
coalesce(p_chlam_throat_07, 0) as positive_chlamydia_tests_throat_07,
coalesce(p_chlam_throat_08, 0) as positive_chlamydia_tests_throat_08,
coalesce(p_chlam_throat_09, 0) as positive_chlamydia_tests_throat_09,
coalesce(p_chlam_throat_10, 0) as positive_chlamydia_tests_throat_10,
coalesce(p_chlam_throat_11, 0) as positive_chlamydia_tests_throat_11,
coalesce(p_chlam_throat_12, 0) as positive_chlamydia_tests_throat_12,
coalesce(p_chlam_throat_13, 0) as positive_chlamydia_tests_throat_13,
coalesce(p_chlam_throat_14, 0) as positive_chlamydia_tests_throat_14,
coalesce(p_chlam_throat_15, 0) as positive_chlamydia_tests_throat_15,
coalesce(p_chlam_throat_16, 0) as positive_chlamydia_tests_throat_16,
coalesce(p_chlam_throat_17, 0) as positive_chlamydia_tests_throat_17,
coalesce(p_chlam_throat_18, 0) as positive_chlamydia_tests_throat_18,
coalesce(p_chlam_throat_19, 0) as positive_chlamydia_tests_throat_19,
coalesce(t_chlam_rectal_06, 0) as total_chlamydia_tests_rectal_06,
coalesce(t_chlam_rectal_07, 0) as total_chlamydia_tests_rectal_07,
coalesce(t_chlam_rectal_08, 0) as total_chlamydia_tests_rectal_08,
coalesce(t_chlam_rectal_09, 0) as total_chlamydia_tests_rectal_09,
coalesce(t_chlam_rectal_10, 0) as total_chlamydia_tests_rectal_10,
coalesce(t_chlam_rectal_11, 0) as total_chlamydia_tests_rectal_11,
coalesce(t_chlam_rectal_12, 0) as total_chlamydia_tests_rectal_12,
coalesce(t_chlam_rectal_13, 0) as total_chlamydia_tests_rectal_13,
coalesce(t_chlam_rectal_14, 0) as total_chlamydia_tests_rectal_14,
coalesce(t_chlam_rectal_15, 0) as total_chlamydia_tests_rectal_15,
coalesce(t_chlam_rectal_16, 0) as total_chlamydia_tests_rectal_16,
coalesce(t_chlam_rectal_17, 0) as total_chlamydia_tests_rectal_17,
coalesce(t_chlam_rectal_18, 0) as total_chlamydia_tests_rectal_18,
coalesce(t_chlam_rectal_19, 0) as total_chlamydia_tests_rectal_19,
coalesce(p_chlam_rectal_06, 0) as positive_chlamydia_tests_rectal_06,
coalesce(p_chlam_rectal_07, 0) as positive_chlamydia_tests_rectal_07,
coalesce(p_chlam_rectal_08, 0) as positive_chlamydia_tests_rectal_08,
coalesce(p_chlam_rectal_09, 0) as positive_chlamydia_tests_rectal_09,
coalesce(p_chlam_rectal_10, 0) as positive_chlamydia_tests_rectal_10,
coalesce(p_chlam_rectal_11, 0) as positive_chlamydia_tests_rectal_11,
coalesce(p_chlam_rectal_12, 0) as positive_chlamydia_tests_rectal_12,
coalesce(p_chlam_rectal_13, 0) as positive_chlamydia_tests_rectal_13,
coalesce(p_chlam_rectal_14, 0) as positive_chlamydia_tests_rectal_14,
coalesce(p_chlam_rectal_15, 0) as positive_chlamydia_tests_rectal_15,
coalesce(p_chlam_rectal_16, 0) as positive_chlamydia_tests_rectal_16,
coalesce(p_chlam_rectal_17, 0) as positive_chlamydia_tests_rectal_17,
coalesce(p_chlam_rectal_18, 0) as positive_chlamydia_tests_rectal_18,
coalesce(p_chlam_rectal_19, 0) as positive_chlamydia_tests_rectal_19,
coalesce(t_syph_06, 0) as total_syphilis_tests_06,
coalesce(t_syph_07, 0) as total_syphilis_tests_07,
coalesce(t_syph_08, 0) as total_syphilis_tests_08,
coalesce(t_syph_09, 0) as total_syphilis_tests_09,
coalesce(t_syph_10, 0) as total_syphilis_tests_10,
coalesce(t_syph_11, 0) as total_syphilis_tests_11,
coalesce(t_syph_12, 0) as total_syphilis_tests_12,
coalesce(t_syph_13, 0) as total_syphilis_tests_13,
coalesce(t_syph_14, 0) as total_syphilis_tests_14,
coalesce(t_syph_15, 0) as total_syphilis_tests_15,
coalesce(t_syph_16, 0) as total_syphilis_tests_16,
coalesce(t_syph_17, 0) as total_syphilis_tests_17,
coalesce(t_syph_18, 0) as total_syphilis_tests_18,
coalesce(t_syph_19, 0) as total_syphilis_tests_19,
coalesce(p_syph_06, 0) as positive_syphilis_tests_06,
coalesce(p_syph_07, 0) as positive_syphilis_tests_07,
coalesce(p_syph_08, 0) as positive_syphilis_tests_08,
coalesce(p_syph_09, 0) as positive_syphilis_tests_09,
coalesce(p_syph_10, 0) as positive_syphilis_tests_10,
coalesce(p_syph_11, 0) as positive_syphilis_tests_11,
coalesce(p_syph_12, 0) as positive_syphilis_tests_12,
coalesce(p_syph_13, 0) as positive_syphilis_tests_13,
coalesce(p_syph_14, 0) as positive_syphilis_tests_14,
coalesce(p_syph_15, 0) as positive_syphilis_tests_15,
coalesce(p_syph_16, 0) as positive_syphilis_tests_16,
coalesce(p_syph_17, 0) as positive_syphilis_tests_17,
coalesce(p_syph_18, 0) as positive_syphilis_tests_18,
coalesce(p_syph_19, 0) as positive_syphilis_tests_19,
coalesce(syphilis_06, 0) as syphilis_per_esp_06,
coalesce(syphilis_07, 0) as syphilis_per_esp_07,
coalesce(syphilis_08, 0) as syphilis_per_esp_08,
coalesce(syphilis_09, 0) as syphilis_per_esp_09,
coalesce(syphilis_10, 0) as syphilis_per_esp_10,
coalesce(syphilis_11, 0) as syphilis_per_esp_11,
coalesce(syphilis_12, 0) as syphilis_per_esp_12,
coalesce(syphilis_13, 0) as syphilis_per_esp_13,
coalesce(syphilis_14, 0) as syphilis_per_esp_14,
coalesce(syphilis_15, 0) as syphilis_per_esp_15,
coalesce(syphilis_16, 0) as syphilis_per_esp_16,
coalesce(syphilis_17, 0) as syphilis_per_esp_17,
coalesce(syphilis_18, 0) as syphilis_per_esp_18,
coalesce(syphilis_19, 0) as syphilis_per_esp_19,
coalesce(ser_test_for_lgv_ever, 0) as ser_testing_for_lgv_ever,
coalesce(anal_cytology_test_ever, 0) as anal_cytology_test_ever,
coalesce(t_hcv_anti_06, 0) as total_hcv_antibody_tests_06,
coalesce(t_hcv_anti_07, 0) as total_hcv_antibody_tests_07,
coalesce(t_hcv_anti_08, 0) as total_hcv_antibody_tests_08,
coalesce(t_hcv_anti_09, 0) as total_hcv_antibody_tests_09,
coalesce(t_hcv_anti_10, 0) as total_hcv_antibody_tests_10,
coalesce(t_hcv_anti_11, 0) as total_hcv_antibody_tests_11,
coalesce(t_hcv_anti_12, 0) as total_hcv_antibody_tests_12,
coalesce(t_hcv_anti_13, 0) as total_hcv_antibody_tests_13,
coalesce(t_hcv_anti_14, 0) as total_hcv_antibody_tests_14,
coalesce(t_hcv_anti_15, 0) as total_hcv_antibody_tests_15,
coalesce(t_hcv_anti_16, 0) as total_hcv_antibody_tests_16,
coalesce(t_hcv_anti_17, 0) as total_hcv_antibody_tests_17,
coalesce(t_hcv_anti_18, 0) as total_hcv_antibody_tests_18,
coalesce(t_hcv_anti_19, 0) as total_hcv_antibody_tests_19,
coalesce(t_hcv_rna_06, 0) as total_hcv_rna_tests_06,
coalesce(t_hcv_rna_07, 0) as total_hcv_rna_tests_07,
coalesce(t_hcv_rna_08, 0) as total_hcv_rna_tests_08,
coalesce(t_hcv_rna_09, 0) as total_hcv_rna_tests_09,
coalesce(t_hcv_rna_10, 0) as total_hcv_rna_tests_10,
coalesce(t_hcv_rna_11, 0) as total_hcv_rna_tests_11,
coalesce(t_hcv_rna_12, 0) as total_hcv_rna_tests_12,
coalesce(t_hcv_rna_13, 0) as total_hcv_rna_tests_13,
coalesce(t_hcv_rna_14, 0) as total_hcv_rna_tests_14,
coalesce(t_hcv_rna_15, 0) as total_hcv_rna_tests_15,
coalesce(t_hcv_rna_16, 0) as total_hcv_rna_tests_16,
coalesce(t_hcv_rna_17, 0) as total_hcv_rna_tests_17,
coalesce(t_hcv_rna_18, 0) as total_hcv_rna_tests_18,
coalesce(t_hcv_rna_19, 0) as total_hcv_rna_tests_19,
coalesce(hcv_anti_or_rna_pos, 0) as hcv_antibody_or_rna_positive,
coalesce(hcv_anti_pos_date, null) as hcv_antibody_first_pos_year,
coalesce(hcv_rna_pos_date, null) as hcv_rna_first_pos_year,
coalesce(acute_hepc_per_esp, 0) as acute_hepc_per_esp,
coalesce(acute_hepc_per_esp_date, null) as acute_hepc_per_esp_diagnosis_year,
coalesce(t_hepb_antigen_06, 0) as total_hepb_surface_antigen_tests_06,
coalesce(t_hepb_antigen_07, 0) as total_hepb_surface_antigen_tests_07,
coalesce(t_hepb_antigen_08, 0) as total_hepb_surface_antigen_tests_08,
coalesce(t_hepb_antigen_09, 0) as total_hepb_surface_antigen_tests_09,
coalesce(t_hepb_antigen_10, 0) as total_hepb_surface_antigen_tests_10,
coalesce(t_hepb_antigen_11, 0) as total_hepb_surface_antigen_tests_11,
coalesce(t_hepb_antigen_12, 0) as total_hepb_surface_antigen_tests_12,
coalesce(t_hepb_antigen_13, 0) as total_hepb_surface_antigen_tests_13,
coalesce(t_hepb_antigen_14, 0) as total_hepb_surface_antigen_tests_14,
coalesce(t_hepb_antigen_15, 0) as total_hepb_surface_antigen_tests_15,
coalesce(t_hepb_antigen_16, 0) as total_hepb_surface_antigen_tests_16,
coalesce(t_hepb_antigen_17, 0) as total_hepb_surface_antigen_tests_17,
coalesce(t_hepb_antigen_18, 0) as total_hepb_surface_antigen_tests_18,
coalesce(t_hepb_antigen_19, 0) as total_hepb_surface_antigen_tests_19,
coalesce(t_hepb_dna_06, 0) as total_hepb_dna_tests_06,
coalesce(t_hepb_dna_07, 0) as total_hepb_dna_tests_07,
coalesce(t_hepb_dna_08, 0) as total_hepb_dna_tests_08,
coalesce(t_hepb_dna_09, 0) as total_hepb_dna_tests_09,
coalesce(t_hepb_dna_10, 0) as total_hepb_dna_tests_10,
coalesce(t_hepb_dna_11, 0) as total_hepb_dna_tests_11,
coalesce(t_hepb_dna_12, 0) as total_hepb_dna_tests_12,
coalesce(t_hepb_dna_13, 0) as total_hepb_dna_tests_13,
coalesce(t_hepb_dna_14, 0) as total_hepb_dna_tests_14,
coalesce(t_hepb_dna_15, 0) as total_hepb_dna_tests_15,
coalesce(t_hepb_dna_16, 0) as total_hepb_dna_tests_16,
coalesce(t_hepb_dna_17, 0) as total_hepb_dna_tests_17,
coalesce(t_hepb_dna_18, 0) as total_hepb_dna_tests_18,
coalesce(t_hepb_dna_19, 0) as total_hepb_dna_tests_19,
coalesce(herpes_dir_tested_06, 0) as herpes_dir_tested_06,
coalesce(herpes_dir_tested_07, 0) as herpes_dir_tested_07,
coalesce(herpes_dir_tested_08, 0) as herpes_dir_tested_08,
coalesce(herpes_dir_tested_09, 0) as herpes_dir_tested_09,
coalesce(herpes_dir_tested_10, 0) as herpes_dir_tested_10,
coalesce(herpes_dir_tested_11, 0) as herpes_dir_tested_11,
coalesce(herpes_dir_tested_12, 0) as herpes_dir_tested_12,
coalesce(herpes_dir_tested_13, 0) as herpes_dir_tested_13,
coalesce(herpes_dir_tested_14, 0) as herpes_dir_tested_14,
coalesce(herpes_dir_tested_15, 0) as herpes_dir_tested_15,
coalesce(herpes_dir_tested_16, 0) as herpes_dir_tested_16,
coalesce(herpes_dir_tested_17, 0) as herpes_dir_tested_17,
coalesce(herpes_dir_tested_18, 0) as herpes_dir_tested_18,
coalesce(herpes_dir_tested_19, 0) as herpes_dir_tested_19,
coalesce(herpes_dir_pos_test_06, 0) as herpes_dir_pos_test_06,
coalesce(herpes_dir_pos_test_07, 0) as herpes_dir_pos_test_07,
coalesce(herpes_dir_pos_test_08, 0) as herpes_dir_pos_test_08,
coalesce(herpes_dir_pos_test_09, 0) as herpes_dir_pos_test_09,
coalesce(herpes_dir_pos_test_10, 0) as herpes_dir_pos_test_10,
coalesce(herpes_dir_pos_test_11, 0) as herpes_dir_pos_test_11,
coalesce(herpes_dir_pos_test_12, 0) as herpes_dir_pos_test_12,
coalesce(herpes_dir_pos_test_13, 0) as herpes_dir_pos_test_13,
coalesce(herpes_dir_pos_test_14, 0) as herpes_dir_pos_test_14,
coalesce(herpes_dir_pos_test_15, 0) as herpes_dir_pos_test_15,
coalesce(herpes_dir_pos_test_16, 0) as herpes_dir_pos_test_16,
coalesce(herpes_dir_pos_test_17, 0) as herpes_dir_pos_test_17,
coalesce(herpes_dir_pos_test_18, 0) as herpes_dir_pos_test_18,
coalesce(herpes_dir_pos_test_19, 0) as herpes_dir_pos_test_19,
coalesce(herpes_ser_tested_06, 0) as herpes_ser_tested_06,
coalesce(herpes_ser_tested_07, 0) as herpes_ser_tested_07,
coalesce(herpes_ser_tested_08, 0) as herpes_ser_tested_08,
coalesce(herpes_ser_tested_09, 0) as herpes_ser_tested_09,
coalesce(herpes_ser_tested_10, 0) as herpes_ser_tested_10,
coalesce(herpes_ser_tested_11, 0) as herpes_ser_tested_11,
coalesce(herpes_ser_tested_12, 0) as herpes_ser_tested_12,
coalesce(herpes_ser_tested_13, 0) as herpes_ser_tested_13,
coalesce(herpes_ser_tested_14, 0) as herpes_ser_tested_14,
coalesce(herpes_ser_tested_15, 0) as herpes_ser_tested_15,
coalesce(herpes_ser_tested_16, 0) as herpes_ser_tested_16,
coalesce(herpes_ser_tested_17, 0) as herpes_ser_tested_17,
coalesce(herpes_ser_tested_18, 0) as herpes_ser_tested_18,
coalesce(herpes_ser_tested_19, 0) as herpes_ser_tested_19,
coalesce(herpes_ser_pos_test_06, 0) as herpes_ser_pos_test_06,
coalesce(herpes_ser_pos_test_07, 0) as herpes_ser_pos_test_07,
coalesce(herpes_ser_pos_test_08, 0) as herpes_ser_pos_test_08,
coalesce(herpes_ser_pos_test_09, 0) as herpes_ser_pos_test_09,
coalesce(herpes_ser_pos_test_10, 0) as herpes_ser_pos_test_10,
coalesce(herpes_ser_pos_test_11, 0) as herpes_ser_pos_test_11,
coalesce(herpes_ser_pos_test_12, 0) as herpes_ser_pos_test_12,
coalesce(herpes_ser_pos_test_13, 0) as herpes_ser_pos_test_13,
coalesce(herpes_ser_pos_test_14, 0) as herpes_ser_pos_test_14,
coalesce(herpes_ser_pos_test_15, 0) as herpes_ser_pos_test_15,
coalesce(herpes_ser_pos_test_16, 0) as herpes_ser_pos_test_16,
coalesce(herpes_ser_pos_test_17, 0) as herpes_ser_pos_test_17,
coalesce(herpes_ser_pos_test_18, 0) as herpes_ser_pos_test_18,
coalesce(herpes_ser_pos_test_19, 0) as herpes_ser_pos_test_19,
coalesce(hepb_pos_antigen_or_dna, 0) as hepb_antigen_or_dna_positive,
coalesce(hepb_pos_antigen_or_dna, hist_of_hep_b, 0) as hist_of_hep_b_ever,
coalesce(hepb_pos_antigen_or_dna_date, null) as hepb_antigen_or_dna_first_positive_year,
coalesce(acute_hepb_per_esp, 0) as acute_hepb_per_esp,
coalesce(acute_hepb_per_esp_date, null) as acute_hepb_per_esp_year,
coalesce(anal_cyt_dysp_carci_hpv_syph, null) as anal_cyt_dysp_carci_hpv_syph,
coalesce(syphillis_of_any_site_or_stage_except_late, null) as syphillis_of_any_site_or_stage_except_late,
coalesce(gonococcal_infection_of_anus_and_rectum, null) as gonococcal_infection_of_anus_and_rectum, 
coalesce(gonococcal_pharyngitis, null) as gonococcal_pharyngitis,
coalesce(chlamydial_infection_of_anus_and_recturm, null) as chlamydial_infection_of_anus_and_recturm,
coalesce(chlamydial_infection_of_pharynx, null) as chlamydial_infection_of_pharynx,
coalesce(lymphgranuloma_venereum, null) as lymphgranuloma_venereum,
coalesce(chancroid, null) as chancroid,
coalesce(granuloma_inguinale, null) as granuloma_inguinale,
coalesce(nongonococcal_urethritis, null) as nongonococcal_urethritis,
coalesce(herpes_simplex_w_complications, null) as herpes_simplex_w_complications,
coalesce(genital_herpes, null) as genital_herpes,
coalesce(anogenital_warts, null) as anogenital_warts,
coalesce(anorectal_ulcer, null) as anorectal_ulcer,
coalesce(unspecified_std, null) as unspecified_std,
coalesce(pelvic_inflammatory_disease, null) as pelvic_inflammatory_disease,
coalesce(contact_with_or_exposure_to_venereal_disease, null) as contact_with_or_exposure_to_venereal_disease,
coalesce(high_risk_sexual_behavior, null) as high_risk_sexual_behavior,
coalesce(hiv_counseling, null) as hiv_counseling,
coalesce(anorexia_nervosa, null) as anorexia_nervosa,
coalesce(bulimia_nervosa, null) as bulimia_nervosa,
coalesce(eating_disorder_nos, null) as eating_disorder_nos,
coalesce(gend_iden_trans_sex_reassign, null) as gend_iden_trans_sex_reassign,
coalesce(counseling_for_child_sexual_abuse, null) as counseling_for_child_sexual_abuse,
coalesce(foreign_body_in_anus, null) as foreign_body_in_anus,
coalesce(alcohol_dependence_abuse, null) as alcohol_dependence_abuse,
coalesce(opioid_dependence_abuse, null) as opioid_dependence_abuse,
coalesce(sed_hypn_anxio_depend_abuse, null) as sed_hypn_anxio_depend_abuse,
coalesce(cocaine_dependence_abuse, null) as cocaine_dependence_abuse,
coalesce(amphet_stim_dependence_abuse, null) as amphet_stim_dependence_abuse,
coalesce(oth_psycho_or_unspec_subs_depend_abuse, null) as oth_psycho_or_unspec_subs_depend_abuse,
coalesce(incarceration, null) as incarceration,
--coalesce(prison_occurrence, null) as prison_occurrence,
--coalesce(under_prison_care, null) as under_prison_care,
--coalesce(prisoner_health_exam, null) as prisoner_health_exam,
coalesce(prison_release_probs, null) as prison_release_probs,
coalesce(oth_sex_inf_with_preg, null) as oth_sex_inf_with_preg,
coalesce(enc_after_alleged_r_sa_or_batt, null) as enc_after_alleged_r_sa_or_batt,
coalesce(adult_sexual_abuse, null) as adult_sexual_abuse,
coalesce(adult_sexual_abuse_sequela, null) as adult_sexual_abuse_sequela,
coalesce(adult_sexual_abuse_subsequent, null) as adult_sexual_abuse_subsequent,
coalesce(adult_sexual_abuse_suspected, null) as adult_sexual_abuse_suspected,
coalesce(adult_maltreatment, null) as adult_maltreatment,
coalesce(adult_physical_abuse, null) as adult_physical_abuse,
coalesce(adult_emotional_abuse, null) as adult_emotional_abuse,
coalesce(child_sexual_abuse, null) as child_sexual_abuse,
coalesce(rape_aslt_bod_force, null) as rape_aslt_bod_force,
coalesce(cannabis_abuse, null) as cannabis_abuse,
coalesce(hallucinogen_abuse, null) as hallucinogen_abuse,
coalesce(inhalent_abuse, null) as inhalent_abuse,
--coalesce(non_psychoact_subs_abuse, null) as non_psychoact_subs_abuse,
--coalesce(herpes_unspecified, null) as herpes_unspecified,
--coalesce(abortion, null) as abortion,
--coalesce(history_of_abortion, null) as history_of_abortion,
coalesce(elec_abort_in_first_sec_tri, null) as elec_abort_in_first_sec_tri,
--coalesce(elec_abortion_history, null) as elec_abortion_history,
coalesce(emergency_contracep, null) as emergency_contracep,
--coalesce(emergency_contracep_couns, null) as emergency_contracep_couns,
--coalesce(oth_spef_personal_risk_factors, null) as oth_spef_personal_risk_factors,
coalesce(enc_for_hiv_screen, null) as enc_for_hiv_screen,
coalesce(contact_w_viral_hepatitis, null) as contact_w_viral_hepatitis,
coalesce(contact_w_pot_hzrd_bod_fluids, null) as contact_w_pot_hzrd_bod_fluids,
coalesce(rx_bicillin_06, 0) as rx_bicillin_06,
coalesce(rx_bicillin_07, 0) as rx_bicillin_07,
coalesce(rx_bicillin_08, 0) as rx_bicillin_08,
coalesce(rx_bicillin_09, 0) as rx_bicillin_09,
coalesce(rx_bicillin_10, 0) as rx_bicillin_10,
coalesce(rx_bicillin_11, 0) as rx_bicillin_11,
coalesce(rx_bicillin_12, 0) as rx_bicillin_12,
coalesce(rx_bicillin_13, 0) as rx_bicillin_13,
coalesce(rx_bicillin_14, 0) as rx_bicillin_14,
coalesce(rx_bicillin_15, 0) as rx_bicillin_15,
coalesce(rx_bicillin_16, 0) as rx_bicillin_16,
coalesce(rx_bicillin_17, 0) as rx_bicillin_17,
coalesce(rx_bicillin_18, 0) as rx_bicillin_18,
coalesce(rx_bicillin_19, 0) as rx_bicillin_19,
coalesce(rx_azithromycin_06,0) as rx_azithromycin_06,
coalesce(rx_azithromycin_07,0) as rx_azithromycin_07,
coalesce(rx_azithromycin_08,0) as rx_azithromycin_08,
coalesce(rx_azithromycin_09,0) as rx_azithromycin_09,
coalesce(rx_azithromycin_10,0) as rx_azithromycin_10,
coalesce(rx_azithromycin_11,0) as rx_azithromycin_11,
coalesce(rx_azithromycin_12,0) as rx_azithromycin_12,
coalesce(rx_azithromycin_13,0) as rx_azithromycin_13,
coalesce(rx_azithromycin_14,0) as rx_azithromycin_14,
coalesce(rx_azithromycin_15,0) as rx_azithromycin_15,
coalesce(rx_azithromycin_16,0) as rx_azithromycin_16,
coalesce(rx_azithromycin_17,0) as rx_azithromycin_17,
coalesce(rx_azithromycin_18,0) as rx_azithromycin_18,
coalesce(rx_azithromycin_19,0) as rx_azithromycin_19,
coalesce(rx_ceftriaxone_06, 0) as rx_ceftriaxone_06,
coalesce(rx_ceftriaxone_07, 0) as rx_ceftriaxone_07,
coalesce(rx_ceftriaxone_08, 0) as rx_ceftriaxone_08,
coalesce(rx_ceftriaxone_09, 0) as rx_ceftriaxone_09,
coalesce(rx_ceftriaxone_10, 0) as rx_ceftriaxone_10,
coalesce(rx_ceftriaxone_11, 0) as rx_ceftriaxone_11,
coalesce(rx_ceftriaxone_12, 0) as rx_ceftriaxone_12,
coalesce(rx_ceftriaxone_13, 0) as rx_ceftriaxone_13,
coalesce(rx_ceftriaxone_14, 0) as rx_ceftriaxone_14,
coalesce(rx_ceftriaxone_15, 0) as rx_ceftriaxone_15,
coalesce(rx_ceftriaxone_16, 0) as rx_ceftriaxone_16,
coalesce(rx_ceftriaxone_17, 0) as rx_ceftriaxone_17,
coalesce(rx_ceftriaxone_18, 0) as rx_ceftriaxone_18,
coalesce(rx_ceftriaxone_19, 0) as rx_ceftriaxone_19,
coalesce(rx_methadone_06, 0) as rx_methadone_06,
coalesce(rx_methadone_07, 0) as rx_methadone_07,
coalesce(rx_methadone_08, 0) as rx_methadone_08,
coalesce(rx_methadone_09, 0) as rx_methadone_09,
coalesce(rx_methadone_10, 0) as rx_methadone_10,
coalesce(rx_methadone_11, 0) as rx_methadone_11,
coalesce(rx_methadone_12, 0) as rx_methadone_12,
coalesce(rx_methadone_13, 0) as rx_methadone_13,
coalesce(rx_methadone_14, 0) as rx_methadone_14,
coalesce(rx_methadone_15, 0) as rx_methadone_15,
coalesce(rx_methadone_16, 0) as rx_methadone_16,
coalesce(rx_methadone_17, 0) as rx_methadone_17,
coalesce(rx_methadone_18, 0) as rx_methadone_18,
coalesce(rx_methadone_19, 0) as rx_methadone_19,
coalesce(rx_suboxone_06, 0) as rx_suboxone_06,
coalesce(rx_suboxone_07, 0) as rx_suboxone_07,
coalesce(rx_suboxone_08, 0) as rx_suboxone_08,
coalesce(rx_suboxone_09, 0) as rx_suboxone_09,
coalesce(rx_suboxone_10, 0) as rx_suboxone_10,
coalesce(rx_suboxone_11, 0) as rx_suboxone_11,
coalesce(rx_suboxone_12, 0) as rx_suboxone_12,
coalesce(rx_suboxone_13, 0) as rx_suboxone_13,
coalesce(rx_suboxone_14, 0) as rx_suboxone_14,
coalesce(rx_suboxone_15, 0) as rx_suboxone_15,
coalesce(rx_suboxone_16, 0) as rx_suboxone_16,
coalesce(rx_suboxone_17, 0) as rx_suboxone_17,
coalesce(rx_suboxone_18, 0) as rx_suboxone_18,
coalesce(rx_suboxone_19, 0) as rx_suboxone_19,
coalesce(rx_viagara_cilais_or_levitra_06, 0) as rx_viagara_cilais_or_levitra_06,
coalesce(rx_viagara_cilais_or_levitra_07, 0) as rx_viagara_cilais_or_levitra_07,
coalesce(rx_viagara_cilais_or_levitra_08, 0) as rx_viagara_cilais_or_levitra_08,
coalesce(rx_viagara_cilais_or_levitra_09, 0) as rx_viagara_cilais_or_levitra_09,
coalesce(rx_viagara_cilais_or_levitra_10, 0) as rx_viagara_cilais_or_levitra_10,
coalesce(rx_viagara_cilais_or_levitra_11, 0) as rx_viagara_cilais_or_levitra_11,
coalesce(rx_viagara_cilais_or_levitra_12, 0) as rx_viagara_cilais_or_levitra_12,
coalesce(rx_viagara_cilais_or_levitra_13, 0) as rx_viagara_cilais_or_levitra_13,
coalesce(rx_viagara_cilais_or_levitra_14, 0) as rx_viagara_cilais_or_levitra_14,
coalesce(rx_viagara_cilais_or_levitra_15, 0) as rx_viagara_cilais_or_levitra_15,
coalesce(rx_viagara_cilais_or_levitra_16, 0) as rx_viagara_cilais_or_levitra_16,
coalesce(rx_viagara_cilais_or_levitra_17, 0) as rx_viagara_cilais_or_levitra_17,
coalesce(rx_viagara_cilais_or_levitra_18, 0) as rx_viagara_cilais_or_levitra_18,
coalesce(rx_viagara_cilais_or_levitra_19, 0) as rx_viagara_cilais_or_levitra_19,
coalesce(t_hiv_06, 0) as total_hiv_tests_06,
coalesce(t_hiv_07, 0) as total_hiv_tests_07,
coalesce(t_hiv_08, 0) as total_hiv_tests_08,
coalesce(t_hiv_09, 0) as total_hiv_tests_09,
coalesce(t_hiv_10, 0) as total_hiv_tests_10,
coalesce(t_hiv_11, 0) as total_hiv_tests_11,
coalesce(t_hiv_12, 0) as total_hiv_tests_12,
coalesce(t_hiv_13, 0) as total_hiv_tests_13,
coalesce(t_hiv_14, 0) as total_hiv_tests_14,
coalesce(t_hiv_15, 0) as total_hiv_tests_15,
coalesce(t_hiv_16, 0) as total_hiv_tests_16,
coalesce(t_hiv_17, 0) as total_hiv_tests_17,
coalesce(t_hiv_18, 0) as total_hiv_tests_18,
coalesce(t_hiv_19, 0) as total_hiv_tests_19,
coalesce(t_hiv_elisa_06, 0) as t_hiv_elisa_06,
coalesce(t_hiv_elisa_07, 0) as t_hiv_elisa_07,
coalesce(t_hiv_elisa_08, 0) as t_hiv_elisa_08,
coalesce(t_hiv_elisa_09, 0) as t_hiv_elisa_09,
coalesce(t_hiv_elisa_10, 0) as t_hiv_elisa_10,
coalesce(t_hiv_elisa_11, 0) as t_hiv_elisa_11,
coalesce(t_hiv_elisa_12, 0) as t_hiv_elisa_12,
coalesce(t_hiv_elisa_13, 0) as t_hiv_elisa_13,
coalesce(t_hiv_elisa_14, 0) as t_hiv_elisa_14,
coalesce(t_hiv_elisa_15, 0) as t_hiv_elisa_15,
coalesce(t_hiv_elisa_16, 0) as t_hiv_elisa_16,
coalesce(t_hiv_elisa_17, 0) as t_hiv_elisa_17,
coalesce(t_hiv_elisa_18, 0) as t_hiv_elisa_18,
coalesce(t_hiv_elisa_19, 0) as t_hiv_elisa_19,
coalesce(t_hiv_wb_06, 0) as t_hiv_wb_06,
coalesce(t_hiv_wb_07, 0) as t_hiv_wb_07,
coalesce(t_hiv_wb_08, 0) as t_hiv_wb_08,
coalesce(t_hiv_wb_09, 0) as t_hiv_wb_09,
coalesce(t_hiv_wb_10, 0) as t_hiv_wb_10,
coalesce(t_hiv_wb_11, 0) as t_hiv_wb_11,
coalesce(t_hiv_wb_12, 0) as t_hiv_wb_12,
coalesce(t_hiv_wb_13, 0) as t_hiv_wb_13,
coalesce(t_hiv_wb_14, 0) as t_hiv_wb_14,
coalesce(t_hiv_wb_15, 0) as t_hiv_wb_15,
coalesce(t_hiv_wb_16, 0) as t_hiv_wb_16,
coalesce(t_hiv_wb_17, 0) as t_hiv_wb_17,
coalesce(t_hiv_wb_18, 0) as t_hiv_wb_18,
coalesce(t_hiv_wb_19, 0) as t_hiv_wb_19,
coalesce(t_hiv_rna_06, 0) as t_hiv_rna_06,
coalesce(t_hiv_rna_07, 0) as t_hiv_rna_07,
coalesce(t_hiv_rna_08, 0) as t_hiv_rna_08,
coalesce(t_hiv_rna_09, 0) as t_hiv_rna_09,
coalesce(t_hiv_rna_10, 0) as t_hiv_rna_10,
coalesce(t_hiv_rna_11, 0) as t_hiv_rna_11,
coalesce(t_hiv_rna_12, 0) as t_hiv_rna_12,
coalesce(t_hiv_rna_13, 0) as t_hiv_rna_13,
coalesce(t_hiv_rna_14, 0) as t_hiv_rna_14,
coalesce(t_hiv_rna_15, 0) as t_hiv_rna_15,
coalesce(t_hiv_rna_16, 0) as t_hiv_rna_16,
coalesce(t_hiv_rna_17, 0) as t_hiv_rna_17,
coalesce(t_hiv_rna_18, 0) as t_hiv_rna_18,
coalesce(t_hiv_rna_19, 0) as t_hiv_rna_19,
coalesce(t_hiv_rna_06, 0) as t_hiv_agab_06,
coalesce(t_hiv_rna_07, 0) as t_hiv_agab_07,
coalesce(t_hiv_rna_08, 0) as t_hiv_agab_08,
coalesce(t_hiv_rna_09, 0) as t_hiv_agab_09,
coalesce(t_hiv_rna_10, 0) as t_hiv_agab_10,
coalesce(t_hiv_rna_11, 0) as t_hiv_agab_11,
coalesce(t_hiv_rna_12, 0) as t_hiv_agab_12,
coalesce(t_hiv_rna_13, 0) as t_hiv_agab_13,
coalesce(t_hiv_rna_14, 0) as t_hiv_agab_14,
coalesce(t_hiv_rna_15, 0) as t_hiv_agab_15,
coalesce(t_hiv_rna_16, 0) as t_hiv_agab_16,
coalesce(t_hiv_rna_17, 0) as t_hiv_agab_17,
coalesce(t_hiv_rna_18, 0) as t_hiv_agab_18,
coalesce(t_hiv_rna_19, 0) as t_hiv_agab_19,
coalesce(hiv_neg_truvada_06,0) as hiv_neg_truvada_06,
coalesce(hiv_neg_truvada_07,0) as hiv_neg_truvada_07,
coalesce(hiv_neg_truvada_08,0) as hiv_neg_truvada_08,
coalesce(hiv_neg_truvada_09,0) as hiv_neg_truvada_09,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_neg_truvada_18,0) as hiv_neg_truvada_18,
coalesce(hiv_neg_truvada_19,0) as hiv_neg_truvada_19,
coalesce(hiv_per_esp_final, 2) as hiv_per_esp_spec,
coalesce(hiv_neg_with_rna_test, 0) as hiv_neg_with_rna_test,
coalesce(hiv_neg_with_meds, 0) as hiv_neg_with_meds,
coalesce(hiv_per_esp_date, null) as hiv_per_esp_first_year,
coalesce(hiv_first_icd_year, null) as hiv_first_icd_year,
coalesce(hiv_on_problem_list, 0) as hiv_on_problem_list,
coalesce(hiv_first_prob_date, null) as hiv_first_prob_date,
coalesce(three_diff_hiv_med_06, 0) three_diff_hiv_med_06, 
coalesce(three_diff_hiv_med_07, 0) three_diff_hiv_med_07,
coalesce(three_diff_hiv_med_08, 0) three_diff_hiv_med_08,
coalesce(three_diff_hiv_med_09, 0) three_diff_hiv_med_09,
coalesce(three_diff_hiv_med_10, 0) three_diff_hiv_med_10,
coalesce(three_diff_hiv_med_11, 0) three_diff_hiv_med_11,
coalesce(three_diff_hiv_med_12, 0) three_diff_hiv_med_12,
coalesce(three_diff_hiv_med_13, 0) three_diff_hiv_med_13,
coalesce(three_diff_hiv_med_14, 0) three_diff_hiv_med_14,
coalesce(three_diff_hiv_med_15, 0) three_diff_hiv_med_15,
coalesce(three_diff_hiv_med_16, 0) three_diff_hiv_med_16,
coalesce(three_diff_hiv_med_17, 0) three_diff_hiv_med_17,
coalesce(three_diff_hiv_med_18, 0) three_diff_hiv_med_18,
coalesce(three_diff_hiv_med_19, 0) three_diff_hiv_med_19,
coalesce(truvada_num_rx_06, 0) truvada_num_rx_06,
coalesce(truvada_num_rx_07, 0) truvada_num_rx_07,
coalesce(truvada_num_rx_08, 0) truvada_num_rx_08,
coalesce(truvada_num_rx_09, 0) truvada_num_rx_09,
coalesce(truvada_num_rx_10, 0) truvada_num_rx_10,
coalesce(truvada_num_rx_11, 0) truvada_num_rx_11,
coalesce(truvada_num_rx_12, 0) truvada_num_rx_12,
coalesce(truvada_num_rx_13, 0) truvada_num_rx_13,
coalesce(truvada_num_rx_14, 0) truvada_num_rx_14,
coalesce(truvada_num_rx_15, 0) truvada_num_rx_15,
coalesce(truvada_num_rx_16, 0) truvada_num_rx_16,
coalesce(truvada_num_rx_17, 0) truvada_num_rx_17,
coalesce(truvada_num_rx_18, 0) truvada_num_rx_18,
coalesce(truvada_num_rx_19, 0) truvada_num_rx_19,
coalesce(truvada_num_pills_06, 0) truvada_num_pills_06,
coalesce(truvada_num_pills_07, 0) truvada_num_pills_07,
coalesce(truvada_num_pills_08, 0) truvada_num_pills_08,
coalesce(truvada_num_pills_09, 0) truvada_num_pills_09,
coalesce(truvada_num_pills_10, 0) truvada_num_pills_10,
coalesce(truvada_num_pills_11, 0) truvada_num_pills_11,
coalesce(truvada_num_pills_12, 0) truvada_num_pills_12,
coalesce(truvada_num_pills_13, 0) truvada_num_pills_13,
coalesce(truvada_num_pills_14, 0) truvada_num_pills_14,
coalesce(truvada_num_pills_15, 0) truvada_num_pills_15,
coalesce(truvada_num_pills_16, 0) truvada_num_pills_16,
coalesce(truvada_num_pills_17, 0) truvada_num_pills_17,
coalesce(truvada_num_pills_18, 0) truvada_num_pills_18,
coalesce(truvada_num_pills_19, 0) truvada_num_pills_19,
coalesce(new_hiv_diagnosis, null) as new_hiv_diagnosis,
coalesce(amphet_tested_06, 0) as amphet_tested_06,
coalesce(amphet_tested_07, 0) as amphet_tested_07,
coalesce(amphet_tested_08, 0) as amphet_tested_08,
coalesce(amphet_tested_09, 0) as amphet_tested_09,
coalesce(amphet_tested_10, 0) as amphet_tested_10,
coalesce(amphet_tested_11, 0) as amphet_tested_11,
coalesce(amphet_tested_12, 0) as amphet_tested_12,
coalesce(amphet_tested_13, 0) as amphet_tested_13,
coalesce(amphet_tested_14, 0) as amphet_tested_14,
coalesce(amphet_tested_15, 0) as amphet_tested_15,
coalesce(amphet_tested_16, 0) as amphet_tested_16,
coalesce(amphet_tested_17, 0) as amphet_tested_17,
coalesce(amphet_tested_18, 0) as amphet_tested_18,
coalesce(amphet_tested_19, 0) as amphet_tested_19,
coalesce(amphet_pos_06, 0) as amphet_pos_06,
coalesce(amphet_pos_07, 0) as amphet_pos_07,
coalesce(amphet_pos_08, 0) as amphet_pos_08,
coalesce(amphet_pos_09, 0) as amphet_pos_09,
coalesce(amphet_pos_10, 0) as amphet_pos_10,
coalesce(amphet_pos_11, 0) as amphet_pos_11,
coalesce(amphet_pos_12, 0) as amphet_pos_12,
coalesce(amphet_pos_13, 0) as amphet_pos_13,
coalesce(amphet_pos_14, 0) as amphet_pos_14,
coalesce(amphet_pos_15, 0) as amphet_pos_15,
coalesce(amphet_pos_16, 0) as amphet_pos_16,
coalesce(amphet_pos_17, 0) as amphet_pos_17,
coalesce(amphet_pos_18, 0) as amphet_pos_18,
coalesce(amphet_pos_19, 0) as amphet_pos_19,
coalesce(cocaine_tested_06, 0) as cocaine_tested_06,
coalesce(cocaine_tested_07, 0) as cocaine_tested_07,
coalesce(cocaine_tested_08, 0) as cocaine_tested_08,
coalesce(cocaine_tested_09, 0) as cocaine_tested_09,
coalesce(cocaine_tested_10, 0) as cocaine_tested_10,
coalesce(cocaine_tested_11, 0) as cocaine_tested_11,
coalesce(cocaine_tested_12, 0) as cocaine_tested_12,
coalesce(cocaine_tested_13, 0) as cocaine_tested_13,
coalesce(cocaine_tested_14, 0) as cocaine_tested_14,
coalesce(cocaine_tested_15, 0) as cocaine_tested_15,
coalesce(cocaine_tested_16, 0) as cocaine_tested_16,
coalesce(cocaine_tested_17, 0) as cocaine_tested_17,
coalesce(cocaine_tested_18, 0) as cocaine_tested_18,
coalesce(cocaine_tested_19, 0) as cocaine_tested_19,
coalesce(cocaine_pos_06, 0) as cocaine_pos_06,
coalesce(cocaine_pos_07, 0) as cocaine_pos_07,
coalesce(cocaine_pos_08, 0) as cocaine_pos_08,
coalesce(cocaine_pos_09, 0) as cocaine_pos_09,
coalesce(cocaine_pos_10, 0) as cocaine_pos_10,
coalesce(cocaine_pos_11, 0) as cocaine_pos_11,
coalesce(cocaine_pos_12, 0) as cocaine_pos_12,
coalesce(cocaine_pos_13, 0) as cocaine_pos_13,
coalesce(cocaine_pos_14, 0) as cocaine_pos_14,
coalesce(cocaine_pos_15, 0) as cocaine_pos_15,
coalesce(cocaine_pos_16, 0) as cocaine_pos_16,
coalesce(cocaine_pos_17, 0) as cocaine_pos_17,
coalesce(cocaine_pos_18, 0) as cocaine_pos_18,
coalesce(cocaine_pos_19, 0) as cocaine_pos_19,
coalesce(methadone_tested_06, 0) as methadone_tested_06,
coalesce(methadone_tested_07, 0) as methadone_tested_07,
coalesce(methadone_tested_08, 0) as methadone_tested_08,
coalesce(methadone_tested_09, 0) as methadone_tested_09,
coalesce(methadone_tested_10, 0) as methadone_tested_10,
coalesce(methadone_tested_11, 0) as methadone_tested_11,
coalesce(methadone_tested_12, 0) as methadone_tested_12,
coalesce(methadone_tested_13, 0) as methadone_tested_13,
coalesce(methadone_tested_14, 0) as methadone_tested_14,
coalesce(methadone_tested_15, 0) as methadone_tested_15,
coalesce(methadone_tested_16, 0) as methadone_tested_16,
coalesce(methadone_tested_17, 0) as methadone_tested_17,
coalesce(methadone_tested_18, 0) as methadone_tested_18,
coalesce(methadone_tested_19, 0) as methadone_tested_19,
coalesce(methadone_pos_06, 0) as methadone_pos_06,
coalesce(methadone_pos_07, 0) as methadone_pos_07,
coalesce(methadone_pos_08, 0) as methadone_pos_08,
coalesce(methadone_pos_09, 0) as methadone_pos_09,
coalesce(methadone_pos_10, 0) as methadone_pos_10,
coalesce(methadone_pos_11, 0) as methadone_pos_11,
coalesce(methadone_pos_12, 0) as methadone_pos_12,
coalesce(methadone_pos_13, 0) as methadone_pos_13,
coalesce(methadone_pos_14, 0) as methadone_pos_14,
coalesce(methadone_pos_15, 0) as methadone_pos_15,
coalesce(methadone_pos_16, 0) as methadone_pos_16,
coalesce(methadone_pos_17, 0) as methadone_pos_17,
coalesce(methadone_pos_18, 0) as methadone_pos_18,
coalesce(methadone_pos_19, 0) as methadone_pos_19,
coalesce(fentanyl_mam_tested_06, 0) as fentanyl_mam_tested_06,
coalesce(fentanyl_mam_tested_07, 0) as fentanyl_mam_tested_07,
coalesce(fentanyl_mam_tested_08, 0) as fentanyl_mam_tested_08,
coalesce(fentanyl_mam_tested_09, 0) as fentanyl_mam_tested_09,
coalesce(fentanyl_mam_tested_10, 0) as fentanyl_mam_tested_10,
coalesce(fentanyl_mam_tested_11, 0) as fentanyl_mam_tested_11,
coalesce(fentanyl_mam_tested_12, 0) as fentanyl_mam_tested_12,
coalesce(fentanyl_mam_tested_13, 0) as fentanyl_mam_tested_13,
coalesce(fentanyl_mam_tested_14, 0) as fentanyl_mam_tested_14,
coalesce(fentanyl_mam_tested_15, 0) as fentanyl_mam_tested_15,
coalesce(fentanyl_mam_tested_16, 0) as fentanyl_mam_tested_16,
coalesce(fentanyl_mam_tested_17, 0) as fentanyl_mam_tested_17,
coalesce(fentanyl_mam_tested_18, 0) as fentanyl_mam_tested_18,
coalesce(fentanyl_mam_tested_19, 0) as fentanyl_mam_tested_19,
coalesce(fentanyl_mam_pos_06, 0) as fentanyl_mam_pos_06,
coalesce(fentanyl_mam_pos_07, 0) as fentanyl_mam_pos_07,
coalesce(fentanyl_mam_pos_08, 0) as fentanyl_mam_pos_08,
coalesce(fentanyl_mam_pos_09, 0) as fentanyl_mam_pos_09,
coalesce(fentanyl_mam_pos_10, 0) as fentanyl_mam_pos_10,
coalesce(fentanyl_mam_pos_11, 0) as fentanyl_mam_pos_11,
coalesce(fentanyl_mam_pos_12, 0) as fentanyl_mam_pos_12,
coalesce(fentanyl_mam_pos_13, 0) as fentanyl_mam_pos_13,
coalesce(fentanyl_mam_pos_14, 0) as fentanyl_mam_pos_14,
coalesce(fentanyl_mam_pos_15, 0) as fentanyl_mam_pos_15,
coalesce(fentanyl_mam_pos_16, 0) as fentanyl_mam_pos_16,
coalesce(fentanyl_mam_pos_17, 0) as fentanyl_mam_pos_17,
coalesce(fentanyl_mam_pos_18, 0) as fentanyl_mam_pos_18,
coalesce(fentanyl_mam_pos_19, 0) as fentanyl_mam_pos_19,
coalesce(opiate_tested_06, 0) as opiate_tested_06,
coalesce(opiate_tested_07, 0) as opiate_tested_07,
coalesce(opiate_tested_08, 0) as opiate_tested_08,
coalesce(opiate_tested_09, 0) as opiate_tested_09,
coalesce(opiate_tested_10, 0) as opiate_tested_10,
coalesce(opiate_tested_11, 0) as opiate_tested_11,
coalesce(opiate_tested_12, 0) as opiate_tested_12,
coalesce(opiate_tested_13, 0) as opiate_tested_13,
coalesce(opiate_tested_14, 0) as opiate_tested_14,
coalesce(opiate_tested_15, 0) as opiate_tested_15,
coalesce(opiate_tested_16, 0) as opiate_tested_16,
coalesce(opiate_tested_17, 0) as opiate_tested_17,
coalesce(opiate_tested_18, 0) as opiate_tested_18,
coalesce(opiate_tested_19, 0) as opiate_tested_19,
coalesce(opiate_pos_06, 0) as opiate_pos_06,
coalesce(opiate_pos_07, 0) as opiate_pos_07,
coalesce(opiate_pos_08, 0) as opiate_pos_08,
coalesce(opiate_pos_09, 0) as opiate_pos_09,
coalesce(opiate_pos_10, 0) as opiate_pos_10,
coalesce(opiate_pos_11, 0) as opiate_pos_11,
coalesce(opiate_pos_12, 0) as opiate_pos_12,
coalesce(opiate_pos_13, 0) as opiate_pos_13,
coalesce(opiate_pos_14, 0) as opiate_pos_14,
coalesce(opiate_pos_15, 0) as opiate_pos_15,
coalesce(opiate_pos_16, 0) as opiate_pos_16,
coalesce(opiate_pos_17, 0) as opiate_pos_17,
coalesce(opiate_pos_18, 0) as opiate_pos_18,
coalesce(opiate_pos_19, 0) as opiate_pos_19,
coalesce(med_asst_trmt_tested_06, 0) as med_asst_trmt_tested_06,
coalesce(med_asst_trmt_tested_07, 0) as med_asst_trmt_tested_07,
coalesce(med_asst_trmt_tested_08, 0) as med_asst_trmt_tested_08,
coalesce(med_asst_trmt_tested_09, 0) as med_asst_trmt_tested_09,
coalesce(med_asst_trmt_tested_10, 0) as med_asst_trmt_tested_10,
coalesce(med_asst_trmt_tested_11, 0) as med_asst_trmt_tested_11,
coalesce(med_asst_trmt_tested_12, 0) as med_asst_trmt_tested_12,
coalesce(med_asst_trmt_tested_13, 0) as med_asst_trmt_tested_13,
coalesce(med_asst_trmt_tested_14, 0) as med_asst_trmt_tested_14,
coalesce(med_asst_trmt_tested_15, 0) as med_asst_trmt_tested_15,
coalesce(med_asst_trmt_tested_16, 0) as med_asst_trmt_tested_16,
coalesce(med_asst_trmt_tested_17, 0) as med_asst_trmt_tested_17,
coalesce(med_asst_trmt_tested_18, 0) as med_asst_trmt_tested_18,
coalesce(med_asst_trmt_tested_19, 0) as med_asst_trmt_tested_19,
coalesce(med_asst_trmt_pos_06, 0) as med_asst_trmt_pos_06,
coalesce(med_asst_trmt_pos_07, 0) as med_asst_trmt_pos_07,
coalesce(med_asst_trmt_pos_08, 0) as med_asst_trmt_pos_08,
coalesce(med_asst_trmt_pos_09, 0) as med_asst_trmt_pos_09,
coalesce(med_asst_trmt_pos_10, 0) as med_asst_trmt_pos_10,
coalesce(med_asst_trmt_pos_11, 0) as med_asst_trmt_pos_11,
coalesce(med_asst_trmt_pos_12, 0) as med_asst_trmt_pos_12,
coalesce(med_asst_trmt_pos_13, 0) as med_asst_trmt_pos_13,
coalesce(med_asst_trmt_pos_14, 0) as med_asst_trmt_pos_14,
coalesce(med_asst_trmt_pos_15, 0) as med_asst_trmt_pos_15,
coalesce(med_asst_trmt_pos_16, 0) as med_asst_trmt_pos_16,
coalesce(med_asst_trmt_pos_17, 0) as med_asst_trmt_pos_17,
coalesce(med_asst_trmt_pos_18, 0) as med_asst_trmt_pos_18,
coalesce(med_asst_trmt_pos_19, 0) as med_asst_trmt_pos_19
FROM hiv_rpt.prep_diag_fields d
FULL OUTER JOIN hiv_rpt.prep_gon_counts g
 ON d.patient_id=g.patient_id
FULL OUTER JOIN hiv_rpt.prep_chlam_counts c
 ON c.patient_id = coalesce(d.patient_id, g.patient_id)
FULL OUTER JOIN hiv_rpt.prep_syph_counts s
 ON s.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hepc_counts hc
 ON hc.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hepc_cases hcc
 ON hcc.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hepb_counts hb
 ON hb.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hebp_cases hbc
 ON hbc.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id)
FULL OUTER JOIN hiv_rpt.prep_rx_counts rx
 ON rx.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id)
FULL OUTER JOIN hiv_rpt.prep_cpts_of_interest cpt
 ON cpt.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id)
 -- New starts here
FULL OUTER JOIN hiv_rpt.prep_hiv_counts hivc
 ON hivc.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hiv_all_details hivd
 ON hivd.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id)
FULL OUTER JOIN hiv_rpt.prep_syph_cases syph
 ON syph.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hiv_new_diag hivdiag
 ON hivdiag.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id)
FULL OUTER JOIN hiv_rpt.prep_hepb_diags_2 hepbdiag
	ON hepbdiag.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id)
FULL OUTER JOIN hiv_rpt.prep_herpes_direct_counts herpes_direct
	ON herpes_direct.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id)
FULL OUTER JOIN hiv_rpt.prep_tox_amph amph
	ON amph.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id)	
FULL OUTER JOIN hiv_rpt.prep_tox_cocaine cocaine
	ON cocaine.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id)	
FULL OUTER JOIN hiv_rpt.prep_tox_methadone methadone
	ON methadone.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id, cocaine.patient_id)	
FULL OUTER JOIN hiv_rpt.prep_tox_fentanyl fentanyl	
	ON fentanyl.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id, cocaine.patient_id, methadone.patient_id)	
FULL OUTER JOIN hiv_rpt.prep_tox_opiate opiate
	ON opiate.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id, cocaine.patient_id, methadone.patient_id, fentanyl.patient_id)
FULL OUTER JOIN hiv_rpt.prep_herpes_serology_counts herpes_serology
	ON herpes_serology.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id, cocaine.patient_id, methadone.patient_id, fentanyl.patient_id, opiate.patient_id)
FULL OUTER JOIN hiv_rpt.prep_tox_med_asst_trmt med_asst 
    ON med_asst.patient_id = coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id, hivc.patient_id, hivd.patient_id, syph.patient_id, hivdiag.patient_id, hepbdiag.patient_id, herpes_direct.patient_id, amph.patient_id, cocaine.patient_id, methadone.patient_id, fentanyl.patient_id, opiate.patient_id, herpes_serology.patient_id);







-- JOIN TO PATIENT TABLE
-- OK
-- ADD IN SOCIAL HISTORY VALUES
-- ADD IN SOCIAL HISTORY VALUES
CREATE TABLE hiv_rpt.prep_output_with_patient AS SELECT 
T1.id as patient_id,
date_part('year',age(date_of_birth)) age,
T1.gender,
T1.race,
T1.home_language,
T1.marital_stat,
T1.zip5,
T2.*
FROM emr_patient T1 
INNER JOIN hiv_rpt.prep_output_part_2 T2 ON ((T1.id = T2.master_patient_id))
--LEFT JOIN hiv_rpt.atrius_soc_history T3 ON (T3.pat_id = T1.natural_key)
WHERE T1.last_name not in ('XBIALIDOCIOUS','XBTEST','XBTESTOBGYN','XB','XYZ');


--GET LAST NON NULL VALUE FOR EACH YEAR AND CATEGORIZE
CREATE TABLE hiv_rpt.prep_sex_part_gend AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_06,
MAX(CASE WHEN rpt_year = '2006' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_06,
MAX(CASE WHEN rpt_year = '2007' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_07,
MAX(CASE WHEN rpt_year = '2007' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_07,
MAX(CASE WHEN rpt_year = '2008' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_08,
MAX(CASE WHEN rpt_year = '2008' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_08,
MAX(CASE WHEN rpt_year = '2009' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_09,
MAX(CASE WHEN rpt_year = '2009' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_09,
MAX(CASE WHEN rpt_year = '2010' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_10,
MAX(CASE WHEN rpt_year = '2010' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_10,
MAX(CASE WHEN rpt_year = '2011' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_11,
MAX(CASE WHEN rpt_year = '2011' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_11,
MAX(CASE WHEN rpt_year = '2012' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_12,
MAX(CASE WHEN rpt_year = '2012' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_12,
MAX(CASE WHEN rpt_year = '2013' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_13,
MAX(CASE WHEN rpt_year = '2013' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_13,
MAX(CASE WHEN rpt_year = '2014' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_14,
MAX(CASE WHEN rpt_year = '2014' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_14,
MAX(CASE WHEN rpt_year = '2015' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_15,
MAX(CASE WHEN rpt_year = '2015' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_15,
MAX(CASE WHEN rpt_year = '2016' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_16,
MAX(CASE WHEN rpt_year = '2016' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_16,
MAX(CASE WHEN rpt_year = '2017' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_17,
MAX(CASE WHEN rpt_year = '2017' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_17,
MAX(CASE WHEN rpt_year = '2018' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_18,
MAX(CASE WHEN rpt_year = '2018' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_18,
MAX(CASE WHEN rpt_year = '2019' and sex_partner_gender in ('FEMALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) female_partner_19,
MAX(CASE WHEN rpt_year = '2019' and sex_partner_gender in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE') then 'Y' else NULL END) male_partner_19
FROM 
	(SELECT T1.patient_id, rpt_year, sex_partner_gender
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE sex_partner_gender is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;



--GET LAST NON NULL VALUE FOR EACH YEAR AND CATEGORIZE

CREATE TABLE hiv_rpt.prep_alcohol_use AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2006' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_06,
MAX(CASE WHEN rpt_year = '2007' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2007' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_07,
MAX(CASE WHEN rpt_year = '2008' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2008' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_08,
MAX(CASE WHEN rpt_year = '2009' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2009' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_09,
MAX(CASE WHEN rpt_year = '2010' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2010' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_10,
MAX(CASE WHEN rpt_year = '2011' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2011' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_11,
MAX(CASE WHEN rpt_year = '2012' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2012' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_12,
MAX(CASE WHEN rpt_year = '2013' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2013' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_13,
MAX(CASE WHEN rpt_year = '2014' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2014' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_14,
MAX(CASE WHEN rpt_year = '2015' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2015' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_15,
MAX(CASE WHEN rpt_year = '2016' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2016' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_16,
MAX(CASE WHEN rpt_year = '2017' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2017' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_17,
MAX(CASE WHEN rpt_year = '2018' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2018' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_18,
MAX(CASE WHEN rpt_year = '2019' and alcohol_use in ('Yes') then 'Y' when rpt_year = '2019' and alcohol_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) alcohol_use_19
FROM 
	(SELECT T1.patient_id, rpt_year, alcohol_use
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN  hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE alcohol_use is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;


-- GET LAST NON NULL VALUE FOR EACH YEAR

CREATE TABLE hiv_rpt.prep_alcohol_oz_per_week AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_06,
MAX(CASE WHEN rpt_year = '2007' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_07,
MAX(CASE WHEN rpt_year = '2008' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_08,
MAX(CASE WHEN rpt_year = '2009' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_09,
MAX(CASE WHEN rpt_year = '2010' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_10,
MAX(CASE WHEN rpt_year = '2011' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_11,
MAX(CASE WHEN rpt_year = '2012' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_12,
MAX(CASE WHEN rpt_year = '2013' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_13,
MAX(CASE WHEN rpt_year = '2014' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_14,
MAX(CASE WHEN rpt_year = '2015' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_15,
MAX(CASE WHEN rpt_year = '2016' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_16,
MAX(CASE WHEN rpt_year = '2017' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_17,
MAX(CASE WHEN rpt_year = '2018' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_18,
MAX(CASE WHEN rpt_year = '2019' then alcohol_oz_per_week else NULL END) alcohol_oz_per_wk_19
FROM 
	(SELECT T1.patient_id, rpt_year, alcohol_oz_per_week
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN  hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE alcohol_oz_per_week is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;





--GET LAST NON NULL VALUE FOR EACH YEAR AND CATEGORIZE
CREATE TABLE hiv_rpt.prep_ill_drug_use AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2006' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_06,
MAX(CASE WHEN rpt_year = '2007' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2007' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_07,
MAX(CASE WHEN rpt_year = '2008' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2008' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_08,
MAX(CASE WHEN rpt_year = '2009' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2009' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_09,
MAX(CASE WHEN rpt_year = '2010' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2010' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_10,
MAX(CASE WHEN rpt_year = '2011' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2011' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_11,
MAX(CASE WHEN rpt_year = '2012' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2012' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_12,
MAX(CASE WHEN rpt_year = '2013' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2013' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_13,
MAX(CASE WHEN rpt_year = '2014' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2014' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_14,
MAX(CASE WHEN rpt_year = '2015' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2015' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_15,
MAX(CASE WHEN rpt_year = '2016' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2016' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_16,
MAX(CASE WHEN rpt_year = '2017' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2017' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_17,
MAX(CASE WHEN rpt_year = '2018' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2018' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_18,
MAX(CASE WHEN rpt_year = '2019' and ill_drug_use in ('Yes') then 'Y' when rpt_year = '2019' and ill_drug_use in ('No', 'Never', 'Not Currently') then 'N' else NULL END) ill_drug_use_19
FROM 
	(SELECT T1.patient_id, rpt_year, ill_drug_use
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN  hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE ill_drug_use is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;



--GET LAST NON NULL VALUE FOR EACH YEAR AND CATEGORIZE
CREATE TABLE hiv_rpt.prep_sexually_active AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' and sexually_active in ('Yes') then 'Y' when rpt_year = '2006' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_06,
MAX(CASE WHEN rpt_year = '2007' and sexually_active in ('Yes') then 'Y' when rpt_year = '2007' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_07,
MAX(CASE WHEN rpt_year = '2008' and sexually_active in ('Yes') then 'Y' when rpt_year = '2008' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_08,
MAX(CASE WHEN rpt_year = '2009' and sexually_active in ('Yes') then 'Y' when rpt_year = '2009' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_09,
MAX(CASE WHEN rpt_year = '2010' and sexually_active in ('Yes') then 'Y' when rpt_year = '2010' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_10,
MAX(CASE WHEN rpt_year = '2011' and sexually_active in ('Yes') then 'Y' when rpt_year = '2011' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_11,
MAX(CASE WHEN rpt_year = '2012' and sexually_active in ('Yes') then 'Y' when rpt_year = '2012' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_12,
MAX(CASE WHEN rpt_year = '2013' and sexually_active in ('Yes') then 'Y' when rpt_year = '2013' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_13,
MAX(CASE WHEN rpt_year = '2014' and sexually_active in ('Yes') then 'Y' when rpt_year = '2014' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_14,
MAX(CASE WHEN rpt_year = '2015' and sexually_active in ('Yes') then 'Y' when rpt_year = '2015' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_15,
MAX(CASE WHEN rpt_year = '2016' and sexually_active in ('Yes') then 'Y' when rpt_year = '2016' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_16,
MAX(CASE WHEN rpt_year = '2017' and sexually_active in ('Yes') then 'Y' when rpt_year = '2017' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_17,
MAX(CASE WHEN rpt_year = '2018' and sexually_active in ('Yes') then 'Y' when rpt_year = '2018' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_18,
MAX(CASE WHEN rpt_year = '2019' and sexually_active in ('Yes') then 'Y' when rpt_year = '2019' and sexually_active in ('No', 'Never', 'Not Currently') then 'N' else NULL END) sexually_active_19
FROM 
	(SELECT T1.patient_id, rpt_year, sexually_active
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN  hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE sexually_active is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;



--GET LAST NON NULL VALUE FOR EACH YEAR AND CATEGORIZE
CREATE TABLE hiv_rpt.prep_birth_control_method AS 
SELECT S1.patient_id as master_patient_id, 
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_06,
MAX(CASE WHEN rpt_year = '2006' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_06,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_07,
MAX(CASE WHEN rpt_year = '2007' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_07,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_08,
MAX(CASE WHEN rpt_year = '2008' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_08,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_09,
MAX(CASE WHEN rpt_year = '2009' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_09,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_10,
MAX(CASE WHEN rpt_year = '2010' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_10,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_11,
MAX(CASE WHEN rpt_year = '2011' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_11,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_12,
MAX(CASE WHEN rpt_year = '2012' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_12,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_13,
MAX(CASE WHEN rpt_year = '2013' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_13,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_14,
MAX(CASE WHEN rpt_year = '2014' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_14,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_15,
MAX(CASE WHEN rpt_year = '2015' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_15,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_16,
MAX(CASE WHEN rpt_year = '2016' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_16,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_17,
MAX(CASE WHEN rpt_year = '2017' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_17,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_18,
MAX(CASE WHEN rpt_year = '2018' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_18,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%CONDOM%' then 'Y' else NULL END) bcm_condom_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%PILL%' then 'Y' else NULL END) bcm_pill_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%DIAPHRAGM%' then 'Y' else NULL END) bcm_diaphragm_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%IUD%' then 'Y' else NULL END) bcm_iud_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%SPERMICIDE%' then 'Y' else NULL END) bcm_spermicide_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%IMPLANT%' then 'Y' else NULL END) bcm_implant_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%RHYTHM%' then 'Y' else NULL END) bcm_rhythm_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%INJECTION%' then 'Y' else NULL END) bcm_injection_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%SPONGE%' then 'Y' else NULL END) bcm_sponge_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%INSERTS%' then 'Y' else NULL END) bcm_inserts_19,
MAX(CASE WHEN rpt_year = '2019' and birth_control_method ilike '%ABSTINENCE%' then 'Y' else NULL END) bcm_abstinence_19
FROM 
	(SELECT T1.patient_id, rpt_year, birth_control_method
	 FROM 
		(SELECT T1.patient_id, extract(year from date) rpt_year, max(date) last_entry_for_year 
		 FROM emr_socialhistory T1
		 JOIN  hiv_rpt.prep_output_with_patient T2 on (T1.patient_id = T2.master_patient_id)
		 WHERE birth_control_method is not null
		 GROUP BY T1.patient_id, extract(year from date)
		 ORDER BY rpt_year) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T1.last_entry_for_year = T2.date)) S1
GROUP BY S1.patient_id;


-- ENCOUNTERS - Gather up encounters
-- OK
CREATE TABLE hiv_rpt.prep_output_pat_and_enc AS 
SELECT T2.master_patient_id, EXTRACT(YEAR FROM date) rpt_year, T1.id as enc_id 
FROM emr_encounter T1 
INNER JOIN hiv_rpt.prep_output_with_patient T2 ON ((T1.patient_id = T2.master_patient_id)) 
LEFT JOIN static_enc_type_lookup T3 on (T3.raw_encounter_type = T1.raw_encounter_type)
WHERE T1.date >= '01-01-2006' 
AND T1.date < '01-01-2020'
AND (T3.ambulatory = 1 or T1.raw_encounter_type is null);

-- ENCOUNTERS - Counts & Column Creation
-- OK
CREATE TABLE hiv_rpt.prep_ouput_pat_and_enc_counts AS 
SELECT T1.master_patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END) t_encounters_06,
count(CASE WHEN rpt_year = '2007' THEN 1 END) t_encounters_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END) t_encounters_08,
count(CASE WHEN rpt_year = '2009' THEN 1 END) t_encounters_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END) t_encounters_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END) t_encounters_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END) t_encounters_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END) t_encounters_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END) t_encounters_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END) t_encounters_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END) t_encounters_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END) t_encounters_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END) t_encounters_18,
count(CASE WHEN rpt_year = '2019' THEN 1 END) t_encounters_19
FROM hiv_rpt.prep_output_pat_and_enc T1 
INNER JOIN hiv_rpt.prep_output_with_patient T2 ON ((T1.master_patient_id = T2.master_patient_id)) 
GROUP BY T1.master_patient_id;

-- HIV Encounters - # of HIV encounters per year
-- OK
CREATE TABLE hiv_rpt.prep_ouput_pat_and_hiv_enc_counts AS 
SELECT T1.master_patient_id,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2006' THEN 1 END) t_hiv_encounters_06,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2007' THEN 1 END) t_hiv_encounters_07,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2008' THEN 1 END) t_hiv_encounters_08,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2009' THEN 1 END) t_hiv_encounters_09,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2010' THEN 1 END) t_hiv_encounters_10,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2011' THEN 1 END) t_hiv_encounters_11,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2012' THEN 1 END) t_hiv_encounters_12,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2013' THEN 1 END) t_hiv_encounters_13,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2014' THEN 1 END) t_hiv_encounters_14,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2015' THEN 1 END) t_hiv_encounters_15,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2016' THEN 1 END) t_hiv_encounters_16,
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2017' THEN 1 END) t_hiv_encounters_17, 
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2018' THEN 1 END) t_hiv_encounters_18, 
count(CASE WHEN T3.dx_code_id~ '^(icd9:042|icd9:V08|icd9:079.53|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:O98.7)' AND rpt_year = '2019' THEN 1 END) t_hiv_encounters_19 
FROM hiv_rpt.prep_output_pat_and_enc T1 
INNER JOIN hiv_rpt.prep_output_with_patient T2 ON ((T1.master_patient_id = T2.master_patient_id)) 
INNER JOIN hiv_rpt.prep_dx_codes T3 ON (T1.enc_id = T3.encounter_id) 
GROUP BY T1.master_patient_id;

-- JOIN TO PREVIOUS DATA WITH ENCOUNTERS FOR FULL REPORT
CREATE TABLE hiv_rpt.prep_report_final_output AS SELECT 
T1.master_patient_id,
age,
gender,
race,
home_language,
marital_stat,
zip5,
coalesce(female_partner_06, null) female_partner_06,
coalesce(male_partner_06, null) male_partner_06,
coalesce(female_partner_07, null) female_partner_07,
coalesce(male_partner_07, null) male_partner_07,
coalesce(female_partner_08, null) female_partner_08,
coalesce(male_partner_08, null) male_partner_08,
coalesce(female_partner_09, null) female_partner_09,
coalesce(male_partner_09, null) male_partner_09,
coalesce(female_partner_10, null) female_partner_10,
coalesce(male_partner_10, null) male_partner_10,
coalesce(female_partner_11, null) female_partner_11,
coalesce(male_partner_11, null) male_partner_11,
coalesce(female_partner_12, null) female_partner_12,
coalesce(male_partner_12, null) male_partner_12,
coalesce(female_partner_13, null) female_partner_13,
coalesce(male_partner_13, null) male_partner_13,
coalesce(female_partner_14, null) female_partner_14,
coalesce(male_partner_14, null) male_partner_14,
coalesce(female_partner_15, null) female_partner_15,
coalesce(male_partner_15, null) male_partner_15,
coalesce(female_partner_16, null) female_partner_16,
coalesce(male_partner_16, null) male_partner_16,
coalesce(female_partner_17, null) female_partner_17,
coalesce(male_partner_17, null) male_partner_17,
coalesce(female_partner_18, null) female_partner_18,
coalesce(male_partner_18, null) male_partner_18,
coalesce(female_partner_19, null) female_partner_19,
coalesce(male_partner_19, null) male_partner_19,
coalesce(alcohol_use_06, NULL) alcohol_use_06,
coalesce(alcohol_use_07, NULL) alcohol_use_07,
coalesce(alcohol_use_08, NULL) alcohol_use_08,
coalesce(alcohol_use_09, NULL) alcohol_use_09,
coalesce(alcohol_use_10, NULL) alcohol_use_10,
coalesce(alcohol_use_11, NULL) alcohol_use_11,
coalesce(alcohol_use_12, NULL) alcohol_use_12,
coalesce(alcohol_use_13, NULL) alcohol_use_13,
coalesce(alcohol_use_14, NULL) alcohol_use_14,
coalesce(alcohol_use_15, NULL) alcohol_use_15,
coalesce(alcohol_use_16, NULL) alcohol_use_16,
coalesce(alcohol_use_17, NULL) alcohol_use_17,
coalesce(alcohol_use_18, NULL) alcohol_use_18,
coalesce(alcohol_use_19, NULL) alcohol_use_19,
coalesce(alcohol_oz_per_wk_06, NULL) alcohol_oz_per_wk_06,
coalesce(alcohol_oz_per_wk_07, NULL) alcohol_oz_per_wk_07,
coalesce(alcohol_oz_per_wk_08, NULL) alcohol_oz_per_wk_08,
coalesce(alcohol_oz_per_wk_09, NULL) alcohol_oz_per_wk_09,
coalesce(alcohol_oz_per_wk_10, NULL) alcohol_oz_per_wk_10,
coalesce(alcohol_oz_per_wk_11, NULL) alcohol_oz_per_wk_11,
coalesce(alcohol_oz_per_wk_12, NULL) alcohol_oz_per_wk_12,
coalesce(alcohol_oz_per_wk_13, NULL) alcohol_oz_per_wk_13,
coalesce(alcohol_oz_per_wk_14, NULL) alcohol_oz_per_wk_14,
coalesce(alcohol_oz_per_wk_15, NULL) alcohol_oz_per_wk_15,
coalesce(alcohol_oz_per_wk_16, NULL) alcohol_oz_per_wk_16,
coalesce(alcohol_oz_per_wk_17, NULL) alcohol_oz_per_wk_17,
coalesce(alcohol_oz_per_wk_18, NULL) alcohol_oz_per_wk_18,
coalesce(alcohol_oz_per_wk_19, NULL) alcohol_oz_per_wk_19,
coalesce(ill_drug_use_06, NULL) ill_drug_use_06,
coalesce(ill_drug_use_07, NULL) ill_drug_use_07,
coalesce(ill_drug_use_08, NULL) ill_drug_use_08,
coalesce(ill_drug_use_09, NULL) ill_drug_use_09,
coalesce(ill_drug_use_10, NULL) ill_drug_use_10,
coalesce(ill_drug_use_11, NULL) ill_drug_use_11,
coalesce(ill_drug_use_12, NULL) ill_drug_use_12,
coalesce(ill_drug_use_13, NULL) ill_drug_use_13,
coalesce(ill_drug_use_14, NULL) ill_drug_use_14,
coalesce(ill_drug_use_15, NULL) ill_drug_use_15,
coalesce(ill_drug_use_16, NULL) ill_drug_use_16,
coalesce(ill_drug_use_17, NULL) ill_drug_use_17,
coalesce(ill_drug_use_18, NULL) ill_drug_use_18,
coalesce(ill_drug_use_19, NULL) ill_drug_use_19,
coalesce(sexually_active_06, NULL) sexually_active_06,
coalesce(sexually_active_07, NULL) sexually_active_07,
coalesce(sexually_active_08, NULL) sexually_active_08,
coalesce(sexually_active_09, NULL) sexually_active_09,
coalesce(sexually_active_10, NULL) sexually_active_10,
coalesce(sexually_active_11, NULL) sexually_active_11,
coalesce(sexually_active_12, NULL) sexually_active_12,
coalesce(sexually_active_13, NULL) sexually_active_13,
coalesce(sexually_active_14, NULL) sexually_active_14,
coalesce(sexually_active_15, NULL) sexually_active_15,
coalesce(sexually_active_16, NULL) sexually_active_16,
coalesce(sexually_active_17, NULL) sexually_active_17,
coalesce(sexually_active_18, NULL) sexually_active_18,
coalesce(sexually_active_19, NULL) sexually_active_19,
coalesce(bcm_condom_06, NULL) bcm_condom_06,
coalesce(bcm_pill_06, NULL) bcm_pill_06,
coalesce(bcm_diaphragm_06, NULL) bcm_diaphragm_06,
coalesce(bcm_iud_06, NULL) bcm_iud_06,
coalesce(bcm_spermicide_06, NULL) bcm_spermicide_06,
coalesce(bcm_implant_06, NULL) bcm_implant_06,
coalesce(bcm_rhythm_06, NULL) bcm_rhythm_06,
coalesce(bcm_injection_06, NULL) bcm_injection_06,
coalesce(bcm_sponge_06, NULL) bcm_sponge_06,
coalesce(bcm_inserts_06, NULL) bcm_inserts_06,
coalesce(bcm_abstinence_06, NULL) bcm_abstinence_06,
coalesce(bcm_condom_07, NULL) bcm_condom_07,
coalesce(bcm_pill_07, NULL) bcm_pill_07,
coalesce(bcm_diaphragm_07, NULL) bcm_diaphragm_07,
coalesce(bcm_iud_07, NULL) bcm_iud_07,
coalesce(bcm_spermicide_07, NULL) bcm_spermicide_07,
coalesce(bcm_implant_07, NULL) bcm_implant_07,
coalesce(bcm_rhythm_07, NULL) bcm_rhythm_07,
coalesce(bcm_injection_07, NULL) bcm_injection_07,
coalesce(bcm_sponge_07, NULL) bcm_sponge_07,
coalesce(bcm_inserts_07, NULL) bcm_inserts_07,
coalesce(bcm_abstinence_07, NULL) bcm_abstinence_07,
coalesce(bcm_condom_08, NULL) bcm_condom_08,
coalesce(bcm_pill_08, NULL) bcm_pill_08,
coalesce(bcm_diaphragm_08, NULL) bcm_diaphragm_08,
coalesce(bcm_iud_08, NULL) bcm_iud_08,
coalesce(bcm_spermicide_08, NULL) bcm_spermicide_08,
coalesce(bcm_implant_08, NULL) bcm_implant_08,
coalesce(bcm_rhythm_08, NULL) bcm_rhythm_08,
coalesce(bcm_injection_08, NULL) bcm_injection_08,
coalesce(bcm_sponge_08, NULL) bcm_sponge_08,
coalesce(bcm_inserts_08, NULL) bcm_inserts_08,
coalesce(bcm_abstinence_08, NULL) bcm_abstinence_08,
coalesce(bcm_condom_09, NULL) bcm_condom_09,
coalesce(bcm_pill_09, NULL) bcm_pill_09,
coalesce(bcm_diaphragm_09, NULL) bcm_diaphragm_09,
coalesce(bcm_iud_09, NULL) bcm_iud_09,
coalesce(bcm_spermicide_09, NULL) bcm_spermicide_09,
coalesce(bcm_implant_09, NULL) bcm_implant_09,
coalesce(bcm_rhythm_09, NULL) bcm_rhythm_09,
coalesce(bcm_injection_09, NULL) bcm_injection_09,
coalesce(bcm_sponge_09, NULL) bcm_sponge_09,
coalesce(bcm_inserts_09, NULL) bcm_inserts_09,
coalesce(bcm_abstinence_09, NULL) bcm_abstinence_09,
coalesce(bcm_condom_10, NULL) bcm_condom_10,
coalesce(bcm_pill_10, NULL) bcm_pill_10,
coalesce(bcm_diaphragm_10, NULL) bcm_diaphragm_10,
coalesce(bcm_iud_10, NULL) bcm_iud_10,
coalesce(bcm_spermicide_10, NULL) bcm_spermicide_10,
coalesce(bcm_implant_10, NULL) bcm_implant_10,
coalesce(bcm_rhythm_10, NULL) bcm_rhythm_10,
coalesce(bcm_injection_10, NULL) bcm_injection_10,
coalesce(bcm_sponge_10, NULL) bcm_sponge_10,
coalesce(bcm_inserts_10, NULL) bcm_inserts_10,
coalesce(bcm_abstinence_10, NULL) bcm_abstinence_10,
coalesce(bcm_condom_11, NULL) bcm_condom_11,
coalesce(bcm_pill_11, NULL) bcm_pill_11,
coalesce(bcm_diaphragm_11, NULL) bcm_diaphragm_11,
coalesce(bcm_iud_11, NULL) bcm_iud_11,
coalesce(bcm_spermicide_11, NULL) bcm_spermicide_11,
coalesce(bcm_implant_11, NULL) bcm_implant_11,
coalesce(bcm_rhythm_11, NULL) bcm_rhythm_11,
coalesce(bcm_injection_11, NULL) bcm_injection_11,
coalesce(bcm_sponge_11, NULL) bcm_sponge_11,
coalesce(bcm_inserts_11, NULL) bcm_inserts_11,
coalesce(bcm_abstinence_11, NULL) bcm_abstinence_11,
coalesce(bcm_condom_12, NULL) bcm_condom_12,
coalesce(bcm_pill_12, NULL) bcm_pill_12,
coalesce(bcm_diaphragm_12, NULL) bcm_diaphragm_12,
coalesce(bcm_iud_12, NULL) bcm_iud_12,
coalesce(bcm_spermicide_12, NULL) bcm_spermicide_12,
coalesce(bcm_implant_12, NULL) bcm_implant_12,
coalesce(bcm_rhythm_12, NULL) bcm_rhythm_12,
coalesce(bcm_injection_12, NULL) bcm_injection_12,
coalesce(bcm_sponge_12, NULL) bcm_sponge_12,
coalesce(bcm_inserts_12, NULL) bcm_inserts_12,
coalesce(bcm_abstinence_12, NULL) bcm_abstinence_12,
coalesce(bcm_condom_13, NULL) bcm_condom_13,
coalesce(bcm_pill_13, NULL) bcm_pill_13,
coalesce(bcm_diaphragm_13, NULL) bcm_diaphragm_13,
coalesce(bcm_iud_13, NULL) bcm_iud_13,
coalesce(bcm_spermicide_13, NULL) bcm_spermicide_13,
coalesce(bcm_implant_13, NULL) bcm_implant_13,
coalesce(bcm_rhythm_13, NULL) bcm_rhythm_13,
coalesce(bcm_injection_13, NULL) bcm_injection_13,
coalesce(bcm_sponge_13, NULL) bcm_sponge_13,
coalesce(bcm_inserts_13, NULL) bcm_inserts_13,
coalesce(bcm_abstinence_13, NULL) bcm_abstinence_13,
coalesce(bcm_condom_14, NULL) bcm_condom_14,
coalesce(bcm_pill_14, NULL) bcm_pill_14,
coalesce(bcm_diaphragm_14, NULL) bcm_diaphragm_14,
coalesce(bcm_iud_14, NULL) bcm_iud_14,
coalesce(bcm_spermicide_14, NULL) bcm_spermicide_14,
coalesce(bcm_implant_14, NULL) bcm_implant_14,
coalesce(bcm_rhythm_14, NULL) bcm_rhythm_14,
coalesce(bcm_injection_14, NULL) bcm_injection_14,
coalesce(bcm_sponge_14, NULL) bcm_sponge_14,
coalesce(bcm_inserts_14, NULL) bcm_inserts_14,
coalesce(bcm_abstinence_14, NULL) bcm_abstinence_14,
coalesce(bcm_condom_15, NULL) bcm_condom_15,
coalesce(bcm_pill_15, NULL) bcm_pill_15,
coalesce(bcm_diaphragm_15, NULL) bcm_diaphragm_15,
coalesce(bcm_iud_15, NULL) bcm_iud_15,
coalesce(bcm_spermicide_15, NULL) bcm_spermicide_15,
coalesce(bcm_implant_15, NULL) bcm_implant_15,
coalesce(bcm_rhythm_15, NULL) bcm_rhythm_15,
coalesce(bcm_injection_15, NULL) bcm_injection_15,
coalesce(bcm_sponge_15, NULL) bcm_sponge_15,
coalesce(bcm_inserts_15, NULL) bcm_inserts_15,
coalesce(bcm_abstinence_15, NULL) bcm_abstinence_15,
coalesce(bcm_condom_16, NULL) bcm_condom_16,
coalesce(bcm_pill_16, NULL) bcm_pill_16,
coalesce(bcm_diaphragm_16, NULL) bcm_diaphragm_16,
coalesce(bcm_iud_16, NULL) bcm_iud_16,
coalesce(bcm_spermicide_16, NULL) bcm_spermicide_16,
coalesce(bcm_implant_16, NULL) bcm_implant_16,
coalesce(bcm_rhythm_16, NULL) bcm_rhythm_16,
coalesce(bcm_injection_16, NULL) bcm_injection_16,
coalesce(bcm_sponge_16, NULL) bcm_sponge_16,
coalesce(bcm_inserts_16, NULL) bcm_inserts_16,
coalesce(bcm_abstinence_16, NULL) bcm_abstinence_16,
coalesce(bcm_condom_17, NULL) bcm_condom_17,
coalesce(bcm_pill_17, NULL) bcm_pill_17,
coalesce(bcm_diaphragm_17, NULL) bcm_diaphragm_17,
coalesce(bcm_iud_17, NULL) bcm_iud_17,
coalesce(bcm_spermicide_17, NULL) bcm_spermicide_17,
coalesce(bcm_implant_17, NULL) bcm_implant_17,
coalesce(bcm_rhythm_17, NULL) bcm_rhythm_17,
coalesce(bcm_injection_17, NULL) bcm_injection_17,
coalesce(bcm_sponge_17, NULL) bcm_sponge_17,
coalesce(bcm_inserts_17, NULL) bcm_inserts_17,
coalesce(bcm_abstinence_17, NULL) bcm_abstinence_17,
coalesce(bcm_condom_18, NULL) bcm_condom_18,
coalesce(bcm_pill_18, NULL) bcm_pill_18,
coalesce(bcm_diaphragm_18, NULL) bcm_diaphragm_18,
coalesce(bcm_iud_18, NULL) bcm_iud_18,
coalesce(bcm_spermicide_18, NULL) bcm_spermicide_18,
coalesce(bcm_implant_18, NULL) bcm_implant_18,
coalesce(bcm_rhythm_18, NULL) bcm_rhythm_18,
coalesce(bcm_injection_18, NULL) bcm_injection_18,
coalesce(bcm_sponge_18, NULL) bcm_sponge_18,
coalesce(bcm_inserts_18, NULL) bcm_inserts_18,
coalesce(bcm_abstinence_18, NULL) bcm_abstinence_18,
coalesce(bcm_condom_19, NULL) bcm_condom_19,
coalesce(bcm_pill_19, NULL) bcm_pill_19,
coalesce(bcm_diaphragm_19, NULL) bcm_diaphragm_19,
coalesce(bcm_iud_19, NULL) bcm_iud_19,
coalesce(bcm_spermicide_19, NULL) bcm_spermicide_19,
coalesce(bcm_implant_19, NULL) bcm_implant_19,
coalesce(bcm_rhythm_19, NULL) bcm_rhythm_19,
coalesce(bcm_injection_19, NULL) bcm_injection_19,
coalesce(bcm_sponge_19, NULL) bcm_sponge_19,
coalesce(bcm_inserts_19, NULL) bcm_inserts_19,
coalesce(bcm_abstinence_19, NULL) bcm_abstinence_19,
coalesce(t_encounters_06, 0) t_encounters_06,
coalesce(t_encounters_07, 0) t_encounters_07,
coalesce(t_encounters_08, 0) t_encounters_08,
coalesce(t_encounters_09, 0) t_encounters_09,
coalesce(t_encounters_10, 0) t_encounters_10,
coalesce(t_encounters_11, 0) t_encounters_11,
coalesce(t_encounters_12, 0) t_encounters_12,
coalesce(t_encounters_13, 0) t_encounters_13,
coalesce(t_encounters_14, 0) t_encounters_14,
coalesce(t_encounters_15, 0) t_encounters_15,
coalesce(t_encounters_16, 0) t_encounters_16,
coalesce(t_encounters_17, 0) t_encounters_17,
coalesce(t_encounters_18, 0) t_encounters_18,
coalesce(t_encounters_19, 0) t_encounters_19,
total_gonorrhea_tests_06,
total_gonorrhea_tests_07,
total_gonorrhea_tests_08,
total_gonorrhea_tests_09,
total_gonorrhea_tests_10,
total_gonorrhea_tests_11,
total_gonorrhea_tests_12,
total_gonorrhea_tests_13,
total_gonorrhea_tests_14,
total_gonorrhea_tests_15,
total_gonorrhea_tests_16,
total_gonorrhea_tests_17,
total_gonorrhea_tests_18,
total_gonorrhea_tests_19,
positive_gonorrhea_tests_06,
positive_gonorrhea_tests_07,
positive_gonorrhea_tests_08,
positive_gonorrhea_tests_09,
positive_gonorrhea_tests_10,
positive_gonorrhea_tests_11,
positive_gonorrhea_tests_12,
positive_gonorrhea_tests_13,
positive_gonorrhea_tests_14,
positive_gonorrhea_tests_15,
positive_gonorrhea_tests_16,
positive_gonorrhea_tests_17,
positive_gonorrhea_tests_18,
positive_gonorrhea_tests_19,
total_gonorrhea_tests_rectal_06,
total_gonorrhea_tests_rectal_07,
total_gonorrhea_tests_rectal_08,
total_gonorrhea_tests_rectal_09,
total_gonorrhea_tests_rectal_10,
total_gonorrhea_tests_rectal_11,
total_gonorrhea_tests_rectal_12,
total_gonorrhea_tests_rectal_13,
total_gonorrhea_tests_rectal_14,
total_gonorrhea_tests_rectal_15,
total_gonorrhea_tests_rectal_16,
total_gonorrhea_tests_rectal_17,
total_gonorrhea_tests_rectal_18,
total_gonorrhea_tests_rectal_19,
positive_gonorrhea_tests_rectal_06,
positive_gonorrhea_tests_rectal_07,
positive_gonorrhea_tests_rectal_08,
positive_gonorrhea_tests_rectal_09,
positive_gonorrhea_tests_rectal_10,
positive_gonorrhea_tests_rectal_11,
positive_gonorrhea_tests_rectal_12,
positive_gonorrhea_tests_rectal_13,
positive_gonorrhea_tests_rectal_14,
positive_gonorrhea_tests_rectal_15,
positive_gonorrhea_tests_rectal_16,
positive_gonorrhea_tests_rectal_17,
positive_gonorrhea_tests_rectal_18,
positive_gonorrhea_tests_rectal_19,
total_gonorrhea_tests_throat_06,
total_gonorrhea_tests_throat_07,
total_gonorrhea_tests_throat_08,
total_gonorrhea_tests_throat_09,
total_gonorrhea_tests_throat_10,
total_gonorrhea_tests_throat_11,
total_gonorrhea_tests_throat_12,
total_gonorrhea_tests_throat_13,
total_gonorrhea_tests_throat_14,
total_gonorrhea_tests_throat_15,
total_gonorrhea_tests_throat_16,
total_gonorrhea_tests_throat_17,
total_gonorrhea_tests_throat_18,
total_gonorrhea_tests_throat_19,
positive_gonorrhea_tests_throat_06,
positive_gonorrhea_tests_throat_07,
positive_gonorrhea_tests_throat_08,
positive_gonorrhea_tests_throat_09,
positive_gonorrhea_tests_throat_10,
positive_gonorrhea_tests_throat_11,
positive_gonorrhea_tests_throat_12,
positive_gonorrhea_tests_throat_13,
positive_gonorrhea_tests_throat_14,
positive_gonorrhea_tests_throat_15,
positive_gonorrhea_tests_throat_16,
positive_gonorrhea_tests_throat_17,
positive_gonorrhea_tests_throat_18,
positive_gonorrhea_tests_throat_19,
total_chlamydia_tests_06,
total_chlamydia_tests_07,
total_chlamydia_tests_08,
total_chlamydia_tests_09,
total_chlamydia_tests_10,
total_chlamydia_tests_11,
total_chlamydia_tests_12,
total_chlamydia_tests_13,
total_chlamydia_tests_14,
total_chlamydia_tests_15,
total_chlamydia_tests_16,
total_chlamydia_tests_17,
total_chlamydia_tests_18,
total_chlamydia_tests_19,
positive_chlamydia_tests_06,
positive_chlamydia_tests_07,
positive_chlamydia_tests_08,
positive_chlamydia_tests_09,
positive_chlamydia_tests_10,
positive_chlamydia_tests_11,
positive_chlamydia_tests_12,
positive_chlamydia_tests_13,
positive_chlamydia_tests_14,
positive_chlamydia_tests_15,
positive_chlamydia_tests_16,
positive_chlamydia_tests_17,
positive_chlamydia_tests_18,
positive_chlamydia_tests_19,
total_chlamydia_tests_rectal_06,
total_chlamydia_tests_rectal_07,
total_chlamydia_tests_rectal_08,
total_chlamydia_tests_rectal_09,
total_chlamydia_tests_rectal_10,
total_chlamydia_tests_rectal_11,
total_chlamydia_tests_rectal_12,
total_chlamydia_tests_rectal_13,
total_chlamydia_tests_rectal_14,
total_chlamydia_tests_rectal_15,
total_chlamydia_tests_rectal_16,
total_chlamydia_tests_rectal_17,
total_chlamydia_tests_rectal_18,
total_chlamydia_tests_rectal_19,
positive_chlamydia_tests_rectal_06,
positive_chlamydia_tests_rectal_07,
positive_chlamydia_tests_rectal_08,
positive_chlamydia_tests_rectal_09,
positive_chlamydia_tests_rectal_10,
positive_chlamydia_tests_rectal_11,
positive_chlamydia_tests_rectal_12,
positive_chlamydia_tests_rectal_13,
positive_chlamydia_tests_rectal_14,
positive_chlamydia_tests_rectal_15,
positive_chlamydia_tests_rectal_16,
positive_chlamydia_tests_rectal_17,
positive_chlamydia_tests_rectal_18,
positive_chlamydia_tests_rectal_19,
total_chlamydia_tests_throat_06,
total_chlamydia_tests_throat_07,
total_chlamydia_tests_throat_08,
total_chlamydia_tests_throat_09,
total_chlamydia_tests_throat_10,
total_chlamydia_tests_throat_11,
total_chlamydia_tests_throat_12,
total_chlamydia_tests_throat_13,
total_chlamydia_tests_throat_14,
total_chlamydia_tests_throat_15,
total_chlamydia_tests_throat_16,
total_chlamydia_tests_throat_17,
total_chlamydia_tests_throat_18,
total_chlamydia_tests_throat_19,
positive_chlamydia_tests_throat_06,
positive_chlamydia_tests_throat_07,
positive_chlamydia_tests_throat_08,
positive_chlamydia_tests_throat_09,
positive_chlamydia_tests_throat_10,
positive_chlamydia_tests_throat_11,
positive_chlamydia_tests_throat_12,
positive_chlamydia_tests_throat_13,
positive_chlamydia_tests_throat_14,
positive_chlamydia_tests_throat_15,
positive_chlamydia_tests_throat_16,
positive_chlamydia_tests_throat_17,
positive_chlamydia_tests_throat_18,
positive_chlamydia_tests_throat_19,
total_syphilis_tests_06,
total_syphilis_tests_07,
total_syphilis_tests_08,
total_syphilis_tests_09,
total_syphilis_tests_10,
total_syphilis_tests_11,
total_syphilis_tests_12,
total_syphilis_tests_13,
total_syphilis_tests_14,
total_syphilis_tests_15,
total_syphilis_tests_16,
total_syphilis_tests_17,
total_syphilis_tests_18,
total_syphilis_tests_19,
positive_syphilis_tests_06,
positive_syphilis_tests_07,
positive_syphilis_tests_08,
positive_syphilis_tests_09,
positive_syphilis_tests_10,
positive_syphilis_tests_11,
positive_syphilis_tests_12,
positive_syphilis_tests_13,
positive_syphilis_tests_14,
positive_syphilis_tests_15,
positive_syphilis_tests_16,
positive_syphilis_tests_17,
positive_syphilis_tests_18,
positive_syphilis_tests_19,
syphilis_per_esp_06,
syphilis_per_esp_07,
syphilis_per_esp_08,
syphilis_per_esp_09,
syphilis_per_esp_10,
syphilis_per_esp_11,
syphilis_per_esp_12,
syphilis_per_esp_13,
syphilis_per_esp_14,
syphilis_per_esp_15,
syphilis_per_esp_16,
syphilis_per_esp_17,
syphilis_per_esp_18,
syphilis_per_esp_19,
ser_testing_for_lgv_ever,
anal_cytology_test_ever,
total_hcv_antibody_tests_06,
total_hcv_antibody_tests_07,
total_hcv_antibody_tests_08,
total_hcv_antibody_tests_09,
total_hcv_antibody_tests_10,
total_hcv_antibody_tests_11,
total_hcv_antibody_tests_12,
total_hcv_antibody_tests_13,
total_hcv_antibody_tests_14,
total_hcv_antibody_tests_15,
total_hcv_antibody_tests_16,
total_hcv_antibody_tests_17,
total_hcv_antibody_tests_18,
total_hcv_antibody_tests_19,
total_hcv_rna_tests_06,
total_hcv_rna_tests_07,
total_hcv_rna_tests_08,
total_hcv_rna_tests_09,
total_hcv_rna_tests_10,
total_hcv_rna_tests_11,
total_hcv_rna_tests_12,
total_hcv_rna_tests_13,
total_hcv_rna_tests_14,
total_hcv_rna_tests_15,
total_hcv_rna_tests_16,
total_hcv_rna_tests_17,
total_hcv_rna_tests_18,
total_hcv_rna_tests_19,
hcv_antibody_or_rna_positive,
hcv_antibody_first_pos_year,
hcv_rna_first_pos_year,
acute_hepc_per_esp,
acute_hepc_per_esp_diagnosis_year,
total_hepb_surface_antigen_tests_06,
total_hepb_surface_antigen_tests_07,
total_hepb_surface_antigen_tests_08,
total_hepb_surface_antigen_tests_09,
total_hepb_surface_antigen_tests_10,
total_hepb_surface_antigen_tests_11,
total_hepb_surface_antigen_tests_12,
total_hepb_surface_antigen_tests_13,
total_hepb_surface_antigen_tests_14,
total_hepb_surface_antigen_tests_15,
total_hepb_surface_antigen_tests_16,
total_hepb_surface_antigen_tests_17,
total_hepb_surface_antigen_tests_18,
total_hepb_surface_antigen_tests_19,
total_hepb_dna_tests_06,
total_hepb_dna_tests_07,
total_hepb_dna_tests_08,
total_hepb_dna_tests_09,
total_hepb_dna_tests_10,
total_hepb_dna_tests_11,
total_hepb_dna_tests_12,
total_hepb_dna_tests_13,
total_hepb_dna_tests_14,
total_hepb_dna_tests_15,
total_hepb_dna_tests_16,
total_hepb_dna_tests_17,
total_hepb_dna_tests_18,
total_hepb_dna_tests_19,
hepb_antigen_or_dna_positive,
hepb_antigen_or_dna_first_positive_year,
acute_hepb_per_esp,
acute_hepb_per_esp_year,
hist_of_hep_b_ever,
herpes_dir_tested_06,
herpes_dir_tested_07,
herpes_dir_tested_08,
herpes_dir_tested_09,
herpes_dir_tested_10,
herpes_dir_tested_11,
herpes_dir_tested_12,
herpes_dir_tested_13,
herpes_dir_tested_14,
herpes_dir_tested_15,
herpes_dir_tested_16,
herpes_dir_tested_17,
herpes_dir_tested_18,
herpes_dir_tested_19,
herpes_dir_pos_test_06,
herpes_dir_pos_test_07,
herpes_dir_pos_test_08,
herpes_dir_pos_test_09,
herpes_dir_pos_test_10,
herpes_dir_pos_test_11,
herpes_dir_pos_test_12,
herpes_dir_pos_test_13,
herpes_dir_pos_test_14,
herpes_dir_pos_test_15,
herpes_dir_pos_test_16,
herpes_dir_pos_test_17,
herpes_dir_pos_test_18,
herpes_dir_pos_test_19,
herpes_ser_tested_06,
herpes_ser_tested_07,
herpes_ser_tested_08,
herpes_ser_tested_09,
herpes_ser_tested_10,
herpes_ser_tested_11,
herpes_ser_tested_12,
herpes_ser_tested_13,
herpes_ser_tested_14,
herpes_ser_tested_15,
herpes_ser_tested_16,
herpes_ser_tested_17,
herpes_ser_tested_18,
herpes_ser_tested_19,
herpes_ser_pos_test_06,
herpes_ser_pos_test_07,
herpes_ser_pos_test_08,
herpes_ser_pos_test_09,
herpes_ser_pos_test_10,
herpes_ser_pos_test_11,
herpes_ser_pos_test_12,
herpes_ser_pos_test_13,
herpes_ser_pos_test_14,
herpes_ser_pos_test_15,
herpes_ser_pos_test_16,
herpes_ser_pos_test_17,
herpes_ser_pos_test_18,
herpes_ser_pos_test_19,
total_hiv_tests_06,
total_hiv_tests_07,
total_hiv_tests_08,
total_hiv_tests_09,
total_hiv_tests_10,
total_hiv_tests_11,
total_hiv_tests_12,
total_hiv_tests_13,
total_hiv_tests_14,
total_hiv_tests_15,
total_hiv_tests_16,
total_hiv_tests_17,
total_hiv_tests_18,
total_hiv_tests_19,
t_hiv_elisa_06,
t_hiv_elisa_07,
t_hiv_elisa_08,
t_hiv_elisa_09,
t_hiv_elisa_10,
t_hiv_elisa_11,
t_hiv_elisa_12,
t_hiv_elisa_13,
t_hiv_elisa_14,
t_hiv_elisa_15,
t_hiv_elisa_16,
t_hiv_elisa_17,
t_hiv_elisa_18,
t_hiv_elisa_19,
t_hiv_wb_06,
t_hiv_wb_07,
t_hiv_wb_08,
t_hiv_wb_09,
t_hiv_wb_10,
t_hiv_wb_11,
t_hiv_wb_12,
t_hiv_wb_13,
t_hiv_wb_14,
t_hiv_wb_15,
t_hiv_wb_16,
t_hiv_wb_17,
t_hiv_wb_18,
t_hiv_wb_19,
t_hiv_rna_06,
t_hiv_rna_07,
t_hiv_rna_08,
t_hiv_rna_09,
t_hiv_rna_10,
t_hiv_rna_11,
t_hiv_rna_12,
t_hiv_rna_13,
t_hiv_rna_14,
t_hiv_rna_15,
t_hiv_rna_16,
t_hiv_rna_17,
t_hiv_rna_18,
t_hiv_rna_19,
t_hiv_agab_06,
t_hiv_agab_07,
t_hiv_agab_08,
t_hiv_agab_09,
t_hiv_agab_10,
t_hiv_agab_11,
t_hiv_agab_12,
t_hiv_agab_13,
t_hiv_agab_14,
t_hiv_agab_15,
t_hiv_agab_16,
t_hiv_agab_17,
t_hiv_agab_18,
t_hiv_agab_19,
hiv_per_esp_spec,
hiv_per_esp_first_year,
hiv_neg_with_rna_test,
hiv_first_icd_year,
new_hiv_diagnosis,
hiv_neg_with_meds,
hiv_on_problem_list,
hiv_first_prob_date,
coalesce(T3.t_hiv_encounters_06, 0) t_hiv_encounters_06,
coalesce(T3.t_hiv_encounters_07, 0) t_hiv_encounters_07,
coalesce(T3.t_hiv_encounters_08, 0) t_hiv_encounters_08,
coalesce(T3.t_hiv_encounters_09, 0) t_hiv_encounters_09,
coalesce(T3.t_hiv_encounters_10, 0) t_hiv_encounters_10,
coalesce(T3.t_hiv_encounters_11, 0) t_hiv_encounters_11,
coalesce(T3.t_hiv_encounters_12, 0) t_hiv_encounters_12,
coalesce(T3.t_hiv_encounters_13, 0) t_hiv_encounters_13,
coalesce(T3.t_hiv_encounters_14, 0) t_hiv_encounters_14,
coalesce(T3.t_hiv_encounters_15, 0) t_hiv_encounters_15,
coalesce(T3.t_hiv_encounters_16, 0) t_hiv_encounters_16,
coalesce(T3.t_hiv_encounters_17, 0) t_hiv_encounters_17,
coalesce(T3.t_hiv_encounters_18, 0) t_hiv_encounters_18,
coalesce(T3.t_hiv_encounters_19, 0) t_hiv_encounters_19,
three_diff_hiv_med_06, 
three_diff_hiv_med_07,
three_diff_hiv_med_08,
three_diff_hiv_med_09,
three_diff_hiv_med_10,
three_diff_hiv_med_11,
three_diff_hiv_med_12,
three_diff_hiv_med_13,
three_diff_hiv_med_14,
three_diff_hiv_med_15,
three_diff_hiv_med_16,
three_diff_hiv_med_17,
three_diff_hiv_med_18,
three_diff_hiv_med_19,
truvada_num_rx_06,
truvada_num_rx_07,
truvada_num_rx_08,
truvada_num_rx_09,
truvada_num_rx_10,
truvada_num_rx_11,
truvada_num_rx_12,
truvada_num_rx_13,
truvada_num_rx_14,
truvada_num_rx_15,
truvada_num_rx_16,
truvada_num_rx_17,
truvada_num_rx_18,
truvada_num_rx_19,
truvada_num_pills_06,
truvada_num_pills_07,
truvada_num_pills_08,
truvada_num_pills_09,
truvada_num_pills_10,
truvada_num_pills_11,
truvada_num_pills_12,
truvada_num_pills_13,
truvada_num_pills_14,
truvada_num_pills_15,
truvada_num_pills_16,
truvada_num_pills_17,
truvada_num_pills_18,
truvada_num_pills_19,
hiv_neg_truvada_06,
hiv_neg_truvada_07,
hiv_neg_truvada_08,
hiv_neg_truvada_09,
hiv_neg_truvada_10,
hiv_neg_truvada_11,
hiv_neg_truvada_12,
hiv_neg_truvada_13,
hiv_neg_truvada_14,
hiv_neg_truvada_15,
hiv_neg_truvada_16,
hiv_neg_truvada_17,
hiv_neg_truvada_18,
hiv_neg_truvada_19,
anal_cyt_dysp_carci_hpv_syph,
syphillis_of_any_site_or_stage_except_late,
gonococcal_infection_of_anus_and_rectum,
gonococcal_pharyngitis,
chlamydial_infection_of_anus_and_recturm,
chlamydial_infection_of_pharynx,
lymphgranuloma_venereum,
chancroid,
granuloma_inguinale,
nongonococcal_urethritis,
herpes_simplex_w_complications,
genital_herpes,
anogenital_warts,
anorectal_ulcer,
unspecified_std,
pelvic_inflammatory_disease,
contact_with_or_exposure_to_venereal_disease,
high_risk_sexual_behavior,
hiv_counseling,
anorexia_nervosa,
bulimia_nervosa,
eating_disorder_nos,
gend_iden_trans_sex_reassign,
counseling_for_child_sexual_abuse,
foreign_body_in_anus,
alcohol_dependence_abuse,
opioid_dependence_abuse,
sed_hypn_anxio_depend_abuse,
cocaine_dependence_abuse,
amphet_stim_dependence_abuse,
oth_psycho_or_unspec_subs_depend_abuse,
incarceration,
--prison_occurrence,
--under_prison_care,
--prisoner_health_exam,
prison_release_probs,
oth_sex_inf_with_preg,
enc_after_alleged_r_sa_or_batt,
adult_sexual_abuse,
adult_sexual_abuse_sequela,
adult_sexual_abuse_subsequent,
adult_sexual_abuse_suspected,
adult_maltreatment,
adult_physical_abuse,
adult_emotional_abuse,
child_sexual_abuse,
rape_aslt_bod_force,
cannabis_abuse,
hallucinogen_abuse,
inhalent_abuse,
--non_psychoact_subs_abuse,
--herpes_unspecified,
--abortion,
--history_of_abortion,
elec_abort_in_first_sec_tri,
--elec_abortion_history,
emergency_contracep,
--emergency_contracep_couns,
--oth_spef_personal_risk_factors,
enc_for_hiv_screen,
contact_w_viral_hepatitis,
contact_w_pot_hzrd_bod_fluids,
rx_bicillin_06,
rx_bicillin_07,
rx_bicillin_08,
rx_bicillin_09,
rx_bicillin_10,
rx_bicillin_11,
rx_bicillin_12,
rx_bicillin_13,
rx_bicillin_14,
rx_bicillin_15,
rx_bicillin_16,
rx_bicillin_17,
rx_bicillin_18,
rx_bicillin_19,
rx_azithromycin_06,
rx_azithromycin_07,
rx_azithromycin_08,
rx_azithromycin_09,
rx_azithromycin_10,
rx_azithromycin_11,
rx_azithromycin_12,
rx_azithromycin_13,
rx_azithromycin_14,
rx_azithromycin_15,
rx_azithromycin_16,
rx_azithromycin_17,
rx_azithromycin_18,
rx_azithromycin_19,
rx_ceftriaxone_06,
rx_ceftriaxone_07,
rx_ceftriaxone_08,
rx_ceftriaxone_09,
rx_ceftriaxone_10,
rx_ceftriaxone_11,
rx_ceftriaxone_12,
rx_ceftriaxone_13,
rx_ceftriaxone_14,
rx_ceftriaxone_15,
rx_ceftriaxone_16,
rx_ceftriaxone_17,
rx_ceftriaxone_18,
rx_ceftriaxone_19,
rx_methadone_06,
rx_methadone_07,
rx_methadone_08,
rx_methadone_09,
rx_methadone_10,
rx_methadone_11,
rx_methadone_12,
rx_methadone_13,
rx_methadone_14,
rx_methadone_15,
rx_methadone_16,
rx_methadone_17,
rx_methadone_18,
rx_methadone_19,
rx_suboxone_06,
rx_suboxone_07,
rx_suboxone_08,
rx_suboxone_09,
rx_suboxone_10,
rx_suboxone_11,
rx_suboxone_12,
rx_suboxone_13,
rx_suboxone_14,
rx_suboxone_15,
rx_suboxone_16,
rx_suboxone_17,
rx_suboxone_18,
rx_suboxone_19,
rx_viagara_cilais_or_levitra_06,
rx_viagara_cilais_or_levitra_07,
rx_viagara_cilais_or_levitra_08,
rx_viagara_cilais_or_levitra_09,
rx_viagara_cilais_or_levitra_10,
rx_viagara_cilais_or_levitra_11,
rx_viagara_cilais_or_levitra_12,
rx_viagara_cilais_or_levitra_13,
rx_viagara_cilais_or_levitra_14,
rx_viagara_cilais_or_levitra_15,
rx_viagara_cilais_or_levitra_16,
rx_viagara_cilais_or_levitra_17,
rx_viagara_cilais_or_levitra_18,
rx_viagara_cilais_or_levitra_19,
amphet_tested_06,
amphet_tested_07,
amphet_tested_08,
amphet_tested_09,
amphet_tested_10,
amphet_tested_11,
amphet_tested_12,
amphet_tested_13,
amphet_tested_14,
amphet_tested_15,
amphet_tested_16,
amphet_tested_17,
amphet_tested_18,
amphet_tested_19,
amphet_pos_06,
amphet_pos_07,
amphet_pos_08,
amphet_pos_09,
amphet_pos_10,
amphet_pos_11,
amphet_pos_12,
amphet_pos_13,
amphet_pos_14,
amphet_pos_15,
amphet_pos_16,
amphet_pos_17,
amphet_pos_18,
amphet_pos_19,
cocaine_tested_06,
cocaine_tested_07,
cocaine_tested_08,
cocaine_tested_09,
cocaine_tested_10,
cocaine_tested_11,
cocaine_tested_12,
cocaine_tested_13,
cocaine_tested_14,
cocaine_tested_15,
cocaine_tested_16,
cocaine_tested_17,
cocaine_tested_18,
cocaine_tested_19,
cocaine_pos_06,
cocaine_pos_07,
cocaine_pos_08,
cocaine_pos_09,
cocaine_pos_10,
cocaine_pos_11,
cocaine_pos_12,
cocaine_pos_13,
cocaine_pos_14,
cocaine_pos_15,
cocaine_pos_16,
cocaine_pos_17,
cocaine_pos_18,
cocaine_pos_19,
methadone_tested_06,
methadone_tested_07,
methadone_tested_08,
methadone_tested_09,
methadone_tested_10,
methadone_tested_11,
methadone_tested_12,
methadone_tested_13,
methadone_tested_14,
methadone_tested_15,
methadone_tested_16,
methadone_tested_17,
methadone_tested_18,
methadone_tested_19,
methadone_pos_06,
methadone_pos_07,
methadone_pos_08,
methadone_pos_09,
methadone_pos_10,
methadone_pos_11,
methadone_pos_12,
methadone_pos_13,
methadone_pos_14,
methadone_pos_15,
methadone_pos_16,
methadone_pos_17,
methadone_pos_18,
methadone_pos_19,
fentanyl_mam_tested_06,
fentanyl_mam_tested_07,
fentanyl_mam_tested_08,
fentanyl_mam_tested_09,
fentanyl_mam_tested_10,
fentanyl_mam_tested_11,
fentanyl_mam_tested_12,
fentanyl_mam_tested_13,
fentanyl_mam_tested_14,
fentanyl_mam_tested_15,
fentanyl_mam_tested_16,
fentanyl_mam_tested_17,
fentanyl_mam_tested_18,
fentanyl_mam_tested_19,
fentanyl_mam_pos_06,
fentanyl_mam_pos_07,
fentanyl_mam_pos_08,
fentanyl_mam_pos_09,
fentanyl_mam_pos_10,
fentanyl_mam_pos_11,
fentanyl_mam_pos_12,
fentanyl_mam_pos_13,
fentanyl_mam_pos_14,
fentanyl_mam_pos_15,
fentanyl_mam_pos_16,
fentanyl_mam_pos_17,
fentanyl_mam_pos_18,
fentanyl_mam_pos_19,
opiate_tested_06,
opiate_tested_07,
opiate_tested_08,
opiate_tested_09,
opiate_tested_10,
opiate_tested_11,
opiate_tested_12,
opiate_tested_13,
opiate_tested_14,
opiate_tested_15,
opiate_tested_16,
opiate_tested_17,
opiate_tested_18,
opiate_tested_19,
opiate_pos_06,
opiate_pos_07,
opiate_pos_08,
opiate_pos_09,
opiate_pos_10,
opiate_pos_11,
opiate_pos_12,
opiate_pos_13,
opiate_pos_14,
opiate_pos_15,
opiate_pos_16,
opiate_pos_17,
opiate_pos_18,
opiate_pos_19,
med_asst_trmt_tested_06,
med_asst_trmt_tested_07,
med_asst_trmt_tested_08,
med_asst_trmt_tested_09,
med_asst_trmt_tested_10,
med_asst_trmt_tested_11,
med_asst_trmt_tested_12,
med_asst_trmt_tested_13,
med_asst_trmt_tested_14,
med_asst_trmt_tested_15,
med_asst_trmt_tested_16,
med_asst_trmt_tested_17,
med_asst_trmt_tested_18,
med_asst_trmt_tested_19,
med_asst_trmt_pos_06,
med_asst_trmt_pos_07,
med_asst_trmt_pos_08,
med_asst_trmt_pos_09,
med_asst_trmt_pos_10,
med_asst_trmt_pos_11,
med_asst_trmt_pos_12,
med_asst_trmt_pos_13,
med_asst_trmt_pos_14,
med_asst_trmt_pos_15,
med_asst_trmt_pos_16,
med_asst_trmt_pos_17,
med_asst_trmt_pos_18,
med_asst_trmt_pos_19
FROM hiv_rpt.prep_output_with_patient T1
LEFT JOIN hiv_rpt.prep_ouput_pat_and_enc_counts T2
	ON T1.master_patient_id = T2.master_patient_id
LEFT JOIN hiv_rpt.prep_ouput_pat_and_hiv_enc_counts T3
	ON T3.master_patient_id = T1.master_patient_id
LEFT JOIN hiv_rpt.prep_sex_part_gend T4
	ON T4.master_patient_id = T1.master_patient_id
LEFT JOIN  hiv_rpt.prep_alcohol_use T5
	ON T5.master_patient_id = T1.master_patient_id
LEFT JOIN  hiv_rpt.prep_ill_drug_use T6
	ON T6.master_patient_id = T1.master_patient_id
LEFT JOIN hiv_rpt.prep_birth_control_method T7
	ON T7.master_patient_id = T1.master_patient_id
LEFT JOIN hiv_rpt.prep_sexually_active T8
	ON T8.master_patient_id = T1.master_patient_id
LEFT JOIN hiv_rpt.prep_alcohol_oz_per_week T9
	ON T9.master_patient_id = T1.master_patient_id;
	
	

CREATE TABLE hiv_rpt.prep_report_final_output_agetestpat_filter AS
SELECT * FROM hiv_rpt.prep_report_final_output
WHERE age >= 15 
AND master_patient_id not in 
(4856539,
2388755,
20021289,
2622639,
7393799,
7405902,
19086,
2245972,
9345414,
1121717,
1120095,
5092053,
1119941,
15810936,
2801547,
7395105,
6808599,
467766,
465618,
9345404,
24120559,
20257460,
6840308,
467872,
4512833,
454136,
8429720,
8000774,
377556,
456070,
8012787,
7441382,
7997968,
3268327,
3268319,
3268323,
8726972,
9645683,
456988,
7395153,
4476409,
1400172,
269278,
5044824,
5092208,
269284,
269622,
269290,
5689686,
5651871,
269282,
269276,
5042048,
6272750,
299112,
269286,
269288,
5044808,
291608,
2733840,
269280,
4476411,
8425284,
7394534,
2733842,
5092285,
7394532,
3906359,
1629211,
7394546,
7394548,
5638755,
3069021,
7394544,
16697245,
16697270,
16700804,
16803981);


-- GET NAMES OF COLUMNS
-- SELECT *
-- FROM information_schema.columns
-- WHERE 
-- table_schema = 'hiv_rpt'
-- AND table_name   = 'prep_report_final_output_agetestpat_filter';

-- MASK PATIENT IDENTIFIED
CREATE TABLE hiv_rpt.prep_report_masked_patient_id AS SELECT 
'MASKED'::text as master_patient_id,
age,
gender,
race,
home_language,
marital_stat,
zip5,
female_partner_06,
female_partner_07,
female_partner_08,
female_partner_09,
female_partner_10,
female_partner_11,
female_partner_12,
female_partner_13,
female_partner_14,
female_partner_15,
female_partner_16,
female_partner_17,
female_partner_18,
female_partner_19,
male_partner_06,
male_partner_07,
male_partner_08,
male_partner_09,
male_partner_10,
male_partner_11,
male_partner_12,
male_partner_13,
male_partner_14,
male_partner_15,
male_partner_16,
male_partner_17,
male_partner_18,
male_partner_19,
alcohol_use_06,
alcohol_use_07,
alcohol_use_08,
alcohol_use_09,
alcohol_use_10,
alcohol_use_11,
alcohol_use_12,
alcohol_use_13,
alcohol_use_14,
alcohol_use_15,
alcohol_use_16,
alcohol_use_17,
alcohol_use_18,
alcohol_use_19,
alcohol_oz_per_wk_06,
alcohol_oz_per_wk_07,
alcohol_oz_per_wk_08,
alcohol_oz_per_wk_09,
alcohol_oz_per_wk_10,
alcohol_oz_per_wk_11,
alcohol_oz_per_wk_12,
alcohol_oz_per_wk_13,
alcohol_oz_per_wk_14,
alcohol_oz_per_wk_15,
alcohol_oz_per_wk_16,
alcohol_oz_per_wk_17,
alcohol_oz_per_wk_18,
alcohol_oz_per_wk_19,
ill_drug_use_06,
ill_drug_use_07,
ill_drug_use_08,
ill_drug_use_09,
ill_drug_use_10,
ill_drug_use_11,
ill_drug_use_12,
ill_drug_use_13,
ill_drug_use_14,
ill_drug_use_15,
ill_drug_use_16,
ill_drug_use_17,
ill_drug_use_18,
ill_drug_use_19,
sexually_active_06,
sexually_active_07,
sexually_active_08,
sexually_active_09,
sexually_active_10,
sexually_active_11,
sexually_active_12,
sexually_active_13,
sexually_active_14,
sexually_active_15,
sexually_active_16,
sexually_active_17,
sexually_active_18,
sexually_active_19,
bcm_condom_06,
bcm_condom_07,
bcm_condom_08,
bcm_condom_09,
bcm_condom_10,
bcm_condom_11,
bcm_condom_12,
bcm_condom_13,
bcm_condom_14,
bcm_condom_15,
bcm_condom_16,
bcm_condom_17,
bcm_condom_18,
bcm_condom_19,
bcm_pill_06,
bcm_pill_07,
bcm_pill_08,
bcm_pill_09,
bcm_pill_10,
bcm_pill_11,
bcm_pill_12,
bcm_pill_13,
bcm_pill_14,
bcm_pill_15,
bcm_pill_16,
bcm_pill_17,
bcm_pill_18,
bcm_pill_19,
bcm_diaphragm_06,
bcm_diaphragm_07,
bcm_diaphragm_08,
bcm_diaphragm_09,
bcm_diaphragm_10,
bcm_diaphragm_11,
bcm_diaphragm_12,
bcm_diaphragm_13,
bcm_diaphragm_14,
bcm_diaphragm_15,
bcm_diaphragm_16,
bcm_diaphragm_17,
bcm_diaphragm_18,
bcm_diaphragm_19,
bcm_iud_06,
bcm_iud_07,
bcm_iud_08,
bcm_iud_09,
bcm_iud_10,
bcm_iud_11,
bcm_iud_12,
bcm_iud_13,
bcm_iud_14,
bcm_iud_15,
bcm_iud_16,
bcm_iud_17,
bcm_iud_18,
bcm_iud_19,
bcm_spermicide_06,
bcm_spermicide_07,
bcm_spermicide_08,
bcm_spermicide_09,
bcm_spermicide_10,
bcm_spermicide_11,
bcm_spermicide_12,
bcm_spermicide_13,
bcm_spermicide_14,
bcm_spermicide_15,
bcm_spermicide_16,
bcm_spermicide_17,
bcm_spermicide_18,
bcm_spermicide_19,
bcm_implant_06,
bcm_implant_07,
bcm_implant_08,
bcm_implant_09,
bcm_implant_10,
bcm_implant_11,
bcm_implant_12,
bcm_implant_13,
bcm_implant_14,
bcm_implant_15,
bcm_implant_16,
bcm_implant_17,
bcm_implant_18,
bcm_implant_19,
bcm_rhythm_06,
bcm_rhythm_07,
bcm_rhythm_08,
bcm_rhythm_09,
bcm_rhythm_10,
bcm_rhythm_11,
bcm_rhythm_12,
bcm_rhythm_13,
bcm_rhythm_14,
bcm_rhythm_15,
bcm_rhythm_16,
bcm_rhythm_17,
bcm_rhythm_18,
bcm_rhythm_19,
bcm_injection_06,
bcm_injection_07,
bcm_injection_08,
bcm_injection_09,
bcm_injection_10,
bcm_injection_11,
bcm_injection_12,
bcm_injection_13,
bcm_injection_14,
bcm_injection_15,
bcm_injection_16,
bcm_injection_17,
bcm_injection_18,
bcm_injection_19,
bcm_sponge_06,
bcm_sponge_07,
bcm_sponge_08,
bcm_sponge_09,
bcm_sponge_10,
bcm_sponge_11,
bcm_sponge_12,
bcm_sponge_13,
bcm_sponge_14,
bcm_sponge_15,
bcm_sponge_16,
bcm_sponge_17,
bcm_sponge_18,
bcm_sponge_19,
bcm_inserts_06,
bcm_inserts_07,
bcm_inserts_08,
bcm_inserts_09,
bcm_inserts_10,
bcm_inserts_11,
bcm_inserts_12,
bcm_inserts_13,
bcm_inserts_14,
bcm_inserts_15,
bcm_inserts_16,
bcm_inserts_17,
bcm_inserts_18,
bcm_inserts_19,
bcm_abstinence_06,
bcm_abstinence_07,
bcm_abstinence_08,
bcm_abstinence_09,
bcm_abstinence_10,
bcm_abstinence_11,
bcm_abstinence_12,
bcm_abstinence_13,
bcm_abstinence_14,
bcm_abstinence_15,
bcm_abstinence_16,
bcm_abstinence_17,
bcm_abstinence_18,
bcm_abstinence_19,
t_encounters_06,
t_encounters_07,
t_encounters_08,
t_encounters_09,
t_encounters_10,
t_encounters_11,
t_encounters_12,
t_encounters_13,
t_encounters_14,
t_encounters_15,
t_encounters_16,
t_encounters_17,
t_encounters_18,
t_encounters_19,
total_gonorrhea_tests_06,
total_gonorrhea_tests_07,
total_gonorrhea_tests_08,
total_gonorrhea_tests_09,
total_gonorrhea_tests_10,
total_gonorrhea_tests_11,
total_gonorrhea_tests_12,
total_gonorrhea_tests_13,
total_gonorrhea_tests_14,
total_gonorrhea_tests_15,
total_gonorrhea_tests_16,
total_gonorrhea_tests_17,
total_gonorrhea_tests_18,
total_gonorrhea_tests_19,
positive_gonorrhea_tests_06,
positive_gonorrhea_tests_07,
positive_gonorrhea_tests_08,
positive_gonorrhea_tests_09,
positive_gonorrhea_tests_10,
positive_gonorrhea_tests_11,
positive_gonorrhea_tests_12,
positive_gonorrhea_tests_13,
positive_gonorrhea_tests_14,
positive_gonorrhea_tests_15,
positive_gonorrhea_tests_16,
positive_gonorrhea_tests_17,
positive_gonorrhea_tests_18,
positive_gonorrhea_tests_19,
total_gonorrhea_tests_rectal_06,
total_gonorrhea_tests_rectal_07,
total_gonorrhea_tests_rectal_08,
total_gonorrhea_tests_rectal_09,
total_gonorrhea_tests_rectal_10,
total_gonorrhea_tests_rectal_11,
total_gonorrhea_tests_rectal_12,
total_gonorrhea_tests_rectal_13,
total_gonorrhea_tests_rectal_14,
total_gonorrhea_tests_rectal_15,
total_gonorrhea_tests_rectal_16,
total_gonorrhea_tests_rectal_17,
total_gonorrhea_tests_rectal_18,
total_gonorrhea_tests_rectal_19,
positive_gonorrhea_tests_rectal_06,
positive_gonorrhea_tests_rectal_07,
positive_gonorrhea_tests_rectal_08,
positive_gonorrhea_tests_rectal_09,
positive_gonorrhea_tests_rectal_10,
positive_gonorrhea_tests_rectal_11,
positive_gonorrhea_tests_rectal_12,
positive_gonorrhea_tests_rectal_13,
positive_gonorrhea_tests_rectal_14,
positive_gonorrhea_tests_rectal_15,
positive_gonorrhea_tests_rectal_16,
positive_gonorrhea_tests_rectal_17,
positive_gonorrhea_tests_rectal_18,
positive_gonorrhea_tests_rectal_19,
total_gonorrhea_tests_throat_06,
total_gonorrhea_tests_throat_07,
total_gonorrhea_tests_throat_08,
total_gonorrhea_tests_throat_09,
total_gonorrhea_tests_throat_10,
total_gonorrhea_tests_throat_11,
total_gonorrhea_tests_throat_12,
total_gonorrhea_tests_throat_13,
total_gonorrhea_tests_throat_14,
total_gonorrhea_tests_throat_15,
total_gonorrhea_tests_throat_16,
total_gonorrhea_tests_throat_17,
total_gonorrhea_tests_throat_18,
total_gonorrhea_tests_throat_19,
positive_gonorrhea_tests_throat_06,
positive_gonorrhea_tests_throat_07,
positive_gonorrhea_tests_throat_08,
positive_gonorrhea_tests_throat_09,
positive_gonorrhea_tests_throat_10,
positive_gonorrhea_tests_throat_11,
positive_gonorrhea_tests_throat_12,
positive_gonorrhea_tests_throat_13,
positive_gonorrhea_tests_throat_14,
positive_gonorrhea_tests_throat_15,
positive_gonorrhea_tests_throat_16,
positive_gonorrhea_tests_throat_17,
positive_gonorrhea_tests_throat_18,
positive_gonorrhea_tests_throat_19,
total_chlamydia_tests_06,
total_chlamydia_tests_07,
total_chlamydia_tests_08,
total_chlamydia_tests_09,
total_chlamydia_tests_10,
total_chlamydia_tests_11,
total_chlamydia_tests_12,
total_chlamydia_tests_13,
total_chlamydia_tests_14,
total_chlamydia_tests_15,
total_chlamydia_tests_16,
total_chlamydia_tests_17,
total_chlamydia_tests_18,
total_chlamydia_tests_19,
positive_chlamydia_tests_06,
positive_chlamydia_tests_07,
positive_chlamydia_tests_08,
positive_chlamydia_tests_09,
positive_chlamydia_tests_10,
positive_chlamydia_tests_11,
positive_chlamydia_tests_12,
positive_chlamydia_tests_13,
positive_chlamydia_tests_14,
positive_chlamydia_tests_15,
positive_chlamydia_tests_16,
positive_chlamydia_tests_17,
positive_chlamydia_tests_18,
positive_chlamydia_tests_19,
total_chlamydia_tests_rectal_06,
total_chlamydia_tests_rectal_07,
total_chlamydia_tests_rectal_08,
total_chlamydia_tests_rectal_09,
total_chlamydia_tests_rectal_10,
total_chlamydia_tests_rectal_11,
total_chlamydia_tests_rectal_12,
total_chlamydia_tests_rectal_13,
total_chlamydia_tests_rectal_14,
total_chlamydia_tests_rectal_15,
total_chlamydia_tests_rectal_16,
total_chlamydia_tests_rectal_17,
total_chlamydia_tests_rectal_18,
total_chlamydia_tests_rectal_19,
positive_chlamydia_tests_rectal_06,
positive_chlamydia_tests_rectal_07,
positive_chlamydia_tests_rectal_08,
positive_chlamydia_tests_rectal_09,
positive_chlamydia_tests_rectal_10,
positive_chlamydia_tests_rectal_11,
positive_chlamydia_tests_rectal_12,
positive_chlamydia_tests_rectal_13,
positive_chlamydia_tests_rectal_14,
positive_chlamydia_tests_rectal_15,
positive_chlamydia_tests_rectal_16,
positive_chlamydia_tests_rectal_17,
positive_chlamydia_tests_rectal_18,
positive_chlamydia_tests_rectal_19,
total_chlamydia_tests_throat_06,
total_chlamydia_tests_throat_07,
total_chlamydia_tests_throat_08,
total_chlamydia_tests_throat_09,
total_chlamydia_tests_throat_10,
total_chlamydia_tests_throat_11,
total_chlamydia_tests_throat_12,
total_chlamydia_tests_throat_13,
total_chlamydia_tests_throat_14,
total_chlamydia_tests_throat_15,
total_chlamydia_tests_throat_16,
total_chlamydia_tests_throat_17,
total_chlamydia_tests_throat_18,
total_chlamydia_tests_throat_19,
positive_chlamydia_tests_throat_06,
positive_chlamydia_tests_throat_07,
positive_chlamydia_tests_throat_08,
positive_chlamydia_tests_throat_09,
positive_chlamydia_tests_throat_10,
positive_chlamydia_tests_throat_11,
positive_chlamydia_tests_throat_12,
positive_chlamydia_tests_throat_13,
positive_chlamydia_tests_throat_14,
positive_chlamydia_tests_throat_15,
positive_chlamydia_tests_throat_16,
positive_chlamydia_tests_throat_17,
positive_chlamydia_tests_throat_18,
positive_chlamydia_tests_throat_19,
total_syphilis_tests_06,
total_syphilis_tests_07,
total_syphilis_tests_08,
total_syphilis_tests_09,
total_syphilis_tests_10,
total_syphilis_tests_11,
total_syphilis_tests_12,
total_syphilis_tests_13,
total_syphilis_tests_14,
total_syphilis_tests_15,
total_syphilis_tests_16,
total_syphilis_tests_17,
total_syphilis_tests_18,
total_syphilis_tests_19,
positive_syphilis_tests_06,
positive_syphilis_tests_07,
positive_syphilis_tests_08,
positive_syphilis_tests_09,
positive_syphilis_tests_10,
positive_syphilis_tests_11,
positive_syphilis_tests_12,
positive_syphilis_tests_13,
positive_syphilis_tests_14,
positive_syphilis_tests_15,
positive_syphilis_tests_16,
positive_syphilis_tests_17,
positive_syphilis_tests_18,
positive_syphilis_tests_19,
syphilis_per_esp_06,
syphilis_per_esp_07,
syphilis_per_esp_08,
syphilis_per_esp_09,
syphilis_per_esp_10,
syphilis_per_esp_11,
syphilis_per_esp_12,
syphilis_per_esp_13,
syphilis_per_esp_14,
syphilis_per_esp_15,
syphilis_per_esp_16,
syphilis_per_esp_17,
syphilis_per_esp_18,
syphilis_per_esp_19,
ser_testing_for_lgv_ever,
anal_cytology_test_ever,
total_hcv_antibody_tests_06,
total_hcv_antibody_tests_07,
total_hcv_antibody_tests_08,
total_hcv_antibody_tests_09,
total_hcv_antibody_tests_10,
total_hcv_antibody_tests_11,
total_hcv_antibody_tests_12,
total_hcv_antibody_tests_13,
total_hcv_antibody_tests_14,
total_hcv_antibody_tests_15,
total_hcv_antibody_tests_16,
total_hcv_antibody_tests_17,
total_hcv_antibody_tests_18,
total_hcv_antibody_tests_19,
total_hcv_rna_tests_06,
total_hcv_rna_tests_07,
total_hcv_rna_tests_08,
total_hcv_rna_tests_09,
total_hcv_rna_tests_10,
total_hcv_rna_tests_11,
total_hcv_rna_tests_12,
total_hcv_rna_tests_13,
total_hcv_rna_tests_14,
total_hcv_rna_tests_15,
total_hcv_rna_tests_16,
total_hcv_rna_tests_17,
total_hcv_rna_tests_18,
total_hcv_rna_tests_19,
hcv_antibody_or_rna_positive,
hcv_antibody_first_pos_year,
hcv_rna_first_pos_year,
acute_hepc_per_esp,
acute_hepc_per_esp_diagnosis_year,
total_hepb_surface_antigen_tests_06,
total_hepb_surface_antigen_tests_07,
total_hepb_surface_antigen_tests_08,
total_hepb_surface_antigen_tests_09,
total_hepb_surface_antigen_tests_10,
total_hepb_surface_antigen_tests_11,
total_hepb_surface_antigen_tests_12,
total_hepb_surface_antigen_tests_13,
total_hepb_surface_antigen_tests_14,
total_hepb_surface_antigen_tests_15,
total_hepb_surface_antigen_tests_16,
total_hepb_surface_antigen_tests_17,
total_hepb_surface_antigen_tests_18,
total_hepb_surface_antigen_tests_19,
total_hepb_dna_tests_06,
total_hepb_dna_tests_07,
total_hepb_dna_tests_08,
total_hepb_dna_tests_09,
total_hepb_dna_tests_10,
total_hepb_dna_tests_11,
total_hepb_dna_tests_12,
total_hepb_dna_tests_13,
total_hepb_dna_tests_14,
total_hepb_dna_tests_15,
total_hepb_dna_tests_16,
total_hepb_dna_tests_17,
total_hepb_dna_tests_18,
total_hepb_dna_tests_19,
hepb_antigen_or_dna_positive,
hepb_antigen_or_dna_first_positive_year,
acute_hepb_per_esp,
acute_hepb_per_esp_year,
hist_of_hep_b_ever,
herpes_dir_tested_06,
herpes_dir_tested_07,
herpes_dir_tested_08,
herpes_dir_tested_09,
herpes_dir_tested_10,
herpes_dir_tested_11,
herpes_dir_tested_12,
herpes_dir_tested_13,
herpes_dir_tested_14,
herpes_dir_tested_15,
herpes_dir_tested_16,
herpes_dir_tested_17,
herpes_dir_tested_18,
herpes_dir_tested_19,
herpes_dir_pos_test_06,
herpes_dir_pos_test_07,
herpes_dir_pos_test_08,
herpes_dir_pos_test_09,
herpes_dir_pos_test_10,
herpes_dir_pos_test_11,
herpes_dir_pos_test_12,
herpes_dir_pos_test_13,
herpes_dir_pos_test_14,
herpes_dir_pos_test_15,
herpes_dir_pos_test_16,
herpes_dir_pos_test_17,
herpes_dir_pos_test_18,
herpes_dir_pos_test_19,
herpes_ser_tested_06,
herpes_ser_tested_07,
herpes_ser_tested_08,
herpes_ser_tested_09,
herpes_ser_tested_10,
herpes_ser_tested_11,
herpes_ser_tested_12,
herpes_ser_tested_13,
herpes_ser_tested_14,
herpes_ser_tested_15,
herpes_ser_tested_16,
herpes_ser_tested_17,
herpes_ser_tested_18,
herpes_ser_tested_19,
herpes_ser_pos_test_06,
herpes_ser_pos_test_07,
herpes_ser_pos_test_08,
herpes_ser_pos_test_09,
herpes_ser_pos_test_10,
herpes_ser_pos_test_11,
herpes_ser_pos_test_12,
herpes_ser_pos_test_13,
herpes_ser_pos_test_14,
herpes_ser_pos_test_15,
herpes_ser_pos_test_16,
herpes_ser_pos_test_17,
herpes_ser_pos_test_18,
herpes_ser_pos_test_19,
total_hiv_tests_06,
total_hiv_tests_07,
total_hiv_tests_08,
total_hiv_tests_09,
total_hiv_tests_10,
total_hiv_tests_11,
total_hiv_tests_12,
total_hiv_tests_13,
total_hiv_tests_14,
total_hiv_tests_15,
total_hiv_tests_16,
total_hiv_tests_17,
total_hiv_tests_18,
total_hiv_tests_19,
t_hiv_elisa_06,
t_hiv_elisa_07,
t_hiv_elisa_08,
t_hiv_elisa_09,
t_hiv_elisa_10,
t_hiv_elisa_11,
t_hiv_elisa_12,
t_hiv_elisa_13,
t_hiv_elisa_14,
t_hiv_elisa_15,
t_hiv_elisa_16,
t_hiv_elisa_17,
t_hiv_elisa_18,
t_hiv_elisa_19,
t_hiv_wb_06,
t_hiv_wb_07,
t_hiv_wb_08,
t_hiv_wb_09,
t_hiv_wb_10,
t_hiv_wb_11,
t_hiv_wb_12,
t_hiv_wb_13,
t_hiv_wb_14,
t_hiv_wb_15,
t_hiv_wb_16,
t_hiv_wb_17,
t_hiv_wb_18,
t_hiv_wb_19,
t_hiv_rna_06,
t_hiv_rna_07,
t_hiv_rna_08,
t_hiv_rna_09,
t_hiv_rna_10,
t_hiv_rna_11,
t_hiv_rna_12,
t_hiv_rna_13,
t_hiv_rna_14,
t_hiv_rna_15,
t_hiv_rna_16,
t_hiv_rna_17,
t_hiv_rna_18,
t_hiv_rna_19,
t_hiv_agab_06,
t_hiv_agab_07,
t_hiv_agab_08,
t_hiv_agab_09,
t_hiv_agab_10,
t_hiv_agab_11,
t_hiv_agab_12,
t_hiv_agab_13,
t_hiv_agab_14,
t_hiv_agab_15,
t_hiv_agab_16,
t_hiv_agab_17,
t_hiv_agab_18,
t_hiv_agab_19,
hiv_per_esp_spec,
hiv_per_esp_first_year,
hiv_neg_with_rna_test,
hiv_first_icd_year,
new_hiv_diagnosis,
hiv_neg_with_meds,
hiv_on_problem_list,
hiv_first_prob_date,
t_hiv_encounters_06,
t_hiv_encounters_07,
t_hiv_encounters_08,
t_hiv_encounters_09,
t_hiv_encounters_10,
t_hiv_encounters_11,
t_hiv_encounters_12,
t_hiv_encounters_13,
t_hiv_encounters_14,
t_hiv_encounters_15,
t_hiv_encounters_16,
t_hiv_encounters_17,
t_hiv_encounters_18,
t_hiv_encounters_19,
three_diff_hiv_med_06,
three_diff_hiv_med_07,
three_diff_hiv_med_08,
three_diff_hiv_med_09,
three_diff_hiv_med_10,
three_diff_hiv_med_11,
three_diff_hiv_med_12,
three_diff_hiv_med_13,
three_diff_hiv_med_14,
three_diff_hiv_med_15,
three_diff_hiv_med_16,
three_diff_hiv_med_17,
three_diff_hiv_med_18,
three_diff_hiv_med_19,
truvada_num_rx_06,
truvada_num_rx_07,
truvada_num_rx_08,
truvada_num_rx_09,
truvada_num_rx_10,
truvada_num_rx_11,
truvada_num_rx_12,
truvada_num_rx_13,
truvada_num_rx_14,
truvada_num_rx_15,
truvada_num_rx_16,
truvada_num_rx_17,
truvada_num_rx_18,
truvada_num_rx_19,
truvada_num_pills_06,
truvada_num_pills_07,
truvada_num_pills_08,
truvada_num_pills_09,
truvada_num_pills_10,
truvada_num_pills_11,
truvada_num_pills_12,
truvada_num_pills_13,
truvada_num_pills_14,
truvada_num_pills_15,
truvada_num_pills_16,
truvada_num_pills_17,
truvada_num_pills_18,
truvada_num_pills_19,
hiv_neg_truvada_06,
hiv_neg_truvada_07,
hiv_neg_truvada_08,
hiv_neg_truvada_09,
hiv_neg_truvada_10,
hiv_neg_truvada_11,
hiv_neg_truvada_12,
hiv_neg_truvada_13,
hiv_neg_truvada_14,
hiv_neg_truvada_15,
hiv_neg_truvada_16,
hiv_neg_truvada_17,
hiv_neg_truvada_18,
hiv_neg_truvada_19,
anal_cyt_dysp_carci_hpv_syph,
syphillis_of_any_site_or_stage_except_late,
gonococcal_infection_of_anus_and_rectum,
gonococcal_pharyngitis,
chlamydial_infection_of_anus_and_recturm,
chlamydial_infection_of_pharynx,
lymphgranuloma_venereum,
chancroid,
granuloma_inguinale,
nongonococcal_urethritis,
herpes_simplex_w_complications,
genital_herpes,
anogenital_warts,
anorectal_ulcer,
unspecified_std,
pelvic_inflammatory_disease,
contact_with_or_exposure_to_venereal_disease,
high_risk_sexual_behavior,
hiv_counseling,
anorexia_nervosa,
bulimia_nervosa,
eating_disorder_nos,
gend_iden_trans_sex_reassign,
counseling_for_child_sexual_abuse,
foreign_body_in_anus,
alcohol_dependence_abuse,
opioid_dependence_abuse,
sed_hypn_anxio_depend_abuse,
cocaine_dependence_abuse,
amphet_stim_dependence_abuse,
oth_psycho_or_unspec_subs_depend_abuse,
incarceration,
prison_release_probs,
oth_sex_inf_with_preg,
enc_after_alleged_r_sa_or_batt,
adult_sexual_abuse,
adult_sexual_abuse_sequela,
adult_sexual_abuse_subsequent,
adult_sexual_abuse_suspected,
adult_maltreatment,
adult_physical_abuse,
adult_emotional_abuse,
child_sexual_abuse,
rape_aslt_bod_force,
cannabis_abuse,
hallucinogen_abuse,
inhalent_abuse,
elec_abort_in_first_sec_tri,
emergency_contracep,
enc_for_hiv_screen,
contact_w_viral_hepatitis,
contact_w_pot_hzrd_bod_fluids,
rx_bicillin_06,
rx_bicillin_07,
rx_bicillin_08,
rx_bicillin_09,
rx_bicillin_10,
rx_bicillin_11,
rx_bicillin_12,
rx_bicillin_13,
rx_bicillin_14,
rx_bicillin_15,
rx_bicillin_16,
rx_bicillin_17,
rx_bicillin_18,
rx_bicillin_19,
rx_azithromycin_06,
rx_azithromycin_07,
rx_azithromycin_08,
rx_azithromycin_09,
rx_azithromycin_10,
rx_azithromycin_11,
rx_azithromycin_12,
rx_azithromycin_13,
rx_azithromycin_14,
rx_azithromycin_15,
rx_azithromycin_16,
rx_azithromycin_17,
rx_azithromycin_18,
rx_azithromycin_19,
rx_ceftriaxone_06,
rx_ceftriaxone_07,
rx_ceftriaxone_08,
rx_ceftriaxone_09,
rx_ceftriaxone_10,
rx_ceftriaxone_11,
rx_ceftriaxone_12,
rx_ceftriaxone_13,
rx_ceftriaxone_14,
rx_ceftriaxone_15,
rx_ceftriaxone_16,
rx_ceftriaxone_17,
rx_ceftriaxone_18,
rx_ceftriaxone_19,
rx_methadone_06,
rx_methadone_07,
rx_methadone_08,
rx_methadone_09,
rx_methadone_10,
rx_methadone_11,
rx_methadone_12,
rx_methadone_13,
rx_methadone_14,
rx_methadone_15,
rx_methadone_16,
rx_methadone_17,
rx_methadone_18,
rx_methadone_19,
rx_suboxone_06,
rx_suboxone_07,
rx_suboxone_08,
rx_suboxone_09,
rx_suboxone_10,
rx_suboxone_11,
rx_suboxone_12,
rx_suboxone_13,
rx_suboxone_14,
rx_suboxone_15,
rx_suboxone_16,
rx_suboxone_17,
rx_suboxone_18,
rx_suboxone_19,
rx_viagara_cilais_or_levitra_06,
rx_viagara_cilais_or_levitra_07,
rx_viagara_cilais_or_levitra_08,
rx_viagara_cilais_or_levitra_09,
rx_viagara_cilais_or_levitra_10,
rx_viagara_cilais_or_levitra_11,
rx_viagara_cilais_or_levitra_12,
rx_viagara_cilais_or_levitra_13,
rx_viagara_cilais_or_levitra_14,
rx_viagara_cilais_or_levitra_15,
rx_viagara_cilais_or_levitra_16,
rx_viagara_cilais_or_levitra_17,
rx_viagara_cilais_or_levitra_18,
rx_viagara_cilais_or_levitra_19,
amphet_tested_06,
amphet_tested_07,
amphet_tested_08,
amphet_tested_09,
amphet_tested_10,
amphet_tested_11,
amphet_tested_12,
amphet_tested_13,
amphet_tested_14,
amphet_tested_15,
amphet_tested_16,
amphet_tested_17,
amphet_tested_18,
amphet_tested_19,
amphet_pos_06,
amphet_pos_07,
amphet_pos_08,
amphet_pos_09,
amphet_pos_10,
amphet_pos_11,
amphet_pos_12,
amphet_pos_13,
amphet_pos_14,
amphet_pos_15,
amphet_pos_16,
amphet_pos_17,
amphet_pos_18,
amphet_pos_19,
cocaine_tested_06,
cocaine_tested_07,
cocaine_tested_08,
cocaine_tested_09,
cocaine_tested_10,
cocaine_tested_11,
cocaine_tested_12,
cocaine_tested_13,
cocaine_tested_14,
cocaine_tested_15,
cocaine_tested_16,
cocaine_tested_17,
cocaine_tested_18,
cocaine_tested_19,
cocaine_pos_06,
cocaine_pos_07,
cocaine_pos_08,
cocaine_pos_09,
cocaine_pos_10,
cocaine_pos_11,
cocaine_pos_12,
cocaine_pos_13,
cocaine_pos_14,
cocaine_pos_15,
cocaine_pos_16,
cocaine_pos_17,
cocaine_pos_18,
cocaine_pos_19,
methadone_tested_06,
methadone_tested_07,
methadone_tested_08,
methadone_tested_09,
methadone_tested_10,
methadone_tested_11,
methadone_tested_12,
methadone_tested_13,
methadone_tested_14,
methadone_tested_15,
methadone_tested_16,
methadone_tested_17,
methadone_tested_18,
methadone_tested_19,
methadone_pos_06,
methadone_pos_07,
methadone_pos_08,
methadone_pos_09,
methadone_pos_10,
methadone_pos_11,
methadone_pos_12,
methadone_pos_13,
methadone_pos_14,
methadone_pos_15,
methadone_pos_16,
methadone_pos_17,
methadone_pos_18,
methadone_pos_19,
fentanyl_mam_tested_06,
fentanyl_mam_tested_07,
fentanyl_mam_tested_08,
fentanyl_mam_tested_09,
fentanyl_mam_tested_10,
fentanyl_mam_tested_11,
fentanyl_mam_tested_12,
fentanyl_mam_tested_13,
fentanyl_mam_tested_14,
fentanyl_mam_tested_15,
fentanyl_mam_tested_16,
fentanyl_mam_tested_17,
fentanyl_mam_tested_18,
fentanyl_mam_tested_19,
fentanyl_mam_pos_06,
fentanyl_mam_pos_07,
fentanyl_mam_pos_08,
fentanyl_mam_pos_09,
fentanyl_mam_pos_10,
fentanyl_mam_pos_11,
fentanyl_mam_pos_12,
fentanyl_mam_pos_13,
fentanyl_mam_pos_14,
fentanyl_mam_pos_15,
fentanyl_mam_pos_16,
fentanyl_mam_pos_17,
fentanyl_mam_pos_18,
fentanyl_mam_pos_19,
opiate_tested_06,
opiate_tested_07,
opiate_tested_08,
opiate_tested_09,
opiate_tested_10,
opiate_tested_11,
opiate_tested_12,
opiate_tested_13,
opiate_tested_14,
opiate_tested_15,
opiate_tested_16,
opiate_tested_17,
opiate_tested_18,
opiate_tested_19,
opiate_pos_06,
opiate_pos_07,
opiate_pos_08,
opiate_pos_09,
opiate_pos_10,
opiate_pos_11,
opiate_pos_12,
opiate_pos_13,
opiate_pos_14,
opiate_pos_15,
opiate_pos_16,
opiate_pos_17,
opiate_pos_18,
opiate_pos_19,
med_asst_trmt_tested_06,
med_asst_trmt_tested_07,
med_asst_trmt_tested_08,
med_asst_trmt_tested_09,
med_asst_trmt_tested_10,
med_asst_trmt_tested_11,
med_asst_trmt_tested_12,
med_asst_trmt_tested_13,
med_asst_trmt_tested_14,
med_asst_trmt_tested_15,
med_asst_trmt_tested_16,
med_asst_trmt_tested_17,
med_asst_trmt_tested_18,
med_asst_trmt_tested_19,
med_asst_trmt_pos_06,
med_asst_trmt_pos_07,
med_asst_trmt_pos_08,
med_asst_trmt_pos_09,
med_asst_trmt_pos_10,
med_asst_trmt_pos_11,
med_asst_trmt_pos_12,
med_asst_trmt_pos_13,
med_asst_trmt_pos_14,
med_asst_trmt_pos_15,
med_asst_trmt_pos_16,
med_asst_trmt_pos_17,
med_asst_trmt_pos_18,
med_asst_trmt_pos_19
FROM 
hiv_rpt.prep_report_final_output_agetestpat_filter;


--COPY hiv_rpt.prep_report_masked_patient_id TO '/tmp/2019-09-25-atrius-prep.csv' DELIMITER ',' CSV HEADER;




--
-- Script shutdown section 
--
-- DROP TABLE IF EXISTS hiv_rpt.prep_hef_w_lab_details CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_cases_of_interest CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_gon_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_chlam_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_syph_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hebp_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_wbpos_elisaposneg CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_gon_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_chlam_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_syph_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_syph_cases CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_cpts_of_interest CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_cases CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hebp_cases CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_cases CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_new_diag CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_meds CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_truvada_array CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_truvada_2mogap CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_truvada_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_all_details CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_dx_codes CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_diag_fields CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_rx_of_interest CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_rx_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_output_part_1 CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_output_part_2 CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_output_with_patient CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_output_pat_and_enc CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_ouput_pat_and_enc_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_diags CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_diags_2 CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_problem_list CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_ouput_pat_and_hiv_enc_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_combo_distinct CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_3_diff CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_rx_3_diff_count CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_truvada_rx_and_pills CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hiv_meds_distinct CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_truvada_rx_and_pills_peryear CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_bicillin_all CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_bicillin_subset CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepb_labs_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_case_history CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_hepc_labs_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_herpes_counts CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_herpes_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_syph_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_syph_labs_events CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_amph CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_cocaine CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_fentanyl CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_labs CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_methadone CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_tox_opiate CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_sex_part_gend CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_alcohol_use CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_alcohol_oz_per_week CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_ill_drug_use CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_sexually_active CASCADE;
-- DROP TABLE IF EXISTS hiv_rpt.prep_birth_control_method CASCADE;
