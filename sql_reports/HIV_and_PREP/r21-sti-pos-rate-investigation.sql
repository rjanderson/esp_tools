

-- PATIENT COUNT

-- 39223
select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table;


-- GONORRHEA

-- Use this code to confirm prior variables

SELECT count(distinct(patient_id)) FROM (
select DISTINCT T1.id, T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
from nodis_case T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where condition = 'gonorrhea'
and T1.date >= week_0 - interval '12 months'
and T1.date < week_0
)T1

--should roughly match:

select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where prior_gon_cases >0;


----------------------------------------------

select count(distinct(patient_id)) from (
select DISTINCT T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
FROM hef_event T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where name = 'lx:gonorrhea:positive'
and T1.date <= week_0 + interval '48 months'
and T1.date >= week_0
and date <= '12-31-2020') T1;

-- should roughly match
select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where 
pos_gon_rectal > 0
OR pos_gon_oropharynx >0 
OR pos_gon_urog > 0;


--CHLAMYDIA

select count(distinct(patient_id)) FROM (
select DISTINCT T1.id, T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
from nodis_case T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where condition = 'chlamydia'
and T1.date >= week_0 - interval '12 months'
and T1.date < week_0
) T1;

--should roughly match:

select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where prior_chlam_cases >0;

--------------------------------------------------------------

select count(distinct(patient_id)) from (
select DISTINCT T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
FROM hef_event T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where name = 'lx:chlamydia:positive'
and T1.date <= week_0 + interval '48 months'
and T1.date >= week_0
and date <= '12-31-2020') T1;

-- should roughly match

select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where 
pos_chlam_rectal > 0
OR pos_chlam_oropharynx >0 
OR pos_chlam_urog > 0;


-- SYPHILIS

select count(distinct(patient_id)) FROM (
select DISTINCT T1.id, T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
from nodis_case T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where condition = 'syphilis'
and T1.date >= week_0 - interval '12 months'
and T1.date < week_0
) T1

-- should roughly match:

select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where prior_syph_cases >0;

---------------------------------------------------------

select count(distinct(patient_id)) from (
select DISTINCT T1.id, T1.patient_id, T1.date, T2.week_0, date - week_0 as date_diff
from nodis_case T1
INNER JOIN hiv_rpt.r21_output_table T2 ON (T1.patient_id = T2.master_patient_id)
where condition = 'syphilis'
and T1.date <= week_0 + interval '48 months'
and T1.date >= week_0
and date <= '12-31-2020') T1;


select count(distinct(master_patient_id))
from hiv_rpt.r21_output_table
where 
esp_syph_case > 0;




-- GET TOTAL COUNTS COMPARE TO PAPER
-- COMPARE TOTAL CASE COUNTS BY YEAR (any drops in cases by year)

select count(*), condition, extract(year from date) case_year
from nodis_case
where condition in ('gonorrhea', 'chlamydia', 'syphilis')
group by condition, case_year
order by condition, case_year;

-- HAVE ANY LABS BEEN UNMAPPED / ARE UNMAPPED?



------------------------------------------------------------------
-- STI MONTHLY RATE WITH ENCOUNTER IN THE MONTH

select count(distinct(master_patient_id)) pats_with_enc_in_month, extract(year from week_start) rpt_year, extract(month from week_start) rpt_month
from hiv_rpt.r21_output_table
where any_enc > 0
group by rpt_year, rpt_month
order by rpt_year, rpt_month;

select count(distinct(master_patient_id)) pats_with_sti_in_month, extract(year from week_start) rpt_year, extract(month from week_start) rpt_month
from hiv_rpt.r21_output_table
where 
pos_gon_rectal > 0
OR pos_gon_oropharynx >0 
OR pos_gon_urog > 0
OR pos_chlam_rectal > 0
OR pos_chlam_oropharynx >0 
OR pos_chlam_urog > 0
OR esp_syph_case > 0
group by rpt_year, rpt_month
order by rpt_year, rpt_month;




-- STI YEARLY RATE WITH ENCOUNTER IN THE MONTH

select count(distinct(master_patient_id)) pats_with_enc_in_year, extract(year from week_start) rpt_year
from hiv_rpt.r21_output_table
where any_enc > 0
group by rpt_year


select count(distinct(master_patient_id)) pats_with_sti_in_year, extract(year from week_start) rpt_year
from hiv_rpt.r21_output_table
where 
pos_gon_rectal > 0
OR pos_gon_oropharynx >0 
OR pos_gon_urog > 0
OR pos_chlam_rectal > 0
OR pos_chlam_oropharynx >0 
OR pos_chlam_urog > 0
OR esp_syph_case > 0
group by rpt_year
order by rpt_year;


--Report cited did not require a negative HIV test?

--We might be getting too many encounters?

--Are we duplicating encounters (using only the A natural_key?)

--If someone had a negative HIV test in 2012 and then chlam or gon in 2018, you won't see this reflected

-- try to compute STI rates for 2015 for Males to compare to report
-- count encounters for 2015 and compare to the report













