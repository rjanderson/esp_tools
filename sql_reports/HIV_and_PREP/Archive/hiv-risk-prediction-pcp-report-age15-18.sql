--Site Name
--Total # patients receiving care, 
--total # of active PCPs at this site, 
--total # of patients who have HIV risk scores in the top 2% (i.e. high-risk), 
--average number of high-risk patients per PCP,
--average number of visits per year for high-risk patients

DROP TABLE IF EXISTS hrp_prov_percentile;
DROP TABLE IF EXISTS hrp_prov_patsbydept;
DROP TABLE IF EXISTS hrp_prov_pcpsbydept;
DROP TABLE IF EXISTS hrp_prov_highriskpats;
DROP TABLE IF EXISTS hrp_prov_highriskpcps;
DROP TABLE IF EXISTS hrp_prov_encs_2017;
DROP TABLE IF EXISTS hrp_prov_output;
--DROP TABLE IF EXISTS

-- Patient Risk Score Percentile
create table hrp_prov_percentile as 
select ntile(100) over (order by hiv_risk_score desc) as percentile, * 
from hrp_data_output
where pcp_site is not null;

-- Count of patients by dept
create table hrp_prov_patsbydept AS
select count(*) total_pts_by_dept, 
count(case when date_part('year',age(date_of_birth)) <=18
and date_part('year',age(date_of_birth)) >=15 then 1 end) total_pts_15_to_18_by_dept,
dept 
from emr_patient pt,
emr_provider pr
where pt.pcp_id = pr.id
and pr.dept is not null
and pr.dept not in ('UNKNOWN')
and pr.last_name not in (
'UNKNOWN PLAINVILLE',
'INTERNAL MEDICINE',
'TEST',
'OPTOMETRIC INTERN',
'PILGRIM PROVIDER MD',
'PROVIDER',
'DO NOT USE',
'WRX SPANISH TRANSLATOR',
'EPIC MD')
and (now()::date - pr.updated_timestamp::date) <= 90
group by dept
order by dept;


-- Count of PCP's by dept
-- Must be assigned as a PCP on a patient record
create table hrp_prov_pcpsbydept AS
select count(distinct(pr.id)) num_of_pcps, dept
from emr_patient pt,
emr_provider pr
where pt.pcp_id = pr.id
and pr.dept is not null
and pr.dept not in ('UNKNOWN')
and pr.last_name not in (
'UNKNOWN PLAINVILLE',
'INTERNAL MEDICINE',
'TEST',
'OPTOMETRIC INTERN',
'PILGRIM PROVIDER MD',
'PROVIDER',
'DO NOT USE',
'WRX SPANISH TRANSLATOR',
'EPIC MD')
and (now()::date - pr.updated_timestamp::date) <= 90
group by dept
order by dept;

-- Count of patients with HIV risk score in the top 2% by dept
-- Provider must be active (updated within the last 90 days)
create table hrp_prov_highriskpats AS
select count(*) highriskpats, 
count(case when current_age <=18 and current_age >=15 then 1 end) highriskpats_15_to_18,
pcp_site
from hrp_prov_percentile T1,
emr_provider T2
where percentile <= 2
and pcp_site not in ('UNKNOWN')
and pcp_name not ilike 'UNKNOWN PLAINVILLE%'
and pcp_name not ilike 'INTERNAL MEDICINE%'
and pcp_name not ilike 'TEST,%'
and pcp_name not ilike 'OPTOMETRIC INTERN%'
and pcp_name not ilike 'PILGRIM PROVIDER MD%'
and pcp_name not ilike 'PROVIDER%'
and pcp_name not ilike 'DO NOT USE%'
and pcp_name not ilike 'WRX SPANISH TRANSLATOR%'
and pcp_name not ilike 'EPIC MD%'
and concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) = T1.pcp_name
and T1.pcp_site = T2.dept
and (now()::date - T2.updated_timestamp::date) <= 90
group by pcp_site
order by pcp_site;


-- Count of highrisk pcp's by site
create table hrp_prov_highriskpcps AS
select count(distinct(pcp_name)) highriskpcps, pcp_site
from hrp_prov_percentile T1,
emr_provider T2
where percentile <= 2
and pcp_site not in ('UNKNOWN')
and pcp_name not ilike 'UNKNOWN PLAINVILLE%'
and pcp_name not ilike 'INTERNAL MEDICINE%'
and pcp_name not ilike 'TEST,%'
and pcp_name not ilike 'OPTOMETRIC INTERN%'
and pcp_name not ilike 'PILGRIM PROVIDER MD%'
and pcp_name not ilike 'PROVIDER%'
and pcp_name not ilike 'DO NOT USE%'
and pcp_name not ilike 'WRX SPANISH TRANSLATOR%'
and pcp_name not ilike 'EPIC MD%'
and concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) = T1.pcp_name
and T1.pcp_site = T2.dept
and (now()::date - T2.updated_timestamp::date) <= 90
group by pcp_site
order by pcp_site;

-- average number of visits per year for high-risk patients
-- had with their PCP's in 2017
-- need to find only encounters where pcp_id matches the visit id
-- limit to ambulatory encounters
-- provider must be active
CREATE TABLE hrp_prov_encs_2017 AS 
SELECT count(*) as hrp_prov_hr_encs_17, 
count(case when current_age <=18 and current_age >=15 then 1 end) hrp_prov_hr_encs_17_15_to_18,
T3.pcp_site
from emr_encounter T1,
emr_patient T2,
hrp_prov_percentile T3,
static_enc_type_lookup T4,
emr_provider T5
WHERE T1.patient_id = T2.id
AND T1.provider_id = T2.pcp_id
AND T1.patient_id = T3.patient_id
AND T2.id = T3.patient_id
AND T1.raw_encounter_type = T4.raw_encounter_type
AND T4.ambulatory = 1
and pcp_site not in ('UNKNOWN')
and pcp_name not ilike 'UNKNOWN PLAINVILLE%'
and pcp_name not ilike 'INTERNAL MEDICINE%'
and pcp_name not ilike 'TEST,%'
and pcp_name not ilike 'OPTOMETRIC INTERN%'
and pcp_name not ilike 'PILGRIM PROVIDER MD%'
and pcp_name not ilike 'PROVIDER%'
and pcp_name not ilike 'DO NOT USE%'
and pcp_name not ilike 'WRX SPANISH TRANSLATOR%'
and pcp_name not ilike 'EPIC MD%'
AND T3.percentile <= 2
and concat(T5.last_name, ', ', T5.first_name, ' ', T5.title) = T3.pcp_name
and T3.pcp_site = T5.dept
and (now()::date - T5.updated_timestamp::date) <= 90
AND date >= '2017-01-01'
AND date < '2018-01-01'
GROUP BY pcp_site;

CREATE TABLE hrp_prov_output AS 
select T1.dept as site, 
total_pts_by_dept as patient_count, 
total_pts_15_to_18_by_dept as patient_count_age15_to_18,
highriskpats as highrisk_pat_count, 
highriskpats_15_to_18 as highrisk_pat_count_age15_to_18,
num_of_pcps as pcp_count, 
highriskpcps highrisk_pcp_count, 
(highriskpats/highriskpcps) as avg_num_hr_pats_per_pcp, 
COALESCE(hrp_prov_hr_encs_17,0) as total_hr_pat_encs_2017, 
coalesce(round((hrp_prov_hr_encs_17::real/highriskpats::real)),0) as avg_num_encs_for_hr_pats_17
FROM hrp_prov_highriskpats T6
LEFT JOIN hrp_prov_patsbydept T1 ON (T1.dept = T6.pcp_site)
LEFT JOIN hrp_prov_pcpsbydept T2 ON (T2.dept = T6.pcp_site)
LEFT JOIN hrp_prov_highriskpcps T4 ON (T4.pcp_site = T6.pcp_site)
LEFT JOIN hrp_prov_encs_2017 T5 ON (T5.pcp_site = T6.pcp_site);

select * from hrp_prov_output order by site;

-- subset for particular sites
select * from hrp_prov_output 
where (site ilike '%Kenmore%'
or site ilike '%Copley%'
or site ilike '%Cambridge%')
and site not ilike 'EXTERNAL%'
order by site;


-- subset provider lineslist query for validation
select concat(pr.last_name, ', ', pr.first_name, ' ', pr.title) pcp_name, dept site
from emr_patient pt,
emr_provider pr,
hrp_prov_highriskpcps hr_pcp
where pt.pcp_id = pr.id
and pr.dept is not null
and pr.dept = hr_pcp.pcp_site
and pr.dept not in ('UNKNOWN')
and pr.last_name not in (
'UNKNOWN PLAINVILLE',
'INTERNAL MEDICINE',
'TEST',
'OPTOMETRIC INTERN',
'PILGRIM PROVIDER MD',
'PROVIDER',
'DO NOT USE',
'WRX SPANISH TRANSLATOR',
'EPIC MD')
and (now()::date - pr.updated_timestamp::date) <= 90
and (dept ilike '%Kenmore%'
or dept ilike '%Copley%'
or dept ilike '%Cambridge%')
and dept not ilike 'EXTERNAL%'
group by pr.last_name, pr.first_name, pr.title, dept
order by dept, pr.last_name, pr.first_name;



