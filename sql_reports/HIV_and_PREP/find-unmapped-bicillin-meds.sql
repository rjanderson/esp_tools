drop table if exists hiv_rpt.unmapped_bicillin_meds;
create table hiv_rpt.unmapped_bicillin_meds as 
select DISTINCT count(*), name, dose, quantity, quantity_float, directions, max(date) most_recent_rx_date
from emr_prescription
where (name ilike '%BICILLIN%'
or name ilike '%PEN G%'
or name ilike '%PENICILLIN G%')
AND name not in (select name from hiv_risk_bicillin_meds)
group by name, dose, quantity, quantity_float, directions
order by name, dose, quantity, quantity_float, directions;