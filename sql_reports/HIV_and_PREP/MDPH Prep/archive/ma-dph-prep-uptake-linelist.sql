DROP TABLE IF EXISTS hiv_rpt.dphll_index_pats;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_diags;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bicillin;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bupren_naloxone_rx;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_gonorrhea;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_hiv_tests;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output;


-- Threshold set to .02
-- Using patients with a risk_score this year
CREATE TABLE hiv_rpt.dphll_index_pats AS
SELECT patient_id, hiv_risk_score
FROM gen_pop_tools.cc_hiv_risk_score
WHERE hiv_risk_score >= .02
AND rpt_year = '2021';


-- Previous PrEP order (Y/N)
-- Date of last PrEP order
CREATE TABLE hiv_rpt.dphll_prep AS
SELECT T1.patient_id, 
       'Y'::text as previous_prep_order, 
	   max(date) last_prep_order_date
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
AND T2.date <= now()
GROUP BY T1.patient_id;

-- Dx code for syphilis (Y/N) EVER
-- Dx code for contact with or exposure to venereal disease (Y/N) EVER

CREATE TABLE hiv_rpt.dphll_prep_diags AS 
SELECT T1.patient_id, 
MAX(CASE WHEN dx_code_id ~  '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)'  or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then 'Y'::text end) AS syph_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then 'Y'::text end) AS cont_w_or_expo_ven_dis_dx_ever
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
WHERE (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6'))
AND date <= now()
GROUP BY T1.patient_id;

-- Bicillin Rx EVER

CREATE TABLE hiv_rpt.dphll_prep_bicillin AS
SELECT T1.patient_id, 'Y'::text AS bicillin_24_inj_ever
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE ( name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
--BMC
'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G IVPB  IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
--FENWAY
'BICILLIN LA 100,000 UNITS',
'BICILLIN LA 100M000 UNITS',
'BICILLIN L-A 2400000 U/4ML SUSPN',
'BICILLIN L-A 2400000 UNIT/4ML INTRAMUSCULAR SUSPENSION',
'BICILLIN L-A 2400000 UNIT/4ML SUSP',
'BICILLIN L-A 2400000 UNIT/4ML  SUSP',
'Med: Bicillin LA 2.4 million units IM, THREE doses over THREE weeks',
'Med:  Bicillin LA 2.4 million units IM, TWO doses 1 week apart',
'PENICILLIN G LA 2.4 MILLION UNITS'
)
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
-- BMC
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE') and (dose = '2.4 Million Units' or dose = '2400000 Units'))
-- FENWAY
OR (name in ('BICILLIN L-A 1200000 U/2ML SUSPN', 'BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'BICILLIN L-A 1200000 UNIT/2ML INTRAMUSCULAR SUSPENSION', 'BICILLIN L-A 1200000 UNIT/2ML SUSP', 'Med:  Bicillin 1.2 mil/u  x 1', 'Med: Bicillin LA 1.2 million units IM') and quantity_float >= 2)
OR (name in ('Bicillin injection, route of administration') and 
	(quantity ilike '%1.2%' 
	or quantity ilike '%2.4%' 
	or quantity ilike '%2ml%' 
	or quantity ilike '%2.5%' 
	or quantity ilike '%2x2.0%' 
	or quantity ilike '%2.0 ml%'
	or quantity ilike '%240million%')) )
AND date <= now()
GROUP BY T1.patient_id;

-- Prescription for buprenorphine/naloxone (suboxone) in prior 2 years (Y/N)
CREATE TABLE hiv_rpt.dphll_prep_bupren_naloxone_rx AS
SELECT T1.patient_id, 'Y'::text AS bupren_naloxone_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE ( name ilike '%suboxone%'  
	or name ilike 'buprenorphine%naloxone%'
	or name ilike '%zubsolv%' 
	or name ilike '%bunavail%' 
	or name ilike '%cassipa%' )
-- must be within last 2 years
AND T2.date >= (now()::date - interval '2 years')
AND T2.date <= now()
GROUP BY T1.patient_id;


-- Number of positive Gonorrhea tests in prior 2 years
-- Only count 1 pos test per date
CREATE TABLE hiv_rpt.dphll_prep_gonorrhea AS
SELECT patient_id, count(*) AS pos_gon_tests_last_2yr
FROM 
	(SELECT T1.patient_id, date
	FROM hiv_rpt.dphll_index_pats T1
	INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name = 'lx:gonorrhea:positive'
	AND T2.date >= (now()::date - interval '2 years')
	AND T2.date <= now()
	GROUP BY T1.patient_id, T2.date) S1
GROUP BY patient_id;

-- Number of HIV tests in prior 2 years
-- Only count 1 test per date
CREATE TABLE hiv_rpt.dphll_prep_hiv_tests AS
SELECT patient_id, count(*) as hiv_tst_dates_last_2yr
FROM
    (SELECT T1.patient_id, date
	FROM hiv_rpt.dphll_index_pats T1
	INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name ilike 'lx:hiv%'
	AND T2.date >= (now()::date - interval '2 years')
	AND T2.date <= now()
	GROUP BY T1.patient_id, T2.date) S1
GROUP BY patient_id;


-- Join with patient demographics
CREATE TABLE hiv_rpt.dphll_prep_ll_output AS
SELECT 
--T1.patient_id,
T1.hiv_risk_score,
upper(T2.first_name) AS first_name,
upper(T2.last_name) AS last_name,
T2.mrn,
T2.gender,
T4.sex_partner_gender,
upper(T2.race) AS race,
upper(T2.ethnicity) AS ethnicity,
date_part('year',age(T2.date_of_birth)) current_age,
upper(T2.home_language) AS home_language,
T2.birth_country,
T2.next_appt_date::date,
CASE WHEN T2.next_appt_fac_provider_id != 1 THEN UPPER(T5.dept) 
     ELSE NULL END AS next_appt_facility,
CASE WHEN T2.next_appt_provider_id != 1 THEN UPPER(concat(T6.last_name, ', ', T6.first_name))
     ELSE NULL END AS next_appt_provider,
UPPER(concat(T7.last_name, ', ', T7.first_name)) AS  pcp_name,
coalesce(T8.previous_prep_order, 'N'::text) AS previous_prep_order,
T8.last_prep_order_date,
CASE WHEN T9.syph_dx_ever = 'Y' 
          AND T10.bicillin_24_inj_ever = 'Y'
		  AND T9.cont_w_or_expo_ven_dis_dx_ever = 'Y'
		  THEN 'Y'::text
		  ELSE 'N'::text
		  END AS syph_dx_and_bicillin_and_contact_dx_all,
CASE WHEN T9.syph_dx_ever = 'Y' 
          OR T10.bicillin_24_inj_ever = 'Y'
		  OR T9.cont_w_or_expo_ven_dis_dx_ever = 'Y'
		  THEN 'Y'::text
		  ELSE 'N'::text
		  END AS syph_dx_and_bicillin_and_contact_dx_any,		  
coalesce(T9.syph_dx_ever, 'N'::text) AS syph_dx_ever,
coalesce(T9.cont_w_or_expo_ven_dis_dx_ever, 'N'::text) AS cont_w_or_expo_ven_dis_dx_ever,
coalesce(T10.bicillin_24_inj_ever, 'N'::text) AS bicillin_24_inj_ever,
coalesce(T11.bupren_naloxone_last_2yr, 'N'::text) AS bupren_naloxone_last_2yr,
coalesce(T12.pos_gon_tests_last_2yr, 0) AS pos_gon_tests_last_2yr,
coalesce(T13.hiv_tst_dates_last_2yr, 0) AS hiv_tst_dates_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN (
    (select max(date) sochist_date, T2.patient_id
	from emr_socialhistory T1,
	hiv_rpt.dphll_index_pats T2
	where T1.patient_id = T2.patient_id
	and sex_partner_gender is not null
	and date <= now()
	GROUP BY T2.patient_id) 
	) T3 on (T1.patient_id = T3.patient_id)
-- If multiple sex_partner_gender values on the same date, just take one of them
LEFT JOIN (select patient_id, max(sex_partner_gender) as sex_partner_gender, date from emr_socialhistory where sex_partner_gender is not null GROUP BY patient_id, date) T4 
               on (T3.patient_id = T4.patient_id and T1.patient_id = T4.patient_id and T3.sochist_date = T4.date)
LEFT JOIN emr_provider T5 ON (T5.id = T2.next_appt_fac_provider_id)
LEFT JOIN emr_provider T6 ON (T6.id = T2.next_appt_provider_id)
LEFT JOIN emr_provider T7 ON (T7.id = T2.pcp_id)
LEFT JOIN hiv_rpt.dphll_prep T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_diags T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_bicillin T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_bupren_naloxone_rx T11 ON (T1.patient_id = T11.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_gonorrhea T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_hiv_tests T13 ON (T1.patient_id = T13.patient_id)
ORDER BY T1.hiv_risk_score desc;














