--risk factor - IDU	karen
--number of quarters they have appeared	karen
--do you want birth_sex	kshema




-- -- NEEDED FOR REPORTING
-- -- ONE TIME RUN ONLY
-- CREATE SEQUENCE hiv_rpt.hiv_risk_pats_masked_id_seq;


-- CREATE TABLE hiv_rpt.hiv_risk_masked_patients
-- (
    -- patient_id integer,
    -- masked_patient_id character varying(128)
-- );


-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST
-- CHA-, BMC-, ATR-, FWY-

INSERT INTO hiv_rpt.hiv_risk_masked_patients
SELECT 
DISTINCT patient_id,
CONCAT(:site_abbr, NEXTVAL('hiv_rpt.hiv_risk_pats_masked_id_seq')) masked_patient_id
FROM hiv_risk_patient_linelist_history
WHERE patient_id not in (select patient_id from hiv_rpt.hiv_risk_masked_patients)
GROUP by patient_id;

DROP TABLE IF EXISTS hiv_rpt.hiv_risk_outcome_report;
CREATE TABLE hiv_rpt.hiv_risk_outcome_report AS
SELECT DISTINCT 
--T1.patient_id as esp_pat_id, 
now()::date as report_generation_date,
T9.masked_patient_id as masked_patient_id,
--date_part('year',age(T2.date_of_birth)) current_age,
CASE   
	when date_part('year', age(date_of_birth)) <= 9 then '0-9'    
	when date_part('year', age(date_of_birth)) <= 19 then '10-19'  
	when date_part('year', age(date_of_birth)) <= 29 then '20-29' 
	when date_part('year', age(date_of_birth)) <= 39 then '30-39' 
	when date_part('year', age(date_of_birth)) <= 49 then '40-49'   
	when date_part('year', age(date_of_birth)) <= 59 then '50-59'  
	when date_part('year', age(date_of_birth)) <= 69 then '60-69' 
	when date_part('year', age(date_of_birth)) <= 79 then '70-79' 
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>= 80' 
	END age_group_10_yr,
CASE 
    WHEN gender is null then 'U'
	WHEN gender in ('M', 'MALE') then 'M'
	WHEN gender in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END gender,		
CASE
    when upper(race) in ('NONE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED') then 'DECLINED TO ANSWER'
    ELSE upper(race) END race, 
CASE 
    when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED') or ethnicity is null then 'UNKNOWN'
	when upper(race) in ('HISPANIC','HISP', 'HISPANIC/LAT' ) or upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED') then 'DECLINED TO ANSWER'
	ELSE upper(ethnicity) END ethnicity,
CASE WHEN gender_identity is null or gender_identity = '' THEN 'UNKNOWN' ELSE upper(gender_identity) END gender_identity,
CASE WHEN sex_orientation is null or sex_orientation = '' THEN 'UNKNOWN' ELSE upper(sex_orientation) END sex_orientation,
--concat(extract(year from min(ll_date)), ' Q', extract(quarter from min(ll_date)) ) first_linelist_quarter,
min(T1.ll_date) first_linelist_date,
max(T1.ll_date) most_recent_linelist_date,
CASE WHEN T3.patient_id is not null THEN 1 ELSE 0 END active_linelist_patient,
T6.disposition_name as disposition,
(now()::date - disposition_date::date) disposition_age_in_days,
MAX(CASE WHEN T6.id in (9,10,11,12,13,14) THEN 'YES' END) prep_discussion,
MAX(CASE WHEN T6.id in (6, 14, 15) then 'YES' END) prep_per_disposition,
MAX(CASE WHEN T7.date >= T8.first_ll_date then 'YES' END) prep_per_rx,
MAX(CASE WHEN T1.ll_date = T10.last_ll_date then T1.hiv_risk_score END) most_recent_risk_score,
MAX(CASE WHEN T1.ll_date = T10.last_ll_date then T1.highest_risk_variable END) most_recent_highest_risk_variable
FROM hiv_risk_patient_linelist_history T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN hiv_risk_patient_linelist T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN (SELECT patient_id, max(id) most_recent_dispo_id FROM hiv_risk_patient_dispositions GROUP BY patient_id) T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_risk_patient_dispositions T5 ON (T1.patient_id = T5.patient_id and T4.patient_id = T5.patient_id and T4.most_recent_dispo_id = T5.id)
LEFT JOIN hiv_risk_static_dispositions T6 ON (T5.disposition_id = T6.id)
LEFT JOIN hef_event T7 ON (T1.patient_id = T7.patient_id and T7.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
'rx:hiv_cabotegravir_er_600')) 
LEFT JOIN (SELECT patient_id, min(ll_date) first_ll_date FROM hiv_risk_patient_linelist_history GROUP BY patient_id) T8 ON (T1.patient_id = T8.patient_id)
JOIN hiv_rpt.hiv_risk_masked_patients T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN (SELECT patient_id, max(ll_date) last_ll_date FROM hiv_risk_patient_linelist_history GROUP BY patient_id) T10 ON (T1.patient_id = T10.patient_id)
GROUP BY T1.patient_id, T2.date_of_birth, T2.gender, race, ethnicity, gender_identity, sex_orientation, T3.patient_id, disposition_name, T9.masked_patient_id, disposition_date;