-- Function: hiv_rpt.hiv_risk_score_calc_for_month(integer, date)

-- DROP FUNCTION hiv_rpt.hiv_risk_score_calc_for_month(integer, date);

CREATE OR REPLACE FUNCTION hiv_rpt.hiv_risk_score_calc_for_month(
    IN pat_id integer,
    IN score_month date,
    OUT risk_score numeric)
  RETURNS numeric AS
$BODY$

BEGIN

-- try doing multiple transactions -- begin/commit/etc


DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_coefficients;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_index_patients;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_pat_encs;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_hiv_labs;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_hiv_lab_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_gon_chlam_events;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_gon_chlam_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_diags;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_diags_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_bicillin;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_bicillin_subset;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_bicillin_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_suboxone;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_suboxone_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_truvada_values;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_truvada;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_syph_events;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_syph_max_dates;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_pat_sum_x_value;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_index_data;
DROP TABLE IF EXISTS hiv_rpt.hiv_risk_score_risk_scores;

CREATE TABLE hiv_rpt.hiv_risk_score_coefficients AS
SELECT 1.00000000::real sex,
1.06186066::real race_black,
-0.6609999::real race_caucasian,
-0.4213635::real english_lang,
-0.0784053::real has_language_data,
-0.0660395::real years_of_data,
-0.6311702::real has_1yr_data,
-0.4035768::real has_2yr_data,
0.14676104::real t_hiv_rna_1_yr,
0.23349732::real t_hiv_test_2yr,
0.12024552::real t_hiv_test_e,
0.16006917::real t_hiv_elisa_e,
1.82082955::real acute_hiv_test_e,
0.15925514::real acute_hiv_test_2yr,
3.07004984::real t_pos_gon_test_2yr,
-0.154315::real t_chla_test_t_e,
1.35516827::real rx_bicillin_1_yr,
0.20795003::real rx_bicillin_2yr,
1.79771045::real rx_bicillin_e,
0.19721102::real rx_suboxone_2yr,
0.9997348::real syphilis_any_site_state_x_late_e,
0.28886449::real contact_w_or_expo_venereal_dis_e,
1.09544347::real hiv_counseling_2yr;

-- GET LAST DAY OF MONTH FOR THE MONTH THAT RISK SCORE SHOULD BE COMPUTED
SELECT (date_trunc('month', SCORE_MONTH::date) + interval '1 month' - interval '1 day')::date
AS end_of_month INTO SCORE_MONTH;


-- ONLY COMPUTING FOR PATIENTS THAT HAD A AMBULATORY ENCOUNTER IN THE 2 YEARS PRIOR TO RUN YEAR
CREATE TABLE hiv_rpt.hiv_risk_score_index_patients AS
SELECT T1.id as patient_id,
SCORE_MONTH as rpt_month,
CASE WHEN gender in ('F', 'FEMALE') then -9.374485
     WHEN gender in ('M', 'MALE') then -7.508866
     END sex,
CASE WHEN upper(race) in ('BLACK') then 1 ELSE 0 END race_black,
CASE WHEN upper(race) in ('WHITE', 'CAUCASIAN') then 1 ELSE 0 END race_caucasian,
CASE WHEN upper(home_language) = 'ENGLISH' then 1 ELSE 0 END english_lang,
CASE WHEN upper(home_language) not in ('DECLINED', 'UNKNOWN', 'UNABLE TO BE DETERMINED', 'NONE', '') and home_language is not null then 1 else 0 END has_language_data
FROM emr_patient T1
JOIN emr_encounter T2 on (T1.id = T2.patient_id) 
LEFT JOIN static_enc_type_lookup T3 on (T2.raw_encounter_type = T3.raw_encounter_type)
WHERE upper(gender) in ('M', 'MALE', 'FEMALE', 'F')
-- must have an ambulatory encounter in the past 2 years
AND T2.date >= (SCORE_MONTH - interval '2 years')
AND T3.ambulatory = 1
-- must be at least age 15
AND date_part('year', age(SCORE_MONTH, date_of_birth)) >= 15
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv' and date <= SCORE_MONTH)
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%'
AND T2.date <= SCORE_MONTH
AND T1.id = PAT_ID
GROUP BY T1.id, rpt_month, gender, race, home_language;


-- years_of_data 
-- length of available EHR history in years (based on encounter of specific type)
-- # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
CREATE TABLE hiv_rpt.hiv_risk_score_pat_encs AS 
SELECT patient_id, (max(abs(date_part('year', age(date, SCORE_MONTH))) ) + 1) years_of_data,
max(CASE WHEN abs(date_part('year', age(date, SCORE_MONTH))) = 0 then 1 END) has_1yr_data,
max(CASE WHEN abs(date_part('year', age(date, SCORE_MONTH))) = 1 then 1 END) has_2yr_data
FROM emr_encounter T1,
static_enc_type_lookup T2
WHERE T1.raw_encounter_type = T2.raw_encounter_type
AND T2.ambulatory = 1
AND DATE >= '01-01-2006' 
AND DATE <= SCORE_MONTH
AND patient_id = PAT_ID
GROUP BY patient_id;

-- Gather up all of the HIV lab tests
-- (ELISA or Ab/Ag or RNA) 
-- NEED TO LIMIT TO THESE TEST TYPES TO ELISA, HIV_RNA_VIRAL, HIV_AG_AB
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH A HEF EVENT OR ELSE YOU WILL MISS VIRAL LOAD TESTS
CREATE TABLE hiv_rpt.hiv_risk_score_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM T1.date) rpt_month, abs(date_part('year', age(T1.date, SCORE_MONTH))) yrs_since_test, T1.date, result_string, name as hef_name, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
LEFT JOIN hef_event T3 ON (T1.id = T3.object_id and T1.patient_id = T3.patient_id)
WHERE test_name in ('hiv_elisa', 'hiv_rna_viral', 'hiv_ag_ab')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
AND result_string not ilike '%cancelled%'
AND result_string is not null
AND T1.date >= '01-01-2006' 
AND T1.date <= SCORE_MONTH
AND T1.patient_id = PAT_ID;


-- Compute HIV test values
-- number of HIV RNA tests in the previous year
-- total number of HIV tests in past 2 years (regardless of test result)
-- total number of HIV tests ever (ELISA or Ab/Ag or RNA)
-- total number of HIV ELISA or Ab/Ag tests ever
-- 1 if had an HIV RNA test in past 2 years, 0 otherwise
-- 1 if had an HIV RNA test ever, 0 otherwise

CREATE TABLE hiv_rpt.hiv_risk_score_hiv_lab_values AS
SELECT
patient_id,
count(CASE WHEN yrs_since_test = 0 AND test_name = 'hiv_rna_viral' THEN 1 END) t_hiv_rna_1_yr,
count(CASE WHEN yrs_since_test <= 1 THEN 1 END) t_hiv_test_2yr,
count(*) t_hiv_test_e,
count(CASE WHEN test_name in ('hiv_elisa', 'hiv_ag_ab') THEN 1 END) t_hiv_elisa_e,
max(CASE WHEN yrs_since_test <= 1 AND test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_2yr,
max(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_e,
-- ADDED FOR COC
max(CASE WHEN rpt_month = (EXTRACT(YEAR FROM SCORE_MONTH::date)) then 1 END) hiv_test_this_yr
FROM hiv_rpt.hiv_risk_score_hiv_labs
WHERE patient_id = PAT_ID
GROUP BY patient_id;


-- Gather up labs hef events for chlamydia and gonorrhea 
CREATE TABLE hiv_rpt.hiv_risk_score_gon_chlam_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, SCORE_MONTH))) yrs_since_test, result_string
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T1.date <= SCORE_MONTH
AND T2.test_name in ('chlamydia', 'gonorrhea')
AND T1.patient_id = PAT_ID;


-- Compute Chlamydia and Gonorrhea values
-- # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years
-- # t_chla_test_t_e	total number of chlamydia tests ever

CREATE TABLE hiv_rpt.hiv_risk_score_gon_chlam_values AS
SELECT 
patient_id,
count(CASE WHEN hef_name = 'lx:gonorrhea:positive' AND yrs_since_test <= 1 THEN 1 END) t_pos_gon_test_2yr,
count(CASE WHEN condition = 'chlamydia' THEN 1 END) t_chla_test_t_e
FROM hiv_rpt.hiv_risk_score_gon_chlam_events
GROUP BY patient_id;

-- Gather up diagnosis codes of interest
-- # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise (icd9:091.*, icd9:092.*, icd9:093.*, icd9:094.*, icd9:095.*, icd10:A51.*, icd9:097.9, icd10:A52.0, icd10:A52.7) 
-- # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	
-- # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)
CREATE TABLE hiv_rpt.hiv_risk_score_diags AS 
SELECT T2.patient_id, T1.id, T1.encounter_id, T1.dx_code_id, abs(date_part('year', age(T2.date, SCORE_MONTH))) yrs_since_diag
FROM emr_encounter_dx_codes T1, 
emr_encounter T2
WHERE 
T1.encounter_id = T2.id
AND (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V65.44','icd10:Z71.7'))
AND T2.date <= SCORE_MONTH
AND T2.patient_id = PAT_ID;


CREATE TABLE hiv_rpt.hiv_risk_score_diags_values AS
SELECT T1.patient_id,
MAX(CASE WHEN dx_code_id ~  '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)'  or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then 1 end) syphilis_any_site_state_x_late_e,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then  1 end) contact_w_or_expo_venereal_dis_e,
MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') and yrs_since_diag <=1  then 1 end) hiv_counseling_2yr
FROM hiv_rpt.hiv_risk_score_diags T1
GROUP BY patient_id;


-- # rx_bicillin_1_year	number of prescriptions for bicillin in previous year
-- # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years
-- # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- Only count 1 rx per date/ per name

CREATE TABLE hiv_rpt.hiv_risk_score_bicillin AS
SELECT patient_id, name, date, abs(date_part('year', age(date, SCORE_MONTH))) yrs_since_rx
FROM emr_prescription 
WHERE ( name in (
-- ATRIUS
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA 
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR',
--BMC
'BICILLIN L-A 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML INTRAMUSCULAR SYRINGE',
'PENICILLIN G IVPB  IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G POTASSIUM 20 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM 5 MILLION UNIT SOLUTION FOR INJECTION',
'PENICILLIN G POTASSIUM IV 5 MILLION UNITS MBP',
'PENICILLIN G SODIUM 5 MILLION UNIT SOLUTION FOR INJECTION')
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)') and quantity_float >= 4)
-- CHA
OR (name in ('BICILLIN L-A 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP') and quantity_float >= 2)
OR (name in ('BICILLIN L-A 600000 UNIT/ML IM SUSP', 'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP') and quantity_float >= 4)
-- BMC
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML INTRAMUSCULAR SYRINGE', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML INTRAMUSCULAR SYRINGE') and (dose = '2.4 Million Units' or dose = '2400000 Units')) )
AND date <= SCORE_MONTH
AND patient_id = PAT_ID
GROUP BY patient_id, name, date;


-- Exclude bicillin prescriptions that match specific dx codes 
-- occurring on the same date
CREATE TABLE hiv_rpt.hiv_risk_score_bicillin_subset AS
SELECT * FROM  hiv_rpt.hiv_risk_score_bicillin
EXCEPT
SELECT T1.*
FROM hiv_rpt.hiv_risk_score_bicillin T1, 
emr_encounter T2,
emr_encounter_dx_codes T3
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
and T1.date = T2.date
order by patient_id;

CREATE TABLE hiv_rpt.hiv_risk_score_bicillin_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx = 0 THEN 1 END) rx_bicillin_1_yr,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_bicillin_2yr,
count(*) rx_bicillin_e
FROM hiv_rpt.hiv_risk_score_bicillin_subset T1
GROUP BY patient_id;

-- # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years
CREATE TABLE hiv_rpt.hiv_risk_score_suboxone AS
SELECT patient_id, name, date, abs(date_part('year', age(date, SCORE_MONTH))) yrs_since_rx
FROM emr_prescription T1
WHERE (T1.name ilike '%suboxone%' 
or T1.name ilike 'buprenorphine%naloxone%' 
or T1.name ilike '%zubsolv%'
or T1.name ilike '%bunavail%'
or T1.name ilike '%cassipa%')
AND date <= SCORE_MONTH
AND T1.patient_id = PAT_ID
GROUP BY patient_id, name, date;

CREATE TABLE hiv_rpt.hiv_risk_score_suboxone_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_suboxone_2yr
FROM hiv_rpt.hiv_risk_score_suboxone T1
GROUP BY patient_id;

-- # For COC, has patient had a prescription for Truvada in this year
CREATE TABLE hiv_rpt.hiv_risk_score_truvada_values AS
SELECT patient_id, 1::integer  truvada_rx_this_yr
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
AND EXTRACT(YEAR FROM T1.date) = EXTRACT(YEAR FROM SCORE_MONTH::date)
AND date <= SCORE_MONTH
AND T1.patient_id = PAT_ID
GROUP BY patient_id;

-- Get most recent Truvada rx date
CREATE TABLE hiv_rpt.hiv_risk_score_truvada AS
SELECT patient_id, max(date) most_recent_truvada_rx
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
AND date <= SCORE_MONTH
AND T1.patient_id = PAT_ID
GROUP BY patient_id;

-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
CREATE TABLE hiv_rpt.hiv_risk_score_syph_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, SCORE_MONTH))) yrs_since_test, result_string, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T1.date <= SCORE_MONTH
AND T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test', 'CANCELLED. DUPLICATE REQUEST.')
AND result_string !~ ('CANCELLED')
AND T1.patient_id = PAT_ID
and result_string is not null;

-- Get date of most recent syph event for each patient
CREATE TABLE hiv_rpt.hiv_risk_score_syph_max_dates AS 
SELECT T1.patient_id, max(date) as syph_recent_date
FROM hiv_rpt.hiv_risk_score_syph_events T1
GROUP BY T1.patient_id;

-- Bring together all of the fields and values
CREATE TABLE hiv_rpt.hiv_risk_score_index_data AS
SELECT T1.patient_id,
T1.rpt_month,
sex,
race_black,
race_caucasian,
english_lang,
has_language_data,
coalesce(years_of_data, 0) years_of_data,
coalesce(has_1yr_data, 0) has_1yr_data,
coalesce(has_2yr_data, 0) has_2yr_data,
coalesce(t_hiv_rna_1_yr, 0) t_hiv_rna_1_yr,
coalesce(t_hiv_test_2yr, 0) t_hiv_test_2yr,
coalesce(t_hiv_test_e, 0) t_hiv_test_e,
coalesce(t_hiv_elisa_e, 0) t_hiv_elisa_e,
coalesce(hiv_test_this_yr, 0) hiv_test_this_yr,
coalesce(truvada_rx_this_yr, 0) truvada_rx_this_yr,
coalesce(acute_hiv_test_2yr, 0) acute_hiv_test_2yr,
coalesce(acute_hiv_test_e, 0) acute_hiv_test_e,
coalesce(rx_bicillin_1_yr, 0) rx_bicillin_1_yr,
coalesce(rx_bicillin_2yr, 0) rx_bicillin_2yr,
coalesce(rx_bicillin_e, 0) rx_bicillin_e,
coalesce(rx_suboxone_2yr, 0) rx_suboxone_2yr,
coalesce(t_pos_gon_test_2yr, 0) t_pos_gon_test_2yr,
coalesce(t_chla_test_t_e, 0) t_chla_test_t_e,
coalesce(syphilis_any_site_state_x_late_e, 0) syphilis_any_site_state_x_late_e,
coalesce(contact_w_or_expo_venereal_dis_e, 0) contact_w_or_expo_venereal_dis_e,
coalesce(hiv_counseling_2yr, 0) hiv_counseling_2yr,
coalesce(syph_recent_date, null) syph_last_date,
coalesce(most_recent_truvada_rx, null) most_recent_truvada_rx
FROM hiv_rpt.hiv_risk_score_index_patients T1
LEFT JOIN hiv_rpt.hiv_risk_score_pat_encs T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_hiv_lab_values T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_bicillin_values T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_suboxone_values T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_gon_chlam_values T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_diags_values T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_truvada_values T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_syph_max_dates T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hiv_rpt.hiv_risk_score_truvada T10 ON (T1.patient_id = T10.patient_id)
;

-- OVERWRITE BICILLIN VALUES FOR PATIENTS WITHOUT OTHER INDICATORS
UPDATE hiv_rpt.hiv_risk_score_index_data
SET rx_bicillin_1_yr = 0,
rx_bicillin_2yr = 0,
rx_bicillin_e = 0
WHERE patient_id IN (
	SELECT patient_id
	FROM hiv_rpt.hiv_risk_score_index_data
	WHERE
	t_hiv_rna_1_yr = 0 and
	t_hiv_test_2yr = 0 and
	t_hiv_test_e = 0 and
	t_hiv_elisa_e = 0 and
	acute_hiv_test_2yr = 0 and
	acute_hiv_test_e = 0 and
	rx_suboxone_2yr = 0 and
	t_pos_gon_test_2yr = 0 and
	t_chla_test_t_e = 0 and
	syphilis_any_site_state_x_late_e = 0 and
	contact_w_or_expo_venereal_dis_e = 0 and
	hiv_counseling_2yr = 0 and
	syph_last_date is null and
	most_recent_truvada_rx is null and 
	rx_bicillin_e > 0)
;

-- Compute the sum to be used in the risk score calculation
CREATE TABLE hiv_rpt.hiv_risk_score_pat_sum_x_value AS
SELECT patient_id, 
(
(i.sex * c.sex) +
(i.race_black * c.race_black) +
(i.race_caucasian * c.race_caucasian) +
(i.english_lang * c.english_lang) +
(i.has_language_data * c.has_language_data) +
(i.years_of_data * c.years_of_data) +
(i.has_1yr_data * c.has_1yr_data) +
(i.has_2yr_data * c.has_2yr_data) +
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) + 
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) +
(i.t_hiv_test_e * c.t_hiv_test_e) +
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) +
(i.acute_hiv_test_e * c.acute_hiv_test_e) +
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) +
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) +
(i.t_chla_test_t_e * c.t_chla_test_t_e) +
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) +
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) +
(i.rx_bicillin_e * c.rx_bicillin_e) +
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) +
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) +
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) +
(i.hiv_counseling_2yr * c.hiv_counseling_2yr)
) as pat_sum_x_value
FROM hiv_rpt.hiv_risk_score_index_data i,
hiv_rpt.hiv_risk_score_coefficients c;


CREATE TABLE hiv_rpt.hiv_risk_score_risk_scores AS
SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value)))) hiv_risk_score
FROM hiv_rpt.hiv_risk_score_pat_sum_x_value;


SELECT round(hiv_risk_score::numeric, 6) from hiv_rpt.hiv_risk_score_risk_scores INTO RISK_SCORE;





END

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION hiv_rpt.hiv_risk_score_calc_for_month(integer, date)
  OWNER TO esp;