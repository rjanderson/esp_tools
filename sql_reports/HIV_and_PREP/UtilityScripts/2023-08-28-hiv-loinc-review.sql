select count(*), T2.test_name, T2.output_code, max(T1.date) most_recent_lab, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE T2.output_code = '29327-4'
AND T1.date >= now() - INTERVAL '1 year'
GROUP BY T2.test_name, T2.output_code, T1.native_code, T1.native_name, T1.procedure_name
ORDER BY most_recent_lab desc, T1.native_code, T1.native_name, T1.procedure_name;


select count(*), T2.test_name, T2.output_code, max(T1.date) most_recent_lab, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE T2.output_code = '43010-8'
AND T1.date >= now() - INTERVAL '1 year'
GROUP BY T2.test_name, T2.output_code, T1.native_code, T1.native_name, T1.procedure_name
ORDER BY most_recent_lab desc, T1.native_code, T1.native_name, T1.procedure_name;


select count(*), T2.test_name, T2.output_code, max(T1.date) most_recent_lab, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE T2.output_code = '48345-3'
AND T1.date >= now() - INTERVAL '1 year'
GROUP BY T2.test_name, T2.output_code, T1.native_code, T1.native_name, T1.procedure_name
ORDER BY most_recent_lab desc, T1.native_code, T1.native_name, T1.procedure_name;


select count(*), T2.test_name, T2.output_code, max(T1.date) most_recent_lab, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE T2.output_code = '34592-6'
AND T1.date >= now() - INTERVAL '1 year'
GROUP BY T2.test_name, T2.output_code, T1.native_code, T1.native_name, T1.procedure_name
ORDER BY most_recent_lab desc, T1.native_code, T1.native_name, T1.procedure_name;




create table kre_report.cases_to_delete as 
select patient_id, id as case_id
from nodis_case
where condition = 'hiv'
and patient_id in (
select distinct(patient_id)
from emr_labresult
where result_string = 'PRELIMINARY POSITIVE for HIV-1 and/or HIV-2 Antibodies'
where native_code = 'LAB6355--4098')

delete from nodis_case_events where case_id in (211871, 67743, 273393, 348502)
delete from nodis_report_cases where case_id in (211871, 67743, 273393, 348502)
delete from nodis_case where id in (211871, 67743, 273393, 348502)



delete from nodis_case_events where id in (
select id from nodis_case_events
where event_id in (select id from hef_event where object_id in (select id from emr_labresult where native_code = 'LAB6355--4098' and name ilike 'lx:hiv_elisa%')))

delete from hef_event where id in (
select T1.id
from hef_event T1
INNER JOIN emr_labresult T2 ON (T1.object_id = T2.id and T1.patient_id = T2.patient_id)
WHERE T2.native_code = 'LAB6355--4098'
and T1.name ilike 'lx:hiv_elisa%') 


========================================================
drop table if exists kre_report.cases_to_delete;
create table kre_report.cases_to_delete as 
select patient_id, id as case_id, criteria
from nodis_case
where condition = 'hiv'
and patient_id in (
select distinct(patient_id)
from emr_labresult
where (result_float >= .99 or result_string = '> 200.00')
and native_code in ('LAB3365--7437--', 'LAB3365--7436--')	)
and criteria = 'Criteria 1b: Positive HIV Antigen/Antibody and Positive ELISA'
and patient_id not in (select patient_id from hef_event where name = 'lx:hiv_ab_diff:positive')





delete from nodis_case_events where id in (
select id from nodis_case_events
where event_id in (select id from hef_event where object_id in (select id from emr_labresult where native_code in ('LAB3365--7437--', 'LAB3365--7436--')) 
				   and name ilike 'lx:hiv_elisa%'))

delete from hef_event where id in (
select T1.id
from hef_event T1
INNER JOIN emr_labresult T2 ON (T1.object_id = T2.id and T1.patient_id = T2.patient_id)
WHERE T2.native_code in ('LAB3365--7437--', 'LAB3365--7436--')
and T1.name ilike 'lx:hiv_elisa%') 


delete from nodis_case_events where case_id in (select case_id from kre_report.cases_to_delete)
delete from nodis_report_cases where case_id in (select case_id from kre_report.cases_to_delete)
delete from nodis_case where id in (select case_id from kre_report.cases_to_delete)

========
update hef_event
set name = REPLACE(name, 'elisa', 'ab_diff' )
where object_id in (select id from emr_labresult where native_code in ('LAB10075--1000007068--','LAB10075--1000007065--' ) )
and name ilike 'lx:hiv_elisa%';

--HIV1
update conf_labtestmap set output_code = '68961-2', test_name = 'hiv_ab_diff' where native_code = 'LAB10075--1000007068--';

--HIV2
update conf_labtestmap set output_code = '81641-3', test_name = 'hiv_ab_diff' where native_code = 'LAB10075--1000007065--';


