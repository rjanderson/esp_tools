select count(*), dx_code_id, T4.name
from hrp_bicillin T1,
emr_encounter T2,
emr_encounter_dx_codes T3,
static_dx_code T4
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T4.combotypecode = T3.dx_code_id
AND T2.date BETWEEN (T1.date - INTERVAL '3 day') AND (T1.date + INTERVAL '3 day')
AND dx_code_id not in ('icd9:799.9')
group by dx_code_id, T4.name
order by count(*) desc