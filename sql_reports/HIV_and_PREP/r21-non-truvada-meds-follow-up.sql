-- array_remove does not work at Fenway
--select T2.masked_patient_id, week_id, rx_hiv_other, array_remove(med_array, NULL)
select T2.masked_patient_id, week_id, rx_hiv_other, med_array
from (
SELECT T1.master_patient_id as patient_id, T1.week_id, T1.week_start,
max(case when T2.rx_truvada = 1 and T2.date >= T1.week_start and T2.date < week_end then 1 end) as rx_truvada,
sum(case when T2.rx_truvada = 1 and T2.date >= T1.week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_truvada_total_quantity,
max(case when T2.other_hiv_rx_non_truvada = 1 and T2.date >= T1.week_start and T2.date < week_end then 1 end) as rx_hiv_other,
max(case when T2.other_hiv_rx_non_truvada = 1 and T2.date >= T1.week_start and T2.date < week_end then T2.name end) as med_name,
array_agg(distinct(CASE WHEN T2.other_hiv_rx_non_truvada = 1 and T2.date >= T1.week_start and T2.date < week_end  then T2.name end)) as med_array,
sum(case when T2.other_hiv_rx_non_truvada  = 1 and T2.date >= T1.week_start and T2.date < week_end then T2.total_quantity_per_rx end) as rx_hiv_other_total_quantity
FROM hiv_rpt.r21_week_intervals T1
--JOIN hiv_rpt.r21_all_rx_of_interest T2 on (T2.patient_id = T1.master_patient_id)
JOIN hiv_rpt.r21_hiv_meds T2 on (T2.patient_id = T1.master_patient_id)
JOIN hiv_rpt.r21_output_table T3 ON (T2.patient_id = T3.master_patient_id)
WHERE other_hiv_rx_non_truvada = 1 
--AND T1.master_patient_id in (select master_patient_id from hiv_rpt.r21_output_table where rx_hiv_other >0)
AND T3.rx_hiv_other > 0
AND T1.week_start <= '2021-01-01' 
AND T2.hef_name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
GROUP BY T1.master_patient_id, T1.week_id, T1.week_start
ORDER BY T1.master_patient_id,T1.week_id) T1
INNER JOIN hiv_rpt.r21_masked_patients T2 ON (T1.patient_id = T2.master_patient_id)
where med_name is not null;

