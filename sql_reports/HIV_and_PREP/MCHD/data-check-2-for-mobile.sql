* specimen sources for chlam and gon
* bicillin names, quantity, dose, quantity_float
* syphilis active algorithm was in use -- rpr positive will be affected?


select count(*), native_code, native_name, procedure_name, ref_high_string, ref_low_string, abnormal_flag, result_string
FROM emr_labresult
WHERE (native_name ilike '%hsv%'or native_name ilike '%herp%')
group by native_code, native_name, procedure_name, result_string, ref_high_string, ref_low_string, abnormal_flag
ORDER BY native_code, native_name, procedure_name,  result_string;


select count(*), test_name, specimen_source, native_name
from emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE test_name in ('chlamydia', 'gonorrhea')
GROUP by test_name, specimen_source, native_name
ORDER BY test_name, specimen_source;

select count(*), name, dose, directions, quantity_float from emr_prescription 
where name ilike '%PENICILLIN G%'
or name ilike '%Benzylpenicillin%'
or name ilike '%Pfizerpen%'
or name ilike '%BICILLIN%'
group by name
order by name;

select count(*), name, dose, directions, quantity_float from emr_prescription 
where name ilike '%Azithromycin%'
or name ilike '%Zithromax%'
or name ilike '%Zmax%'
group by name
order by name;

select count(*), name, dose, directions, quantity_float from emr_prescription 
where name ilike '%CEFTRIAXONE%'
or name ilike '%Rocephin%'
group by name
order by name;


** check for unmapped result strings
** check ICD code format
** plugins to install
	- syphilis-active
	- hepatitis_c
	- chlamydia
	- gonorrhea
	- hepatitis-b
	- hiv
** run concordance


- hsv lab mapping
- review hiv meds (multiple on one date)
- med investigations
- check min and max data dates  2021 - 2023  Last date 2023-04-29

	




patient score check
1147863