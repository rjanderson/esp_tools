do $$
  declare
  case_id int;
  insrtxt text;
  updtxt text;
  priorrxdate date;
  priordxdate date;
  cursrow record;
  begin
    for case_id in select id from nodis_case nc where condition='asthma' and not exists
      (select null from nodis_caseactivehistory cah where cah.case_id=nc.id)
    loop
      priorrxdate:=null;
      priordxdate:=null;
      for cursrow in execute 'select he.id, he.date, he.name from hef_event he ' || 
         'join nodis_case_events nce on nce.event_id=he.id ' ||
         'where nce.case_id=' || case_id || ' order by he.date'
      loop
        if substr(cursrow.name,1,2)='rx' and priorrxdate is not null and cursrow.date >= priorrxdate
          and cursrow.date < priorrxdate + interval '2 years' then
          insrtxt:='insert into nodis_caseactivehistory (case_id, status, date, change_reason, ' ||
             'latest_event_date, content_type_id, object_id) values (' || case_id || ', ''I'', ''' ||
             to_char(cursrow.date,'yyyy-mm-dd') || '''::date, ''Q'', ''' || to_char(cursrow.date,'yyyy-mm-dd') || 
             '''::date, 45, ' || cursrow.id || ')';
          execute insrtxt;
          updtxt:='update nodis_case set criteria=''Criteria #2: Asthma >=2 prescriptions''' ||
                  ' where id = ' || case_id;
          execute updtxt;
          exit;
        elsif substr(cursrow.name,1,2)='dx' and priordxdate is not null and cursrow.date >= priordxdate
          and cursrow.date < priordxdate + interval '2 years' then
          insrtxt:='insert into nodis_caseactivehistory (case_id, status, date, change_reason, ' ||
             'latest_event_date, content_type_id, object_id) values (' || case_id || ', ''I'', ''' ||
             to_char(cursrow.date,'yyyy-mm-dd') || '''::date, ''Q'', ''' || to_char(cursrow.date,'yyyy-mm-dd') || 
             '''::date, 45, ' || cursrow.id || ')';
          execute insrtxt;
          updtxt:='update nodis_case set criteria=''Criteria #1: Asthma >=2 diagnoses''' ||
                  ' where id = ' || case_id;
          execute updtxt;
          exit;
        end if;
        if substr(cursrow.name,1,2)='rx' then
          priorrxdate:=cursrow.date;
        elsif substr(cursrow.name,1,2)='dx' then
          priordxdate:=cursrow.date;
        end if;
      end loop;
    end loop;
  end;
$$ language plpgsql;

delete from nodis_case_events ce where not exists (select null from nodis_caseactivehistory h where h.case_id=ce.case_id)
and exists (select null from nodis_case c where c.id=ce.case_id and c.condition='asthma');

delete from nodis_case c where not exists (select null from nodis_caseactivehistory h where h.case_id=c.id)
and c.condition='asthma';
