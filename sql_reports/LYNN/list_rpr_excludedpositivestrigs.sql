select distinct t.test_name, e.native_code, v.value
from conf_labtestmap t
join emr_labresult e on e.native_code=t.native_code
join conf_labtestmap_excluded_positive_strings x on x.labtestmap_id=t.id
join conf_resultstring v on x.resultstring_id=v.id
where t.test_name='rpr' order by e.native_code, v.value;
