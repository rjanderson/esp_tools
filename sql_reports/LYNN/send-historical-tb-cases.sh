#!/bin/bash

APP_INI="/srv/esp/prod/etc/application.ini"
DB_NAME="esp"
ESP_SCRIPT="/srv/esp/prod/bin/esp"

DB_PASS='grep database_password /srv/esp/prod/etc/secrets.ini | cut -d "=" -f2'
DB_USER=espuser
DB_HOST=sa133espproddb.cy6winfyzzo.us-west-2.rds.amazonaws.com

NUM_HIST_CASES=100;
STD_BATCH_SIZE=10;

CONDITION="tuberculosis"
CURRENT_STATUS="AR"
QUEUED_STATUS="FP"

NUM_QUEUED_QUERY="select count(*) from nodis_case where condition = '$CONDITION' and status = '$QUEUED_STATUS'"
QUEUE_CASES_QUERY="update nodis_case set status = '$QUEUED_STATUS' where id in (select id from nodis_case where condition = '$CONDITION' and date >= '2018-01-01' and status = '$CURRENT_STATUS' order by date desc limit 100)"


#######################################################################
# Adjust the number of cases to be sent in the DPH batch/message
sed -i "s/^cases_per_message = $STD_BATCH_SIZE$/cases_per_message = $NUM_HIST_CASES/" $APP_INI

num_queued=$(PGPASSWORD=$DB_PASS psql -h $DB_HOST -d $DB_NAME -U $DB_USER -c "$NUM_QUEUED_QUERY" -t | tr -d ' \n')
echo $num_queued

# Keeping it simple. Only queue cases if none are queued
if [ $num_queued -eq 0 ]; then
echo "Queueing cases..."
queue_case=$(PGPASSWORD=$DB_PASS psql -h $DB_HOST -d $DB_NAME -U $DB_USER -c "$QUEUE_CASES_QUERY" -t | tr -d ' \n')
fi

num_queued=$(PGPASSWORD=$DB_PASS psql -h $DB_HOST -d $DB_NAME -U $DB_USER -c "$NUM_QUEUED_QUERY" -t | tr -d ' \n')

# Again...keeping it simple, only send if the number queued matches the batch size
if [ $num_queued -eq $NUM_HIST_CASES ]; then
echo "BATCH SIZE IS CORRECT....TRANSMITTING TO DPH"
( $ESP_SCRIPT case_report --status=FP --mdph --transmit )
fi

# Reset application.ini
sed -i "s/^cases_per_message = $NUM_HIST_CASES$/cases_per_message = $STD_BATCH_SIZE/" $APP_INI
