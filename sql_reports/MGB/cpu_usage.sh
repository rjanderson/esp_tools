#!/bin/bash

CPU_USAGE=$(top -b -n2 -p 1 | fgrep "Cpu(s)" | tail -1 | awk -F'id,' -v prefix="$prefix" '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%s%.1f%%\n", prefix, 100 - v }')

DATE=$(date "+%Y-%m-%d %H:%M:")

CPU_USAGE="$DATE CPU: $CPU_USAGE"

echo $CPU_USAGE >> /srv/esp/logs/cpu_usage.out

ps -eo pcpu,pid,user,args | sort -k 1 -r | head -5 | while read ps_line
do
   echo "$ps_line" >> /srv/esp/logs/cpu_usage.out
done

echo "" >> ./cpu_usage.out


#*/1 * * * * /srv/esp/scripts/cpu_usage.sh
