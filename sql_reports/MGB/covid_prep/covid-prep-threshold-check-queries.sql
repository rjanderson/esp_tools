-- covid19_igg
select count(*), native_code, ref_high_string, ref_low_string
from emr_labresult
where (ref_high_string is not null or ref_low_string is not null)
and native_code in (
'84999--5290007405',
'84999--5290007406',
'84999--5290007412',
'84999--5290007423'
)
group by native_code, ref_high_string, ref_low_string
order by native_code;

--covid19_igm
select count(*), native_code, ref_high_string, ref_low_string
from emr_labresult
where (ref_high_string is not null or ref_low_string is not null)
and native_code in (
'84999--5290007404',
'84999--5290007411',
'84999--5290007424'
)
group by native_code, ref_high_string, ref_low_string
order by native_code;

-- m_pneumoniae_igm
select count(*), native_code, ref_high_string, ref_low_string
from emr_labresult
where (ref_high_string is not null or ref_low_string is not null)
and native_code in (
'84999--5200009227',
'84999--5220000391',
'84999--7000005749'
)
group by native_code, ref_high_string, ref_low_string
order by native_code;

-- adenovirus
select count(*), native_code, ref_high_string, ref_low_string
from emr_labresult
where (ref_high_string is not null or ref_low_string is not null)
and native_code in (
'84999--5200000895',
'84999--5200002632',
'84999--5200014959',
'84999--5290003782'
)
group by native_code, ref_high_string, ref_low_string
order by native_code;



