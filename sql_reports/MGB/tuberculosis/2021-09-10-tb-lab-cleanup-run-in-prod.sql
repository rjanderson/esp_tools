delete from conf_labtestmap
where native_code in (
'84999--5290006039',
'84999--5290006040',
'84999--70000012069',
'84999--70000024691',
'84999--7000013577',
'84999--7000013578',
'84999--7000014543',
'84999--7000032929',
'84999--7000032930',
'84999--7000032931',
'84999--7000032932');


insert into conf_ignoredcode VALUES(default, '84999--5290006039' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--5290006040' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--70000012069' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--70000024691' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000013577' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000013578' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000032929' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000032930' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000032931' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '84999--7000032932' ) ON CONFLICT DO NOTHING;


insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--70000050447', 'tb_afb', 'exact', 'both', false, '11545-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--5290007127', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--5290007128', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--7000013439', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--5290007127', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--70000020782', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;

insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '84999--7000012462', 'tb_afb', 'exact', 'both', false, '11475-1', null, null, null ) ON CONFLICT DO NOTHING;


update conf_labtestmap 
set output_code = '11475-1'
where native_code in ('84999--5290003630', '84999--5290007127', '84999--5290007128', '84999--70000012427', '84999--70000020782', '84999--7000012462', '84999--7000013439');

update conf_labtestmap 
set output_code = '11545-1'
where native_code in ('84999--5030001956', '84999--70000050447');



-- Run these on dev and prod!

update conf_labtestmap 
set output_code = '11475-1'
where native_code in ('84999--5290007127', '84999--70000020782', '84999--7000012462');


-- run on both prod and dev

update conf_labtestmap 
set output_code = '45323-3'
where test_name = 'tb_igra';


update conf_labtestmap 
set output_code = 'MDPH-196'
where native_code = '84999--52000161633';