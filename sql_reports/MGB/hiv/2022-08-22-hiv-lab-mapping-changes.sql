update conf_labtestmap 
set snomed_ind = null 
where test_name = 'hiv_ag_ab'
or output_code in ('30361-0');

update conf_labtestmap 
set snomed_ind = null, snomed_neg = null 
where output_code in ('43010-8', '48345-3', '29327-4');

update conf_labtestmap
set output_code = '25842-6'
where native_code in ('84999--7000006206', '84999--5200006861');

update conf_labtestmap
set output_code = '30245-5'
where native_code in ('84999--7000006134', '84999--5200006851', '84999--5200006869');

update conf_labtestmap
set output_code = '69354-9'
where native_code in ('84999--5200006862');


--unmap #1

delete from hef_event
where object_id in (select id from emr_labresult where native_code = '84999--4001235')
and name ilike 'lx:hiv_elisa%';

delete from conf_labtestmap where native_code = '84999--4001235';

insert into conf_ignoredcode VALUES (default, '84999--4001235');


-- unmap #2

delete from hef_event
where object_id in (select id from emr_labresult where native_code = '84999--7000037688')
and name ilike 'lx:hiv_elisa%';

delete from conf_labtestmap where native_code = '84999--7000037688';

insert into conf_ignoredcode VALUES (default, '84999--7000037688');


delete from hef_event
where object_id in (select id from emr_labresult where native_code = '84999--7000037689')
and name ilike 'lx:hiv_elisa%';

delete from conf_labtestmap where native_code = '84999--7000037689';

insert into conf_ignoredcode VALUES (default, '84999--7000037689');







