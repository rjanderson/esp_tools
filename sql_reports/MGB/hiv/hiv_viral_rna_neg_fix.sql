

HIV-1 RNA detected, <20 cp/mL

HIV-1 RNA detected, <30

TARGET NOT DETECTED

<20



'Final Report',



84999--5200006887
84999--5200006892
84999--52000158506
84999--5290001534
84999--70000016968
84999--70000026758

-- Find the strings

drop table if exists kre_report.temp_hiv_viral_fix_neg_strings;
create table kre_report.temp_hiv_viral_fix_neg_strings as
select T1.*, T2.value, T3.test_name
from conf_labtestmap_extra_negative_strings T1
INNER JOIN conf_resultstring T2 ON (T1.resultstring_id = T2.id)
INNER JOIN conf_labtestmap T3 ON (T1.labtestmap_id = T3.id)
WHERE test_name = 'hiv_rna_viral'
and value ilike '%<%%';

delete from conf_labtestmap_extra_negative_strings
where id in (select id from kre_report.temp_hiv_viral_fix_neg_strings);


drop table if exists kre_report.temp_hiv_viral_fix_labs;
create table kre_report.temp_hiv_viral_fix_labs as 
select id as lab_id, result_string, native_code, patient_id
from emr_labresult
where result_string ilike '%<%'
and native_code in (select native_code from conf_labtestmap where test_name = 'hiv_rna_viral');

drop table if exists kre_report.temp_hiv_viral_fix_hef;
create table kre_report.temp_hiv_viral_fix_hef as
select id as hef_id, name
from hef_event 
where object_id in (select lab_id from kre_report.temp_hiv_viral_fix_labs)
and name ilike 'lx:%';

drop table if exists kre_report.temp_hiv_viral_fix_cases;
create table kre_report.temp_hiv_viral_fix_cases as
select id as case_id
from nodis_case
where patient_id in (select patient_id from kre_report.temp_hiv_viral_fix_labs)
and condition = 'hiv';

-- First you need to delete all of the nodis_case_events for the case
delete from nodis_case_events 
where case_id in (select case_id from kre_report.temp_hiv_viral_fix_cases);

-- Next delete the nodis_report_cases
delete from nodis_report_cases where case_id in (select case_id from kre_report.temp_hiv_viral_fix_cases);

-- Next delete from nodis_reported
delete from nodis_reported where case_reported_id in 
	(select id from nodis_casereportreported  where case_report_id in 
		(select id from nodis_casereport T1, kre_report.temp_hiv_viral_fix_cases T2 
	 	where T1.case_id = T2.case_id));

-- Next delete from nodis_casereportreported
delete from nodis_casereportreported 
where case_report_id in 
	(select id from nodis_casereport T1, kre_report.temp_hiv_viral_fix_cases T2 
	 where T1.case_id = T2.case_id);

-- Next delete from nodis_report
delete from nodis_casereport 
where case_id in (select case_id from kre_report.temp_hiv_viral_fix_cases);


-- Delete from nodis_caseactivehistory
delete from nodis_caseactivehistory
where case_id in (select case_id from kre_report.temp_hiv_viral_fix_cases);


-- Next delete the cases
delete from nodis_case 
where id in (select case_id from kre_report.temp_hiv_viral_fix_cases);

-- Now delete the HEF events
delete from hef_event where id in (select hef_id from kre_report.temp_hiv_viral_fix_hef);

drop table if exists kre_report.temp_hiv_viral_fix_neg_strings;
drop table if exists kre_report.temp_hiv_viral_fix_labs;
drop table if exists kre_report.temp_hiv_viral_fix_hef;
drop table if exists kre_report.temp_hiv_viral_fix_cases;


-- Make sure result_strings have been removed
-- Rerun hef for hiv_rna_viral
-- Rerun nodis for hiv
-- Maybe Change initial status for HIV
-- Send cases
-- Return initial status to Q









