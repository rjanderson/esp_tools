CURRENT DATA DIRECTORY:

/var/lib/pgsql/9.6/data/

Tablespaces: 
/data/postgres/tablespaces/esp 
/home/postgres/mdphnet 


NEW DATA DIRECTORY:

/var/lib/pgsql/13/data/


-----------------------------------------------------------------------------------------------
-- Postgres database Upgrade Steps for MLCHC - using pg_upgrade                             -- 
-----------------------------------------------------------------------------------------------


1 - Add the Postgresql repository (and keys) to the system
RHEL:
  sudo yum install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm 
  sudo yum repolist    
    --accept keys for importing - this will also show you which repos and postgres versions are available

NOTES: 


2 - Refresh Packages 	
RHEL:
    sudo yum update 
	

NOTES: 
	The PostgreSQL 9.6 7Server - x86_64 was failing so for now, ran the following to temporaily disable it: sudo yum --disablerepo=pgdg96 update
	Once upgrade complete, run this to permanently remove: sudo yum-config-manager --disable pgdg96

	Got the following error: Package: postgresql11-devel-11.19-1PGDG.rhel7.x86_64 (pgdg11) Requires: llvm-toolset-7-clang >= 4.0.1
	Did the following to resolve:

		sudo yum -y install http://mirror.centos.org/centos/7/extras/x86_64/Packages/centos-release-scl-rh-2-3.el7.centos.noarch.rpm
		sudo yum -y install http://mirror.centos.org/centos/7/extras/x86_64/Packages/centos-release-scl-2-3.el7.centos.noarch.rpm
		sudo yum --disablerepo=pgdg96 -y install http://mirror.centos.org/centos/7/sclo/x86_64/rh/Packages/l/llvm-toolset-7-clang-4.0.1-1.el7.x86_64.rpm
		sudo yum install --disablerepo=pgdg96 -y llvm-toolset-7-clang
		
	Now try update again:  sudo yum --disablerepo=pgdg96 update	

NOTES:

3 - Verify Postgresql 13 pkgs are available
RHEL:
  sudo yum list available | grep -i ^postgresql13 
  sudo yum --disablerepo=pgdg96 list available | grep -i ^postgresql13
  -- to see what is installed:   
  sudo yum list installed | grep -i ^postgres
  sudo yum --disablerepo=pgdg96 list installed | grep -i ^postgres

NOTES:

4 - Install Postgresql 13 client pkg 
RHEL:
  sudo yum install postgresql13
  sudo yum --disablerepo=pgdg96 install postgresql13
  -- Verify 
  sudo yum --disablerepo=pgdg96 list installed | grep -i ^postgres

NOTES:

5 - Create a dump of existing esp Database using the old and clients     (Older version dump for emergency fallback scenario)
RHEL:
 /usr/pgsql-9.6/bin/pg_dump -N 'gen_pop_tools' -F c esp30 > /srv/esp30/backup/esp30.9.6.dump 

NOTES:

6. Install the Postgresql 13 Server
RHEL:
  sudo yum install postgresql13-server
  sudo yum --disablerepo=pgdg96 install postgresql13-server
     -- Verify 
     sudo yum --disablerepo=pgdg96 list installed | grep -i ^postgres

NOTES:

7. Initialize the Database																
## if needed Create the new data directory and update the data directory 
location in the postgresql.conf file and the initdb command line
RHEL:
 sudo su postgres
 /usr/pgsql-13/bin/initdb -D /var/lib/pgsql/13/data/
  

8 - SET PORT and CONFIG settings for PG13
. CONFIG FILE

sudo su postgres

 cd /var/lib/pgsql/13/data/
 cp postgresql.conf postgresql.conf.ORIG
 cp pg_hba.conf pg_hba.conf.ORIG
 cp pg_ident.conf pg_ident.conf.ORIG

 vi  /var/lib/pgsql/13/data/postgresql.conf
 
 listen_addresses = '*'
 port = 5433
 shared_buffers = 4GB
 work_mem = 225MB
 maintenance_work_mem = 1920MB
 effective_io_concurrency = 2
 max_worker_processes = 4 
 max_wal_size = 16GB
 min_wal_size = 4GB
 checkpoint_completion_target = 0.9
 effective_cache_size = 11GB
 default_statistics_target = 500
 log_min_duration_statement = 600000
 
 
 vi  /var/lib/pgsql/13/data/pg_hba.conf
   - COPY OVER CHANGES FROM EXISTING FILE

 vi /var/lib/pgsql/13/data/pg_ident.conf
  - COPY OVER CHANGES FROM EXISTING FILE

  

 
 /usr/pgsql-13/bin/pg_ctl -D /var/lib/pgsql/13/data/ -l logfile start





##########################
9.6 
datadir = /var/lib/pgsql/9.6/data
configfile = /var/lib/pgsql/9.6/data/postgresql.conf

13 
datadir = /var/lib/pgsql/13/data
configdir = /var/lib/pgsql/13/data/postgresql.conf

##########################
5 - CONFIRM PG VERSION 13 CAN START
   sudo su postgres 
   /usr/pgsql-13/bin/pg_ctl -D /var/lib/pgsql/13/data/ -l logfile start

 
6 - VERIFY PSQL
  sudo su postgres 
  psql -p 5433


7 - Shutdown PG-9.6 and 13 
 
 sudo -u postgres /usr/pgsql-13/bin/pg_ctl -D /var/lib/pgsql/13/data/ stop
 sudo systemctl stop postgresql **OR** sudo service postgresql-9.6 stop


8 - ADD Links for pg_upgrade KRE-- NOT SURE IF THIS IS NEEDED?!?!! DID NOT DO YET

sudo su postgres
--9.6
 ln -s /esp2/9.6/data/ /etc/postgresql/9.6/main/conf.d/          
 ln -s /esp2/9.6/data/postgresql.conf /etc/postgresql/9.6/main/postgresql.conf  
 
 ln -s /var/lib/pgsql/9.6/data/ /etc/postgresql/9.6/main/conf.d/
 ln -s /var/lib/pgsql/9.6/data/postgresql.conf /etc/postgresql/9.6/main/postgresql.conf 
 
 
--13  
 ln -s /esp2/postgresql/13/data/postgresql.conf /etc/postgresql/13/main/postgresql.conf   
 ln -s /esp2/postgresql/13/data/ /etc/postgresql/13/main/conf.d/           

 
9 - run pg_upgrade  with --check and --link options

sudo su postgres
cd /var/lib/pgsql


/usr/pgsql-13/bin/pg_upgrade --check \
--old-datadir=/var/lib/pgsql/9.6/data/ \
--new-datadir=/var/lib/pgsql/13/data/ \
--old-bindir=/usr/pgsql-9.6/bin \
--new-bindir=/usr/pgsql-13/bin \
--old-port 5432 --new-port 5433 --link





10 - IF GOOD check then RUN LIVE 


/usr/pgsql-13/bin/pg_upgrade \
--old-datadir=/var/lib/pgsql/9.6/data/ \
--new-datadir=/var/lib/pgsql/13/data/ \
--old-bindir=/usr/pgsql-9.6/bin \
--new-bindir=/usr/pgsql-13/bin \
--old-port 5432 --new-port 5433 --link



11 - Start PG13 and login to the new db and verify the esp database.,etc
  systemctl enable postgresql-13  -- SKIPPED FOR NOW
  
  
  sudo su 
  sudo systemctl start postgresql-13 
  
  sudo su postgres
  psql -p 5433 
  \l+      
  \dn
  \du
   verify PGAdmin
   verify popmednet client
   gui, etc...

   
12 - Stop postgres
  sudo su
  systemctl stop postgresql-13

NOTES:

13 - Disable old version from system startup and move data directory to .OLD  - KRE TO LOOK FOR THIS FILE!!
sudo vi /etc/postgresql/9.6/main/start.conf
  -- Change "auto" to "disabled"


14 - Update the port on the new database to 5432
 sudo vi /var/lib/pgsql/13/data/postgresql.conf
 
NOTES:

15 - Update data location in startup script
  sudo vi /lib/systemd/system/postgresql-13.service
    Environment=PGDATA=/esp2/postgresql/13/data/
	
NOT DONE -- DID NOT NEED TO DO
NOTES:	


16 - Start Postgres (only version 13 should be started)
  sudo systemctl start postgresql-13

run checks 
 psql
 esp command line 
 esp gui
 
 sudo systemctl disable postgresql-9.6

TODO:
 pgadmin check
 popmednet client check
 update backup scripts

-----------------------------------------------------------------------------
Optional 
--reboot the system and make sure that the Database starts up automatically
  sudo reboot
  Login via ssh 
  run psql as esp
  verify UI/Apache via a browser
	
--Follow up and cleanup items:
--remove postgres-9.x and files from the system
apt-get --purge remove postgresql-9.5

-------------------------------------------------------------------------------
