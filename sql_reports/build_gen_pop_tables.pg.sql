﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                         General Population Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2013 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;
---
-- Build the mapping table.  If your local site has different values, please add them here
---
drop table if exists gpr_racemap;
create table gpr_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the GPR set here.
insert into gpr_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK');
---
-- Build the patient_encounter list
---
select 'Starting to build gpr_enc_pat at: ',now();
drop table if exists gpr_enc_pat;
create table gpr_enc_pat as
select patient_id, max(date) as date, min(date) as first_enc
from emr_encounter
group by patient_id;
alter table gpr_enc_pat add primary key (patient_id);
analyze gpr_enc_pat;

--
-- now build patient table
--
select 'Starting to build gpr_pat at: ',now();
drop table if exists gpr_pat;
create table gpr_pat as
SELECT 
  pat.id AS patient_id, pat.mrn, date_part('year', age(pat.date_of_birth::date)) as age, upper(substr(pat.gender,1,1)) gender, 
  case 
     when (select target_value from gpr_racemap t00 where t00.source_field='ethnicity' and t00.source_value=pat.ethnicity) is not null
	   then (select target_value from gpr_racemap t00 where t00.source_field='ethnicity' and t00.source_value=pat.ethnicity)
	 else (select target_value from gpr_racemap t00 where t00.source_field='race' and t00.source_value=pat.race)
  end as race, -- pat.race as old_race, pat.ethnicity, --uncomment this for QA
  pat.date_of_death, pat.date_of_birth,
  case
    when substring(pat.zip,6,1)='-' then substring(pat.zip,1,5)
    else pat.zip
  end as zip, lastenc.date as last_enc_date
FROM emr_patient pat
join gpr_enc_pat lastenc on lastenc.patient_id=pat.id;
alter table gpr_pat add primary key (patient_id);
analyze gpr_pat;
--
-- Max blood pressure between two and three years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gpr_enc_bp at: ',now();
drop table if exists gpr_enc_bp;
create table gpr_enc_bp as
select patient_id, date, bp_systolic, bp_diastolic, (2*bp_diastolic + bp_systolic)/3 as mean_arterial, height
from emr_encounter
where bp_systolic is not null and bp_diastolic is not null;
create index gpr_enc_bp_idx
on gpr_enc_bp (patient_id, date);
analyze gpr_enc_bp;

select 'Starting to build gpr_bp3 at: ',now();
drop table if exists gpr_bp3;
create table gpr_bp3 as 
      select * from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_systolic::numeric, 
					 'SYS'::varchar, 
					 'BPPCT'::varchar) as syspct
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_diastolic::numeric, 
					 'DIA'::varchar, 
					 'BPPCT'::varchar) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gpr_enc_bp
              WHERE date between ( now() - interval '3 years' ) and ( now() - interval '2 years' )
              group by patient_id) as t1,
             emr_patient as pat
	WHERE t0.date between ( now() - interval '3 years' ) and ( now() - interval '2 years' )
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
--
-- Max blood pressure between one and two years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gpr_bp2 at: ',now();
drop table if exists gpr_bp2;
create table gpr_bp2 as 
     select * from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_systolic::numeric, 
					 'SYS'::varchar, 
					 'BPPCT'::varchar) as syspct
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_diastolic::numeric, 
					 'DIA'::varchar, 
					 'BPPCT'::varchar) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gpr_enc_bp
              WHERE date between ( now() - interval '2 years' ) and ( now() - interval '1 years' )
              group by patient_id) as t1,
             emr_patient as pat
	 WHERE t0.date between ( now() - interval '2 years' ) and ( now() - interval '1 years' )
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
--
-- Max blood pressure between now and one years
--  using most recent max mean aterial pressure for the period
--
select 'Starting to build gpr_bp1 at: ',now();
drop table if exists gpr_bp1;
create table gpr_bp1 as
     select * from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
        , t1.max_mean_arterial as map
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_systolic::numeric, 
					 'SYS'::varchar, 
					 'BPPCT'::varchar) as syspct
        , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_diastolic::numeric, 
					 'DIA'::varchar, 
					 'BPPCT'::varchar) as diapct
        , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
	FROM gpr_enc_bp t0,
             (select patient_id, max(mean_arterial) as max_mean_arterial
              from gpr_enc_bp
              WHERE date between ( now() - interval '1 years' ) and now()
              group by patient_id) as t1,
             emr_patient as pat
	WHERE t0.date between ( now() - interval '1 years' ) and now() 
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
--
-- most recent blood pressure 
--
select 'Starting to build gpr_recbp at: ',now();
drop table if exists gpr_recbp;
create table gpr_recbp as
	SELECT 
	  t0.patient_id
	, max(t0.bp_systolic) as bp_systolic
	, max(t0.bp_diastolic) as bp_diastolic
        , max(gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_systolic::numeric, 
					 'SYS'::varchar, 
					 'BPPCT'::varchar)) as syspct
        , max(gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_diastolic::numeric, 
					 'DIA'::varchar, 
					 'BPPCT'::varchar)) as diapct
	, t0.date
        , max(t0.mean_arterial) as map
	FROM gpr_enc_bp as t0,
	     (select patient_id, max(date) as date from gpr_enc_bp 
	           group by patient_id) as t1,
             emr_patient as pat
	WHERE   t0.patient_id=t1.patient_id and
	   t0.date = t1.date and t0.date >= now() - interval '2 years'
           and t0.patient_id=pat.id
	GROUP BY t0.patient_id, t0.date;
--
-- BMI PCT 
--
select 'Starting to build gpr_bmi_pct at: ',now();
drop table if exists gpr_bmi_pct;
create table gpr_bmi_pct as
         select t0.*
        , gen_pop_tools.cdc_bmi((extract(year from age(t0.date, pat.date_of_birth::date))*12
                               + extract(month from age(t0.date, pat.date_of_birth::date)))::numeric,
                (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar,
                null::numeric,
                null::numeric,
                t0.bmi::numeric,
                'BMIPCT'::varchar ) as bmipct
      from (
        SELECT
          t0.patient_id,
          t0.date,
          max(bmi) bmi
        FROM emr_encounter t0,
             (select patient_id, max(date) as date
              from emr_encounter
              where bmi is not null
              group by patient_id) t1
        WHERE t0.date >= ( now() - interval '2 years' )
          and bmi is not null
          and t0.date=t1.date
          and t0.patient_id=t1.patient_id
        GROUP BY t0.patient_id, t0.date

		) t0,
  emr_patient as pat
  where t0.patient_id=pat.id;
  
 --
-- BMI - Most recent BMI condition for patients age >=20 on detection date 
--
SELECT 'Starting to build gpr_bmi at: ',now();
DROP TABLE IF EXISTS gpr_bmi;
CREATE TABLE gpr_bmi AS
  SELECT DISTINCT ON (T2.patient_id) T2.patient_id, T2.max_date date, 
  case when T1.condition = 'BMI <25' then 1
       WHEN T1.condition = 'BMI >=25 and <30' THEN 2
       WHEN T1.condition = 'BMI >= 30' THEN 3
       WHEN T1.condition = 'No Measured BMI' THEN 4
  END condition
            FROM esp_condition AS T1,
				(SELECT patid, id patient_id, max(date) max_date_sas, max(date + '1960-01-01'::date) max_date 
					FROM esp_condition, emr_patient 
					WHERE condition ilike '%bmi%' AND natural_key = patid 
					GROUP BY patid, patient_id) AS T2
            WHERE T1.patid = T2.patid
            AND T1.date = T2.max_date_sas
            AND condition ilike '%bmi%'
            AND T1.age_at_detect_year >= 20
            order by patient_id;  
 
--
-- lab subset
--
select 'Starting to build gpr_sublab at: ',now();
drop table if exists gpr_sublab;
create table gpr_sublab as
select patient_id, date, result_float, t1.test_name
from emr_labresult t0
  inner join conf_labtestmap t1
    on t1.native_code=t0.native_code 
where t1.test_name in ('a1c','cholesterol-ldl','triglycerides','gonorrhea','chlamydia') 
  and (t0.result_float is not null or t1.test_name in ('gonorrhea','chlamydia'));
create index gpr_sublab_patid_idx on gpr_sublab (patient_id);
create index gpr_sublab_testname_idx on gpr_sublab (test_name);
create index gpr_sublab_date_idx on gpr_sublab (date);
analyze gpr_sublab;
--
-- Recent A1C lab result
--
select 'Starting to build gpr_a1c at: ',now();
drop table if exists gpr_a1c;
create table gpr_a1c as
	SELECT 
	  l0.patient_id
	, l0.date  
	, MAX(l0.result_float) AS recent_a1c
	FROM gpr_sublab l0,
	(
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gpr_sublab l1
		where l1.test_name = 'a1c'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
	WHERE l0.test_name = 'a1c'
	  and u0.patient_id = l0.patient_id
		AND u0.test_name = l0.test_name
		AND u0.date=l0.date
	  AND l0.date >= ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id, l0.date;
--
-- Max A1C lab result last year
--
select 'Starting to build gpr_a1c1 at: ',now();
drop table if exists gpr_a1c1;
create table gpr_a1c1 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c1
	FROM gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( now() - interval '1 years' ) and now()
	GROUP BY 
	  l0.patient_id;
--
-- Max A1C lab result between 1 and 2 years ago
--
select 'Starting to build gpr_a1c2 at: ',now();
drop table if exists gpr_a1c2;
create table gpr_a1c2 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c2
	FROM gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( now() - interval '2 years' ) and ( now() - interval '1 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Max A1C lab result between 2 and 3 years ago
--
select 'Starting to build gpr_a1c3 at: ',now();
drop table if exists gpr_a1c3;
create table gpr_a1c3 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_a1c3
	FROM gpr_sublab l0
	WHERE l0.test_name = 'a1c'
	  AND l0.date between ( now() - interval '3 years' ) and ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Recent cholesterol LDL lab result
--
select 'Starting to build gpr_ldl at: ',now();
drop table if exists gpr_ldl;
create table gpr_ldl as
	SELECT 
	  l0.patient_id
	, l0.date
	, MAX(l0.result_float) AS recent_ldl
	FROM gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gpr_sublab l1
        where l1.test_name = 'cholesterol-ldl'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'cholesterol-ldl'
	AND l0.date >= ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id, l0.date;
--
-- Max ldl lab result last year
--
select 'Starting to build gpr_ldl1 at: ',now();
drop table if exists gpr_ldl1;
create table gpr_ldl1 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl1
	FROM gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( now() - interval '1 years' ) and now()
	GROUP BY 
	  l0.patient_id;
--
-- Max ldl lab result between 1 and 2 years ago
--
select 'Starting to build gpr_ldl2 at: ',now();
drop table if exists gpr_ldl2;
create table gpr_ldl2 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl2
	FROM gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( now() - interval '2 years' ) and ( now() - interval '1 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Max ldl lab result between 2 and 3 years ago
--
select 'Starting to build gpr_ldl3 at: ',now();
drop table if exists gpr_ldl3;
create table gpr_ldl3 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_ldl3
	FROM gpr_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	  AND l0.date between ( now() - interval '3 years' ) and ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Recent Triglycerides lab result
--
select 'Starting to build gpr_trig at: ',now();
drop table if exists gpr_trig;
create table gpr_trig as
	SELECT 
	  l0.patient_id
	, l0.date
	, MAX(l0.result_float) AS recent_tgl 
	FROM gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, l1.test_name
		, MAX(l1.date) AS date
		FROM gpr_sublab l1
		where l1.test_name = 'triglycerides'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.test_name = l0.test_name
		AND u0.date = l0.date
	WHERE l0.test_name = 'triglycerides'
	AND l0.date >= ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id, l0.date;
--
-- Max trig lab result last year
--
select 'Starting to build gpr_trig1 at: ',now();
drop table if exists gpr_trig1;
create table gpr_trig1 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig1
	FROM gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( now() - interval '1 years' ) and now()
	GROUP BY 
	  l0.patient_id;
--
-- Max trig lab result between 1 and 2 years ago
--
select 'Starting to build gpr_trig2 at: ',now();
drop table if exists gpr_trig2;
create table gpr_trig2 as 
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig2
	FROM gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( now() - interval '2 years' ) and ( now() - interval '1 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Max trig lab result between 2 and 3 years ago
--
select 'Starting to build gpr_trig3 at: ',now();
drop table if exists gpr_trig3;
create table gpr_trig3 as
	SELECT 
	  l0.patient_id
	, MAX(l0.result_float) AS max_trig3
	FROM gpr_sublab l0
	WHERE l0.test_name = 'triglycerides'
	  AND l0.date between ( now() - interval '3 years' ) and ( now() - interval '2 years' )
	GROUP BY 
	  l0.patient_id;
--
-- Prediabetes
--
select 'Starting to build gpr_predm at: ',now();
drop table if exists gpr_predm;
create table gpr_predm as
	SELECT 
	  DISTINCT c0.patient_id
	, 1 AS prediabetes
	FROM nodis_case c0
	INNER JOIN nodis_case c1
		ON c1.patient_id = c0.patient_id
		AND c1.condition NOT LIKE 'diabetes:type-%'
	WHERE c0.condition = 'diabetes:prediabetes'
	     and not exists (select null from nodis_case c2 
	                     where c2.date <= to_date('2008-01-01','yyyy-mm-dd')
	                         and c2.patient_id=c0.patient_id
	                         and c2.condition = 'diabetes:prediabetes')
	     and not exists (select null from nodis_case c3
	                     where c3.patient_id=c0.patient_id
	                         and c3.condition in ('diabetes:type-1','diabetes:type-2'));
--
-- Type 1 Diabetes
--
select 'Starting to build gpr_type1 at: ',now();
drop table if exists gpr_type1;
create table gpr_type1 as
	SELECT 
	DISTINCT patient_id
	, 1 AS type_1_diabetes
	FROM nodis_case
	WHERE condition = 'diabetes:type-1';
--
-- Type 2 Diabetes
--
select 'Starting to build gpr_type2 at: ',now();
drop table if exists gpr_type2;
create table gpr_type2 as
	SELECT 
	DISTINCT patient_id
	, 1 AS type_2_diabetes
	FROM nodis_case
	WHERE condition = 'diabetes:type-2';
--
-- Current Gestational diabetes
--
select 'Starting to build gpr_gdm at: ',now();
drop table if exists gpr_gdm;
create table gpr_gdm as
	SELECT distinct
	c0.patient_id
	, 1 AS current_gdm
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational'
            AND ts0.start_date <= now()
            AND ( ts0.end_date >= now() OR ts0.end_date IS NULL);
--
-- Recent Gestational diabetes 1 year
--
select 'Starting to build gpr_recgdm1 at: ',now();
drop table if exists gpr_recgdm1;
create table gpr_recgdm1 as
	SELECT distinct
	c0.patient_id
	, 1 AS recent_gdm
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational' and
	             ((start_date between (now() - interval '1 years')  and now() and (end_date >= start_date or end_date is null))
	           or end_date between (now() - interval '1 years') and now());
--
-- Recent Gestational diabetes 2 year
--
select 'Starting to build gpr_recgdm2 at: ',now();
drop table if exists gpr_recgdm2;
create table gpr_recgdm2 as
	SELECT distinct
	c0.patient_id
	, 1 AS recent_gdm
	FROM nodis_case AS c0
        INNER JOIN nodis_case_timespans AS nct0
            ON nct0.case_id = c0.id
        INNER JOIN hef_timespan AS ts0
            ON nct0.timespan_id = ts0.id
	WHERE c0.condition = 'diabetes:gestational' and
	             ((start_date between (now() - interval '2 years') and now() and (end_date >= start_date or end_date is null))
	           or end_date between (now() - interval '2 years') and now());
--
-- Recent pregnancy 2 year
--
select 'Starting to build gpr_recpreg2 at: ',now();
drop table if exists gpr_recpreg2;
create table gpr_recpreg2 as
	SELECT
	  patient_id
	, 1 AS recent_pregnancy -- if there is more than one pregnancy in the period, take the last one
	FROM hef_timespan
	WHERE name = 'pregnancy' and 
	             ((start_date between (now() - interval '2 years') and now() and (end_date >= start_date or end_date is null))
	           or end_date between (now() - interval '2 years') and now())
	GROUP BY patient_id;
--
-- Recent pregnancy 1 year
--
select 'Starting to build gpr_recpreg1 at: ',now();
drop table if exists gpr_recpreg1;
create table gpr_recpreg1 as
	SELECT
	  patient_id
	, 1 AS recent_pregnancy
	FROM hef_timespan
	WHERE name = 'pregnancy' and 
	             ((start_date between (now() - interval '1 years') and now() and (end_date >= start_date or end_date is null))
	           or end_date between (now() - interval '1 years') and now())
	GROUP BY patient_id;
--
-- Current pregnancy
--
select 'Starting to build gpr_curpreg at: ',now();
drop table if exists gpr_curpreg;
create table gpr_curpreg as
	SELECT
	  DISTINCT ts.patient_id
	, 1 AS currently_pregnant
	FROM hef_timespan ts
	WHERE name = 'pregnancy'
            AND start_date between (now() - interval ' 9 months') and now() 
            AND ( end_date >= now() OR end_date IS NULL);
--
-- Insulin
--    Prescription for insulin within the previous year
--
select 'Starting to build gpr_insulin at: ',now();
drop table if exists gpr_insulin;
create table gpr_insulin as
	SELECT 
	  DISTINCT patient_id
	, 1 AS insulin
	FROM emr_prescription
	WHERE name ILIKE '%insulin%'
            AND date >= ( now() - interval '1 year' );
--
-- Metformin
--     Prescription for metformin within the previous year
--
select 'Starting to build gpr_metformin at: ',now();
drop table if exists gpr_metformin;
create table gpr_metformin as 
	SELECT 
	  DISTINCT patient_id
	, 1 AS metformin
	FROM emr_prescription
	WHERE name ILIKE '%metformin%'
            AND date >= ( now() - interval '1 year' );
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
select 'Starting to build gpr_flu_cur at: ',now();
drop table if exists gpr_flu_cur;
create table gpr_flu_cur as
	SELECT 
	  DISTINCT patient_id
	, 1 AS influenza_vaccine
	FROM emr_immunization
	WHERE (name ILIKE '%influenza%' or name = 'flu')
	AND case
		  when extract(quarter from now())=4 then extract(year from now())+1
		  else extract(year from now())
	    end =
	    case
		  when extract(quarter from date)=4 then extract(year from date)+1
		  else extract(year from date)
	    end;
--
-- Influenza vaccine
--     Prescription for influenza vaccine previous flu season
--
select 'Starting to build gpr_flu_prev at: ',now();
drop table if exists gpr_flu_prev;
create table gpr_flu_prev as
	SELECT 
	  DISTINCT patient_id
	, 1 AS influenza_vaccine
	FROM emr_immunization
	WHERE (name ILIKE '%influenza%' or name = 'flu')
	AND case
		  when extract(quarter from (now()-interval '1 years'))=4 then extract(year from (now()-interval '1 years'))+1
		  else extract(year from (now()-interval '1 years'))
	    end =
	    case
		  when extract(quarter from date)=4 then extract(year from date)+1
		  else extract(year from date)
	    end;
--
-- most recent chlamydia lab result
--
select 'Starting to build gpr_chlamydia at: ',now();
drop table if exists gpr_chlamydia;
create table gpr_chlamydia as
	SELECT 
	  l0.patient_id
	, case 
            when l0.date >= ( now() - interval '1 year' ) then '1'
            when l0.date >= ( now() - interval '2 years' ) then '2'
            when l0.date < (now() - interval '2 years') then '3'
            else '4'     
          end  AS recent_chlamydia 
	FROM gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, MAX(l1.date) AS date
		FROM gpr_sublab l1
		where l1.test_name = 'chlamydia'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'chlamydia'
	GROUP BY 
	  l0.patient_id, l0.date;
--
-- most recent gonorrhea lab result
--
select 'Starting to build gpr_gonorrhea at: ',now();
drop table if exists gpr_gonorrhea;
create table gpr_gonorrhea as
	SELECT 
	  l0.patient_id
	, case 
            when l0.date >= ( now() - interval '1 year' ) then '1'
            when l0.date >= ( now() - interval '2 years' ) then '2'
            when l0.date < (now() - interval '2 years') then '3'
            else '4'     
          end  AS recent_gonorrhea 
	FROM gpr_sublab l0
	INNER JOIN (
		SELECT 
		  l1.patient_id
		, MAX(l1.date) AS date
		FROM gpr_sublab l1
		where l1.test_name = 'gonorrhea'
		GROUP BY 
		  l1.patient_id
		, l1.test_name
	) u0
		ON u0.patient_id = l0.patient_id
		AND u0.date = l0.date
	WHERE l0.test_name = 'gonorrhea'
	GROUP BY 
	  l0.patient_id, l0.date;
--
-- Smoking
--
select 'Starting to build gpr_smoking at: ',now();
drop table if exists gpr_smoking;
create table gpr_smoking as
select 
case when T1.smoking = 1 then 1
WHEN T2.smoked_ever = 1 THEN 2
ELSE T1.smoking END smoking,
T1.patient_id
FROM 
(
-- Passive
SELECT  DISTINCT ON (patient_id) CASE when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 4
-- Never      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 3
-- Former  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 2
-- Current  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 1
-- Not Available                      
else 5 end smoking,
T2.patient_id,
tobacco_use
FROM 
(SELECT patient_id, max(date) max_smoking_date FROM emr_socialhistory WHERE tobacco_use is not null and tobacco_use != '' GROUP BY patient_id) T1,
emr_socialhistory T2
WHERE T1.patient_id = T2.patient_id
AND tobacco_use is not null and tobacco_use != ''
AND T1.max_smoking_date = T2.date) T1
LEFT JOIN 
(select distinct on (patient_id) patient_id, 
	max(CASE when tobacco_use ILIKE ANY(ARRAY['%adults%', '%aunt%', '%both%', '%brother%', '%cousin%', '%dad%', '%everyone%', '%exposure%','%family smokes%', '%father%', '%fob%', '%foc%', '%gm%', '%gf%',  '%grandaughter%','%grandfather%', '%grandma%', '%grandmom%', '%grandmother%', '%grandparent%', '%husband%', '%many smokers%', '%members%', '%mgf%', '% mo %', '%mob%', '%moc%',  '%mgm%','%mggm%', '%mom%', '%mothr%', '%mothe%', '%parent%', '%paretns%', '%passive%', '%sister%', '% uncle %', '%uncle.%'])  
	AND tobacco_use NOT ILIKE ALL(ARRAY['have cut back%']) 
then 0
-- Never      
when (
	tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .','none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None','Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',         'Non smoker.','nonsmoker')
	or tobacco_use ILIKE ANY(ARRAY['%abstain%', 'No%', '%doesn%smoke%', 'negative%', '%never%'])
	)
	AND tobacco_use NOT ILIKE ALL(ARRAY['%quit%', '%plans%', '%smoke outside%', '%not clear%', '%daily%', '%not%', 'yes%', '%cut back%', 'smoked%', '%day%'])
then 0
-- Former  
when (
	tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker', 'Former','prev.','Quit.','quit smoking', 'X', '0 currently', 'Complete cessation', 'No. not at new apartment. .')
	or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}'           
	or tobacco_use ILIKE ANY(ARRAY['%quit%', '%previous%', '%used to%', '%former%', 'jan%', 'june%', '%long time ago', '%not%smoking%', '%patch%', '%smoked%', '%pt quit%', '%quit%chantix%'])
	)           
	AND tobacco_use NOT ILIKE ALL(ARRAY['%less%', '%trying%', '%plans%', '%advised%', '%complications%', 'current%', '%quitting%', '%cig%patch%',  '%needs to%', '%not in house%', '%on/off%',            '%would like to%', '%quit by%', '%pick a date%', '%to quit%', '%to have more money%', 'up to%'])           
then 1
-- Current  
when (
	tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','one', 'Very Ready','Post Partum','ready','contemplative', 'Yes. outside. .','relapse','rare', 'rarely')
	or tobacco_use ~ '^[0123456789]'        
	or tobacco_use ~ '^.[0123456789]'        
	or tobacco_use ILIKE ANY(ARRAY['%<%','%>%','%~%','about%','%addicted%','%advised%', '%a lot%', '%almost%', '%approx%', '%apprx%','%as much%', '%avoid%', '%breath%', '%cancer%', '%casual%', '%cessation%', '%chew%', '%children%',         '%cig%', '%complica%', '%contemplat%','%continue%','%continues%', '%couple%','%current%', '%cut back%','%cut down%', '%daily%', '%day%', '%depends%', '%drinking%', '%enough%', '%expensive%', '%feel%', '%few%', '%half%', '%handout%', '%hardly%', '%harm%', '%health%','%infreq%','%improve%','%interm%', '%less%', '%live%', '%more than%', '%needs to%', '%occ%','%off and on%', '%on weekends%', '%one%', '%only%', '%pack%', '%cig%patch%', '%pipe%', '%pkg%','%positive%', '%quit%', '%rare%', '%ready%', '%recreation%', '%roll%', '%since%', '%smoke outside%',                       '%seldom%', '%smoke%', '%smoking%', '%social%', '%some%', '%start%', '%still%','%stress%', '%to be%','%tobacco%','%trying%', '%up to%', '%under%', '%var%', '%wants to%', '%while%', '%yes%']) 
	)
	AND tobacco_use not ILIKE ALL(ARRAY['~', '%refus%','%unknown%', '%not cigarettes%', '%pt is no%', '%w/o%'])
then 1
-- Not Available                      
else 0 end) smoked_ever
FROM 
emr_socialhistory 
WHERE tobacco_use is not null and tobacco_use != ''
GROUP BY patient_id) T2 ON (T1.patient_id = T2.patient_id);


-- Asthma
--
select 'Starting to build gpr_asthma at: ',now();
drop table if exists gpr_asthma;
create table gpr_asthma as
  select case when count(*) > 0 then 1 else 2 end as asthma,
  patient_id
from nodis_case
where condition='asthma'
group by patient_id;
--
-- Number of encounters last year
--
select 'Starting to build gpr_enc at: ',now();
drop table if exists gpr_enc;
create table gpr_enc as
  select case when count(*) >= 2 then 2
              else count(*) end as nvis,
  patient_id
  from emr_encounter
  where date>=current_date - interval '1 year'
  group by patient_id;        
--
-- Depression
--
select 'Starting to build gpr_depression at: ',now();
drop table if exists gpr_depression;
create table gpr_depression as
        SELECT
          hef.patient_id
        , case
            when hef.date >= ( now() - interval '1 year' ) then '1'
            when hef.date >= ( now() - interval '2 years' ) then '2'
            else '3'
          end  AS depression
from nodis_case nodis
  join nodis_case_events nce on nce.case_id=nodis.id
  join hef_event hef on hef.id=nce.event_id
  JOIN (
        SELECT
          hf.patient_id
         , MAX(hf.date) AS date
        FROM nodis_case nds
          join nodis_case_events nce on nce.case_id=nds.id
          join hef_event hf on hf.id=nce.event_id
                where nds.condition = 'depression'
                GROUP BY
                  hf.patient_id
        ) u0
                ON u0.patient_id = hef.patient_id
                AND u0.date = hef.date
where condition='depression'
        GROUP BY
          hef.patient_id, hef.date;
--
-- Opioid
--
select 'Starting to build gpr_opi_in_progress at: ',now();
DROP TABLE if EXISTS gpr_opi_in_progress;
DROP TABLE if EXISTS gpr_opi;
CREATE TABLE gpr_opi_in_progress AS
    SELECT T2.id as patient_id,
    case
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '1 year' - interval '1 day' ) and condition = 'opioidrx' then '1'
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '2 year' - interval '1 day' )and condition = 'opioidrx' then '2'
        when (max(T1.date) + '1960-01-01'::date) < ( now() - interval '2 year' - interval '1 day' ) and condition = 'opioidrx' then '3'
        else '4'
        end any_opi,
    case
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '1 year' - interval '1 day' ) and condition = 'benzodiarx' then '1'
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '2 year' - interval '1 day' ) and condition = 'benzodiarx' then '2'
        when (max(T1.date) + '1960-01-01'::date) < ( now() - interval '2 year' - interval '1 day' ) and condition = 'benzodiarx' then '3'
        else '4'
        end any_benzo,
    case
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '1 year' - interval '1 day' )and condition = 'benzopiconcurrent' then '1'
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '2 year' - interval '1 day' ) and condition = 'benzopiconcurrent' then '2'
        when (max(T1.date) + '1960-01-01'::date) < ( now() - interval '2 year' - interval '1 day' ) and condition = 'benzopiconcurrent' then '3'
        else '4'
        end  concur_opi_benzo,
    case
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '1 year' - interval '1 day' ) and condition = 'highopioiduse' then '1'
        when (max(T1.date) + '1960-01-01'::date) >= ( now() - interval '2 year' - interval '1 day' ) and condition = 'highopioiduse' then '2'
        when (max(T1.date) + '1960-01-01'::date) < ( now() - interval '2 year' - interval '1 day' ) and condition = 'highopioiduse'  then '3'
        else '4'
        end   high_dose_opi
    FROM public.esp_condition T1
    INNER JOIN public.emr_patient T2 ON ((T1.patid = T2.natural_key))
    GROUP BY T1.condition, T2.id;

select 'Starting to build gpr_opi at: ',now();
CREATE TABLE gpr_opi AS
    SELECT T1.patient_id,
           min(T1.any_opi) any_opi,
           min(T1.any_benzo) any_benzo,
           min(T1.concur_opi_benzo) concur_opi_benzo,
           min(T1.high_dose_opi) high_dose_opi
    FROM  gpr_opi_in_progress T1
    GROUP BY T1.patient_id;

DROP TABLE gpr_opi_in_progress;
--
-- Hypertension
--
select 'Starting to build gpr_hypertension at: ',now();
DROP TABLE IF EXISTS gpr_hypertension;
CREATE TABLE gpr_hypertension AS
  SELECT C1.patient_id,
    min(
	case 
       when C1.isactive or H1.status = 'D' and h1.date >= (now() - interval '1 years') then '1'
       when H1.status = 'D' and h1.date < (now() - interval '1 years') and h1.date >= (now() - interval '2 years') then '2'
       when H1.status = 'D' and h1.date < (now() - interval '2 years') then '3'
       else '4'
    end) as hypertension
--    '4'::varchar as hypertension
FROM nodis_case C1 join nodis_caseactivehistory h1 on c1.id=h1.case_id
WHERE C1.condition='hypertension'
group by c1.patient_id;
