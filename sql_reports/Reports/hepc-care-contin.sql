﻿DROP TABLE IF EXISTS cc_hepc_pos_labs;
DROP TABLE IF EXISTS cc_hepc_index_dates;
DROP TABLE IF EXISTS cc_hepc_esp_hepc;
DROP TABLE IF EXISTS cc_hepc_all_rx;
DROP TABLE IF EXISTS cc_hepc_rx_after_ip;
DROP TABLE IF EXISTS cc_hepc_first_last_rx;
DROP TABLE IF EXISTS cc_hepc_rna_lab_details;
DROP TABLE IF EXISTS cc_hepc_most_recent_rna;
DROP TABLE IF EXISTS cc_hepc_index_dates_patinfo;
DROP TABLE IF EXISTS cc_hepc_report_output;

-- Find all patients who ever had a positive hep_c lab result
CREATE TABLE cc_hepc_pos_labs AS
SELECT patient_id, name, date 
FROM hef_event 
WHERE name in (
'lx:hepatitis_c_elisa:positive',
'lx:hepatitis_c_riba:positive',
'lx:hepatitis_c_rna:positive',
'lx:hepatitis_c_signal_cutoff:positive');


-- Find the date of the first pos hep_c lab result (any test type)
CREATE TABLE cc_hepc_index_dates AS
SELECT patient_id, min(date) index_pos_date
FROM cc_hepc_pos_labs
GROUP BY patient_id;

 -- Find patients with an ESP case for acute hep_c
CREATE TABLE cc_hepc_esp_hepc AS
SELECT patient_id, '1'::INTEGER as hepc_per_esp
FROM nodis_case 
WHERE condition = 'hepatitis_c:acute';
 
-- Find all hepc meds for index patients
CREATE TABLE cc_hepc_all_rx AS
select ip.patient_id, rx.name, rx.date, start_date, end_date, refills 
from emr_prescription rx, cc_hepc_pos_labs ip
WHERE rx.name ~* 
'interferon alfa-2b|pegylated interferon alfa|sofosbuvir|ribavirin|boceprevir|telaprevir|simeprevir|ledipasvir.*sofosbuvir|ombitasvir.*paritaprevir.*ritonavir.*dasabuvir
|ombitasvir.*paritaprevir.*ritonavir|daclatasvir|elbasvir.*grazoprevir|sofosbuvir.*velpatasvir|Pegintron|Pegasys|Sovaldi|Copegus|Rebetol|Virazole|Rebetron|Victrelis
|Incivek|Olysio|Harvoni|Viekira Pak|Technivie|Daklinza|Zepatier|Epclusa|OMBITAS.*PARITAPRE.*RITONA.*DASAB|OMBITA.*PARITAP.*RITON.*DASABUVIR|OMBIT.*PARITAP.*RITONAV
|PEG-INTRON|INTRON|RIBASPHERE|PEGINTERFERON ALFA-2A|INTERFERON ALPHA 2-B'
AND rx.patient_id = ip.patient_id;

-- Filter For HepC Rx with a Date or Start Date Following the Index Positive Date
-- Compute variations on the end_date
CREATE TABLE cc_hepc_rx_after_ip AS
select ip.patient_id, ip.index_pos_date, rx.name, rx.date, rx.start_date, rx.end_date, rx.refills,
start_date + (30 * (coalesce(refills::integer +1, 1::integer)))computed_end_date_start_30_refill,
start_date + 90 computed_end_date_start_plus90
FROM cc_hepc_index_dates ip, cc_hepc_all_rx rx
WHERE ip.patient_id = rx.patient_id
AND (rx.date >= ip.index_pos_date or rx.start_date >= ip.index_pos_date)
ORDER BY patient_id;


-- Get First Treatment Date (use start_date but if start_date is not available use date field)
-- Get Treatment Date End (use the greatest of the possible end dates)
CREATE TABLE cc_hepc_first_last_rx AS
SELECT patient_id, 1::INTEGER hepc_trmt, min(coalesce(start_date, date)) first_trmt_date, max(GREATEST (end_date, computed_end_date_start_30_refill, computed_end_date_start_plus90)) end_trmt_date
FROM cc_hepc_rx_after_ip
GROUP BY patient_id
ORDER BY patient_id;


-- Get RNA lab details for the index pos patients
CREATE TABLE cc_hepc_rna_lab_details AS
SELECT ip.patient_id, l.native_name, l.result_string, l.date, ip.index_pos_date
FROM cc_hepc_index_dates ip, emr_labresult l, conf_labtestmap m
WHERE ip.patient_id = l.patient_id
AND l.native_code = m.native_code
AND l.date >= ip.index_pos_date
AND m.test_name = 'hepatitis_c_rna'
ORDER BY patient_id;

-- Get details on most recent RNA test
CREATE TABLE cc_hepc_most_recent_rna AS
SELECT l.patient_id, 1::INTEGER rna_since_index_pos, max_date last_rna_date, array_agg(result_string ORDER BY max_date) last_rna_result_string
FROM cc_hepc_rna_lab_details l,
(SELECT patient_id, max(date) max_date from cc_hepc_rna_lab_details GROUP BY patient_id)l2
WHERE l.patient_id = l2.patient_id
AND l.date = l2.max_date
GROUP BY l.patient_id, max_date
ORDER BY patient_id;

-- Gather up the patient details for all index positive patients
CREATE TABLE cc_hepc_index_dates_patinfo AS
SELECT ip.patient_id, ip.index_pos_date, 
date_part('year', age(index_pos_date, date_of_birth)) age_on_pos_date, 
date_part('year', date_of_birth) as birth_year, 
d.sex,
CASE when d.race_ethnicity = 6 then 'hispanic'  
when d.race_ethnicity = 5 then 'white' 
when d.race_ethnicity = 3 then 'black' 
when d.race_ethnicity = 2 then 'asian' 
when d.race_ethnicity = 1 then 'native_american' 
when d.race_ethnicity = 0 then 'unknown' 
end race_ethnicity,
'0'::text center_id
--comment line above and uncomment line below for MASS LEAGUE
--center_id
FROM cc_hepc_index_dates ip
LEFT JOIN emr_patient p ON ip.patient_id = p.id
LEFT JOIN esp_mdphnet.esp_demographic d ON p.natural_key = d.patid;


-- Bring all the data elements together
CREATE TABLE cc_hepc_report_output AS
SELECT 
ip.patient_id esp_id,
ip.index_pos_date,
coalesce(hepc_per_esp, 0) AS hepc_per_esp,
age_on_pos_date,
birth_year,
sex,
race_ethnicity,
center_id,
COALESCE(hepc_trmt, 0) as hepc_trmt,
first_trmt_date,
end_trmt_date,
COALESCE(rna_since_index_pos, 0) AS rna_since_index_pos,
last_rna_date,
last_rna_result_string
FROM cc_hepc_index_dates_patinfo ip
LEFT JOIN cc_hepc_esp_hepc esp_case ON ip.patient_id = esp_case.patient_id
LEFT JOIN cc_hepc_first_last_rx rx ON ip.patient_id = rx.patient_id
LEFT JOIN cc_hepc_most_recent_rna rna ON ip.patient_id = rna.patient_id
ORDER BY ip.patient_id;

select * from cc_hepc_report_output order by first_trmt_date asc;


DROP TABLE IF EXISTS cc_hepc_pos_labs;
DROP TABLE IF EXISTS cc_hepc_index_dates;
DROP TABLE IF EXISTS cc_hepc_esp_hepc;
DROP TABLE IF EXISTS cc_hepc_all_rx;
DROP TABLE IF EXISTS cc_hepc_rx_after_ip;
DROP TABLE IF EXISTS cc_hepc_first_last_rx;
DROP TABLE IF EXISTS cc_hepc_rna_lab_details;
DROP TABLE IF EXISTS cc_hepc_most_recent_rna;
DROP TABLE IF EXISTS cc_hepc_index_dates_patinfo;
