	
    DROP TABLE IF EXISTS kre_report.lc__output_cath_new;
	CREATE TABLE kre_report.lc_output_cath_new AS
	select age_group_10_yr,
	sex, 
	race,
	cci,
	count(distinct(patient_id)) total_patients,
	count(distinct(case when (fatigue_fui_one > 0 or fatigue_fui_two > 0 or fatigue_fui_three > 0 or fatigue_fui_four > 0) then patient_id else null end)) fatigue_any_time,
	count(distinct(case when (fever_fui_one > 0 or fever_fui_two > 0 or fever_fui_three > 0 or fever_fui_four > 0) then patient_id else null end)) fever_any_time,
	count(distinct(case when (dyspnea_fui_one > 0 or dyspnea_fui_two > 0 or dyspnea_fui_three > 0 or dyspnea_fui_four > 0) then patient_id else null end)) dyspnea_any_time,
	count(distinct(case when (cough_fui_one > 0 or cough_fui_two > 0 or cough_fui_three > 0 or cough_fui_four > 0) then patient_id else null end)) cough_any_time,
	count(distinct(case when (headache_fui_one > 0 or headache_fui_two > 0 or headache_fui_three > 0 or headache_fui_four > 0) then patient_id else null end)) headache_any_time,
	count(distinct(case when (changes_in_smell_fui_one > 0 or changes_in_smell_fui_two > 0 or changes_in_smell_fui_three > 0 or changes_in_smell_fui_four > 0) then patient_id else null end)) changes_in_smell_any_time,
	count(distinct(case when (changes_in_taste_fui_one > 0 or changes_in_taste_fui_two > 0 or changes_in_taste_fui_three > 0 or changes_in_taste_fui_four > 0) then patient_id else null end)) changes_in_taste_any_time,
	count(distinct(case when (diarrhea_fui_one > 0 or diarrhea_fui_two > 0 or diarrhea_fui_three > 0 or diarrhea_fui_four > 0) then patient_id else null end)) diarrhea_any_time,
	count(distinct(case when (ab_pain_fui_one > 0 or ab_pain_fui_two > 0 or ab_pain_fui_three > 0 or ab_pain_fui_four > 0) then patient_id else null end)) ab_pain_any_time,
	count(distinct(case when (muscle_pain_fui_one > 0 or muscle_pain_fui_two > 0 or muscle_pain_fui_three > 0 or muscle_pain_fui_four > 0) then patient_id else null end)) muscle_pain_any_time,
	count(distinct(case when (nausea_or_vomit_fui_one > 0 or nausea_or_vomit_fui_two > 0 or nausea_or_vomit_fui_three > 0 or nausea_or_vomit_fui_four > 0) then patient_id else null end)) nausea_or_vomit_any_time,
	count(distinct(case when (sore_throat_fui_one > 0 or sore_throat_fui_two > 0 or sore_throat_fui_three > 0 or sore_throat_fui_four > 0) then patient_id else null end)) sore_throat_any_time,
	count(distinct(case when (back_pain_fui_one > 0 or back_pain_fui_two > 0 or back_pain_fui_three > 0 or back_pain_fui_four > 0) then patient_id else null end)) back_pain_any_time,
	count(distinct(case when (other_pain_fui_one > 0 or other_pain_fui_two > 0 or other_pain_fui_three > 0 or other_pain_fui_four > 0) then patient_id else null end)) other_pain_any_time,
	count(distinct(case when (pins_needles_fui_one > 0 or pins_needles_fui_two > 0 or pins_needles_fui_three > 0 or pins_needles_fui_four > 0) then patient_id else null end)) pins_needles_any_time,
	count(distinct(case when (chest_pain_fui_one > 0 or chest_pain_fui_two > 0 or chest_pain_fui_three > 0 or chest_pain_fui_four > 0) then patient_id else null end)) chest_pain_any_time,
	count(distinct(case when (palpitations_fui_one > 0 or palpitations_fui_two > 0 or palpitations_fui_three > 0 or palpitations_fui_four > 0) then patient_id else null end)) palpitations_any_time,
	count(distinct(case when (arthralgia_fui_one > 0 or arthralgia_fui_two > 0 or arthralgia_fui_three > 0 or arthralgia_fui_four > 0) then patient_id else null end)) arthralgia_any_time,
	count(distinct(case when (diff_think_concen_fui_one > 0 or diff_think_concen_fui_two > 0 or diff_think_concen_fui_three > 0 or diff_think_concen_fui_four > 0) then patient_id else null end)) diff_think_concen_any_time,
	sum(fatigue_fui_one) fatigue_fui_one,
	sum(fatigue_fui_two) fatigue_fui_two,
	sum(fatigue_fui_three) fatigue_fui_three,
	sum(fatigue_fui_four) fatigue_fui_four,
	sum(fever_fui_one) fever_fui_one,
	sum(fever_fui_two) fever_fui_two,
	sum(fever_fui_three) fever_fui_three,
	sum(fever_fui_four) fever_fui_four,
	sum(dyspnea_fui_one) dyspnea_fui_one,
	sum(dyspnea_fui_two) dyspnea_fui_two,
	sum(dyspnea_fui_three) dyspnea_fui_three,
	sum(dyspnea_fui_four) dyspnea_fui_four,
	sum(cough_fui_one) cough_fui_one,
	sum(cough_fui_two) cough_fui_two,
	sum(cough_fui_three) cough_fui_three,
	sum(cough_fui_four) cough_fui_four,
	sum(headache_fui_one) headache_fui_one,
	sum(headache_fui_two) headache_fui_two,
	sum(headache_fui_three) headache_fui_three,
	sum(headache_fui_four) headache_fui_four,
	sum(changes_in_smell_fui_one) changes_in_smell_fui_one,
	sum(changes_in_smell_fui_two) changes_in_smell_fui_two,
	sum(changes_in_smell_fui_three) changes_in_smell_fui_three,
	sum(changes_in_smell_fui_four) changes_in_smell_fui_four,
	sum(changes_in_taste_fui_one) changes_in_taste_fui_one,
	sum(changes_in_taste_fui_two) changes_in_taste_fui_two,
	sum(changes_in_taste_fui_three) changes_in_taste_fui_three,
	sum(changes_in_taste_fui_four) changes_in_taste_fui_four,
	sum(diarrhea_fui_one) diarrhea_fui_one,
	sum(diarrhea_fui_two) diarrhea_fui_two,
	sum(diarrhea_fui_three) diarrhea_fui_three,
	sum(diarrhea_fui_four) diarrhea_fui_four,
	sum(ab_pain_fui_one) ab_pain_fui_one,
	sum(ab_pain_fui_two) ab_pain_fui_two,
	sum(ab_pain_fui_three) ab_pain_fui_three,
	sum(ab_pain_fui_four) ab_pain_fui_four,
	sum(muscle_pain_fui_one) muscle_pain_fui_one,
	sum(muscle_pain_fui_two) muscle_pain_fui_two,
	sum(muscle_pain_fui_three) muscle_pain_fui_three,
	sum(muscle_pain_fui_four) muscle_pain_fui_four,
	sum(nausea_or_vomit_fui_one) nausea_or_vomit_fui_one,
	sum(nausea_or_vomit_fui_two) nausea_or_vomit_fui_two,
	sum(nausea_or_vomit_fui_three) nausea_or_vomit_fui_three,
	sum(nausea_or_vomit_fui_four) nausea_or_vomit_fui_four,
	sum(sore_throat_fui_one) sore_throat_fui_one,
	sum(sore_throat_fui_two) sore_throat_fui_two,
	sum(sore_throat_fui_three) sore_throat_fui_three,
	sum(sore_throat_fui_four) sore_throat_fui_four,
	sum(back_pain_fui_one) back_pain_fui_one,
	sum(back_pain_fui_two) back_pain_fui_two,
	sum(back_pain_fui_three) back_pain_fui_three,
	sum(back_pain_fui_four) back_pain_fui_four,
	sum(other_pain_fui_one) other_pain_fui_one,
	sum(other_pain_fui_two) other_pain_fui_two,
	sum(other_pain_fui_three) other_pain_fui_three,
	sum(other_pain_fui_four) other_pain_fui_four,
	sum(pins_needles_fui_one) pins_needles_fui_one,
	sum(pins_needles_fui_two) pins_needles_fui_two,
	sum(pins_needles_fui_three) pins_needles_fui_three,
	sum(pins_needles_fui_four) pins_needles_fui_four,
	sum(chest_pain_fui_one) chest_pain_fui_one,
	sum(chest_pain_fui_two) chest_pain_fui_two,
	sum(chest_pain_fui_three) chest_pain_fui_three,
	sum(chest_pain_fui_four) chest_pain_fui_four,
	sum(palpitations_fui_one) palpitations_fui_one,
	sum(palpitations_fui_two) palpitations_fui_two,
	sum(palpitations_fui_three) palpitations_fui_three,
	sum(palpitations_fui_four) palpitations_fui_four,	
	sum(arthralgia_fui_one) arthralgia_fui_one,
	sum(arthralgia_fui_two) arthralgia_fui_two,
	sum(arthralgia_fui_three) arthralgia_fui_three,
	sum(arthralgia_fui_four) arthralgia_fui_four,	
	sum(diff_think_concen_fui_one) diff_think_concen_fui_one,
	sum(diff_think_concen_fui_two) diff_think_concen_fui_two,
	sum(diff_think_concen_fui_three) diff_think_concen_fui_three,
	sum(diff_think_concen_fui_four) diff_think_concen_fui_four,	
	count(distinct(case when (fatigue_fui_one +fatigue_fui_two + fatigue_fui_three + fatigue_fui_four +  
	                          fever_fui_one + fever_fui_two + fever_fui_three + fever_fui_four +  
							  dyspnea_fui_one + dyspnea_fui_two + dyspnea_fui_three + dyspnea_fui_four +  
							  cough_fui_one + cough_fui_two + cough_fui_three + cough_fui_four +  
							  headache_fui_one + headache_fui_two + headache_fui_three + headache_fui_four +  
							  changes_in_smell_fui_one + changes_in_smell_fui_two + changes_in_smell_fui_three + changes_in_smell_fui_four +  
							  changes_in_taste_fui_one + changes_in_taste_fui_two + changes_in_taste_fui_three + changes_in_taste_fui_four +  
							  diarrhea_fui_one + diarrhea_fui_two + diarrhea_fui_three + diarrhea_fui_four +  
							  ab_pain_fui_one + ab_pain_fui_two + ab_pain_fui_three + ab_pain_fui_four +  
							  muscle_pain_fui_one + muscle_pain_fui_two + muscle_pain_fui_three + muscle_pain_fui_four +  
							  nausea_or_vomit_fui_one + nausea_or_vomit_fui_two + nausea_or_vomit_fui_three + nausea_or_vomit_fui_four +  
							  sore_throat_fui_one + sore_throat_fui_two + sore_throat_fui_three + sore_throat_fui_four +  
							  back_pain_fui_one + back_pain_fui_two + back_pain_fui_three + back_pain_fui_four +  
							  other_pain_fui_one + other_pain_fui_two + other_pain_fui_three + other_pain_fui_four +  
							  pins_needles_fui_one + pins_needles_fui_two + pins_needles_fui_three + pins_needles_fui_four +  
							  chest_pain_fui_one + chest_pain_fui_two + chest_pain_fui_three + chest_pain_fui_four +  
							  palpitations_fui_one + palpitations_fui_two + palpitations_fui_three + palpitations_fui_four +  
							  arthralgia_fui_one + arthralgia_fui_two + arthralgia_fui_three + arthralgia_fui_four +  
							  diff_think_concen_fui_one + diff_think_concen_fui_two + diff_think_concen_fui_three + diff_think_concen_fui_four) = 0
		  then patient_id else null end)) zero_all_symp_any_time,
  	count(distinct(case when (fatigue_fui_one + fatigue_fui_two + fatigue_fui_three + fatigue_fui_four +
	                          fever_fui_one + fever_fui_two + fever_fui_three + fever_fui_four +
							  dyspnea_fui_one + dyspnea_fui_two + dyspnea_fui_three + dyspnea_fui_four +
							  cough_fui_one + cough_fui_two + cough_fui_three + cough_fui_four +
							  headache_fui_one + headache_fui_two + headache_fui_three + headache_fui_four +
							  changes_in_smell_fui_one + changes_in_smell_fui_two + changes_in_smell_fui_three + changes_in_smell_fui_four +
							  changes_in_taste_fui_one + changes_in_taste_fui_two + changes_in_taste_fui_three + changes_in_taste_fui_four +
							  diarrhea_fui_one + diarrhea_fui_two + diarrhea_fui_three + diarrhea_fui_four +
							  ab_pain_fui_one + ab_pain_fui_two + ab_pain_fui_three + ab_pain_fui_four +
							  muscle_pain_fui_one + muscle_pain_fui_two + muscle_pain_fui_three + muscle_pain_fui_four +
							  nausea_or_vomit_fui_one + nausea_or_vomit_fui_two + nausea_or_vomit_fui_three + nausea_or_vomit_fui_four +
							  sore_throat_fui_one + sore_throat_fui_two + sore_throat_fui_three + sore_throat_fui_four +
							  back_pain_fui_one + back_pain_fui_two + back_pain_fui_three + back_pain_fui_four +
							  other_pain_fui_one + other_pain_fui_two + other_pain_fui_three + other_pain_fui_four +
							  pins_needles_fui_one + pins_needles_fui_two + pins_needles_fui_three + pins_needles_fui_four +
							  chest_pain_fui_one + chest_pain_fui_two + chest_pain_fui_three + chest_pain_fui_four +
							  palpitations_fui_one + palpitations_fui_two + palpitations_fui_three + palpitations_fui_four +
							  arthralgia_fui_one + arthralgia_fui_two + arthralgia_fui_three + arthralgia_fui_four +
							  diff_think_concen_fui_one + diff_think_concen_fui_two + diff_think_concen_fui_three + diff_think_concen_fui_four) = 1 
		  then patient_id else null end)) one_all_symp_any_time,
	count(distinct(case when (fatigue_fui_one + fatigue_fui_two + fatigue_fui_three + fatigue_fui_four +
	                          fever_fui_one + fever_fui_two + fever_fui_three + fever_fui_four +
							  dyspnea_fui_one + dyspnea_fui_two + dyspnea_fui_three + dyspnea_fui_four +
							  cough_fui_one + cough_fui_two + cough_fui_three + cough_fui_four +
							  headache_fui_one + headache_fui_two + headache_fui_three + headache_fui_four +
							  changes_in_smell_fui_one + changes_in_smell_fui_two + changes_in_smell_fui_three + changes_in_smell_fui_four +
							  changes_in_taste_fui_one + changes_in_taste_fui_two + changes_in_taste_fui_three + changes_in_taste_fui_four +
							  diarrhea_fui_one + diarrhea_fui_two + diarrhea_fui_three + diarrhea_fui_four +
							  ab_pain_fui_one + ab_pain_fui_two + ab_pain_fui_three + ab_pain_fui_four +
							  muscle_pain_fui_one + muscle_pain_fui_two + muscle_pain_fui_three + muscle_pain_fui_four +
							  nausea_or_vomit_fui_one + nausea_or_vomit_fui_two + nausea_or_vomit_fui_three + nausea_or_vomit_fui_four +
							  sore_throat_fui_one + sore_throat_fui_two + sore_throat_fui_three + sore_throat_fui_four +
							  back_pain_fui_one + back_pain_fui_two + back_pain_fui_three + back_pain_fui_four +
							  other_pain_fui_one + other_pain_fui_two + other_pain_fui_three + other_pain_fui_four +
							  pins_needles_fui_one + pins_needles_fui_two + pins_needles_fui_three + pins_needles_fui_four +
							  chest_pain_fui_one + chest_pain_fui_two + chest_pain_fui_three + chest_pain_fui_four +
							  palpitations_fui_one + palpitations_fui_two + palpitations_fui_three + palpitations_fui_four +
							  arthralgia_fui_one + arthralgia_fui_two + arthralgia_fui_three + arthralgia_fui_four +
							  diff_think_concen_fui_one + diff_think_concen_fui_two + diff_think_concen_fui_three + diff_think_concen_fui_four) = 2 
		  then patient_id else null end)) two_all_symp_any_time,
	count(distinct(case when (fatigue_fui_one + fatigue_fui_two + fatigue_fui_three + fatigue_fui_four +
	                          fever_fui_one + fever_fui_two + fever_fui_three + fever_fui_four +
							  dyspnea_fui_one + dyspnea_fui_two + dyspnea_fui_three + dyspnea_fui_four +
							  cough_fui_one + cough_fui_two + cough_fui_three + cough_fui_four +
							  headache_fui_one + headache_fui_two + headache_fui_three + headache_fui_four +
							  changes_in_smell_fui_one + changes_in_smell_fui_two + changes_in_smell_fui_three + changes_in_smell_fui_four +
							  changes_in_taste_fui_one + changes_in_taste_fui_two + changes_in_taste_fui_three + changes_in_taste_fui_four +
							  diarrhea_fui_one + diarrhea_fui_two + diarrhea_fui_three + diarrhea_fui_four +
							  ab_pain_fui_one + ab_pain_fui_two + ab_pain_fui_three + ab_pain_fui_four +
							  muscle_pain_fui_one + muscle_pain_fui_two + muscle_pain_fui_three + muscle_pain_fui_four +
							  nausea_or_vomit_fui_one + nausea_or_vomit_fui_two + nausea_or_vomit_fui_three + nausea_or_vomit_fui_four +
							  sore_throat_fui_one + sore_throat_fui_two + sore_throat_fui_three + sore_throat_fui_four +
							  back_pain_fui_one + back_pain_fui_two + back_pain_fui_three + back_pain_fui_four +
							  other_pain_fui_one + other_pain_fui_two + other_pain_fui_three + other_pain_fui_four +
							  pins_needles_fui_one + pins_needles_fui_two + pins_needles_fui_three + pins_needles_fui_four +
							  chest_pain_fui_one + chest_pain_fui_two + chest_pain_fui_three + chest_pain_fui_four +
							  palpitations_fui_one + palpitations_fui_two + palpitations_fui_three + palpitations_fui_four +
							  arthralgia_fui_one + arthralgia_fui_two + arthralgia_fui_three + arthralgia_fui_four +
							  diff_think_concen_fui_one + diff_think_concen_fui_two + diff_think_concen_fui_three + diff_think_concen_fui_four) = 3 
		  then patient_id else null end)) three_all_symp_any_time,	
	count(distinct(case when (fatigue_fui_one + fatigue_fui_two + fatigue_fui_three + fatigue_fui_four +
	                          fever_fui_one + fever_fui_two + fever_fui_three + fever_fui_four +
							  dyspnea_fui_one + dyspnea_fui_two + dyspnea_fui_three + dyspnea_fui_four +
							  cough_fui_one + cough_fui_two + cough_fui_three + cough_fui_four +
							  headache_fui_one + headache_fui_two + headache_fui_three + headache_fui_four +
							  changes_in_smell_fui_one + changes_in_smell_fui_two + changes_in_smell_fui_three + changes_in_smell_fui_four +
							  changes_in_taste_fui_one + changes_in_taste_fui_two + changes_in_taste_fui_three + changes_in_taste_fui_four +
							  diarrhea_fui_one + diarrhea_fui_two + diarrhea_fui_three + diarrhea_fui_four +
							  ab_pain_fui_one + ab_pain_fui_two + ab_pain_fui_three + ab_pain_fui_four +
							  muscle_pain_fui_one + muscle_pain_fui_two + muscle_pain_fui_three + muscle_pain_fui_four +
							  nausea_or_vomit_fui_one + nausea_or_vomit_fui_two + nausea_or_vomit_fui_three + nausea_or_vomit_fui_four +
							  sore_throat_fui_one + sore_throat_fui_two + sore_throat_fui_three + sore_throat_fui_four +
							  back_pain_fui_one + back_pain_fui_two + back_pain_fui_three + back_pain_fui_four +
							  other_pain_fui_one + other_pain_fui_two + other_pain_fui_three + other_pain_fui_four +
							  pins_needles_fui_one + pins_needles_fui_two + pins_needles_fui_three + pins_needles_fui_four +
							  chest_pain_fui_one + chest_pain_fui_two + chest_pain_fui_three + chest_pain_fui_four +
							  palpitations_fui_one + palpitations_fui_two + palpitations_fui_three + palpitations_fui_four +
							  arthralgia_fui_one + arthralgia_fui_two + arthralgia_fui_three + arthralgia_fui_four +
							  diff_think_concen_fui_one + diff_think_concen_fui_two + diff_think_concen_fui_three + diff_think_concen_fui_four) >= 4 
		  then patient_id else null end)) four_plus_all_symp_any_time,		
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_fui_one + changes_in_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one) = 0 
		  then patient_id else null end)) zero_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_two + fever_fui_two + dyspnea_fui_two + cough_fui_two + 
							  headache_fui_two + changes_in_smell_fui_two + changes_in_taste_fui_two + 
							  diarrhea_fui_two + ab_pain_fui_two + muscle_pain_fui_two + nausea_or_vomit_fui_two + 
							  sore_throat_fui_two + back_pain_fui_two + other_pain_fui_two + pins_needles_fui_two + 
							  chest_pain_fui_two + palpitations_fui_two + arthralgia_fui_two + diff_think_concen_fui_two) = 0 
		  then patient_id else null end)) zero_all_symp_fui_two,
  	count(distinct(case when (fatigue_fui_three + fever_fui_three + dyspnea_fui_three + cough_fui_three + 
							  headache_fui_three + changes_in_smell_fui_three + changes_in_taste_fui_three + 
							  diarrhea_fui_three + ab_pain_fui_three + muscle_pain_fui_three + nausea_or_vomit_fui_three + 
							  sore_throat_fui_three + back_pain_fui_three + other_pain_fui_three + pins_needles_fui_three + 
							  chest_pain_fui_three + palpitations_fui_three + arthralgia_fui_three + diff_think_concen_fui_three) = 0 
		  then patient_id else null end)) zero_all_symp_fui_three,
   	count(distinct(case when (fatigue_fui_four + fever_fui_four + dyspnea_fui_four + cough_fui_four + 
							  headache_fui_four + changes_in_smell_fui_four + changes_in_taste_fui_four + 
							  diarrhea_fui_four + ab_pain_fui_four + muscle_pain_fui_four + nausea_or_vomit_fui_four + 
							  sore_throat_fui_four + back_pain_fui_four + other_pain_fui_four + pins_needles_fui_four + 
							  chest_pain_fui_four + palpitations_fui_four + arthralgia_fui_four + diff_think_concen_fui_four) = 0 
		  then patient_id else null end)) zero_all_symp_fui_four,
   	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_fui_one + changes_in_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one) = 1 
		  then patient_id else null end)) one_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_two + fever_fui_two + dyspnea_fui_two + cough_fui_two + 
							  headache_fui_two + changes_in_smell_fui_two + changes_in_taste_fui_two + 
							  diarrhea_fui_two + ab_pain_fui_two + muscle_pain_fui_two + nausea_or_vomit_fui_two + 
							  sore_throat_fui_two + back_pain_fui_two + other_pain_fui_two + pins_needles_fui_two + 
							  chest_pain_fui_two + palpitations_fui_two + arthralgia_fui_two + diff_think_concen_fui_two) = 1 
		  then patient_id else null end)) one_all_symp_fui_two,
   	count(distinct(case when (fatigue_fui_three + fever_fui_three + dyspnea_fui_three + cough_fui_three + 
							  headache_fui_three + changes_in_smell_fui_three + changes_in_taste_fui_three + 
							  diarrhea_fui_three + ab_pain_fui_three + muscle_pain_fui_three + nausea_or_vomit_fui_three + 
							  sore_throat_fui_three + back_pain_fui_three + other_pain_fui_three + pins_needles_fui_three + 
							  chest_pain_fui_three + palpitations_fui_three + arthralgia_fui_three + diff_think_concen_fui_three) = 1 
		  then patient_id else null end)) one_all_symp_fui_three,
  	count(distinct(case when (fatigue_fui_four + fever_fui_four + dyspnea_fui_four + cough_fui_four + 
							  headache_fui_four + changes_in_smell_fui_four + changes_in_taste_fui_four + 
							  diarrhea_fui_four + ab_pain_fui_four + muscle_pain_fui_four + nausea_or_vomit_fui_four + 
							  sore_throat_fui_four + back_pain_fui_four + other_pain_fui_four + pins_needles_fui_four + 
							  chest_pain_fui_four + palpitations_fui_four + arthralgia_fui_four + diff_think_concen_fui_four) = 1 
		  then patient_id else null end)) one_all_symp_fui_four,
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_fui_one + changes_in_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one) = 2 
		  then patient_id else null end)) two_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_two + fever_fui_two + dyspnea_fui_two + cough_fui_two + 
							  headache_fui_two + changes_in_smell_fui_two + changes_in_taste_fui_two + 
							  diarrhea_fui_two + ab_pain_fui_two + muscle_pain_fui_two + nausea_or_vomit_fui_two + 
							  sore_throat_fui_two + back_pain_fui_two + other_pain_fui_two + pins_needles_fui_two + 
							  chest_pain_fui_two + palpitations_fui_two + arthralgia_fui_two + diff_think_concen_fui_two) = 2 
		  then patient_id else null end)) two_all_symp_fui_two,
   	count(distinct(case when (fatigue_fui_three + fever_fui_three + dyspnea_fui_three + cough_fui_three + 
							  headache_fui_three + changes_in_smell_fui_three + changes_in_taste_fui_three + 
							  diarrhea_fui_three + ab_pain_fui_three + muscle_pain_fui_three + nausea_or_vomit_fui_three + 
							  sore_throat_fui_three + back_pain_fui_three + other_pain_fui_three + pins_needles_fui_three + 
							  chest_pain_fui_three + palpitations_fui_three + arthralgia_fui_three + diff_think_concen_fui_three) = 2 
		  then patient_id else null end)) two_all_symp_fui_three,
	count(distinct(case when (fatigue_fui_four + fever_fui_four + dyspnea_fui_four + cough_fui_four + 
							  headache_fui_four + changes_in_smell_fui_four + changes_in_taste_fui_four + 
							  diarrhea_fui_four + ab_pain_fui_four + muscle_pain_fui_four + nausea_or_vomit_fui_four + 
							  sore_throat_fui_four + back_pain_fui_four + other_pain_fui_four + pins_needles_fui_four + 
							  chest_pain_fui_four + palpitations_fui_four + arthralgia_fui_four + diff_think_concen_fui_four) = 2 
		  then patient_id else null end)) two_all_symp_fui_four,
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_fui_one + changes_in_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one) = 3 
		  then patient_id else null end)) three_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_two + fever_fui_two + dyspnea_fui_two + cough_fui_two + 
							  headache_fui_two + changes_in_smell_fui_two + changes_in_taste_fui_two + 
							  diarrhea_fui_two + ab_pain_fui_two + muscle_pain_fui_two + nausea_or_vomit_fui_two + 
							  sore_throat_fui_two + back_pain_fui_two + other_pain_fui_two + pins_needles_fui_two + 
							  chest_pain_fui_two + palpitations_fui_two + arthralgia_fui_two + diff_think_concen_fui_two) = 3 
		  then patient_id else null end)) three_all_symp_fui_two,
   	count(distinct(case when (fatigue_fui_three + fever_fui_three + dyspnea_fui_three + cough_fui_three + 
							  headache_fui_three + changes_in_smell_fui_three + changes_in_taste_fui_three + 
							  diarrhea_fui_three + ab_pain_fui_three + muscle_pain_fui_three + nausea_or_vomit_fui_three + 
							  sore_throat_fui_three + back_pain_fui_three + other_pain_fui_three + pins_needles_fui_three + 
							  chest_pain_fui_three + palpitations_fui_three + arthralgia_fui_three + diff_think_concen_fui_three) = 3 
		  then patient_id else null end)) three_all_symp_fui_three,
	count(distinct(case when (fatigue_fui_four + fever_fui_four + dyspnea_fui_four + cough_fui_four + 
							  headache_fui_four + changes_in_smell_fui_four + changes_in_taste_fui_four + 
							  diarrhea_fui_four + ab_pain_fui_four + muscle_pain_fui_four + nausea_or_vomit_fui_four + 
							  sore_throat_fui_four + back_pain_fui_four + other_pain_fui_four + pins_needles_fui_four + 
							  chest_pain_fui_four + palpitations_fui_four + arthralgia_fui_four + diff_think_concen_fui_four) = 3 
		  then patient_id else null end)) three_all_symp_fui_four,
   	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_fui_one + changes_in_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one) >= 4 
		  then patient_id else null end)) four_plus_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_two + fever_fui_two + dyspnea_fui_two + cough_fui_two + 
							  headache_fui_two + changes_in_smell_fui_two + changes_in_taste_fui_two + 
							  diarrhea_fui_two + ab_pain_fui_two + muscle_pain_fui_two + nausea_or_vomit_fui_two + 
							  sore_throat_fui_two + back_pain_fui_two + other_pain_fui_two + pins_needles_fui_two + 
							  chest_pain_fui_two + palpitations_fui_two + arthralgia_fui_two + diff_think_concen_fui_two) >= 4 
		  then patient_id else null end)) four_plus_all_symp_fui_two,
   	count(distinct(case when (fatigue_fui_three + fever_fui_three + dyspnea_fui_three + cough_fui_three + 
							  headache_fui_three + changes_in_smell_fui_three + changes_in_taste_fui_three + 
							  diarrhea_fui_three + ab_pain_fui_three + muscle_pain_fui_three + nausea_or_vomit_fui_three + 
							  sore_throat_fui_three + back_pain_fui_three + other_pain_fui_three + pins_needles_fui_three + 
							  chest_pain_fui_three + palpitations_fui_three + arthralgia_fui_three + diff_think_concen_fui_three) >= 4 
		  then patient_id else null end)) four_plus_all_symp_fui_three,
	count(distinct(case when (fatigue_fui_four + fever_fui_four + dyspnea_fui_four + cough_fui_four + 
							  headache_fui_four + changes_in_smell_fui_four + changes_in_taste_fui_four + 
							  diarrhea_fui_four + ab_pain_fui_four + muscle_pain_fui_four + nausea_or_vomit_fui_four + 
							  sore_throat_fui_four + back_pain_fui_four + other_pain_fui_four + pins_needles_fui_four + 
							  chest_pain_fui_four + palpitations_fui_four + arthralgia_fui_four + diff_think_concen_fui_four) >= 4 
		  then patient_id else null end)) four_plus_all_symp_fui_four,
    count(distinct(case when (pos_in_fui_one > 0 or pos_in_fui_two > 0 or pos_in_fui_three > 0 or pos_in_fui_four > 0) then patient_id else null end)) infected_any_time,
    sum(pos_in_fui_one) infected_fui_one,
	sum(pos_in_fui_two) infected_fui_two,
	sum(pos_in_fui_three) infected_fui_three,
	sum(pos_in_fui_four) infected_fui_four
    --FROM symptom_by_pat					
	FROM kre_report.lc_symptom_by_pat_cath_new
	GROUP BY age_group_10_yr, sex, race, cci;
	
	
			
