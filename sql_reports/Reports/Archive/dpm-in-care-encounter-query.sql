﻿DROP TABLE IF EXISTS tmp_incare_enc_2011;
DROP TABLE IF EXISTS tmp_incare_enc_2011_next;
DROP TABLE IF EXISTS tmp_incare_enc_2011_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2012;
DROP TABLE IF EXISTS tmp_incare_enc_2012_next;
DROP TABLE IF EXISTS tmp_incare_enc_2012_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2013;
DROP TABLE IF EXISTS tmp_incare_enc_2013_next;
DROP TABLE IF EXISTS tmp_incare_enc_2013_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2014;
DROP TABLE IF EXISTS tmp_incare_enc_2014_next;
DROP TABLE IF EXISTS tmp_incare_enc_2014_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2015;
DROP TABLE IF EXISTS tmp_incare_enc_2015_next;
DROP TABLE IF EXISTS tmp_incare_enc_2015_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2016;
DROP TABLE IF EXISTS tmp_incare_enc_2016_next;
DROP TABLE IF EXISTS tmp_incare_enc_2016_next_days;
DROP TABLE IF EXISTS tmp_incare_enc_2017;
DROP TABLE IF EXISTS tmp_incare_enc_2017_next;
DROP TABLE IF EXISTS tmp_incare_enc_2017_next_days;

DROP TABLE IF EXISTS tmp_incare_enc_all_year_counts;


--2011
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2011 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2011-01-01' and date < '2012-01-01'
AND date_part('year', age('2011-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2011
CREATE TABLE tmp_incare_enc_2011_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2011 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2011
CREATE TABLE tmp_incare_enc_2011_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2011'::text as rpt_year
FROM tmp_incare_enc_2011 t
LEFT JOIN tmp_incare_enc_2011_next n ON (n.patient_id = t.patient_id);

--2012
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2012 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2012-01-01' and date < '2013-01-01'
AND date_part('year', age('2012-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2012
CREATE TABLE tmp_incare_enc_2012_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2012 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2012
CREATE TABLE tmp_incare_enc_2012_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2012'::text as rpt_year
FROM tmp_incare_enc_2012 t
LEFT JOIN tmp_incare_enc_2012_next n ON (n.patient_id = t.patient_id);

--2013
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2013 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2013-01-01' and date < '2014-01-01'
AND date_part('year', age('2013-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2013
CREATE TABLE tmp_incare_enc_2013_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2013 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2013
CREATE TABLE tmp_incare_enc_2013_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2013'::text as rpt_year
FROM tmp_incare_enc_2013 t
LEFT JOIN tmp_incare_enc_2013_next n ON (n.patient_id = t.patient_id);

--2014
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2014 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2014-01-01' and date < '2015-01-01'
AND date_part('year', age('2014-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2014
CREATE TABLE tmp_incare_enc_2014_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2014 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2014
CREATE TABLE tmp_incare_enc_2014_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2014'::text as rpt_year
FROM tmp_incare_enc_2014 t
LEFT JOIN tmp_incare_enc_2014_next n ON (n.patient_id = t.patient_id);

--2015
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2015 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2015-01-01' and date < '2016-01-01'
AND date_part('year', age('2015-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2015
CREATE TABLE tmp_incare_enc_2015_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2015 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2015
CREATE TABLE tmp_incare_enc_2015_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2015'::text as rpt_year
FROM tmp_incare_enc_2015 t
LEFT JOIN tmp_incare_enc_2015_next n ON (n.patient_id = t.patient_id);

--2016
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2016 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2016-01-01' and date < '2017-01-01'
AND date_part('year', age('2016-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2016
CREATE TABLE tmp_incare_enc_2016_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2016 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2016
CREATE TABLE tmp_incare_enc_2016_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2016'::text as rpt_year
FROM tmp_incare_enc_2016 t
LEFT JOIN tmp_incare_enc_2016_next n ON (n.patient_id = t.patient_id);

--2017
-- LIMIT BY ENCOUNTER TYPE
-- LIMIT BY CENTER
CREATE TABLE tmp_incare_enc_2017 AS
select patient_id, min(date) first_encounter 
FROM emr_encounter e, emr_patient p, static_enc_type_lookup s 
WHERE e.patient_id = p.id
AND e.raw_encounter_type = s.raw_encounter_type 
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
--AND center_id not in ('1') 
AND date >= '2017-01-01' and date < '2018-01-01'
AND date_part('year', age('2017-01-01', date_of_birth)) >= 20
GROUP by patient_id;

--2017
CREATE TABLE tmp_incare_enc_2017_next AS
select t.patient_id, min(date) AS next_encounter
FROM tmp_incare_enc_2017 t
LEFT JOIN emr_encounter e ON (e.patient_id = t.patient_id)
JOIN static_enc_type_lookup s ON (e.raw_encounter_type = s.raw_encounter_type)
AND e.date > t.first_encounter
AND (e.raw_encounter_type is NULL or s.ambulatory = 1)
GROUP BY t.patient_id;

--2017
CREATE TABLE tmp_incare_enc_2017_next_days AS
select t.patient_id, coalesce((next_encounter - first_encounter), 0) as next_encounter_days, '2017'::text as rpt_year
FROM tmp_incare_enc_2017 t
LEFT JOIN tmp_incare_enc_2017_next n ON (n.patient_id = t.patient_id);


-- ALL YEARS
CREATE TABLE tmp_incare_enc_all_year_counts AS 
select rpt_year, count(*) patient_count, next_encounter_days from tmp_incare_enc_2011_next_days GROUP by next_encounter_days, rpt_year 
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2012_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2013_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2014_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2015_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2016_next_days GROUP by next_encounter_days, rpt_year
UNION
select rpt_year, count(*) patient_count, next_encounter_days FROM tmp_incare_enc_2017_next_days GROUP by next_encounter_days, rpt_year;


-- For output
SELECT * from tmp_incare_enc_all_year_counts
order by rpt_year, next_encounter_days;



