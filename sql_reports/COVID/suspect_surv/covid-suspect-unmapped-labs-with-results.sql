drop table if exists temp_covid_unmapped;
DROP TABLE IF EXISTS temp_covid_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_covid_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_flu_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_flu_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_std_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_std_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_other_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_other_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_pneumo_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_pneumo_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_virus_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_virus_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_parapertussis_strings_unmapped_with_result_strings;
DROP TABLE IF EXISTS temp_parapertussis_strings_unmapped_with_result_strings_final;
DROP TABLE IF EXISTS temp_all_covid_suspect_unmapped;

create table temp_covid_unmapped as
select e.count, e.native_name, e.native_code, procedure_name
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
--and e.native_name ~* '(alt|sgpt|ast|aminotrans|sgot|rbc|red bl|wbc|white bl|platelet|plt|cd4|helper|genotype|creat|hematocrit|hct)'
and e.native_name ~* '(covid|sars|cov|corona|influenza|flu|wbc|white bl|hct|hematocrit|platelet|plt|lymph|po2|partial|dimer|reactive|crp|pneumo|parainflu|adenovirus|rhinovirus|enterovirus|rsv|syncytial|syncitial|synctial|parapertussis)'
group by e.count, e.native_name, e.native_code, procedure_name;


DROP TABLE IF EXISTS temp_covid_strings_unmapped_with_result_strings;
CREATE TABLE temp_covid_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(covid|sars|cov|corona)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(covid|sars|cov|corona)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_covid_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_covid_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_covid_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_flu_strings_unmapped_with_result_strings;
CREATE TABLE temp_flu_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(influenza|flu|parainflu)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(influenza|flu|parainflu)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_flu_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_flu_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_flu_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_std_strings_unmapped_with_result_strings;
CREATE TABLE temp_std_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(wbc|white bl|hct|hematocrit|platelet|plt)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(wbc|white bl|hct|hematocrit|platelet|plt)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_std_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_std_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_std_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_other_strings_unmapped_with_result_strings;
CREATE TABLE temp_other_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(lymph|po2|partial|dimer|reactive|crp)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(lymph|po2|partial|dimer|reactive|crp)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_other_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_other_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_other_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;

DROP TABLE IF EXISTS temp_pneumo_strings_unmapped_with_result_strings;
CREATE TABLE temp_pneumo_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(pneumo)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(pneumo)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_pneumo_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_pneumo_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_pneumo_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_virus_strings_unmapped_with_result_strings;
CREATE TABLE temp_virus_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(adenovirus|rhinovirus|enterovirus|rsv|syncytial|syncitial|synctial)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(adenovirus|rhinovirus|enterovirus|rsv|syncytial|syncitial|synctial)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_virus_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_virus_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_virus_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_parapertussis_strings_unmapped_with_result_strings;
CREATE TABLE temp_parapertussis_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_covid_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(parapertussis)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_covid_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(parapertussis)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_parapertussis_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_parapertussis_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_parapertussis_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


DROP TABLE IF EXISTS temp_all_covid_suspect_unmapped;

CREATE TABLE temp_all_covid_suspect_unmapped AS
SELECT * from temp_covid_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_flu_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_std_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_other_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_pneumo_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_virus_strings_unmapped_with_result_strings_final
UNION
SELECT * from temp_parapertussis_strings_unmapped_with_result_strings_final;


-- \COPY temp_covid_strings_unmapped_with_result_strings_final  TO '/tmp/coronavirus_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_flu_strings_unmapped_with_result_strings_final  TO '/tmp/flu_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_std_strings_unmapped_with_result_strings_final  TO '/tmp/std_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_other_strings_unmapped_with_result_strings_final TO '/tmp/other_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_pneumo_strings_unmapped_with_result_strings_final TO '/tmp/pneumo_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_virus_strings_unmapped_with_result_strings_final TO '/tmp/virus_labs_for_review.csv' DELIMITER ',' CSV HEADER;
-- \COPY temp_parapertussis_strings_unmapped_with_result_strings_final TO '/tmp/parap_labs_for_review.csv' DELIMITER ',' CSV HEADER;

--\COPY temp_all_covid_suspect_unmapped TO '/tmp/all_unmapped_covid_suspect_for_review.csv' DELIMITER ',' CSV HEADER;



