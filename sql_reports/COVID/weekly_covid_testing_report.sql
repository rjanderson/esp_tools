DROP TABLE IF EXISTS covid19_report.tst_covid_pcr_testing;
DROP TABLE IF EXISTS covid19_report.tst_covid_cumu_pcr_testing;
DROP TABLE IF EXISTS covid19_report.tst_covid_wk_tests_no_res;
DROP TABLE IF EXISTS covid19_report.tst_covid_cumu_tests_no_res;
DROP TABLE IF EXISTS covid19_report.tst_covid_pcr_orders;
DROP TABLE IF EXISTS covid19_report.tst_covid_cumu_pcr_orders;
DROP TABLE IF EXISTS covid19_report.tst_covid_pcr_testing_no_order;
DROP TABLE IF EXISTS covid19_report.tst_covid_cumu_pcr_testing_no_order;
DROP TABLE IF EXISTS covid19_report.tst_covid_demog_groups;
DROP TABLE IF EXISTS covid19_report.tst_covid_tot_full_strat;
DROP TABLE IF EXISTS covid19_report.tst_covid_output;
DROP TABLE IF EXISTS covid19_report.tst_covid_output_formatted;


-- WEEKLY TESTING
-- NOT DISTINCT PATIENTS PER REQUEST!
CREATE TABLE covid19_report.tst_covid_pcr_testing AS
SELECT 
COUNT(patient_id) as pcr_test_results,
COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pcr_pos_tests,
COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as pcr_neg_tests,
COUNT(CASE WHEN name ilike '%inde%' then 1 end ) as pcr_ind_tests,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE name ilike 'lx:covid19_pcr:%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- CUMULATIVE TESTING 
-- NOT DISTINCT PATIENTS PER REQUEST!
CREATE TABLE covid19_report.tst_covid_cumu_pcr_testing AS
SELECT 
COUNT(patient_id) as pcr_test_results_cumu,
COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pcr_pos_tests_cumu,
COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as pcr_neg_tests_cumu,
COUNT(CASE WHEN name ilike '%inde%' then 1 end ) as pcr_ind_tests_cumu,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE name ilike 'lx:covid19_pcr:%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;

-- TESTS WITH NO RESULT
CREATE TABLE covid19_report.tst_covid_wk_tests_no_res AS
SELECT 
count(patient_id) as pcr_tests_no_result,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, week_end_date, covid19_report.mmwrwkn(T1.date) as week_number, zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		 END as age_group,
	CASE
	     WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		 END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM 
		-- Pats with laborder but no result yet
		(SELECT DISTINCT T1.patient_id, T2.natural_key,  T2.date
	         FROM hef_event T1,
	         emr_laborder T2
	         WHERE T1.patient_id = T2.patient_id
	         AND T1.object_id = T2.id
	         AND T1.name = 'lx:covid19_pcr_order:order'
	         AND T2.date >= '2020-02-01'
        EXCEPT
        SELECT DISTINCT patient_id, order_natural_key, date
		    FROM emr_labresult
		    WHERE date >= '2020-02-01'
			-- test needs to be resulted before report week end date
			AND date <= :run_date::date) T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
	AND T1.date <= week_end_date
	AND T5.date_of_birth is not null) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- CUMULATIVE NO TEST RESULT 
CREATE TABLE covid19_report.tst_covid_cumu_tests_no_res AS
SELECT 
count(patient_id) as pcr_tests_no_result_cumu,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, week_end_date, covid19_report.mmwrwkn(week_end_date::date) as week_number, zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		 END as age_group,
	CASE
	     WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		 END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM 
		-- Pats with laborder but no result yet
		(SELECT DISTINCT T1.patient_id, T2.natural_key,  T2.date
	         FROM hef_event T1,
	         emr_laborder T2
	         WHERE T1.patient_id = T2.patient_id
	         AND T1.object_id = T2.id
	         AND T1.name = 'lx:covid19_pcr_order:order'
	         AND T2.date >= '2020-02-01'
        EXCEPT
        SELECT DISTINCT patient_id, order_natural_key, date
		    FROM emr_labresult
		    WHERE date >= '2020-02-01'
			-- test needs to be resulted before report week end date
			AND date <= :run_date::date) T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE T1.date >= '2020-02-01'
	AND T1.date <= week_end_date
	AND T5.date_of_birth is not null) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- WEEKLY LAB ORDERS
-- NOT DISTINCT PATIENTS PER REQUEST!
CREATE TABLE covid19_report.tst_covid_pcr_orders AS
SELECT 
COUNT(patient_id) as pcr_tests_ordered,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM hef_event T1
	JOIN emr_laborder T2 on (T1.object_id = T2.id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE T1.name = 'lx:covid19_pcr_order:order'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- CUMULATIVE LAB ORDERS
-- NOT DISTINCT PATIENTS PER REQUEST!
CREATE TABLE covid19_report.tst_covid_cumu_pcr_orders AS
SELECT 
COUNT(patient_id) as pcr_tests_ordered_cumu,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM hef_event T1
	JOIN emr_laborder T2 on (T1.object_id = T2.id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE T1.name = 'lx:covid19_pcr_order:order'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- COUNT OF LABS WHERE WE HAVE A RESULT BUT NO ORDER
-- TYPICALLY EXTERNAL RESULTS
CREATE TABLE covid19_report.tst_covid_pcr_testing_no_order AS
SELECT 
COUNT(patient_id) as pcr_test_results_no_order,
--COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pcr_pos_tests,
--COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as pcr_neg_tests,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.native_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM emr_labresult T1
	JOIN conf_labtestmap T2 on (T1.native_code = T2.native_code)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE test_name = 'covid19_pcr'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
	AND T1.order_natural_key not in (
		select natural_key 
		from emr_laborder T1,
		conf_labtestmap T2
		where T1.procedure_code = T2.native_code
		and test_name = 'covid19_pcr_order')
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- CUMULATIVE COUNT OF LABS WHERE WE HAVE A RESULT BUT NO ORDER
-- TYPICALLY EXTERNAL RESULTS
CREATE TABLE covid19_report.tst_covid_cumu_pcr_testing_no_order AS
SELECT 
COUNT(patient_id) as pcr_test_results_no_order_cumu,
--COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pcr_pos_tests,
--COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as pcr_neg_tests,
week_end_date, week_number, zip5, age_group, gender, race_ethnicity
FROM (
	SELECT T1.patient_id, T1.native_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, zip5,
	CASE 
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 20 then '0-19'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 20 and 29 then '20-29'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 30 and 39 then '30-39'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 40 and 49 then '40-49'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 50 and 59 then '50-59'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 60 and 69 then '60-69'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 69 then '70+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO') or UPPER(race) = 'HISPANIC' then 'HISPANIC'
         WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
         WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
         WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
         WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
         WHEN UPPER(race) in ('ASIAN', 'A') then 'ASIAN'
         WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER') then 'PACIFIC_ISLANDER'
         WHEN race is null or race = '' then 'UNKNOWN' 
        ELSE 'UNKNOWN' END as race_ethnicity
	FROM emr_labresult T1
	JOIN conf_labtestmap T2 on (T1.native_code = T2.native_code)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	WHERE test_name = 'covid19_pcr'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
    AND T1.date <= week_end_date
	AND T1.order_natural_key not in (
		select natural_key 
		from emr_laborder T1,
		conf_labtestmap T2
		where T1.procedure_code = T2.native_code
		and test_name = 'covid19_pcr_order')
    ) T1
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;



-- GENERATE THE GROUPS
CREATE TABLE covid19_report.tst_covid_demog_groups AS
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_pcr_testing
UNION 
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_cumu_pcr_testing
UNION
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_wk_tests_no_res
UNION 
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_cumu_tests_no_res
UNION
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_pcr_orders
UNION
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_cumu_pcr_orders
UNION
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_pcr_testing_no_order
UNION
SELECT week_end_date, week_number, zip5, age_group, gender, race_ethnicity FROM covid19_report.tst_covid_cumu_pcr_testing_no_order
GROUP BY week_end_date, week_number, zip5, age_group, gender, race_ethnicity;


-- GENERATE THE FULLY STRATIFIED OUTPUT
CREATE TABLE covid19_report.tst_covid_tot_full_strat AS
select T1.week_end_date, T1.week_number, T1.zip5, T1.age_group, T1.gender, T1.race_ethnicity,
coalesce(pcr_test_results, 0) as pcr_test_results,
coalesce(pcr_pos_tests, 0) as pcr_pos_tests, 
coalesce(pcr_neg_tests, 0) as pcr_neg_tests,
coalesce(pcr_ind_tests, 0) as pcr_ind_tests,
coalesce(pcr_test_results_cumu, 0) as pcr_test_results_cumu,
coalesce(pcr_pos_tests_cumu, 0) as pcr_pos_tests_cumu,
coalesce(pcr_neg_tests_cumu, 0) as pcr_neg_tests_cumu,
coalesce(pcr_ind_tests_cumu, 0) as pcr_ind_tests_cumu,
coalesce(pcr_tests_no_result, 0) as pcr_tests_no_result,
coalesce(pcr_tests_no_result_cumu, 0) as pcr_tests_no_result_cumu,
coalesce(pcr_tests_ordered, 0) as pcr_tests_ordered,
coalesce(pcr_tests_ordered_cumu, 0) as pcr_tests_ordered_cumu,
coalesce(pcr_test_results_no_order, 0) as pcr_test_results_no_order,
coalesce(pcr_test_results_no_order_cumu, 0) as pcr_test_results_no_order_cumu
FROM covid19_report.tst_covid_demog_groups T1
LEFT JOIN covid19_report.tst_covid_pcr_testing T2 on (T1.week_end_date = T2.week_end_date and T1.week_number = T2.week_number and T1.zip5 = T2.zip5 and T1.age_group = T2.age_group and T1.gender = T2.gender and T1.race_ethnicity = T2.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_cumu_pcr_testing T3 on (T1.week_end_date = T3.week_end_date and T1.week_number = T3.week_number and T1.zip5 = T3.zip5 and T1.age_group = T3.age_group and T1.gender = T3.gender and T1.race_ethnicity = T3.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_cumu_tests_no_res T4 on (T1.week_end_date = T4.week_end_date and T1.week_number = T4.week_number and T1.zip5 = T4.zip5 and T1.age_group = T4.age_group and T1.gender = T4.gender and T1.race_ethnicity = T4.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_wk_tests_no_res T5 on (T1.week_end_date = T5.week_end_date and T1.week_number = T5.week_number and T1.zip5 = T5.zip5 and T1.age_group = T5.age_group and T1.gender = T5.gender and T1.race_ethnicity = T5.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_pcr_orders T6 on (T1.week_end_date = T6.week_end_date and T1.week_number = T6.week_number and T1.zip5 = T6.zip5 and T1.age_group = T6.age_group and T1.gender = T6.gender and T1.race_ethnicity = T6.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_cumu_pcr_orders T7 on (T1.week_end_date = T7.week_end_date and T1.week_number = T7.week_number and T1.zip5 = T7.zip5 and T1.age_group = T7.age_group and T1.gender = T7.gender and T1.race_ethnicity = T7.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_pcr_testing_no_order T8 on (T1.week_end_date = T8.week_end_date and T1.week_number = T8.week_number and T1.zip5 = T8.zip5 and T1.age_group = T8.age_group and T1.gender = T8.gender and T1.race_ethnicity = T8.race_ethnicity)
LEFT JOIN covid19_report.tst_covid_cumu_pcr_testing_no_order T9 on (T1.week_end_date = T9.week_end_date and T1.week_number = T9.week_number and T1.zip5 = T9.zip5 and T1.age_group = T9.age_group and T1.gender = T9.gender and T1.race_ethnicity = T9.race_ethnicity)
order by T1.week_end_date, T1.week_number, T1.age_group, T1.gender, T1.zip5, T1.race_ethnicity;


-- ADD IN THE "ALL" VALUES
CREATE TABLE covid19_report.tst_covid_output AS
SELECT *, '9999' as position
FROM covid19_report.tst_covid_tot_full_strat
UNION
SELECT 
week_end_date, week_number, 'ALL' as zip5, 'ALL' as age_group, 'ALL' as gender, 'ALL' as race_ethnicity,
sum(pcr_test_results) wk_pcr_test_results, 
sum(pcr_pos_tests) wk_pcr_pos_tests, 
sum(pcr_neg_tests) wk_pcr_neg_tests,
sum(pcr_ind_tests) wk_pcr_ind_tests,
sum(pcr_test_results_cumu) cumu_pcr_test_results,
sum(pcr_pos_tests_cumu) cumu_pcr_pos_tests,
sum(pcr_neg_tests_cumu) cumu_pcr_neg_tests,
sum(pcr_ind_tests_cumu) cumu_pcr_ind_tests,
sum(pcr_tests_no_result) wk_pcr_tests_no_result,
sum(pcr_tests_no_result_cumu) cumu_pcr_tests_no_result,
sum(pcr_tests_ordered) wk_pcr_tests_ordered,
sum(pcr_tests_ordered_cumu) cumu_pcr_tests_ordered,
sum(pcr_test_results_no_order) wk_pcr_test_results_no_order,
sum(pcr_test_results_no_order_cumu) cumu_pcr_test_results_no_order,
'1' as position
FROM covid19_report.tst_covid_tot_full_strat
GROUP BY week_end_date, week_number
UNION
SELECT 
week_end_date, week_number, 'ALL' as zip5, 'ALL' as age_group, gender, 'ALL' as race_ethnicity,
sum(pcr_test_results) wk_pcr_test_results, 
sum(pcr_pos_tests) wk_pcr_pos_tests, 
sum(pcr_neg_tests) wk_pcr_neg_tests,
sum(pcr_ind_tests) wk_pcr_ind_tests,
sum(pcr_test_results_cumu) cumu_pcr_test_results,
sum(pcr_pos_tests_cumu) cumu_pcr_pos_tests,
sum(pcr_neg_tests_cumu) cumu_pcr_neg_tests,
sum(pcr_ind_tests_cumu) cumu_pcr_ind_tests,
sum(pcr_tests_no_result) wk_pcr_tests_no_result,
sum(pcr_tests_no_result_cumu) cumu_pcr_tests_no_result,
sum(pcr_tests_ordered) wk_pcr_tests_ordered,
sum(pcr_tests_ordered_cumu) cumu_pcr_tests_ordered,
sum(pcr_test_results_no_order) wk_pcr_test_results_no_order,
sum(pcr_test_results_no_order_cumu) cumu_pcr_test_results_no_order,
'2' as position
FROM covid19_report.tst_covid_tot_full_strat
GROUP BY week_end_date, week_number, gender
UNION
SELECT 
week_end_date, week_number, 'ALL' as zip5, age_group, 'ALL' as gender, 'ALL' as race_ethnicity,
sum(pcr_test_results) wk_pcr_test_results, 
sum(pcr_pos_tests) wk_pcr_pos_tests, 
sum(pcr_neg_tests) wk_pcr_neg_tests,
sum(pcr_ind_tests) wk_pcr_ind_tests,
sum(pcr_test_results_cumu) cumu_pcr_test_results,
sum(pcr_pos_tests_cumu) cumu_pcr_pos_tests,
sum(pcr_neg_tests_cumu) cumu_pcr_neg_tests,
sum(pcr_ind_tests_cumu) cumu_pcr_ind_tests,
sum(pcr_tests_no_result) wk_pcr_tests_no_result,
sum(pcr_tests_no_result_cumu) cumu_pcr_tests_no_result,
sum(pcr_tests_ordered) wk_pcr_tests_ordered,
sum(pcr_tests_ordered_cumu) cumu_pcr_tests_ordered,
sum(pcr_test_results_no_order) wk_pcr_test_results_no_order,
sum(pcr_test_results_no_order_cumu) cumu_pcr_test_results_no_order,
'3' as position
FROM covid19_report.tst_covid_tot_full_strat
GROUP BY week_end_date, week_number, age_group
UNION
SELECT 
week_end_date, week_number, 'ALL' as zip5, 'ALL' as age_group, 'ALL' as gender, race_ethnicity,
sum(pcr_test_results) wk_pcr_test_results, 
sum(pcr_pos_tests) wk_pcr_pos_tests, 
sum(pcr_neg_tests) wk_pcr_neg_tests,
sum(pcr_ind_tests) wk_pcr_ind_tests,
sum(pcr_test_results_cumu) cumu_pcr_test_results,
sum(pcr_pos_tests_cumu) cumu_pcr_pos_tests,
sum(pcr_neg_tests_cumu) cumu_pcr_neg_tests,
sum(pcr_ind_tests_cumu) cumu_pcr_ind_tests,
sum(pcr_tests_no_result) wk_pcr_tests_no_result,
sum(pcr_tests_no_result_cumu) cumu_pcr_tests_no_result,
sum(pcr_tests_ordered) wk_pcr_tests_ordered,
sum(pcr_tests_ordered_cumu) cumu_pcr_tests_ordered,
sum(pcr_test_results_no_order) wk_pcr_test_results_no_order,
sum(pcr_test_results_no_order_cumu) cumu_pcr_test_results_no_order,
'4' as position
FROM covid19_report.tst_covid_tot_full_strat
GROUP BY week_end_date, week_number, race_ethnicity
UNION
SELECT 
week_end_date, week_number, zip5::text, 'ALL' as age_group, 'ALL' as gender, 'ALL' as race_ethnicity,
sum(pcr_test_results) wk_pcr_test_results, 
sum(pcr_pos_tests) wk_pcr_pos_tests, 
sum(pcr_neg_tests) wk_pcr_neg_tests,
sum(pcr_ind_tests) wk_pcr_ind_tests,
sum(pcr_test_results_cumu) cumu_pcr_test_results,
sum(pcr_pos_tests_cumu) cumu_pcr_pos_tests,
sum(pcr_neg_tests_cumu) cumu_pcr_neg_tests,
sum(pcr_ind_tests_cumu) cumu_pcr_ind_tests,
sum(pcr_tests_no_result) wk_pcr_tests_no_result,
sum(pcr_tests_no_result_cumu) cumu_pcr_tests_no_result,
sum(pcr_tests_ordered) wk_pcr_tests_ordered,
sum(pcr_tests_ordered_cumu) cumu_pcr_tests_ordered,
sum(pcr_test_results_no_order) wk_pcr_test_results_no_order,
sum(pcr_test_results_no_order_cumu) cumu_pcr_test_results_no_order,
'5' as position
FROM covid19_report.tst_covid_tot_full_strat
GROUP BY week_end_date, week_number, zip5
ORDER BY position, age_group, gender, race_ethnicity, zip5;


CREATE TABLE covid19_report.tst_covid_output_formatted AS
SELECT
week_end_date,
week_number,
zip5,
age_group,
gender,
race_ethnicity,
pcr_test_results wk_pcr_test_results,
pcr_pos_tests wk_pcr_pos_tests, 
pcr_neg_tests wk_pcr_neg_tests,
pcr_ind_tests wk_pcr_ind_tests,
pcr_tests_no_result wk_pcr_tests_no_result,
pcr_tests_ordered wk_pcr_tests_ordered,
pcr_test_results_no_order wk_pcr_test_results_no_order,
pcr_test_results_cumu cumu_pcr_test_results,
pcr_pos_tests_cumu cumu_pcr_pos_tests,
pcr_neg_tests_cumu cumu_pcr_neg_tests,
pcr_ind_tests_cumu cumu_pcr_ind_tests,
pcr_tests_no_result_cumu cumu_pcr_tests_no_result,
pcr_tests_ordered_cumu cumu_pcr_tests_ordered,
pcr_test_results_no_order_cumu cumu_pcr_test_results_no_order
FROM covid19_report.tst_covid_output;

