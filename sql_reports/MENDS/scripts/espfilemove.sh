#!/bin/bash
## AC- Move and rename files from dmadmin home to /srv/esp/data/epic

DATA_DIR=/srv/esp/data/epic/incoming
ARCH_DIR=/srv/esp/data/epic/archive/
#INITIAL_DIR=/srv/esp/data/epic/working
INITIAL_DIR=/home/dmadmin
runtime=`date +%Y%m%d_%H%M`
rundate=`date +%m%d%Y`
LOGFILE=/srv/esp/logs/espfile_move.log.$runtime
#LOGFILE=/srv/esp/scripts/espfile_move.log.$runtime

exec 5>&1 6>&2 >>$LOGFILE 2>&1
echo "\n\n\nStarting script"
echo "Incoming dir is $INITIAL_DIR"
echo "Destination dir is $DATA_DIR"

oldFiles=()
while IFS= read -r -d $'\0' foundFile; do
    oldFiles+=("$foundFile")
done < <(find "$INITIAL_DIR" -maxdepth 1 -type f -name "Rep*" -print0 2> /dev/null)


if [[ ${#oldFiles[@]} -ne 0 ]]; then
    for file in "${oldFiles[@]}"; do
     
     case $file in
	*Member*)
		echo "Member file"
		cp $file $DATA_DIR/epicmem.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicmem.esp.$rundate 
		sed -i "s/\"//g" $DATA_DIR/epicmem.esp.$rundate
		;;
	*Visit*)
		echo "Visit file"
		cp $file $DATA_DIR/epicvis.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicvis.esp.$rundate 
		sed -i "s/\"//g" $DATA_DIR/epicvis.esp.$rundate
		;;
	*Immunization*)
		echo "Immunization file!"
		cp $file $DATA_DIR/epicimm.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicimm.esp.$rundate 
		sed -i "s/\"//g" $DATA_DIR/epicimm.esp.$rundate
		;;
	*Lab*)
		echo "LabResult file!"
		cp $file $DATA_DIR/epicres.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicres.esp.$rundate 
		sed -i "s/\"//g" $DATA_DIR/epicres.esp.$rundate
		;;
	*Medication*)
		echo "Medication file!"
		cp $file $DATA_DIR/epicmed.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicmed.esp.$rundate 
		##sed -i "s/\"//g" $DATA_DIR/epicmed.esp.$rundate
		;;
	*Social*)
		echo "Social file!"
		cp $file $DATA_DIR/epicsoc.esp.$rundate
		mv $file $ARCH_DIR
		sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*)
		echo "Unknown File Found!!"
		;;
     esac
    done
fi

echo "Exiting script \n\n"
exit

