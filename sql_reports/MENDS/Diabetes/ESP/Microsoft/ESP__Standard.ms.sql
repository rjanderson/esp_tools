-- Step 1
SELECT patient_id, date, condition 
INTO #esp_diab 
FROM nodis_case 
WHERE condition IN ('diabetes:type-1','diabetes:type-2');

-- Step 2
SELECT DISTINCT patient_id, date, name 
INTO #hef_a1c_gte
FROM hef_event 
WHERE name = 'lx:a1c:threshold:gte:6.5' AND date < '2022-12-31';

-- Step 3
SELECT DISTINCT patient_id, date, 'lx:a1c:threshold:lt:6.5' AS threshold
INTO #hef_a1c_lt
FROM emr_labresult t0
JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE result_float < 6.5 AND date < '2022-12-31';

-- Step 4
SELECT * 
INTO #hef_a1c
FROM (
    SELECT * FROM #hef_a1c_gte
    UNION 
    SELECT * FROM #hef_a1c_lt
) AS t0;

-- Step 5
SELECT patient_id, date, name
INTO #last_hef_a1c
FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC, name) AS rn
    FROM #hef_a1c
) AS t
WHERE rn = 1;

-- Step 6
SELECT id AS patient_id, DATEDIFF(YEAR, date_of_birth, '2023-12-31') AS AGE
INTO #pat_w_age
FROM emr_patient
WHERE DATEDIFF(YEAR, date_of_birth, '2023-12-31') BETWEEN 4 AND 85
    AND EXISTS (SELECT 1 FROM gen_pop_tools.clin_enc ce 
                WHERE ce.patient_id = emr_patient.id AND ce.date BETWEEN '2020-12-31' AND '2022-12-31');

-- Step 7
SELECT t0.patient_id, condition, ISNULL(name, 'not tested') AS control
INTO #diab_assessed_control
FROM #esp_diab t0
LEFT JOIN #last_hef_a1c t1 ON t1.patient_id = t0.patient_id AND t0.date <= t1.date;

-- Step 8
SELECT COUNT(*) AS counts, condition, ISNULL(control, 'not diabetic') AS control
FROM #pat_w_age t0
LEFT JOIN #diab_assessed_control t1 ON t1.patient_id = t0.patient_id 
GROUP BY condition, ISNULL(control, 'not diabetic');