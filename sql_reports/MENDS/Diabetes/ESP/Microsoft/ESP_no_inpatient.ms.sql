/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 8/03/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Added row labels using CTID()
	V7				RJA			 Added filtering to show total number of non-diabetic patients

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/


/*
any(event for event in events if (event.name=='rx:insulin' and not 
                                      Timespan.objects.filter(name = 'pregnancy', patient = event.patient, 
                                                              start_date__lte = event.date, end_date__gte = event.date).exists()))
*/

/**********************************
* Step 0: Identify inpatient days *
**********************************/

if object_id('tempdb..#inpat_days') IS NOT NULL
	DROP TABLE #inpat_days;
if object_id('tempdb..#a1c_results') IS NOT NULL
	DROP TABLE #a1c_results;
if object_id('tempdb..#fasting_glucose') IS NOT NULL
	DROP TABLE #fasting_glucose;
if object_id('tempdb..#random_glucose') IS NOT NULL
	DROP TABLE #random_glucose;
if object_id('tempdb..#medication_disp') IS NOT NULL
	DROP TABLE #medication_disp;
if object_id('tempdb..#outpatient_code') IS NOT NULL
	DROP TABLE #outpatient_code;
if object_id('tempdb..#insulin_disp') IS NOT NULL
	DROP TABLE #insulin_disp;
if object_id('tempdb..#frank_diabetes') IS NOT NULL
	DROP TABLE #frank_diabetes;
if object_id('tempdb..#cpeptide') IS NOT NULL
	DROP TABLE #cpeptide
IF OBJECT_ID('tempdb..#ab1') IS NOT NULL
		DROP TABLE #ab1;
IF OBJECT_ID('tempdb..#acetone') IS NOT NULL
		DROP TABLE #acetone;
IF OBJECT_ID('tempdb..#fiftypercent_nohypoglycemic') IS NOT NULL
		DROP TABLE #fiftypercent_nohypoglycemic;
IF OBJECT_ID('tempdb..#FRANK_Summary') IS NOT NULL
		DROP TABLE #FRANK_Summary;
IF OBJECT_ID('tempdb..#fiftypercent_glucagon') IS NOT NULL
		DROP TABLE #fiftypercent_glucagon;
IF OBJECT_ID('tempdb..#Type1_Summary') IS NOT NULL
		DROP TABLE #Type1_Summary;
IF OBJECT_ID('tempdb..#Type2_Summary') IS NOT NULL
		DROP TABLE #Type2_Summary;
IF OBJECT_ID('tempdb..#final_counts') IS NOT NULL
		DROP TABLE #final_counts;
IF OBJECT_ID('tempdb..#diabetes_results') IS NOT NULL
		DROP TABLE #diabetes_results;

SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO #inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';

/***************************************
* Step 1:  Classify as Frank Diabetes  *
***************************************/

--a1c where glucose >= 6.5
--uncomment 'l.result_float < 6.5' to obtain results for patients where a1c is under 6.5

select 
	count(*) as count_column,
	patient_id,
	date,
	'a1c' as event_type
into #a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5'
AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
group by 
	patient_id,
	date;


--fasting plasma glucose > 126 mg

select 
	count(*) as count_column,
	patient_id,
	date,
	'fasting glucose' as event_type
into #fasting_glucose
from
	hef_event h
where 
	name = 'lx:glucose-fasting:threshold:gte:126'
AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
group by
	patient_id,
	date;


--random plasma glucose > 200 mg

select 
	count(*) as count_column,
	patient_id,
	date,
	'random glucose' as event_type
into #random_glucose
from
	hef_event h
where 
	name = 'lx:glucose-random:threshold:gte:200'
AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
group by
	patient_id,
	date;


--outpatient diagnosis code for diabetes 

SELECT 
    DISTINCT
	enc.patient_id, 
	enc.date,
	codes.dx_code_id as code,
	'dx' as event_type
INTO #outpatient_code
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
JOIN 
	static_dx_code AS codeList 
ON
	codes.dx_code_id = codeList.combotypecode
WHERE 
	(codes.dx_code_id like 'icd9:250%' 
	OR 
	codes.dx_code_id like 'icd9:357.2' 
	OR 
	codes.dx_code_id like 'icd9:366.41'
	OR
	codes.dx_code_id like 'icd9:362.01'
	OR
	codes.dx_code_id like 'icd9:362.02'
	OR
	codes.dx_code_id like 'icd9:362.03'
	OR
	codes.dx_code_id like 'icd9:362.04'
	OR
	codes.dx_code_id like 'icd9:362.05'
	OR
	codes.dx_code_id like 'icd9:362.06'
	OR
	codes.dx_code_id like 'icd9:362.07'
	OR
	codes.dx_code_id like 'icd10:E10.%'
	OR
	codes.dx_code_id like 'icd10:E11.%'
	OR
	codes.dx_code_id like 'icd10:E14.%');
	
--any dispensation of antihyperglycemic medication
SELECT DISTINCT
	patient_id, 
	date,
	'rx' as event_type
INTO #medication_disp
FROM 
	hef_event AS pre 
WHERE 
	lower(name)
	IN 
	('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
);


--Insulin outside of pregnancy

SELECT
	e.patient_id,
	e.date,
	'insulin_preg' as event_type
INTO 
	#insulin_disp
FROM
	hef_event e
JOIN 
	hef_timespan t 
ON 
	e.id = t.id 
WHERE e.name like '%rx:insulin%' 
	AND 
	t.name = 'pregnancy';


--2 HbA1c tests OR 2 diagnoses OR 2 filled prescriptions, but only if the 2 events occurred on separate days
WITH t1
AS
(SELECT 
 	patient_id, 
 	MIN(date) AS date 
 FROM 
 	#a1c_results
 GROUP BY 
 	patient_id),
t2 AS
(SELECT 
 	patient_id, 
 	MIN(date) AS date 
 FROM 
 	#fasting_glucose 
 GROUP BY 
 	patient_id),
t0_3 AS
(SELECT 
 	patient_id, 
 	row_number() OVER(PARTITION BY patient_id ORDER BY date) AS row_n, 
 	date 
 FROM 
 	#random_glucose),
t3 AS
(SELECT 
 	patient_id, 
 	date 
 FROM 
 	t0_3 
 WHERE 
 	row_n = 2),
t4 AS
(SELECT 
 	patient_id, 
 	date 
 FROM 
 	#insulin_disp),
t0_5 AS
(SELECT 
 	patient_id, 
 	row_number() OVER(partition by patient_id ORDER BY date) as row_n, 
 	date
 FROM 
 	#outpatient_code),
t5 AS
(SELECT 
 	patient_id, 
 	date 
 FROM
 	t0_5 
 WHERE 
 	row_n = 2),
t6 AS
(
 SELECT 
	patient_id, 
	date 
 FROM 
	#medication_disp),
t7 AS
(SELECT 
 	patient_id, 
 	date 
 FROM 
 	t1
 UNION
 SELECT 
 	patient_id, 
 	date 
 FROM 
 	t2
 UNION
 SELECT 
 	patient_id, 
 	date 
 FROM t3
UNION
SELECT 
 	patient_id, 
 	date 
 FROM 
 	t4
 UNION
 SELECT 
 	patient_id, date 
 FROM 
 	t5
 UNION
 SELECT 
 	patient_id, date 
 FROM 
 	t6
)
SELECT 
	patient_id, 
	min(date) AS frank_date 
INTO #frank_diabetes
FROM 
	t7 
GROUP BY 
	patient_id;

/**************************************************
* STEP 2: Differentiate Type 1 or Type 2 Diabetes *
**************************************************/

--C-peptide test <0.8

SELECT 
	patient_id, 
	date, 
	result_float, 
	test_name 
INTO #cpeptide
FROM 
	emr_labresult l 
JOIN 
	conf_labtestmap ltm 
ON 
	ltm.native_code = l.native_code  
WHERE 
	ltm.test_name like '%peptide%' 
	AND l.result_float < .8 
AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);
	
--Diabetes auto-antibodies positive
	--> Antibodies #1:  GAD-65 AB > 1.0
	IF OBJECT_ID('tempdb..#ab1') IS NOT NULL
		DROP TABLE #ab1;

	CREATE TABLE #ab1
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		event_type VARCHAR(50)
	);

	INSERT INTO #ab1
	(
		patient_id,
		date,
		result_float,
		event_type
	)
	SELECT
		l.patient_id,
		l.date,
		l.result_float,
		'ab1'
	FROM
		emr_labresult l
	JOIN 
		conf_labtestmap ltm
		ON ltm.native_code = l.native_code
	WHERE
		ltm.test_name LIKE '%GAD%'
		AND l.result_float > 1.0
		AND NOT EXISTS
		(
			SELECT 1
			FROM #inpat_days as indays
			WHERE indays.patient_id = l.patient_id
				AND l.date BETWEEN indays.start_date AND indays.end_date
		);
	--> Antibodies #2:  ICA-512 Autoantibodies > 0.8
	IF OBJECT_ID('tempdb..#ab2') IS NOT NULL
		DROP TABLE #ab2;

	CREATE TABLE #ab2
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		event_type VARCHAR(50)
	);

	INSERT INTO #ab2
	(
		patient_id,
		date,
		result_float,
		event_type
	)
	SELECT
		l.patient_id,
		l.date,
		l.result_float,
		'ab2'
	FROM
		emr_labresult l
	JOIN 
		conf_labtestmap ltm
		ON ltm.native_code = l.native_code
	WHERE
		LOWER(ltm.test_name) LIKE '%insulin%'
		AND l.result_float >= .8
		AND NOT EXISTS
		(
			SELECT 1
			FROM #inpat_days as indays
			WHERE indays.patient_id = l.patient_id
				AND l.date BETWEEN indays.start_date AND indays.end_date
		);
	
	--> Antibodies #3:  Query to separate numeric values from titre string and return true if titre ratio is greater than 1:4
	IF OBJECT_ID('tempdb..#ab3') IS NOT NULL
		DROP TABLE #ab3;
	
	CREATE TABLE #ab3
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		result_string VARCHAR(50),
		event_type VARCHAR(50)
	);

	INSERT INTO #ab3
	(
		patient_id,
		date,
		result_float,
		result_string,
		event_type
	)
	SELECT 
		l.patient_id, 
		l.date, 
		l.result_float,
		l.result_string,
		'ab3'
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'islet-cell-antibody' 
		AND
		l.result_string IS NOT NULL;
	/*AND
		(
		CASE 
			WHEN UPPER(l.result_string) = 'POSITIVE' THEN 1
			WHEN POSITION(':' IN l.result_string) > 0 
				AND SUBSTRING(l.result_string, 1, POSITION(':' IN l.result_string)-1) SIMILAR TO '[0-9]+'
				AND CAST(SUBSTRING(l.result_string, 1, POSITION(':' IN l.result_string)-1) AS INT) = 1 
				AND CAST(SUBSTRING(l.result_string, POSITION(':' IN l.result_string)+1, LENGTH(l.result_string)) AS INT) >= 4 THEN 1
			WHEN l.result_float > .8 THEN 1	
			WHEN POSITION('<' IN l.result_string) > 0 THEN 0
			ELSE 0
	END
	) = 1*/

	--> Antibodies #4:  Islet Cell Antibody Titer >= 1.25
	IF OBJECT_ID('tempdb..#ab4') IS NOT NULL
		DROP TABLE #ab4;
	
	CREATE TABLE #ab4
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		test_name VARCHAR(100),
		event_type VARCHAR(3)
	);
	
	INSERT INTO #ab4
	(
		patient_id,
		date,
		result_float,
		test_name,
		event_type
	) 
	SELECT 
		l.patient_id, 
		l.date, 
		l.result_float, 
		ltm.test_name,
		'ab4'
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'islet-cell-antibody' 
	AND 
		l.result_float <= 1.25;
		
	--> Antibodies #5:  Insulin Auto Antibody >= .4
	IF OBJECT_ID('tempdb..#ab5') IS NOT NULL
		DROP TABLE #ab5;

	CREATE TABLE #ab5
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		event_type VARCHAR(3)
	);
	
	INSERT INTO #ab5
	(
		patient_id,
		date,
		result_float,
		event_type
	) 
	SELECT 
		l.patient_id, 
		l.date, 
		l.result_float,
		'ab5'
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'insulin-antibody' 
	AND 
		l.result_float >= 0.4
	AND NOT EXISTS
		(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);
	-->Antibodies #6:  Insulin AB >= .8
	IF OBJECT_ID('tempdb..#ab6') IS NOT NULL
		DROP TABLE #ab6;

	CREATE TABLE #ab6
	(
		patient_id INT,
		date DATE,
		result_float FLOAT,
		event_type VARCHAR(3)
	);
	
	INSERT INTO #ab6
	(
		patient_id,
		date,
		result_float,
		event_type
	) 
	SELECT 
		l.patient_id, 
		l.date, 
		l.result_float,
		'ab6'
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name like '%insulin%' 
	AND 
		l.result_float >= 0.8
	AND NOT EXISTS
		(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);

--Prescription for URINE ACETONE TEST STRIPS (search on keyword: ACETONE)

SELECT 
	patient_id, 
	name, 
	start_date, 
	end_date
INTO #acetone
FROM 
	emr_prescription 
AS 
	pre 
JOIN 
	static_drugsynonym 
AS 
	syn 
ON 
	LOWER(pre.name) 
	LIKE CONCAT('%', LOWER(syn.other_name), '%')
WHERE 
	LOWER(syn.generic_name) 
		LIKE '%acetone%' 
	OR 
	LOWER(syn.generic_name) 
		LIKE 'ketone'
AND 
NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays 
	 WHERE indays.patient_id = pre.patient_id 
	 AND pre.date BETWEEN indays.start_date 
	 AND indays.end_date);

--50% ratio and glucagon prescription

SELECT DISTINCT
	enc.patient_id, 
	enc.date,
	'fiftypercent_glucagon' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO #fiftypercent_glucagon
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
WHERE 
	(
		(codes.dx_code_id like 'ICD10:E10.%'
		OR
		codes.dx_code_id like 'ICD10:E11.%')
	
	AND enc.patient_id IN(
		SELECT
			patient_id
		FROM emr_prescription
		WHERE LOWER(name) LIKE
			'%glucagon%')
	AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = enc.patient_id AND enc.date BETWEEN indays.start_date AND indays.end_date)
	)
GROUP BY
	enc.patient_id,
	enc.date
HAVING
	(SUM(CASE WHEN codes.dx_code_id like 'E10' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0
	AND
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;

--50% ratio and no oral hypoglycemic

SELECT DISTINCT 
	hef.patient_id,
	hef.date,
	'fiftypercent_nohypo' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO #fiftypercent_nohypoglycemic
FROM 
	emr_encounter AS enc
JOIN 
	hef_event AS hef 
ON 
	enc.patient_id = hef.patient_id
JOIN
	emr_encounter_dx_codes as codes
ON
	enc.id = codes.encounter_id
WHERE 
	hef.name not in ('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
)
	AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = hef.patient_id AND hef.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY 
	hef.patient_id,
	hef.date
HAVING 
	(SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0 
	AND 
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;
	
/***************
* Summary View *
***************/

-- Add patient_id and date for pre diabetic patients into FRANK_Summary temp table
SELECT 
	patient_id, 
	date
INTO #FRANK_Summary
FROM 
	#a1c_results
UNION
SELECT
	patient_id,
	date
FROM 
	#fasting_glucose
UNION
SELECT
	patient_id,
	date
FROM
	#random_glucose
UNION
SELECT 
	o.patient_id,
	o.date
FROM
	#outpatient_code o
UNION
SELECT
	f.patient_id,
	f.frank_date
FROM
	#frank_diabetes f;

--If patient is prediabetic and meets any one of the following criteria, they are considered type 1 diabetic
SELECT 
	f.patient_id, 
	f.date
INTO #Type1_Summary
FROM
	#cpeptide c
JOIN
	#FRANK_Summary f
ON 
	c.patient_id = f.patient_id
UNION
SELECT
	a1.patient_id,
	a1.date
FROM
	#ab1 a1
JOIN 
	#FRANK_Summary f
ON
	a1.patient_id = f.patient_id
UNION
SELECT
	a2.patient_id,
	a2.date
FROM
	#ab2 a2
JOIN 
	#FRANK_Summary f
ON
	a2.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	f.date
FROM
	#acetone a
JOIN
	#FRANK_Summary f
ON
	a.patient_id = f.patient_id
UNION
SELECT
	gluc.patient_id,
	gluc.date
FROM
	#fiftypercent_glucagon gluc
JOIN 
	#FRANK_Summary f
ON
	gluc.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	f.date
FROM
	#fiftypercent_nohypoglycemic hyp
JOIN 
	#FRANK_Summary f
ON
	hyp.patient_id = f.patient_id;

--If the patient is considered Frank Diabetic and does NOT meet any of the criteria for Type 1 Diabetes then they are Type2
CREATE TABLE #diabetes_results
(
	patient_id INT,
	diabetes_type NVARCHAR(50),
	diagnosis_date DATETIME
);

INSERT INTO #diabetes_results (patient_id, diabetes_type, diagnosis_date)
SELECT 
	f.patient_id,
	'type 2',
	CAST(f.date AS DATETIME)
FROM
	#FRANK_Summary f
JOIN
	emr_patient p
ON
	f.patient_id = p.id
WHERE
	patient_id NOT IN (SELECT patient_id FROM #type1_Summary)
	AND 
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) < 85
	AND
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) > 4;

-- Create a separate table to hold the information for type 1 diabetics

INSERT INTO #diabetes_results(patient_id, diabetes_type, diagnosis_date)
SELECT 
	t1.patient_id,
	'type 1',
	cast(t1.date as datetime)
FROM 
	#type1_Summary t1
JOIN
	emr_patient p
ON
	t1.patient_id = p.id
WHERE 
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) < 85
	AND
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) > 4;


--Apply A1C filtering to show Patients Under A1C threshold and Over A1C threshold.  Also show population of --non-diabetic
-- Step 1
SELECT patient_id, date, condition 
INTO #esp_diab 
FROM #diabetes_results
WHERE condition IN ('diabetes:type-1','diabetes:type-2');

-- Step 2
SELECT DISTINCT patient_id, date, name 
INTO #hef_a1c_gte
FROM hef_event 
WHERE name = 'lx:a1c:threshold:gte:6.5' AND date < '2022-12-31';

-- Step 3
SELECT DISTINCT patient_id, date, 'lx:a1c:threshold:lt:6.5' AS threshold
INTO #hef_a1c_lt
FROM emr_labresult t0
JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE result_float < 6.5 AND date < '2022-12-31';

-- Step 4
SELECT * 
INTO #hef_a1c
FROM (
    SELECT * FROM #hef_a1c_gte
    UNION 
    SELECT * FROM #hef_a1c_lt
) AS t0;

-- Step 5
SELECT patient_id, date, name
INTO #last_hef_a1c
FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC, name) AS rn
    FROM #hef_a1c
) AS t
WHERE rn = 1;

-- Step 6
SELECT id AS patient_id, DATEDIFF(YEAR, date_of_birth, '2023-12-31') AS AGE
INTO #pat_w_age
FROM emr_patient
WHERE DATEDIFF(YEAR, date_of_birth, '2023-12-31') BETWEEN 4 AND 85
    AND EXISTS (SELECT 1 FROM gen_pop_tools.clin_enc ce 
                WHERE ce.patient_id = emr_patient.id AND ce.date BETWEEN '2020-12-31' AND '2022-12-31');

-- Step 7
SELECT t0.patient_id, condition, ISNULL(name, 'not tested') AS control
INTO #diab_assessed_control
FROM #esp_diab t0
LEFT JOIN #last_hef_a1c t1 ON t1.patient_id = t0.patient_id AND t0.date <= t1.date;

-- Step 8
SELECT COUNT(*) AS counts, condition, ISNULL(control, 'not diabetic') AS control
FROM #pat_w_age t0
LEFT JOIN #diab_assessed_control t1 ON t1.patient_id = t0.patient_id 
GROUP BY condition, ISNULL(control, 'not diabetic');
