/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.08
Last Revision: 9/07/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Added row labels
	V7				RJA			 Added filtering to show total number of non-diabetic patients
	V8				RJA			 Updated inpatient/no-inpatient logic to no longer be inverted; reworked fiftypercent_glucagon
								 and fiftypercent_nohypoglycemic code.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

/**********************************
* Step 0: Identify inpatient days *
**********************************/
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS a1c_results;
	DROP TABLE IF EXISTS fasting_glucose;
	DROP TABLE IF EXISTS random_glucose;
	DROP TABLE IF EXISTS medication_disp;
	DROP TABLE IF EXISTS outpatient_code;
	DROP TABLE IF EXISTS frank_diabetes;
	DROP TABLE IF EXISTS ab1;
 	DROP TABLE IF EXISTS ab2;
	DROP TABLE IF EXISTS ab3;
	DROP TABLE IF EXISTS acetone;
	DROP TABLE IF EXISTS fiftypercent_nohypoglycemic;
	DROP TABLE IF EXISTS Frank_Diabetes;
	DROP TABLE IF EXISTS fiftypercent_glucagon;
	DROP TABLE IF EXISTS Type1_Summary;
	DROP TABLE IF EXISTS Type2_Summary;
	DROP TABLE IF EXISTS final_counts;
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS insulin_disp;
	DROP TABLE IF EXISTS cpeptide;
	DROP TABLE IF EXISTS ab4;
	DROP TABLE IF EXISTS ab5;
	DROP TABLE IF EXISTS ab6;

SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO TEMPORARY 
	inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';

/***************************************
* Step 1:  Classify as Frank Diabetes  *
***************************************/

--a1c where glucose >= 6.5
--uncomment 'l.result_float < 6.5' to obtain results for patients where a1c is under 6.5

select 
	count(*),
	patient_id,
	date,
	'a1c' as event_type
into temporary
	a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
group by 
	patient_id,
	date;

	
--fasting plasma glucose concentration >= 126 mg/dl
select 
	count(*),
	patient_id,
	date,
	'fasting glucose' as event_type
into temporary
	fasting_glucose
from
	hef_event h 
WHERE 
	name = 'lx:glucose-fasting:threshold:gte:126'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY
	patient_id,
	date;

--random plasma glucose concentration >= 200 mg/dl
select 
	count(*),
	patient_id,
	date,
	'random glucose' as event_type
into temporary
	random_glucose
from
	hef_event h 
where 
	name = 'lx:glucose-random:threshold:gte:200'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
group by
	patient_id,
	date;

--outpatient diagnosis code for diabetes	
SELECT 
    DISTINCT
	enc.patient_id, 
	enc.date,
	'dx' as event_type
INTO TEMPORARY
	outpatient_code
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
JOIN 
	static_dx_code AS codeList 
ON
	codes.dx_code_id = codeList.combotypecode
WHERE 
	(codes.dx_code_id LIKE 'ICD9:250%' 
	OR 
	codes.dx_code_id = 'ICD9:357.2%' 
	OR 
	codes.dx_code_id = 'ICD9:366.41'
	OR
	codes.dx_code_id = 'ICD9:362.01'
	OR
	codes.dx_code_id = 'ICD9:362.02'
	OR
	codes.dx_code_id = 'ICD9:362.03'
	OR
	codes.dx_code_id = 'ICD9:362.04'
	OR
	codes.dx_code_id = 'ICD9:362.05'
	OR
	codes.dx_code_id = 'ICD9:362.06'
	OR
	codes.dx_code_id = 'ICD9:362.07'
	OR
	codes.dx_code_id like 'ICD10:E10.%'
	OR
	codes.dx_code_id like 'ICD10:E11.%'
	OR
	codes.dx_code_id like 'ICD10:E14.%'
	)
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = enc.patient_id AND enc.date BETWEEN indays.start_date AND indays.end_date);

--any dispensation of antihyperglycemic medication
SELECT DISTINCT
	patient_id, 
	date,
	'rx' as event_type
INTO TEMPORARY
	medication_disp
FROM 
	hef_event AS h 
WHERE 
	lower(name)
	IN 
	('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
	)
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

--Insulin outside of pregnancy

SELECT 
	patient_id, 
	name, 
	start_date, 
	end_date 
INTO TEMPORARY
	insulin_disp
FROM 
	emr_prescription AS pre 
JOIN 
	static_drugsynonym AS syn 
ON 
	lower(pre.name) LIKE CONCAT('%', lower(syn.other_name), '%')
WHERE 
	lower(syn.generic_name) = 'insulin' 
	AND NOT EXISTS 
	(SELECT NULL FROM hef_timespan AS ts 
	 	WHERE ts.patient_id = pre.patient_id 
		AND ts.name = 'pregnancy'
		AND pre.date BETWEEN ts.start_date 
		AND COALESCE(ts.end_date, NOW()))
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = pre.patient_id AND pre.date BETWEEN indays.start_date AND indays.end_date);

-- Union all tables into a single collection of Frank Diabetic patients
WITH t1
AS
(SELECT 
 	patient_id, 
 	MIN(date) AS date 
 FROM 
 	a1c_results
 GROUP BY 
 	patient_id),
t2 AS
(SELECT 
 	patient_id, 
 	MIN(date) AS date 
 FROM 
 	fasting_glucose 
 GROUP BY 
 	patient_id),
t0_3 AS
(SELECT 
 	patient_id, 
 	row_number() OVER(PARTITION BY patient_id ORDER BY date) AS row_n, 
 	date 
 FROM 
 	random_glucose),
t3 AS
(SELECT 
 	patient_id, 
 	date 
 FROM 
 	t0_3 
 WHERE 
 	row_n = 2),
t4 AS
(SELECT 
 	patient_id, 
 	start_date 
 FROM 
 	insulin_disp),
t0_5 AS
(SELECT 
 	patient_id, 
 	row_number() OVER(partition by patient_id ORDER BY date) as row_n, 
 	date
 FROM 
 	outpatient_code),
t5 AS
(SELECT 
 	patient_id, 
 	date 
 FROM
 	t0_5 
 WHERE 
 	row_n = 2),
t6 AS
(
 SELECT 
	patient_id, 
	date 
 FROM 
	medication_disp),
t7 AS
(SELECT 
 	patient_id, 
 	date 
 FROM 
 	t1
 UNION
 SELECT 
 	patient_id, 
 	date 
 FROM 
 	t2
 UNION
 SELECT 
 	patient_id, 
 	date 
 FROM t3
UNION
SELECT 
 	patient_id, 
 	start_date 
 FROM 
 	t4
 UNION
 SELECT 
 	patient_id, date 
 FROM 
 	t5
 UNION
 SELECT 
 	patient_id, date 
 FROM 
 	t6
)
SELECT 
	patient_id, 
	min(date) AS date
INTO TEMPORARY 
	frank_diabetes
FROM 
	t7 
GROUP BY 
	patient_id;

/**************************************************
* STEP 2: Differentiate Type 1 or Type 2 Diabetes *
**************************************************/

--C-peptide test <0.8

SELECT 
	patient_id, 
	date, 
	result_float, 
	test_name 
INTO TEMPORARY 
	cpeptide
FROM 
	hef_event   
WHERE 
	name = 'lx:c-peptide:threshold:lt:0.8'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);
	
--Diabetes auto-antibodies positive

	--> Antibodies #1:  GAD-65 AB > 1.0
	select 
		count(*),
		patient_id,
		date,
		'ab1' as event_type
	into temporary
		ab1
	from
		hef_event h
	where 
		name = 'lx:gad65:threshold:gt:1'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
	--> Antibodies #2:  ICA-512 Autoantibodies > 0.8
	select 
		count(*),
		patient_id,
		date,
		'ab2' as event_type
	into temporary
		ab2
	from
		hef_event h
	where 
		name = 'lx:ica512:threshold:gt:0.8'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;


	-- Antibodies #3:  Query to separate numeric values from titre string and return true if titre ratio is greater than 1:4
	CREATE TEMPORARY TABLE 
		ab3 
	AS
	SELECT 
		l.patient_id, 
		l.date, 
		l.result_float,
		l.result_string
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'islet-cell-antibody' 
		AND
		l.result_string IS NOT NULL
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date)
		AND
		(
		CASE 
			WHEN UPPER(l.result_string) = 'POSITIVE' THEN 1
			WHEN POSITION(':' IN l.result_string) > 0 
				AND SUBSTRING(l.result_string, 1, POSITION(':' IN l.result_string)-1) SIMILAR TO '[0-9]+'
				AND CAST(SUBSTRING(l.result_string, 1, POSITION(':' IN l.result_string)-1) AS INT) = 1 
				AND CAST(SUBSTRING(l.result_string, POSITION(':' IN l.result_string)+1, LENGTH(l.result_string)) AS INT) >= 4 THEN 1
			WHEN l.result_float > .8 THEN 1	
			WHEN POSITION('<' IN l.result_string) > 0 THEN 0
			ELSE 0
	END
	) = 1;
	--> Antibodies #4:  Islet Cell Antibody Titer >= 1.25*/
	select 
		count(*),
		patient_id,
		date,
		'ab1' as event_type
	into temporary
		ab4
	from
		hef_event h
	where 
		name = 'lx:islet-cell-antibody:threshold:gte:1.25'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
		
	--> Antibodies #5:  Insulin Auto Antibody >= .4
	CREATE TEMPORARY TABLE 
		ab5 
	AS SELECT 
		l.patient_id, 
		l.date, 
		l.result_float
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'insulin-antibody' 
	AND 
		l.result_float >= 0.4
	AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);
	-->Antibodies #6:  Insulin AB >= .8 */
	select 
		count(*),
		patient_id,
		date,
		'ab6' as event_type
	into temporary
		ab6
	from
		hef_event h
	where 
		name = 'lx:insulin-antibody:threshold:gt:0.8'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
		
--Prescription for URINE ACETONE TEST STRIPS (search on keyword: ACETONE)

SELECT 
	patient_id, 
	date, 
	'acetone' as event_type
INTO TEMPORARY
	acetone
FROM 
	hef_event h
WHERE
	name = 'rx:acetone'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY
	patient_id,
	date;

--50% ratio and glucagon prescription

SELECT DISTINCT
	enc.patient_id, 
	enc.date,
	'fiftypercent_glucagon' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO TEMPORARY
	fiftypercent_glucagon
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
WHERE 
	(
		(codes.dx_code_id like 'ICD10:E10.%'
		OR
		codes.dx_code_id like 'ICD10:E11.%')
	
	AND enc.patient_id IN(
		SELECT
			patient_id
		FROM emr_prescription
		WHERE LOWER(name) LIKE
			'%glucagon%')
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = enc.patient_id AND enc.date BETWEEN indays.start_date AND indays.end_date)
	)
GROUP BY
	enc.patient_id,
	enc.date
HAVING
	(SUM(CASE WHEN codes.dx_code_id like 'E10' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0
	AND
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;

--50% ratio and no oral hypoglycemic

SELECT DISTINCT 
	hef.patient_id,
	hef.date,
	'fiftypercent_nohypo' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO TEMPORARY 
	fiftypercent_nohypoglycemic
FROM 
	emr_encounter AS enc
JOIN 
	hef_event AS hef 
ON 
	enc.patient_id = hef.patient_id
JOIN
	emr_encounter_dx_codes as codes
ON
	enc.id = codes.encounter_id
WHERE 
	hef.name not in ('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
)
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = hef.patient_id AND hef.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY 
	hef.patient_id,
	hef.date
HAVING 
	(SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0 
	AND 
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;
	
/***************
* Summary View *
***************/

--If patient is prediabetic and meets any one of the following criteria, they are considered type 1 diabetic
SELECT 
	f.patient_id, 
	c.date
INTO TEMPORARY 
	Type1_Summary
FROM
	cpeptide c
JOIN
	Frank_Diabetes f
ON 
	c.patient_id = f.patient_id
UNION
SELECT
	a1.patient_id,
	a1.date
FROM
	ab1 a1
JOIN 
	Frank_Diabetes f
ON
	a1.patient_id = f.patient_id
UNION
SELECT
	a2.patient_id,
	a2.date
FROM
	ab2 a2
JOIN 
	Frank_Diabetes f
ON
	a2.patient_id = f.patient_id
UNION
SELECT
	a3.patient_id,
	a3.date
FROM ab3 a3
JOIN 
	Frank_Diabetes f
ON
	a3.patient_id = f.patient_id
UNION
SELECT
	a4.patient_id,
	a4.date
FROM ab4 a4
JOIN
	Frank_Diabetes f
ON
	a4.patient_id = f.patient_id
UNION
SELECT
	a5.patient_id,
	a5.date
FROM ab5 a5
JOIN
	Frank_Diabetes f
ON
	a5.patient_id = f.patient_id
UNION
SELECT
	a6.patient_id,
	a6.date
FROM ab6 a6
JOIN
	Frank_Diabetes f
ON a6.patient_id = f.patient_id
UNION
SELECT
	gluc.patient_id,
	gluc.date
FROM
	fiftypercent_glucagon gluc
JOIN 
	Frank_Diabetes f
ON
	gluc.patient_id = f.patient_id
UNION
SELECT
	a.patient_id,
	a.date
FROM 
	acetone a
JOIN 
	Frank_Diabetes f
ON
	a.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	hyp.date
FROM
	fiftypercent_nohypoglycemic hyp
JOIN 
	Frank_Diabetes f
ON
	hyp.patient_id = f.patient_id;

--If the patient is considered Frank Diabetic and does NOT meet any of the criteria for Type 1 Diabetes then they are Type2

SELECT 
	f.patient_id,
	f.date
INTO TEMPORARY
	type2_Summary
FROM
	Frank_Diabetes f
WHERE
	patient_id NOT IN (SELECT patient_id FROM type1_Summary);
	
--Display Count of Patients filtering out any patients over 85 and younger than 4
SELECT 
	patient_id,
	date,
	'type 2' as diabetes_type
INTO TEMPORARY
	final_counts
FROM 
	type2_Summary t2
JOIN
	emr_patient p
ON
	t2.patient_id = p.id
WHERE 
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) < 85
	AND
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) > 4
UNION
SELECT 
	patient_id,
	date,
	'type 1' as diabetes_type
FROM 
	type1_Summary t1
JOIN
	emr_patient p
ON
	t1.patient_id = p.id
WHERE 
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) < 85
	AND
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) > 4;


--this gets all patients with type1 and type2 diabetes as identified by the ESP diabetes plugin algorithm.
select patient_id, date, diabetes_type 
into temporary esp_diab 
from final_counts where diabetes_type in ('type 1','type 2');

select distinct patient_id, date, name 
into temporary hef_a1c_gte
from hef_event 
where name = 'lx:a1c:threshold:gte:6.5' and date<'2022-12-31'::Date;

select distinct patient_id, date, 'lx:a1c:threshold:lt:6.5'
into temporary hef_a1c_lt
from emr_labresult t0
join conf_labtestmap t1 on t1.native_code=t0.native_code and t1.test_name='a1c'
where result_float<6.5 and date<'2022-12-31'::Date;

select * 
into temporary hef_a1c
from (
	select * from hef_a1c_gte
	union select * from hef_a1c_lt) t0;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id, DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, diabetes_type, coalesce(name,'not tested') as control
into temporary diab_assessed_control
from esp_diab t0
left join last_hef_a1c t1 on t1.patient_id=t0.patient_id and t0.date<=t1.date;

select count(*) as counts, diabetes_type, coalesce(control,'not diabetic') as control
from pat_w_age t0
left join diab_assessed_control t1
on t1.patient_id=t0.patient_id 
group by diabetes_type, coalesce(control,'not diabetic');
