-- Define a function to check if a patient had specific results
CREATE OR REPLACE FUNCTION check_patient_results(input_patient_id INTEGER)
RETURNS TABLE
(
    patient_mrn INTEGER,
    a1c_found BOOLEAN,
    fasting_glucose_found BOOLEAN,
    random_glucose_found BOOLEAN,
    outpatient_code_found BOOLEAN,
    insulin_dispensed_found BOOLEAN,
    ogtt_found BOOLEAN,
    anti_hyperglycemic_medication_found BOOLEAN
) AS $$
BEGIN
    RETURN QUERY
    SELECT
        h.patient_id AS patient_mrn,
        COALESCE(MAX(CASE WHEN h.name = 'lx:a1c:threshold:gte:6.5' THEN 1 ELSE 0 END), 0) = 1 AS a1c_found,
        COALESCE(MAX(CASE WHEN h.name = 'lx:ogtt50-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS ogtt_found,
        COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS fasting_glucose_found,
        COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-random:threshold:gte:200' THEN 1 ELSE 0 END), 0) = 1 AS random_glucose_found,
        COALESCE(MAX(CASE WHEN oc.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS outpatient_code_found,
        COALESCE(MAX(CASE WHEN i.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS insulin_dispensed_found,
        COALESCE(MAX(CASE WHEN h.name = '' THEN 1 ELSE 0 END), 0) = 1 AS anti_hyperglycemic_medication_found
    FROM
        hef_event h
    LEFT JOIN (
        SELECT DISTINCT
            enc.patient_id
        FROM
            emr_encounter AS enc 
        JOIN
            emr_encounter_dx_codes codes
        ON
            enc.id = codes.encounter_id
        WHERE
            enc.patient_id = input_patient_id
            AND (
                codes.dx_code_id ilike 'ICD9:250%'
                OR codes.dx_code_id ilike 'ICD9:357.2%'
                OR codes.dx_code_id = 'ICD9:366.41'
                OR codes.dx_code_id = 'ICD9:362.01'
                OR codes.dx_code_id = 'ICD9:362.02'
                OR codes.dx_code_id = 'ICD9:362.03'
                OR codes.dx_code_id = 'ICD9:362.04'
                OR codes.dx_code_id = 'ICD9:362.05'
                OR codes.dx_code_id = 'ICD9:362.06'
                OR codes.dx_code_id = 'ICD9:362.07'
                OR codes.dx_code_id ilike 'ICD10:E10%'
                OR codes.dx_code_id ilike 'ICD10:E11%'
                OR codes.dx_code_id ilike 'ICD10:E14%'
            )
    ) AS oc ON h.patient_id = oc.patient_id
    LEFT JOIN (
        SELECT DISTINCT
            pre.patient_id
        FROM
            emr_prescription AS pre
        JOIN
            static_drugsynonym AS syn
        ON
            lower(pre.name) LIKE CONCAT('%', lower(syn.other_name), '%')
        WHERE
            lower(syn.generic_name) = 'insulin'
            AND NOT EXISTS (
                SELECT NULL FROM hef_timespan AS ts 
                WHERE ts.patient_id = pre.patient_id 
                AND ts.name = 'pregnancy'
                AND pre.date BETWEEN ts.start_date 
                AND COALESCE(ts.end_date, NOW())
            )
    ) AS i ON h.patient_id = i.patient_id
    WHERE
        h.patient_id = input_patient_id
    GROUP BY
        h.patient_id;

    RETURN;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM check_patient_results(123);
