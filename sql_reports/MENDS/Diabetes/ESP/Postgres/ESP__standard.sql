--this gets all patients with type1 and type2 diabetes as identified by the ESP diabetes plugin algorithm.
select patient_id, date, condition 
into temporary esp_diab 
from nodis_case where condition in ('diabetes:type-1','diabetes:type-2');

select distinct patient_id, date, name 
into temporary hef_a1c_gte
from hef_event 
where name = 'lx:a1c:threshold:gte:6.5' and date<'2022-12-31'::Date;

select distinct patient_id, date, 'lx:a1c:threshold:lt:6.5'
into temporary hef_a1c_lt
from emr_labresult t0
join conf_labtestmap t1 on t1.native_code=t0.native_code and t1.test_name='a1c'
where result_float<6.5 and date<'2022-12-31'::Date;

select * 
into temporary hef_a1c
from (
	select * from hef_a1c_gte
	union select * from hef_a1c_lt) t0;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id, DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, condition, coalesce(name,'not tested') as control
into temporary diab_assessed_control
from esp_diab t0
left join last_hef_a1c t1 on t1.patient_id=t0.patient_id and t0.date<=t1.date;

select count(*) as counts, condition, coalesce(control,'not diabetic') as control
from pat_w_age t0
left join diab_assessed_control t1
on t1.patient_id=t0.patient_id 
group by condition, coalesce(control,'not diabetic');
