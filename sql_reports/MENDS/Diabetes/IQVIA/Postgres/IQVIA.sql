/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 8/03/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Branched development from the ESP version to IQVIA.  Implemented IQVIA logic.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

--Patient immediately qualifies as diabetic if they have 1 inpatient diagnosis of codes 250.x, 357.2, 366.41, 362.01-362.07)
DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS a1c_results;
	DROP TABLE IF EXISTS fasting_glucose;
	DROP TABLE IF EXISTS random_glucose;
	DROP TABLE IF EXISTS medication_disp;
	DROP TABLE IF EXISTS outpatient_code;
	DROP TABLE IF EXISTS frank_diabetes;
	DROP TABLE IF EXISTS ab1;
 	DROP TABLE IF EXISTS ab2; 
	DROP TABLE IF EXISTS acetone;
	DROP TABLE IF EXISTS fiftypercent_nohypoglycemic;
	DROP TABLE IF EXISTS FRANK_Summary;
	DROP TABLE IF EXISTS fiftypercent_glucagon;
	DROP TABLE IF EXISTS Type1_Summary;
	DROP TABLE IF EXISTS Type2_Summary;
	DROP TABLE IF EXISTS final_counts;
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS insulin_disp;
	DROP TABLE IF EXISTS cpeptide;
	DROP TABLE IF EXISTS ab4;
	DROP TABLE IF EXISTS ab5;
	DROP TABLE IF EXISTS ab6;
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS test_union;
	DROP TABLE IF EXISTS test_date_diffs;
	DROP TABLE IF EXISTS two_event_qual_patients;
	
SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO TEMPORARY 
	inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
JOIN 
	static_dx_code AS codeList 
ON
	codes.dx_code_id = codeList.combotypecode
WHERE 
	gpt.mapped_value = 'inpatient'
	AND
	(codes.dx_code_id like 'icd9:250%' 
	OR 
	codes.dx_code_id like 'icd9:357.2' 
	OR 
	codes.dx_code_id like 'icd9:366.41'
	OR
	codes.dx_code_id like 'icd9:362.01'
	OR
	codes.dx_code_id like 'icd9:362.02'
	OR
	codes.dx_code_id like 'icd9:362.03'
	OR
	codes.dx_code_id like 'icd9:362.04'
	OR
	codes.dx_code_id like 'icd9:362.05'
	OR
	codes.dx_code_id like 'icd9:362.06'
	OR
	codes.dx_code_id like 'icd9:362.07'
	OR
	codes.dx_code_id like 'icd10:E10.%'
	OR
	codes.dx_code_id like 'icd10:E11.%'
	OR
	codes.dx_code_id like 'icd10:E14.%');
	
	
	
--OR Any combination of 2 of the following events occurring within 24 months of each other

--HbA1C concentration of >= 6.5%
select 
	count(*),
	patient_id,
	date,
	'a1c' as event_type
into temporary
	a1c_results
from
	hef_event 
where 
	name = 'lx:a1c:threshold:lt:6.5'
group by 
	patient_id,
	date;

	
--fasting plasma glucose concentration >= 126 mg/dl
select 
	count(*),
	patient_id,
	date,
	'fasting glucose' as event_type
into temporary
	fasting_glucose
from
	hef_event 
where 
	name = 'lx:glucose-fasting:threshold:gte:126'
group by
	patient_id,
	date;

--random plasma glucose concentration >= 200 mg/dl
select 
	count(*),
	patient_id,
	date,
	'random glucose' as event_type
into temporary
	random_glucose
from
	hef_event 
where 
	name = 'lx:glucose-random:threshold:gte:200'
group by
	patient_id,
	date;

--outpatient diagnosis code for diabetes	
SELECT 
    DISTINCT
	enc.patient_id, 
	enc.date,
	'dx' as event_type
INTO TEMPORARY
	outpatient_code
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
JOIN 
	static_dx_code AS codeList 
ON
	codes.dx_code_id = codeList.combotypecode
WHERE 
	(codes.dx_code_id like 'icd9:250%' 
	OR 
	codes.dx_code_id like 'icd9:357.2' 
	OR 
	codes.dx_code_id like 'icd9:366.41'
	OR
	codes.dx_code_id like 'icd9:362.01'
	OR
	codes.dx_code_id like 'icd9:362.02'
	OR
	codes.dx_code_id like 'icd9:362.03'
	OR
	codes.dx_code_id like 'icd9:362.04'
	OR
	codes.dx_code_id like 'icd9:362.05'
	OR
	codes.dx_code_id like 'icd9:362.06'
	OR
	codes.dx_code_id like 'icd9:362.07'
	OR
	codes.dx_code_id like 'icd10:E10.%'
	OR
	codes.dx_code_id like 'icd10:E11.%'
	OR
	codeLIst.code like 'icd10:E14.%');

--any dispensation of antihyperglycemic medication
SELECT DISTINCT
	patient_id, 
	date,
	'rx' as event_type
INTO TEMPORARY
	medication_disp
FROM 
	hef_event AS pre 
WHERE 
	lower(name)
	IN 
	('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
);

-- Bring all of the events together
-- Each event type will have a unique date, however different event types can have the same date	 
create temporary table test_union as	 
select patient_id, event_type, date from a1c_results
union all
select patient_id, event_type, date from fasting_glucose
union all
select patient_id, event_type, date from random_glucose
union all
select patient_id, event_type, date from medication_disp
union all
select patient_id, event_type, date from outpatient_code
order by patient_id, date;

-- compute the difference in # of days between events
create temporary table test_date_diffs as 
select patient_id,
event_type,
date,
coalesce(date::date - lag(date::date) over (partition by patient_id order by date), 5000000) as diff
from test_union;

-- identify patients that meet the criteria of "2 events within 24 months"
create temporary table two_event_qual_patients as 
select distinct(patient_id), date from test_date_diffs
where diff <= 730;

-- add back in immediately qualifying patients
CREATE TEMP TABLE diabetes_results
(
	patient_id INT,
	date TIMESTAMP,
	diabetes_type VARCHAR(10)
);

-- First insert
INSERT INTO diabetes_results
(
	patient_id,
	date,
	diabetes_type
)
SELECT 
	patient_id, 
	start_date,
	'inpatient'
FROM inpat_days;

-- Second insert
INSERT INTO diabetes_results
(
	patient_id,
	date,
	diabetes_type
)
SELECT 
	patient_id, 
	CAST(date AS TIMESTAMP),
	'diabetic'
FROM 
	two_event_qual_patients;

--this gets all patients with type1 and type2 diabetes as identified by the ESP diabetes plugin algorithm.
select patient_id, date
into temporary esp_diab 
from diabetes_results;

select distinct patient_id, date, name 
into temporary hef_a1c_gte
from hef_event 
where name = 'lx:a1c:threshold:gte:6.5' and date<'2022-12-31'::Date;

select distinct patient_id, date, 'lx:a1c:threshold:lt:6.5'
into temporary hef_a1c_lt
from emr_labresult t0
join conf_labtestmap t1 on t1.native_code=t0.native_code and t1.test_name='a1c'
where result_float<6.5 and date<'2022-12-31'::Date;

select * 
into temporary hef_a1c
from (
	select * from hef_a1c_gte
	union select * from hef_a1c_lt) t0;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id, DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, coalesce(name,'not tested') as control
into temporary diab_assessed_control
from esp_diab t0
left join last_hef_a1c t1 on t1.patient_id=t0.patient_id and t0.date<=t1.date;

select count(*) as counts, coalesce(control,'not diabetic') as control
from pat_w_age t0
left join diab_assessed_control t1
on t1.patient_id=t0.patient_id 
group by coalesce(control,'not diabetic');
