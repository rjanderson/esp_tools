/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 8/03/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Branched development from the ESP version to IQVIA.  Implemented IQVIA logic.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

if object_id('tempdb..#inpat_days') IS NOT NULL
	DROP TABLE #inpat_days;
if object_id('tempdb..#a1c_results') IS NOT NULL
	DROP TABLE #a1c_results;
if object_id('tempdb..#fasting_glucose') IS NOT NULL
	DROP TABLE #fasting_glucose;
if object_id('tempdb..#random_glucose') IS NOT NULL
	DROP TABLE #random_glucose;
if object_id('tempdb..#medication_disp') IS NOT NULL
	DROP TABLE #medication_disp;
if object_id('tempdb..#outpatient_code') IS NOT NULL
	DROP TABLE #outpatient_code;
if object_id('tempdb..#insulin_disp') IS NOT NULL
	DROP TABLE #insulin_disp;
if object_id('tempdb..#frank_diabetes') IS NOT NULL
	DROP TABLE #frank_diabetes;
if object_id('tempdb..#cpeptide') IS NOT NULL
	DROP TABLE #cpeptide
IF OBJECT_ID('tempdb..#ab1') IS NOT NULL
		DROP TABLE #ab1;
IF OBJECT_ID('tempdb..#acetone') IS NOT NULL
		DROP TABLE #acetone;
IF OBJECT_ID('tempdb..#fiftypercent_nohypoglycemic') IS NOT NULL
		DROP TABLE #fiftypercent_nohypoglycemic;
IF OBJECT_ID('tempdb..#FRANK_Summary') IS NOT NULL
		DROP TABLE #FRANK_Summary;
IF OBJECT_ID('tempdb..#fiftypercent_glucagon') IS NOT NULL
		DROP TABLE #fiftypercent_glucagon;
IF OBJECT_ID('tempdb..#Type1_Summary') IS NOT NULL
		DROP TABLE #Type1_Summary;
IF OBJECT_ID('tempdb..#Type2_Summary') IS NOT NULL
		DROP TABLE #Type2_Summary;
IF OBJECT_ID('tempdb..#final_counts') IS NOT NULL
		DROP TABLE #final_counts;
IF OBJECT_ID('tempdb..#test_union') IS NOT NULL
		DROP TABLE #test_union;
IF OBJECT_ID('tempdb..#test_date_diffs') IS NOT NULL
		DROP TABLE #test_date_diffs;
IF OBJECT_ID('tempdb..#two_event_qual_patients') IS NOT NULL
		DROP TABLE #two_event_qual_patients;
IF OBJECT_ID('tempdb..#diabetes_results') IS NOT NULL
		DROP TABLE #diabetes_results;
IF OBJECT_ID('tempdb..#esp_diab') IS NOT NULL
		DROP TABLE #esp_diab;
IF OBJECT_ID('tempdb..#hef_a1c_gte') IS NOT NULL
		DROP TABLE #hef_a1c_gte;
IF OBJECT_ID('tempdb..#hef_a1c_lt') IS NOT NULL
		DROP TABLE #hef_a1c_lt;
IF OBJECT_ID('tempdb..#hef_a1c') IS NOT NULL
		DROP TABLE #hef_a1c;
IF OBJECT_ID('tempdb..#last_hef_a1c') IS NOT NULL
		DROP TABLE #last_hef_a1c;
IF OBJECT_ID('tempdb..#pat_w_age') IS NOT NULL
		DROP TABLE #pat_w_age;
IF OBJECT_ID('tempdb..#diab_assessed_control') IS NOT NULL
		DROP TABLE #diab_assessed_control;


--Patient immediately qualifies as diabetic if they have 1 inpatient diagnosis of codes 250.x, 357.2, 366.41, 362.01-362.07)
SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO #inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
WHERE 
	gpt.mapped_value = 'inpatient'
	AND
	(codes.dx_code_id like 'icd9:250%' 
	OR 
	codes.dx_code_id like 'icd9:357.2' 
	OR 
	codes.dx_code_id like 'icd9:366.41'
	OR
	codes.dx_code_id like 'icd9:362.01'
	OR
	codes.dx_code_id like 'icd9:362.02'
	OR
	codes.dx_code_id like 'icd9:362.03'
	OR
	codes.dx_code_id like 'icd9:362.04'
	OR
	codes.dx_code_id like 'icd9:362.05'
	OR
	codes.dx_code_id like 'icd9:362.06'
	OR
	codes.dx_code_id like 'icd9:362.07'
	OR
	codes.dx_code_id like 'icd10:E10.%'
	OR
	codes.dx_code_id like 'icd10:E11.%'
	OR
	codes.dx_code_id like 'icd10:E14.%');

--HbA1C concentration of >= 6.5%
CREATE TABLE #a1c_results
(
	num_records INT,
	patient_id INT,
	date DATE,
	event_type VARCHAR(3)
);

INSERT INTO #a1c_results 
(	num_records,
	patient_id,
	date,
	event_type
)
SELECT 
	COUNT(*),
	patient_id,
	date,
	'a1c'
FROM 
	hef_event 
where 
	name = 'lx:a1c:threshold:gte:6.5'
group by 
	patient_id,
	date;

	
--fasting plasma glucose concentration >= 126 mg/dl
CREATE TABLE #fasting_glucose
(
	num_records INT,
	patient_id INT,
	date DATE,
	event_type VARCHAR(20)
);

INSERT INTO #fasting_glucose
(
	num_records,
	patient_id,
	date,
	event_type
)
SELECT
	count(*),
	patient_id,
	date,
	'fasting glucose' as event_type
FROM
	hef_event 
WHERE 
	name = 'lx:glucose-fasting:threshold:gte:126'
GROUP BY
	patient_id,
	date;

--random plasma glucose concentration >= 200 mg/dl
CREATE TABLE #random_glucose
(
	num_records INT,
	patient_id INT,
	date DATE,
	event_type VARCHAR(20)
);

INSERT INTO #random_glucose
(
	num_records,
	patient_id,
	date,
	event_type
)
SELECT
	count(*),
	patient_id,
	date,
	'random glucose'
FROM
	hef_event 
WHERE 
	name = 'lx:glucose-random:threshold:gte:200'
GROUP BY
	patient_id,
	date;

--outpatient diagnosis code for diabetes	
CREATE TABLE #outpatient_code
(
	patient_id INT,
	date DATE,
	event_type VARCHAR(2)
);

INSERT INTO #outpatient_code
(
	patient_id,
	date,
	event_type
)
SELECT 
    DISTINCT
	enc.patient_id, 
	enc.date,
	'dx' as event_type
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
JOIN 
	static_dx_code AS codeList 
ON
	codes.dx_code_id = codeList.combotypecode
WHERE 
	(codes.dx_code_id like 'icd9:250%' 
	OR 
	codes.dx_code_id like 'icd9:357.2' 
	OR 
	codes.dx_code_id like 'icd9:366.41'
	OR
	codes.dx_code_id like 'icd9:362.01'
	OR
	codes.dx_code_id like 'icd9:362.02'
	OR
	codes.dx_code_id like 'icd9:362.03'
	OR
	codes.dx_code_id like 'icd9:362.04'
	OR
	codes.dx_code_id like 'icd9:362.05'
	OR
	codes.dx_code_id like 'icd9:362.06'
	OR
	codes.dx_code_id like 'icd9:362.07'
	OR
	codes.dx_code_id like 'icd10:E10.%'
	OR
	codes.dx_code_id like 'icd10:E11.%'
	OR
	codes.dx_code_id like 'icd10:E14.%');

--any dispensation of antihyperglycemic medication
CREATE TABLE #medication_disp
(
	patient_id INT,
	date DATE,
	event_type VARCHAR(2)
);

INSERT INTO #medication_disp
(
	patient_id,
	date,
	event_type
)
SELECT DISTINCT
	patient_id, 
	date,
	'rx' as event_type
FROM 
	hef_event AS pre 
WHERE 
	lower(name)
	IN 
	('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
);

-- Bring all of the events together
-- Each event type will have a unique date, however different event types can have the same date	 

SELECT patient_id, event_type, [date]
into #test_union
FROM (
		select patient_id, event_type, [date] from #a1c_results
		union all
		select patient_id, event_type, [date] from #fasting_glucose
		union all
		select patient_id, event_type, [date] from #random_glucose
		union all
		select patient_id, event_type, [date] from #medication_disp
		union all
		select patient_id, event_type, [date] from #outpatient_code
) as subquery
order by patient_id, [date];

-- compute the difference in # of days between events
SELECT 
	patient_id,
	event_type,
	date,
	COALESCE(DATEDIFF(DAY, LAG([date]) OVER (PARTITION BY patient_id ORDER BY [date]), [date]), 5000000) AS diff
INTO 
	#test_date_diffs
FROM 
	#test_union;
-- identify patients that meet the criteria of "2 events within 24 months" 
SELECT 
	distinct(patient_id),
	date
INTO 
	#two_event_qual_patients
FROM
	#test_date_diffs
WHERE diff <= 730;


-- add back in immediately qualifying patients
CREATE TABLE #diabetes_results
(
	patient_id INT,
	date DATETIME,
	diabetes_type VARCHAR(10)
);

INSERT INTO #diabetes_results
(
	patient_id,
	date,
	diabetes_type
)
select 
	patient_id, 
	start_date,
	'inpatient'
from #inpat_days;


INSERT INTO #diabetes_results
(
	patient_id,
	date,
	diabetes_type
)
select 
	patient_id, 
	cast(date as datetime),
	'diabetic'
from 
	#two_event_qual_patients;

select top 10 * from #two_event_qual_patients;


--Apply A1C filtering to show Patients Under A1C threshold and Over A1C threshold.  Also show population of --non-diabetic
-- Step 1
SELECT patient_id, date
INTO #esp_diab 
FROM #diabetes_results

-- Step 2
SELECT DISTINCT patient_id, date, name 
INTO #hef_a1c_gte
FROM hef_event 
WHERE name = 'lx:a1c:threshold:gte:6.5' AND date < '2022-12-31';

-- Step 3
SELECT DISTINCT patient_id, date, 'lx:a1c:threshold:lt:6.5' AS threshold
INTO #hef_a1c_lt
FROM emr_labresult t0
JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE result_float < 6.5 AND date < '2022-12-31';

-- Step 4
SELECT * 
INTO #hef_a1c
FROM (
    SELECT * FROM #hef_a1c_gte
    UNION 
    SELECT * FROM #hef_a1c_lt
) AS t0;

-- Step 5
SELECT patient_id, date, name
INTO #last_hef_a1c
FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC, name) AS rn
    FROM #hef_a1c
) AS t
WHERE rn = 1;

-- Step 6
SELECT id AS patient_id, DATEDIFF(YEAR, date_of_birth, '2023-12-31') AS AGE
INTO #pat_w_age
FROM emr_patient
WHERE DATEDIFF(YEAR, date_of_birth, '2023-12-31') BETWEEN 4 AND 85
    AND EXISTS (SELECT 1 FROM gen_pop_tools.clin_enc ce 
                WHERE ce.patient_id = emr_patient.id AND ce.date BETWEEN '2020-12-31' AND '2022-12-31');

-- Step 7
SELECT t0.patient_id, ISNULL(name, 'not tested') AS control
INTO #diab_assessed_control
FROM #esp_diab t0
LEFT JOIN #last_hef_a1c t1 ON t1.patient_id = t0.patient_id AND t0.date <= t1.date;

-- Step 8
SELECT COUNT(*) AS counts, ISNULL(control, 'not diabetic') AS control
FROM #pat_w_age t0
LEFT JOIN #diab_assessed_control t1 ON t1.patient_id = t0.patient_id 
GROUP BY ISNULL(control, 'not diabetic')
