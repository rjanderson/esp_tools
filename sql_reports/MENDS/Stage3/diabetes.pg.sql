drop table random_diab_patients;
select p.id as patient_id, p.natural_key, random() as random, 
race, ethnicity, date_of_birth::date dob
into temporary random_diab_patients
from emr_patient p;

drop table tmp_first_diabdx;
select patient_id, min(date) as date
into temporary tmp_first_diabdx
from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id like 'icd10:E10%' or dx.dx_code_id like 'icd10:E11%' 
   or dx.dx_code_id like 'icd10:E13%' 
group by patient_id;

--controlled diabetes 
drop table tmp_diab_selected;
select p.natural_key, c.patient_id, 
  c.date, 'type1'::varchar(5) as ttype
into temporary tmp_diab_selected
from emr_patient p
join tmp_first_diabdx c on c.patient_id=p.id 
  and c.date <= '2022-11-30'::date and c.date>= '2020-01-01'::Date
join nodis_case ctyp on ctyp.patient_id=p.id and ctyp.condition='diabetes:type-1';

insert into tmp_diab_selected
select p.natural_key, c.patient_id, c.date, 'type2'::varchar(5) as ttype
from emr_patient p
join tmp_first_diabdx c on c.patient_id=p.id 
  and c.date <= '2022-11-30'::date and c.date>= '2020-01-01'::Date
join nodis_case ctyp on ctyp.patient_id=p.id and ctyp.condition='diabetes:type-2';

drop table tmp_diab_selected_a1c;
select distinct on (d.natural_key) d.natural_key, d.patient_id, d.date, d.ttype, 
  l.result_float, l.date as ldate
into temporary tmp_diab_selected_a1c
from tmp_diab_selected d
join emr_patient p on p.id=d.patient_id
left join (select l.* 
		   from emr_labresult l
		   join conf_labtestmap lm on lm.native_code=l.native_code and lm.test_name='a1c'
		   and l.result_float is not null and l.date <='2022-09-30'::date) l
  on l.patient_id=d.patient_id and l.date>'2021-09-30' and l.date>d.date
    and date_part('years', age(l.date,p.date_of_birth))>=20
order by d.natural_key, l.date desc;

Select * into temporary tmp_diab_report
from (select r.natural_key, race, ethnicity, dob,
  ttype || ': ' || 'Controlled status-HbA1c < 9.0%' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type1' and td.result_float < 9
order by random limit 4) t0
union 
Select * from (select r.natural_key, race, ethnicity, dob,
  ttype|| ': ' || 'Controlled status-HbA1c > 9.0%' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type1' and td.result_float > 9
order by random limit 3) t1
union 
select * from  (select r.natural_key, race, ethnicity, dob, 
   ttype|| ': ' || 'Unknown status-HbA1c unmeasured in last year' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type1' and td.result_float is null
order by random limit 3) t2
union
select * from  (select r.natural_key, race, ethnicity, dob,
  ttype || ': ' || 'Controlled status-HbA1c < 9.0%' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type2' and td.result_float < 9
order by random limit 4) t3
union
select * from  (select r.natural_key, race, ethnicity, dob,
   ttype|| ': ' || 'Controlled status-HbA1c > 9.0%' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type2' and td.result_float > 9
order by random limit 3) t4
union 
select * from  (select r.natural_key, race, ethnicity, dob, 
   ttype|| ': ' || 'Unknown status-HbA1c unmeasured in last year' as testtype, date as dx_date, result_float, ldate as lab_date
from tmp_diab_selected_a1c td
join random_diab_patients r on r.patient_id=td.patient_id
where td.ttype='type2' and td.result_float is null
order by random limit 3) t5
order by testtype, natural_key;

\copy tmp_diab_report to '/tmp/diabetes.csv' CSV delimiter ',' header;
