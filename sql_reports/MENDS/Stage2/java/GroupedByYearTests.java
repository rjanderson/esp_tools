package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Generator for numeric variables grouped by year of measurement.
 */
public class GroupedByYearTests {
    
    /**
     * GroupedByYearTest - generates SQL for Stats Grouped by year tests.
     * This gives the stats for the the specified visit column, grouped by the year portion of the visit date.
     * Expects input file, stats_grouped_by_year_in.txt, to have these values for each test:
     *   ESP_Column, in the form table.column
     *   YearMin - smallest year to be included in report
     *   YearMax = largest year to be included in report
     *   MinValue = minimum value on which to report
     *   MaxValue = maximum value on which to report
     *   
     *   NOTE (2021-02-16) For data from emr_encounter, added a constraint that encounter_date >= 2015-01-01
     */
    public static void groupedByYearTests () {

        System.out.println("Starting Grouped by Year Test");
        
        try {
            Scanner in = new Scanner(Path.of("grouped_by_year_in.txt"));
            
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            PrintWriter out = new PrintWriter("grouped_by_year." + rdbms + ".sql", StandardCharsets.UTF_8); 
            

            /*
            // Override default Scanner delimiter of whitespace with one that treats \r\n as delimiter
            in.useDelimiter("\r\n");
            */

            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + " gen)");
            out.println(" * Counts Grouped by Year tests ...");
            out.println("*/"); 
            out.println("");
            
            // Delimiter for Scanner defaults to whitespace. Use tabs or blanks as separators.
            
            // Drop and create table 
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.grouped_by_year','U') is not null drop table cii_qa.grouped_by_year;");               
                out.println("create table cii_qa.grouped_by_year ");
                out.println("(test_name nvarchar(MAX),  esp_column varchar(MAX)");
                out.println(", grouping_column_name varchar(MAX), year_min integer, year_max integer, year integer"); 
                out.println(", min float, p10 float, p25 float, median float, p75 float, p90 float, max float");
                out.println(", mean float, stddev float");
                out.println(", pct_null float, count integer);");
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.grouped_by_year;");
                out.println("create table cii_qa.grouped_by_year ");
                out.println("(test_name text,  esp_column text");
                out.println(", grouping_column_name text, year_min integer, year_max integer, year integer"); 
                out.println(", min float, p10 float, p25 float, median float, p75 float, p90 float, max float");
                out.println(", mean float, stddev float");
                out.println(", pct_null float, count integer);");
            }
           
            // Loop through the input, generating SQL for each line
            while (in.hasNextLine()) {

                //Read the qualified column name. Split it into TableName, ColumnName, and ColumnExp

                String ESP_Column = in.next();
                String TableName = Utils.extractTableName(ESP_Column); 
                String ColumnName = Utils.extractColumnName(ESP_Column); 
                String ColumnExp = Utils.extractColumnExp(ESP_Column); 
                String PartitionSourceColumn = "date";
                System.out.println("ESP_Column: " + ESP_Column);
                String GroupingColumnName = "year of record creation";
                
                // Override ColumnExp for emr_patient.age_yrs_derived
                if (ESP_Column.equalsIgnoreCase("emr_patient.age_yrs_derived")) {
                    if (rdbms.equals("ms")) {
                        ColumnExp = "datediff(year, date_of_birth, sysdatetime())";
                    } else if (rdbms.equals("pg")) {
                        // ColumnExp = "(cast(date_of_birth as date) - current_date)";
                        ColumnExp = "trunc((current_date - cast(date_of_birth as date))/365.25)"; // derive age in years
                    }
                    PartitionSourceColumn = "created_timestamp";                    
                }
                String YearMin = in.next();
                String YearMax = in.next();
                String IncludedValueMin = in.next();
                String IncludedValueMax = in.next();
                
                System.out.println("TableName: " + TableName);
                System.out.println("ColumnName: " + ColumnName);
                System.out.println("GroupingColumnName: " + GroupingColumnName);
                System.out.println("ColumnExp: " + ColumnExp);
                System.out.println("Min and Max years: " + YearMin + ", " + YearMax);
                System.out.println("Min and Max column values: " + IncludedValueMin +", " + IncludedValueMax);
                System.out.println("");
            
                if (rdbms.equals("ms")) {      // ********************************************* Start ms ******************************************             
                    // Write test description information
                    out.println();
                    out.println("/* " + TableName + "." + ColumnName + " */");
                    out.println("insert into cii_qa.grouped_by_year (");
                    out.println("  test_name, esp_column, grouping_column_name, year_min, year_max, year");
                    out.println(", min, p10, p25, median, p75, p90, max");
                    out.println(", mean, stddev, pct_null, count)");
                    out.println("select");
                    out.println(" 'grouped by year' as test_name");
                    out.println(", '" + TableName + "." + ColumnName + "' as esp_column");
                    out.println(", '" + GroupingColumnName + "' as grouping_column_mame");
                    out.println(", "+YearMin+" as year_min, "+YearMax+" as year_max");   
                    out.println(",  year");
                    out.println(", round(max(p0),2) as min");
                    out.println(", round(max(p10),2)  as p10");
                    out.println(", round(max(p25),2)  as p25");                
                    out.println(", round(max(median),2)  as median");
                    out.println(", round(max(p75),2)  as p75");    
                    out.println(", round(max(p90),2)  as p90");
                    out.println(", round(max(p100),2)  as max");
                    out.println(", round(max(mean),2) as mean");
                    out.println(", round(max(stddev),2) as stddev");                    
                    out.println(", round(100.0*(count(*) - count(" + ColumnExp + "))/count(*),2) as pct_null");
                    out.println(", count(*) as count");
                    out.println("from (");
                    out.println("  select");
                    if (ESP_Column.equalsIgnoreCase("emr_patient.age_yrs_derived")) {
                        out.println("    year(created_timestamp) as year");
                    } else {
                        out.println("    year(date) as year");
                    }  
                    out.println("  , percentile_disc(0.0) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p0");
                    out.println("  , percentile_disc(0.1) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p10");
                    out.println("  , percentile_disc(0.25) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p25");
                    out.println("  , percentile_disc(0.75) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p75");                   
                    out.println("  , percentile_disc(0.9) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p90");
                    out.println("  , percentile_disc(1.0) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as p100");
                    if (TableName.equalsIgnoreCase("emr_patient")) {
                        out.println("  , avg(datediff(year, date_of_birth, sysdatetime())) over (partition  by year(created_timestamp)) as mean"); 
                        out.println("  , stdev(datediff(year, date_of_birth, sysdatetime())) over (partition  by year(created_timestamp)) as stddev");
                    } else {
                        out.println("  , avg(" + ColumnExp + ") over (partition by year(date)) as mean");
                        out.println("  , stdev(" + ColumnExp + ") over (partition by year(date)) as stddev");
                    }
                    out.println("  , percentile_disc(0.5) within group (order by " + ColumnExp + ") over(partition by year("+PartitionSourceColumn+")) as median");
                    if (TableName.equalsIgnoreCase("emr_patient")) {
                        out.println("  , date_of_birth");
                    } else {
                        out.println("  , " + ColumnName); 
                    }                
                    out.println("  from " + TableName);
                    out.println("  where year("+PartitionSourceColumn+") between "+YearMin+" and "+YearMax);                                    
                    out.print("  and " + ColumnExp + " between " + IncludedValueMin + " and " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion");
                    if (TableName.equalsIgnoreCase("emr_encounter")) {
                        out.print("  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()");                          
                        out.println("  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.");
                    }
                    out.println("  and " + ColumnExp + " is not null");
                    out.println(") a");
                    out.println("group by year");
                    out.println(";");                     
                } // rdbms.equals("ms")           // ********************************************* End ms ******************************************    

                else if (rdbms.equals("pg")) {    // ********************************************* Start pg ******************************************                 
                    // Write test description information
                    out.println();
                    out.println("/* " + TableName + "." + ColumnName + " */");
                    out.println("insert into cii_qa.grouped_by_year (");
                    out.println("  test_name, esp_column, grouping_column_name, year_min, year_max, year");
                    out.println(", min, p10, p25, median, p75, p90, max");
                    out.println(", mean, stddev, pct_null, count)");
                    out.println("select");
                    out.println(" 'grouped by year' as test_name");
                    out.println(", '" + TableName + "." + ColumnName + "' as esp_column");
                    out.println(", '" + GroupingColumnName + "' as grouping_column_mame");
                    out.println(", "+YearMin+" as year_min, "+YearMax+" as year_max");
                    out.println(", year");
                    out.println(", percentile_disc(0.0) within group (order by " + ColumnName + ") as min");
                    out.println(", percentile_disc(0.10) within group (order by " + ColumnName+ ") as p10");
                    out.println(", percentile_disc(0.25) within group (order by " + ColumnName + ") as p25");
                    out.println(", percentile_disc(0.50) within group (order by " + ColumnName + ") as median");
                    out.println(", percentile_disc(0.75) within group (order by " + ColumnName + ") as p75");
                    out.println(", percentile_disc(0.90) within group (order by " + ColumnName + ") as p90");
                    out.println(", percentile_disc(1.0) within group (order by " + ColumnName + ") as max");
                    out.println(", avg(" + ColumnName + ")  as mean");
                    out.println(", stddev(" + ColumnName + ") as stddev");                   
                    out.println(", 100*((count(*) - count(" + ColumnName +"))/count(*)) as pct_null");
                    out.println("  , count(*) as count");               
                    out.println("from (");
                    out.println("select ");
                    if (ESP_Column.equalsIgnoreCase("emr_patient.age_yrs_derived")) {
                        out.println("  extract(year from created_timestamp) as year");    
                    } else {
                        out.println("  extract(year from date) as year");
                    }
                    out.println(", " + ColumnExp);
                    if (ESP_Column.equalsIgnoreCase("emr_patient.age_yrs_derived")) {
                        out.println("as "+ ColumnName);
                    }
                    out.println("from " + TableName);
                    out.println("  where extract(year from "+PartitionSourceColumn+") between "+YearMin+" and "+YearMax);                     
                    out.print("  and " + ColumnExp + " between " + IncludedValueMin + " and " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion");
                    if (TableName.equalsIgnoreCase("emr_encounter")) {
                        out.print("  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date");                          
                        out.println("  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.");
                    }
                    out.println("  and " + ColumnExp + " is not null");

                    out.println(") a");
                    out.println("group by year");
                    out.println(";");                    
                }  // rdbms.equals("pg")              // ********************************************* End pg ******************************************    

                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                        if (!junk.isEmpty()) {
                    System.out.println("junk: " + junk);
                    } 
                } 
            
            }  // while (in.hasNextLine())
            // Finished the loop. Clean up.
            out.println("");
            out.println("select * from cii_qa.grouped_by_year");
            out.println("order by esp_column, year;");
            out.println("/* ...end of Counts Grouped by Year tests */");
            out.flush();
            out.close();
            System.out.println("Finishing GroupedByYearTest");
            
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        groupedByYearTests();

    }

}