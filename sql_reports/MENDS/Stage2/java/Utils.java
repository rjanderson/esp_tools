package esp_mends_stage2_scripts;

public class Utils {
    
    /** extractTableName
     * Get the text before the period
     * @param TabColSpec TableName.ColumnName
     * @return Table name
     * */
    public static String extractTableName(String TabColSpec ) {
        String p = "\\.";
        String[] periodSeparatedStrings = TabColSpec.split(p);
        return periodSeparatedStrings[0];
    }
    
    /** extractColumnName
     * Get the text following the period.  If this contains a caret, discard the
     * caret and the text following it.
     * @param TabColSpec TableName.ColumnName
     * @return Column name
     */
    public static String extractColumnName(String TabColSpec ) {
        String p = "\\.";
        String[] periodSeparatedStrings = TabColSpec.split(p);
        // Column name is that which follows the period, and precedes a caret, if
        // there is one.
        String[] caretSeparatedStrings = periodSeparatedStrings[1].split("\\^");
        return caretSeparatedStrings[0];
    }
    
    /** extractColumnExp
     * If the input string contains a caret, return text following the caret. 
     * Otherwise, return the text before the caret, which is the column name.
     * @param TabColSpec TableName.ColumnName
     * @return Column Expression
     */
    public static String extractColumnExp(String TabColSpec ) {
        String ColumnExp = "";
        String[] periodSeparatedStrings = TabColSpec.split("\\.");
        String period2 = periodSeparatedStrings[1];
        String[] caretSeparatedStrings = period2.split("\\^");
        if (caretSeparatedStrings.length > 1 ) {            
            ColumnExp = caretSeparatedStrings[1];
        } else {
            ColumnExp = caretSeparatedStrings[0];
        }
        return ColumnExp;
    }
    
    /** extractSecondString
     * If the input string contains a space, return first string following the space. 
     * Otherwise, return null.
     * @param TabColSpec TableName.ColumnName
     * @return Second of two strings separated by a space
     */
    public static String extractSecondString(String TabColSpec ) {
        String secondString = "";
        String[] spaceSeparatedStrings = TabColSpec.split("\\ ");
        if (spaceSeparatedStrings.length > 1 ) {            
            secondString = spaceSeparatedStrings[1];
        } else {
            secondString = "";
        }
        return secondString;
    }
    
    public static void main(String[] args) {
        String s = "a.b^d wg w";
        //System.out.println(extractTableName(s)); 
       // System.out.println(extractColumnName(s)); 
       // System.out.println(extractColumnExp(s)); 
        System.out.println(extractSecondString("a.b c d"));

    }

}
