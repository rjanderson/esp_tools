package esp_mends_stage2_scripts;
import java.util.Scanner;

/**
 * 
 * @author dkacher
 * Read a parameter value from the console.
 * Each method defines how to read a particular parameter
 *
 */
public class ParamReader {

    String rdbms;
    
    public  String getRdbms() {
       
        // Ask for value for rdbms. Expect either pg or ms        
        Scanner sc = new Scanner(System.in);        
        String askAgain = "y";
        
        while (askAgain.equals("y")) {   
          System.out.print("Rdbms (ms or pg): ");
          String s = sc.nextLine();
          System.out.println("Entered string: " + s);
          if (s.equalsIgnoreCase("pg") || s.equalsIgnoreCase("ms")) {
              askAgain = "n";
              rdbms = s.toLowerCase();
          } else if (s.equalsIgnoreCase("q") ) {
              askAgain = "n";
          } else  {
              System.out.println("Invalid answer. Answer must be \"ms\" for Microsoft SQL Server, or \"pg\" for Postgres. Enter \"q\" to quit." );                   
          }
      }
    sc.close();
    return rdbms;
    }
        
            
        
    public static void main(String[] args) {
        String rdbms;
        ParamReader pr = new ParamReader();
        rdbms = pr.getRdbms();
        System.out.println("main received rdbms " + rdbms);
    }
}
