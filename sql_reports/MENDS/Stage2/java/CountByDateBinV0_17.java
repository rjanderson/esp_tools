package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Generator for Count by Date Bin queries.  For a column containing dates or timestamps, creates SQL to break 
 * the range between minimum and maximum date into 10 bins, and count the number of records in each bin. 
 * @author dkacher
 *
 */
public class CountByDateBinV0_17 {
    
    /**
     * countByDateBinTest - generates SQL for Count by Date Bin test.
     * Expects input file, count_by_date_bin_in.txt, to have four values for each test, separated by whitespace:
     *   - ESP_Column, in the form table.column
     *   - Data_type of the date column, either "date" or "timestamp"
     *   - YearMin - earliest year on which to report
     *   - YearMax - latest year on which to report
     */

    public static void countByDateBin () {
        System.out.println("Starting countByDateBin");

        
        try {
            Scanner in = new Scanner(Path.of("count_by_date_bin_in.txt"));
            
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            PrintWriter out = new PrintWriter("count_by_date_bin." + rdbms + ".sql", StandardCharsets.UTF_8);
            
            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + ")");
            out.println(" * Counts by Date bin (ungrouped)...");
            out.println("*/"); 
            out.println("");
            
            // Drop and create result table 
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.count_by_date_bin', 'U') is not null drop table cii_qa.count_by_date_bin;");
                out.println("create table cii_qa.count_by_date_bin ");
                out.println("(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)");
                out.println(", year_min date, year_max date");
                out.println(", bin integer, min_bin_date date, max_bin_date date, span_days integer, count integer);");                
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.count_by_date_bin;");
                out.println("create table cii_qa.count_by_date_bin");
                out.println("(test_name text, esp_table text, esp_column text");
                out.println(", year_min date, year_max date");
                out.println(", bin integer, min_bin_date date, max_bin_date date, span_days integer, count integer);");
            }
 
            // Loop through the input, generating SQL for each input line
            while (in.hasNextLine()) {
                // ESP_Column is table.col.  Read it, and split it into components TableName and ColumnName
                String ESP_Column = in.next();
                if(ESP_Column.startsWith("#")) {
                    continue; // Column has been commented out. Skip it
                }
                String TableName = ESP_Column.substring(0,ESP_Column.indexOf("."));
                String ColumnName = ESP_Column.substring(ESP_Column.indexOf(".")+1);
                
                // Get the data type of the column
                String date_data_type = in.next();
                System.out.println("ESP_Column: " + ESP_Column + "; TableName: "+TableName+"; ColumnName: "+ColumnName+"; data_type: " + date_data_type);
                
                // Report to log, and skip, if date_data_type hasn't got an expected value
                if (!((date_data_type.equals("date") || 
                    date_data_type.equals("timestamp")))) {
                    System.out.println("ERROR: Unknown date data type: " + date_data_type + " for " + ESP_Column + ". Skipping."); 
                    continue;
                }

                // Determine the string to use for YearMin and YearMax, depending on rdbms 
                String YearMin = "";
                String YearMax = "";
                if (rdbms.equals("ms")) {
                    YearMin = "convert(datetime, '" + in.next() + "-01-01', 102)";
                    YearMax = "getdate()";
                    System.out.println("YearMin, YearMax: " + YearMin + ", " + YearMax);
                } else if (rdbms.equals("pg")) {
                    YearMin = "to_date('" + in.next() + "-01-01', 'YYYY-MM-DD')";
                    YearMax = "current_date";
                    System.out.println("YearMin, YearMax: " + YearMin + ", " + YearMax);
                }
                
                
                out.println();
                out.println("");
                out.println("");
                out.println("/* Determine bin values for "+ESP_Column+" */");
                
                // Drop tmp_bin_size, using rdbms-specific syntax
                if (rdbms.equals("ms")) {
                    out.println("if object_id('cii_qa.tmp_bin_size', 'U') is not null drop table cii_qa.tmp_bin_size;");
                } else if (rdbms.equals("pg")) {
                    out.println("drop table if exists cii_qa.tmp_bin_size;");
                }
                
                out.println("create table cii_qa.tmp_bin_size (");
                out.println("  min_date date, max_date date, diff_days integer, bin_width_days integer");
                out.println("  );");
                out.println("");
                
                out.println("insert into cii_qa.tmp_bin_size (min_date, max_date, diff_days, bin_width_days)");
                
                // rdbms-specific select to insert into cii_qa.tmp_bin_size
                if (rdbms.equals("ms")) {
                    out.println("select  min("+ColumnName+") as min_date, max("+ColumnName+") as max_date, datediff(day, min("+ColumnName+"), max("+ColumnName+")) as diff_days");
                    out.println(", datediff(day, min("+ColumnName+"), max("+ColumnName+"))/10 as bin_width_days");
                    out.println("from "+TableName);
                    out.println("where " + ColumnName + " between " + YearMin + " and " + YearMax);
                } else if (rdbms.equals("pg")) {
                    out.println("select min("+ColumnName+") as min_date, max("+ColumnName+") as max_date, max(cast("+ColumnName+" as date)) - min(cast("+ColumnName+" as date)) as diff_days");
                    out.println(", (max(cast("+ColumnName+" as date)) - min(cast("+ColumnName+" as date)))/10 as bin_width_days");
                    out.println("from "+TableName);
                    out.println("where " + ColumnName + " between " + YearMin + " and " + YearMax);
                }
                out.println(";");
                out.println("");
                out.println("-- select * from cii_qa.tmp_bin_size;");
                
                out.println("");
                out.println("/* Figure out start and end of each bin (cii_qa.tmp_bin_extremes) */");
                
                // rdbms-specific idiom to delete cii_qa.tmp_bin_extremes         
                if (rdbms.equals("ms")) {
                    out.println("if object_id('cii_qa.tmp_bin_extremes', 'U') is not null drop table cii_qa.tmp_bin_extremes;");
                } else if (rdbms.equals("pg")) {
                    out.println("drop table if exists cii_qa.tmp_bin_extremes;");
                }
                
                out.println("create table cii_qa.tmp_bin_extremes (bin integer, min_bin_date date, max_bin_date date);");               
                out.println("");
                
                out.println("/* Populate cii_qa.tmp_bin_extremes for this table.column */");
                int iPlus1;
                for (int i=0; i<10; i++) {
                    iPlus1 = i + 1;
                    out.println("insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)");
                    out.println("select "+i);
                    if (rdbms.equals("ms")) {
                        out.println(", dateadd(day,"+i+"*bin_width_days, min_date) as min_bin_date");
                        out.println(", dateadd(day,("+iPlus1+"*bin_width_days)-1, min_date) as max_bin_date ");
                    } else if (rdbms.equals("pg")) {
                        out.println(", (min_date + cast(((" + i +  " * bin_width_days)   ||' days') as interval)) as min_bin_date");
                        out.println(", (min_date + cast(((" + iPlus1 + "*bin_width_days)-1 ||' days') as interval)) as max_bin_date");
                    }
                    out.println("from cii_qa.tmp_bin_size;");               
                }
                
                out.println("");  
                if (rdbms.equals("ms")) {
                out.println("-- select *, datediff(day, min_bin_date, max_bin_date) as span_days from cii_qa.tmp_bin_extremes order by bin;");
                } else if (rdbms.equals("pg")) {
                out.println("-- select *, cast(max_bin_date as date) - cast(min_bin_date as date) as span_days from cii_qa.tmp_bin_extremes order by bin;");
                }
                out.println("");
                out.println("/* Add the records for this column to result table */");
                out.println("insert into cii_qa.count_by_date_bin (");
                out.println("test_name, esp_table, esp_column, year_min, year_max, bin, min_bin_date, max_bin_date");
                out.println(", span_days, count )");
                out.println("select ");
                out.println("  'count by date bin' as test_name, '"+TableName+"' as esp_table, '"+ColumnName+"' as esp_column");
                out.println(", " + YearMin + " as year_min, " + YearMax + " as year_max");
                out.println(", e.bin, e.min_bin_date, e.max_bin_date");
                if (rdbms.equals("ms")) {
                    out.println(", datediff(day, min_bin_date, max_bin_date) as span_days, d.count");
                } else if (rdbms.equals("pg")) {
                    out.println(", cast(max_bin_date as date) - cast(min_bin_date as date) as span_days, d.count");
                }
                out.println("from (");
                out.println("select bin, count(*) count");
                out.println("from (");
                if (rdbms.equals("ms")) {
                    out.println("select "+ColumnName+", datediff(day, min_date, "+ColumnName+")/bin_width_days as bin");
                } else if (rdbms.equals("pg")) {
                    out.println("select "+ColumnName+", case when bin_width_days < 1 then 999 else (cast("+ColumnName+" as date) - cast(min_date as date))/bin_width_days end as bin"); 
                    
                }
                out.println("from "+TableName+" a");
                out.println("cross join cii_qa.tmp_bin_size b");
                // out.println("where year(" + ColumnName + ") between " + YearMin + " and " + YearMax);
                out.println("where " + ColumnName + " between " + YearMin + " and " + YearMax);
                out.println(" ) c");
                out.println("group by bin) d");
                out.println("right join cii_qa.tmp_bin_extremes e");
                out.println("on e.bin = d.bin");
                out.println("order by bin");
                out.println(";");
                out.println("");
                
                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                    if (!junk.isEmpty()) {
                        System.out.println("junk: " + junk);
                    }
                }
            
            }  // while (in.hasNextLine())

            // Finished the loop. Clean up.
            out.println();
            out.println("/* After all tests, drop tmp tables */");
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.tmp_bin_size', 'U') is not null drop table cii_qa.tmp_bin_size;");
                out.println("if object_id('cii_qa.tmp_bin_extremes', 'U') is not null drop table cii_qa.tmp_bin_extremes;");
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.tmp_bin_size;");
                out.println("drop table if exists cii_qa.tmp_bin_extremes;");                
            }
            out.println("");
            
            // Inspect the results
            out.println("/* Inspect the results */");
            out.println("select * from cii_qa.count_by_date_bin");
            out.println("order by esp_table, esp_column, bin");
            out.println(";");
            
            out.println("/* ...end of Count by Date Bin (ungrouped) */");            
            out.flush();
            out.close();    
            
            System.out.println("Finishing countByDateBinTest");

        } catch (IOException e) {
            e.printStackTrace();
        }          
    }


    public static void main(String[] args) {
        countByDateBin ();

    }

}
