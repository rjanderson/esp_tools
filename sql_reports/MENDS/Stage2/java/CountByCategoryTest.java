package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Generator for Counts by Category tests
 */
public class CountByCategoryTest {
    

    /**
     * Generates SQL for Count by Category test.
     * Reads from count_by_category_in.txt, writes to count_by_category.pg.sql
     * <p>Expects each input record to have one value for each test:
     * <p>- ESP_Column, in the form table.column
     */
    public static void countByCategoryTest () {

        System.out.println("Starting countByCategoryTest");
        try {
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            
            Scanner in = new Scanner(Path.of("count_by_category_in.txt"));
            PrintWriter out = new PrintWriter("count_by_category." + rdbms + ".sql", StandardCharsets.UTF_8);
            // Delimiter for Scanner defaults to whitespace. Use tabs or blanks as separators.
            
            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + ")");
            out.println(" * Counts by Category (ungrouped)...");
            out.println("*/"); 
            out.println("");
            
            // Drop and create table, using rdbms-specific syntax
            if (rdbms.equals("ms")) {
              out.println("if object_id('cii_qa.count_by_category', 'U') is not null drop table cii_qa.count_by_category;");
              out.println("create table cii_qa.count_by_category ");
              out.println("(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)");
              out.println(", category varchar(MAX), count integer);");  
              
            } else if (rdbms.equals("pg")) {           
              out.println("drop table if exists cii_qa.count_by_category;");
              out.println("create table cii_qa.count_by_category ");
              out.println("(test_name text, esp_table text, esp_column text");
              out.println(", category text, count integer);");                
            }
            
            // Loop through the input, generating SQL for each line
            while (in.hasNextLine()) {
                // Get ESP_Column, split it into TableName and ColumnName
                String ESP_Column = in.next();
                if(ESP_Column.startsWith("#")) {
                    continue; // Column has been commented out. Skip it
                }
                System.out.println("ESP_Column: " + ESP_Column);
                String TableName = ESP_Column.substring(0,ESP_Column.indexOf("."));
                String ColumnName = ESP_Column.substring(ESP_Column.indexOf(".")+1);
            
                // Write out the SQL for this test
                out.println("insert into cii_qa.count_by_category");
                out.println("(test_name, esp_table, esp_column, category, count) ");
                out.println("select");
                out.println("'Count by category' as test_name");
                if (ESP_Column.equals("emr_socialhistory.tobacco_use")) {
                    out.println(", '" + TableName + "' as ESP_Table, 'tobacco_use (mapped)' as ESP_Column");
                    out.println(", category, count(*) as count");
                    out.println("from (");
                    // out.println("select case when mapped_use is null then a.tobacco_use else mapped_use end as category"); 
                    out.println("select coalesce(mapped_value, a.tobacco_use) as category -- v0.17:1");
                    out.println("from emr_socialhistory a");
                    // out.println("left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use");
                    out.println("left outer join (");
                    out.println("  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'");
                    out.println("  ) b on b.src_value = a.tobacco_use");
                    out.println(") p");
                    out.println("group by category");
                    
                } else {
                    out.println(", '" + TableName + "' as ESP_Table, '" + ColumnName + "' as ESP_Column");
                    if (rdbms.equals("ms")) {
                        out.println(", cast("+ ColumnName + " as varchar(MAX)) as category, count(*) as count");                     
                    } else if (rdbms.equals("pg")) {
                        // out.println(", cast("+ ColumnName + " as text) as category, count(*) as count");
                        // Escape text beginning with "--"
                        out.println(", case ");
                        out.println("  when cast("+ ColumnName + " as text) like '--%' then '''' || cast("+ ColumnName + " as text)");
                        out.println("  else cast("+ ColumnName + " as text)");
                        out.println("  end as category, count(*) as count");
                    }
                    if (ESP_Column.equals("emr_encounter.pregnant")) {
                        out.println("from (");
                        out.println("  select ");
                        out.println("    pregnant as pregnant");
                        if (rdbms.equals("ms")) {
                            out.println("  , case when cast(pregnant as varchar(MAX)) = '' then 1 else 0 end as is_empty");   
                        } else if (rdbms.equals("pg")) {
                            out.println("  , case when cast(pregnant as text) = '' then 1 else 0 end as is_empty"); 
                        }
                        out.println("  from (");
                        out.println("    select ts.name as event_name");
                        out.println("    , case ");
                        out.println("      when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'");
                        out.println("      else 'n'");
                        out.println("      end as pregnant");
                        out.println("    from emr_encounter enc");
                        out.println("    left outer join hef_timespan ts");
                        out.println("    on ts.patient_id = enc.patient_id");
                        out.println("  ) p");
                        out.println(") q");

                    } else {
                        out.println("from " + TableName);
                    }
                    out.println("group by " + ColumnName);   
                }
                out.println(";");
                
                // Collect any trailing fields on the line
                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                    if (!junk.isEmpty()) {
                    System.out.println("junk: " + junk);
                    }
                }
            
            }  // while (in.hasNextLine())
            
            // Finished the loop. Write out select..order by statement for viewing the results. Clean up.
            out.println("");
            out.println("select * from cii_qa.count_by_category");
            out.println("order by esp_table, esp_column, category");
            out.println(";");
            
            out.println("/* ...end of Counts by Category (ungrouped) */");
            out.flush();
            out.close();
            
            System.out.println("Finishing countByCategoryTest");
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Main method. Use for exercising countByCategoryTest
     * @param args Input arguments. None are expected.
     */
    public static void main(String[] args) {
        CountByCategoryTest.countByCategoryTest();
    }

}
