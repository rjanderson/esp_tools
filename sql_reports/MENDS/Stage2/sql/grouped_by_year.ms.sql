

/* MENDS Stage 2 Tests (ms gen)
 * Counts Grouped by Year tests ...
*/

if object_id('cii_qa.grouped_by_year','U') is not null drop table cii_qa.grouped_by_year;
create table cii_qa.grouped_by_year 
(test_name nvarchar(MAX),  esp_column varchar(MAX)
, grouping_column_name varchar(MAX), year_min integer, year_max integer, year integer
, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float
, pct_null float, count integer);

/* emr_patient.age_yrs_derived */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_patient.age_yrs_derived' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(datediff(year, date_of_birth, sysdatetime())))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(created_timestamp) as year
  , percentile_disc(0.0) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p0
  , percentile_disc(0.1) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p10
  , percentile_disc(0.25) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p25
  , percentile_disc(0.75) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p75
  , percentile_disc(0.9) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p90
  , percentile_disc(1.0) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as p100
  , avg(datediff(year, date_of_birth, sysdatetime())) over (partition  by year(created_timestamp)) as mean
  , stdev(datediff(year, date_of_birth, sysdatetime())) over (partition  by year(created_timestamp)) as stddev
  , percentile_disc(0.5) within group (order by datediff(year, date_of_birth, sysdatetime())) over(partition by year(created_timestamp)) as median
  , date_of_birth
  from emr_patient
  where year(created_timestamp) between 2015 and 9999
  and datediff(year, date_of_birth, sysdatetime()) between 0 and 999  -- Apply Vitals Exclusion
  and datediff(year, date_of_birth, sysdatetime()) is not null
) a
group by year
;

/* emr_encounter.temperature */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.temperature' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(temperature))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by temperature) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by temperature) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by temperature) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by temperature) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by temperature) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by temperature) over(partition by year(date)) as p100
  , avg(temperature) over (partition by year(date)) as mean
  , stdev(temperature) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by temperature) over(partition by year(date)) as median
  , temperature
  from emr_encounter
  where year(date) between 2015 and 9999
  and temperature between 68 and 113  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and temperature is not null
) a
group by year
;

/* emr_encounter.weight */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.weight' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(weight))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by weight) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by weight) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by weight) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by weight) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by weight) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by weight) over(partition by year(date)) as p100
  , avg(weight) over (partition by year(date)) as mean
  , stdev(weight) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by weight) over(partition by year(date)) as median
  , weight
  from emr_encounter
  where year(date) between 2015 and 9999
  and weight between 0 and 453.6  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and weight is not null
) a
group by year
;

/* emr_encounter.height */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.height' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(height))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by height) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by height) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by height) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by height) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by height) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by height) over(partition by year(date)) as p100
  , avg(height) over (partition by year(date)) as mean
  , stdev(height) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by height) over(partition by year(date)) as median
  , height
  from emr_encounter
  where year(date) between 2015 and 9999
  and height between 0 and 228.6  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and height is not null
) a
group by year
;

/* emr_encounter.bp_systolic */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bp_systolic' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(bp_systolic))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by bp_systolic) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by bp_systolic) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by bp_systolic) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by bp_systolic) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by bp_systolic) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by bp_systolic) over(partition by year(date)) as p100
  , avg(bp_systolic) over (partition by year(date)) as mean
  , stdev(bp_systolic) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by bp_systolic) over(partition by year(date)) as median
  , bp_systolic
  from emr_encounter
  where year(date) between 2015 and 9999
  and bp_systolic between 20 and 300  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bp_systolic is not null
) a
group by year
;

/* emr_encounter.bp_diastolic */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bp_diastolic' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(bp_diastolic))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by bp_diastolic) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by bp_diastolic) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by bp_diastolic) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by bp_diastolic) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by bp_diastolic) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by bp_diastolic) over(partition by year(date)) as p100
  , avg(bp_diastolic) over (partition by year(date)) as mean
  , stdev(bp_diastolic) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by bp_diastolic) over(partition by year(date)) as median
  , bp_diastolic
  from emr_encounter
  where year(date) between 2015 and 9999
  and bp_diastolic between 5 and 200  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bp_diastolic is not null
) a
group by year
;

/* emr_encounter.bmi */
insert into cii_qa.grouped_by_year (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bmi' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
,  year
, round(max(p0),2) as min
, round(max(p10),2)  as p10
, round(max(p25),2)  as p25
, round(max(median),2)  as median
, round(max(p75),2)  as p75
, round(max(p90),2)  as p90
, round(max(p100),2)  as max
, round(max(mean),2) as mean
, round(max(stddev),2) as stddev
, round(100.0*(count(*) - count(bmi))/count(*),2) as pct_null
, count(*) as count
from (
  select
    year(date) as year
  , percentile_disc(0.0) within group (order by bmi) over(partition by year(date)) as p0
  , percentile_disc(0.1) within group (order by bmi) over(partition by year(date)) as p10
  , percentile_disc(0.25) within group (order by bmi) over(partition by year(date)) as p25
  , percentile_disc(0.75) within group (order by bmi) over(partition by year(date)) as p75
  , percentile_disc(0.9) within group (order by bmi) over(partition by year(date)) as p90
  , percentile_disc(1.0) within group (order by bmi) over(partition by year(date)) as p100
  , avg(bmi) over (partition by year(date)) as mean
  , stdev(bmi) over (partition by year(date)) as stddev
  , percentile_disc(0.5) within group (order by bmi) over(partition by year(date)) as median
  , bmi
  from emr_encounter
  where year(date) between 2015 and 9999
  and bmi between 12 and 70  -- Apply Vitals Exclusion
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bmi is not null
) a
group by year
;

select * from cii_qa.grouped_by_year
order by esp_column, year;
/* ...end of Counts Grouped by Year tests */
