/* Create fixed-size bins for current age (in years). 
 *   <5, 5-9, �80-85, 85+
 * Calculate age for each patient. 
 * Count number of patients in each bin.
 */

drop table if exists cii_qa.count_by_age_bin;
create table cii_qa.count_by_age_bin 
(test_name text, esp_table text, esp_column text
, bin text, count integer);

insert into cii_qa.count_by_age_bin (
test_name, esp_table, esp_column, bin, count )
select 
  'count by date bin' as test_name, 'emr_patient' as esp_table, 'current age (yrs)' as esp_column
, bin, count(*) as count from (
select 
  case
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 <  5 then 'b01_lt5'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 10 then 'b02_ge5_lt10'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 15 then 'b03_ge10_lt15'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 20 then 'b04_ge15_lt20'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 25 then 'b05_ge20_lt25'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 30 then 'b06_ge25_lt30'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 35 then 'b07_ge30_lt35'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 40 then 'b08_ge35_lt40'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 45 then 'b09_ge40_lt45'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 50 then 'b10_ge45_lt50'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 55 then 'b11_ge50_lt55'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 60 then 'b12_ge55_lt60'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 65 then 'b13_ge60_lt65'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 70 then 'b14_ge65_lt70'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 75 then 'b16_ge70_lt75'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 80 then 'b17_ge75_lt80'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 < 85 then 'b18_ge80_lt85'
  when (cast(current_date as date) - cast(date_of_birth as date))/365.25 >= 85 then 'b19_ge85'
  else null
  end as bin
from emr_patient pt
) a
group by bin;
select * from cii_qa.count_by_age_bin order by bin;
