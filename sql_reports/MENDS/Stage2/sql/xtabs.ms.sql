-- 1: Crosstab of emr_patient.state by emr_patient.gender
if object_id('cii_qa.xtab_state_by_gender', 'U') is not null drop table cii_qa.xtab_state_by_gender;
create table cii_qa.xtab_state_by_gender
(state varchar(MAX), gender varchar(MAX), n integer);
insert into cii_qa.xtab_state_by_gender 
(state, gender, n)
select state, gender, count(*) as n 
from (
  select
    case
    when pt.state in (
      'AL','AK','AZ','AR','CA','CO','CT','DC','DE','FL','GA','HI','ID','IL','IN'
    ,'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH'
    ,'NJ','NM','NY','NC','ND','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX'
    ,'UT','VI','VT','VA','WA','WV','WI','WY'
    )  then pt.state
    else 'UNK'
    end as state
  , case 
    when pt.gender is null or pt.gender = '' then 'UNK' 
    else upper(pt.gender) 
    end as gender
  from dbo.emr_patient pt) x
where upper(substring(pt.gender,1,1)) = 'F'
group by state, gender
; 
select * from cii_qa.xtab_state_by_gender order by state, gender;

-- 2: Crosstab of emr_patient.state by emr_encounter.pregnant
if object_id('cii_qa.xtab_state_by_pregnant', 'U') is not null drop table cii_qa.xtab_state_by_pregnant;
create table cii_qa.xtab_state_by_pregnant
(state varchar(MAX), is_pregnant varchar(MAX), n integer);
insert into cii_qa.xtab_state_by_pregnant 
(state, is_pregnant, n)
select state, is_pregnant, count(*) as n 
from (
  select
    case
    when pt.state in (
     'AL','AK','AZ','AR','CA','CO','CT','DC','DE','FL','GA','HI','ID','IL','IN'
    ,'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH'
    ,'NJ','NM','NY','NC','ND','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX'
    ,'UT','VI','VT','VA','WA','WV','WI','WY'
    )  then pt.state
    else 'UNK'
    end as state
  /* 3 lines replaced for calculation of is_pregant from hef_timespan
  , lower(substring(cast(enc.pregnant as varchar(MAX)),1,1)) as is_pregnant
  from dbo.emr_patient pt
  join dbo.emr_encounter enc on enc.patient_id = pt.id) x
  */

  , lower(substring(cast(ep.is_pregnant as varchar(MAX)),1,1)) as is_pregnant
  from dbo.emr_patient pt
  join -- dbo.emr_encounter enc on enc.patient_id = pt.id) x
     (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as is_pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     ) ep
  on ep.patient_id = pt.id
  ) y
    
group by state, is_pregnant
; 
select * from cii_qa.xtab_state_by_pregnant order by state, is_pregnant;

-- 3: Crosstab of emr_patient.state by emr_encounter_dx_codes.dx_code_id
if object_id('cii_qa.xtab_state_by_dx_code_id', 'U') is not null drop table cii_qa.xtab_state_by_dx_code_id;
create table cii_qa.xtab_state_by_dx_code_id
(state varchar(MAX), dx_code_id varchar(MAX), count integer);
insert into cii_qa.xtab_state_by_dx_code_id (
state, dx_code_id, count)
select state, dx_code_id, count(*) from (
    select
      case
      when p.state in (
      'AL','AK','AZ','AR','CA','CO','CT','DC','DE','FL','GA','HI','ID','IL','IN'
      ,'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH'
      ,'NJ','NM','NY','NC','ND','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX'
      ,'UT','VI','VT','VA','WA','WV','WI','WY'
      )  then p.state
      else 'UNK'
      end as state
--  , dx_code_id
   , case 
     when charindex('.',dx_code_id) > 0 then substring(dx_code_id, 1, charindex('.',dx_code_id)-1) 
     else dx_code_id 
     end as dx_code_id
    from (
      select 
        case when edc.dx_code_id is null or edc.dx_code_id = '' then 'UNK' else edc.dx_code_id end as dx_code_id
      , patient_id
      from dbo.emr_encounter_dx_codes edc
      join dbo.emr_encounter e on e.id = edc.encounter_id
      where lower(dx_code_id) <> 'icd9:799.9' -- v0.17:2
      ) d
    join dbo.emr_patient p
    on p.id = d.patient_id) x
group by state, dx_code_id
;
select * from cii_qa.xtab_state_by_dx_code_id order by state, dx_code_id;

-- 7: Crosstab of emr_patient.state by emr_immunization.name
if object_id('cii_qa.xtab_state_by_imm_name', 'U') is not null drop table cii_qa.xtab_state_by_imm_name;
create table cii_qa.xtab_state_by_imm_name
(state varchar(MAX), immunization_name varchar(MAX), count integer);
insert into cii_qa.xtab_state_by_imm_name 
(state, immunization_name, count)
select
  case
  when pt.state in (
   'AL','AK','AZ','AR','CA','CO','CT','DC','DE','FL','GA','HI','ID','IL','IN'
  ,'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH'
  ,'NJ','NM','NY','NC','ND','OH','OK','OR','PA','PR','RI','SC','SD','TN','TX'
  ,'UT','VI','VT','VA','WA','WV','WI','WY'
  )  then pt.state
  else 'UNK'
  end as state
, case 
  when imm.name is null then 'UNK'
  else imm.name
  end as immunization_name
, count(*) as count
from dbo.emr_patient pt
join dbo.emr_immunization imm on imm.patient_id = pt.id
group by state, name 
;
select * from cii_qa.xtab_state_by_imm_name order by state, immunization_name;


-- 15: Crosstab of emr_patient.race by emr_patient.gender
if object_id('cii_qa.xtab_race_by_gender', 'U') is not null drop table cii_qa.xtab_race_by_gender;
create table cii_qa.xtab_race_by_gender
(race varchar(MAX), gender varchar(MAX), n integer);
insert into cii_qa.xtab_race_by_gender(
race, gender, n)
select race, gender, count(*) as n from (
select 
 case when race is null or race = '' then 'UNK' else race end as race
, case when gender is null or gender = '' then 'UNK' else upper(gender) end as gender
from dbo.emr_patient) x
group by race, gender
;
select * from cii_qa.xtab_race_by_gender order by race, gender;

-- 16: Crosstab of emr_patient.race by emr_encounter.pregnant
if object_id('cii_qa.xtab_race_by_pregnant', 'U') is not null drop table cii_qa.xtab_race_by_pregnant;
create table cii_qa.xtab_race_by_pregnant
(race varchar(MAX), is_pregnant varchar(MAX), n integer);
insert into cii_qa.xtab_race_by_pregnant 
(race, is_pregnant, n)
select race, is_pregnant, count(*) as n 
from (
  select
    case
    when pt.race is null or pt.race = '' then 'UNK'
    else pt.race
    end as race
    /* Next 3 lines replaced with hef_timestamp-based lines 
    , lower(substring(cast(enc.pregnant as varchar(MAX)), 1, 1)) as is_pregnant
  from dbo.emr_patient pt
  join dbo.emr_encounter enc on enc.patient_id = pt.id) x
  */
  /* Start hef_timestamp-based code */
  , lower(substring(cast(ep.is_pregnant as varchar(MAX)),1,1)) as is_pregnant
  from dbo.emr_patient pt
  join -- dbo.emr_encounter enc on enc.patient_id = pt.id) x
     (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as is_pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     ) ep
  on ep.patient_id = pt.id
  where upper(substring(pt.gender,1,1)) = 'F' --14.16 limit to females
  ) y
  /* End hef_timestamp-based code */
group by race, is_pregnant
; 
select * from cii_qa.xtab_race_by_pregnant order by race, is_pregnant;

-- 17: Crosstab of emr_patient.race by emr_encounter_dx_codes.dx_code_id
if object_id('cii_qa.xtab_race_by_dx_code_id', 'U') is not null drop table cii_qa.xtab_race_by_dx_code_id;
create table cii_qa.xtab_race_by_dx_code_id
(race varchar(MAX), dx_code_id varchar(MAX), count integer);
insert into cii_qa.xtab_race_by_dx_code_id (
race, dx_code_id, count)
select race, dx_code_id, count(*) from (
    select
      case when p.race is null or p.race = '' then 'UNK' else p.race end as race
--  , dx_code_id
   , case 
     when charindex('.',dx_code_id) > 0 then substring(dx_code_id, 1, charindex('.',dx_code_id)-1) 
     else dx_code_id 
     end as dx_code_id
    from (
      select 
        case when edc.dx_code_id is null or edc.dx_code_id = '' then 'UNK' else edc.dx_code_id end as dx_code_id
      , patient_id
      from dbo.emr_encounter_dx_codes edc
      join dbo.emr_encounter e on e.id = edc.encounter_id
      where lower(dx_code_id) <> 'icd9:799.9' -- v0.17:2
      ) d
    join dbo.emr_patient p
    on p.id = d.patient_id) x
group by race, dx_code_id
;
select * from cii_qa.xtab_race_by_dx_code_id order by race, dx_code_id;


-- 21: Crosstab of emr_patient.race by emr_prescription.tobacco_use
if object_id('cii_qa.xtab_race_by_tobacco_use', 'U') is not null drop table cii_qa.xtab_race_by_tobacco_use;
create table cii_qa.xtab_race_by_tobacco_use
(race varchar(MAX), tobacco_use varchar(MAX), count integer);
insert into cii_qa.xtab_race_by_tobacco_use 
(race, tobacco_use, count)
select
  case
  when pt.race is null or pt.race = '' then 'UNK' 
  else pt.race
  end as race 
, case 
  when sochist.tobacco_use is null then 'UNK'
  else sochist.tobacco_use
  end as tobacco_use
, count(*) as count
from dbo.emr_patient pt
-- join dbo.emr_socialhistory sochist on sochist.patient_id = pt.id
join (
select a.patient_id as patient_id
/*
, case when b.mapped_use is null then a.tobacco_use else b.mapped_use end as tobacco_use
from
dbo.emr_socialhistory a
left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use
*/

, case when b.mapped_value is null then a.tobacco_use else b.mapped_value end as tobacco_use
from
dbo.emr_socialhistory a
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use

) sochist
on sochist.patient_id = pt.id
group by race, tobacco_use;
select * from cii_qa.xtab_race_by_tobacco_use order by race, tobacco_use;

-- 22: Crosstab of emr_patient.race by emr_immunization.name
if object_id('cii_qa.xtab_race_by_imm_name', 'U') is not null drop table cii_qa.xtab_race_by_imm_name;
create table cii_qa.xtab_race_by_imm_name
(race varchar(MAX), immunization_name varchar(MAX), count integer);
insert into cii_qa.xtab_race_by_imm_name 
(race, immunization_name, count)
select
  case
  when pt.race is null or pt.race = '' then 'UNK' 
  else pt.race
  end as race
, case 
  when imm.name is null then 'UNK'
  else imm.name
  end as immunization_name
, count(*) as count
from dbo.emr_patient pt
join dbo.emr_immunization imm on imm.patient_id = pt.id
group by race, name;
select * from cii_qa.xtab_race_by_imm_name order by race, immunization_name;

-- 23: Crosstab of emr_patient.ethnicity by emr_patient.gender
if object_id('cii_qa.xtab_ethn_by_gender', 'U') is not null drop table cii_qa.xtab_ethn_by_gender;
create table cii_qa.xtab_ethn_by_gender
(ethnicity varchar(MAX), gender varchar(MAX), n integer);
insert into cii_qa.xtab_ethn_by_gender(
ethnicity, gender, n)
select ethnicity, gender, count(*) as n from (
select 
 case when ethnicity is null or ethnicity = '' then 'UNK' else ethnicity end as ethnicity
, case when gender is null or gender = '' then 'UNK' else upper(gender) end as gender
from dbo.emr_patient) x
group by ethnicity, gender
;
select * from cii_qa.xtab_ethn_by_gender order by ethnicity, gender;

-- 24: Crosstab of emr_patient.ethnicity by emr_encounter.pregnant
if object_id('cii_qa.xtab_ethn_by_pregnant', 'U') is not null drop table cii_qa.xtab_ethn_by_pregnant;
create table cii_qa.xtab_ethn_by_pregnant
(ethnicity varchar(MAX), is_pregnant varchar(MAX), n integer);
insert into cii_qa.xtab_ethn_by_pregnant 
(ethnicity, is_pregnant, n)
select ethnicity, is_pregnant, count(*) as n 
from (
  select
    case
    when pt.ethnicity is null or pt.ethnicity = '' then 'UNK'
    else pt.ethnicity
    end as ethnicity
    /* 3 lines are replaced by hef_timespan lookup
  , lower(substring(cast(enc.pregnant as varchar(MAX)), 1, 1)) as is_pregnant
  from dbo.emr_patient pt
  join dbo.emr_encounter enc on enc.patient_id = pt.id) x
  */
  /* Start hef_timestamp lookup */
  , lower(substring(cast(ep.is_pregnant as varchar(MAX)),1,1)) as is_pregnant
  from dbo.emr_patient pt
  join -- dbo.emr_encounter enc on enc.patient_id = pt.id) x
     (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as is_pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     where upper(substring(pt.gender,1,1)) = 'F' --14.16 limit to females
     ) ep
  on ep.patient_id = pt.id
  ) y
  /* End hef_timestamp lookup code */
group by ethnicity, is_pregnant
; 
select * from cii_qa.xtab_ethn_by_pregnant order by ethnicity, is_pregnant;

-- 25: Crosstab of emr_patient.ethnicity by emr_encounter_dx_codes.dx_code_id
if object_id('cii_qa.xtab_ethn_by_dx_code_id', 'U') is not null drop table cii_qa.xtab_ethn_by_dx_code_id;
create table cii_qa.xtab_ethn_by_dx_code_id
(ethnicity varchar(MAX), dx_code_id varchar(MAX), count integer);
insert into cii_qa.xtab_ethn_by_dx_code_id (
ethnicity, dx_code_id, count)
select ethnicity, dx_code_id, count(*) from (
    select
      case when p.ethnicity is null or p.ethnicity = '' then 'UNK' else p.ethnicity end as ethnicity
--  , dx_code_id
   , case 
     when charindex('.',dx_code_id) > 0 then substring(dx_code_id, 1, charindex('.',dx_code_id)-1) 
     else dx_code_id 
     end as dx_code_id
    from (
      select 
        case when edc.dx_code_id is null or edc.dx_code_id = '' then 'UNK' else edc.dx_code_id end as dx_code_id
      , patient_id
      from dbo.emr_encounter_dx_codes edc
      join dbo.emr_encounter e on e.id = edc.encounter_id
      where lower(dx_code_id) <> 'icd9:799.9' -- v0.17:2
      ) d
    join dbo.emr_patient p
    on p.id = d.patient_id) x
group by ethnicity, dx_code_id
;
select * from cii_qa.xtab_ethn_by_dx_code_id order by ethnicity, dx_code_id;

-- 29: Crosstab of emr_patient.ethnicity by emr_prescription.tobacco_use
if object_id('cii_qa.xtab_ethn_by_tobacco_use', 'U') is not null drop table cii_qa.xtab_ethn_by_tobacco_use;
create table cii_qa.xtab_ethn_by_tobacco_use
(ethnicity varchar(MAX), tobacco_use varchar(MAX), count integer);
insert into cii_qa.xtab_ethn_by_tobacco_use 
(ethnicity, tobacco_use, count)
select
  case
  when pt.ethnicity is null or pt.ethnicity = '' then 'UNK' 
  else pt.ethnicity
  end as ethnicity 
, case 
  when sochist.tobacco_use is null then 'UNK'
  else sochist.tobacco_use
  end as tobacco_use
, count(*) as count
from dbo.emr_patient pt
-- join dbo.emr_socialhistory sochist on sohist.patient_id = pt.id
join (
select a.patient_id as patient_id
-- , case when b.mapped_use is null then a.tobacco_use else b.mapped_use end as tobacco_use
, case when b.mapped_value is null then a.tobacco_use else b.mapped_value end as tobacco_use
from
dbo.emr_socialhistory a
-- left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use
) sochist
on sochist.patient_id = pt.id
--
group by ethnicity, tobacco_use;
select * from cii_qa.xtab_ethn_by_tobacco_use order by ethnicity, tobacco_use;

-- 30: Crosstab of emr_patient.ethnicity by emr_immunization.name
if object_id('cii_qa.xtab_ethn_by_imm_name', 'U') is not null drop table cii_qa.xtab_ethn_by_imm_name;
create table cii_qa.xtab_ethn_by_imm_name
(ethnicity varchar(MAX), immunization_name varchar(MAX), count integer);
insert into cii_qa.xtab_ethn_by_imm_name 
(ethnicity, immunization_name, count)
select
  case
  when pt.ethnicity is null or pt.ethnicity = '' then 'UNK' 
  else pt.ethnicity
  end as ethnicity
, case 
  when imm.name is null then 'UNK'
  else imm.name
  end as immunization_name
, count(*) as count
from dbo.emr_patient pt
join dbo.emr_immunization imm on imm.patient_id = pt.id
group by ethnicity, name;
select * from cii_qa.xtab_ethn_by_imm_name order by ethnicity, immunization_name;

-- 31: Crosstab of emr_patient.gender by emr_encounter.pregnant
if object_id('cii_qa.xtab_gender_by_pregnant', 'U') is not null drop table cii_qa.xtab_gender_by_pregnant;
create table cii_qa.xtab_gender_by_pregnant
(gender varchar(MAX), is_pregnant varchar(MAX), n integer);
insert into cii_qa.xtab_gender_by_pregnant 
(gender, is_pregnant, n)
select gender, is_pregnant, count(*) as n 
from (
  select
    case
    when pt.gender is null or pt.gender = '' then 'UNK'
    else pt.gender
    end as gender
  /* 3 lines are replaced by hef_timespan lookup	    
  , lower(substring(cast(enc.pregnant as varchar(MAX)), 1, 1)) as is_pregnant
  from dbo.emr_patient pt
  join dbo.emr_encounter enc on enc.patient_id = pt.id) x
  */
  /* Start hef_timestamp lookup code */
  , lower(substring(cast(ep.is_pregnant as varchar(MAX)),1,1)) as is_pregnant
  from dbo.emr_patient pt
  join -- dbo.emr_encounter enc on enc.patient_id = pt.id) x
     (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as is_pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     ) ep
  on ep.patient_id = pt.id
  ) y
  /* End hef_timestamp lookup code */    
group by gender, is_pregnant
; 
select * from cii_qa.xtab_gender_by_pregnant order by gender, is_pregnant;

-- 32: Crosstab of emr_patient.gender by emr_encounter_dx_codes.dx_code_id
if object_id('cii_qa.xtab_gender_by_dx_code_id', 'U') is not null drop table cii_qa.xtab_gender_by_dx_code_id;
create table cii_qa.xtab_gender_by_dx_code_id
(gender varchar(MAX), dx_code_id varchar(MAX), count integer);
insert into cii_qa.xtab_gender_by_dx_code_id (
gender, dx_code_id, count)
select gender, dx_code_id, count(*) from (
    select
      case when p.gender is null or p.gender = '' then 'UNK' else p.gender end as gender
--  , dx_code_id
   , case 
     when charindex('.',dx_code_id) > 0 then substring(dx_code_id, 1, charindex('.',dx_code_id)-1) 
     else dx_code_id 
     end as dx_code_id
    from (
      select 
        case when edc.dx_code_id is null or edc.dx_code_id = '' then 'UNK' else edc.dx_code_id end as dx_code_id
      , patient_id
      from dbo.emr_encounter_dx_codes edc
      join dbo.emr_encounter e on e.id = edc.encounter_id
      where lower(dx_code_id) <> 'icd9:799.9' -- v0.17:2
      ) d
    join dbo.emr_patient p
    on p.id = d.patient_id) x
group by gender, dx_code_id
;
select * from cii_qa.xtab_gender_by_dx_code_id order by gender, dx_code_id;

-- 36: Crosstab of emr_patient.gender by emr_prescription.tobacco_use
if object_id('cii_qa.xtab_gender_by_tobacco_use', 'U') is not null drop table cii_qa.xtab_gender_by_tobacco_use;
create table cii_qa.xtab_gender_by_tobacco_use
(gender varchar(MAX), tobacco_use varchar(MAX), count integer);
insert into cii_qa.xtab_gender_by_tobacco_use 
(gender, tobacco_use, count)
select
  case
  when pt.gender is null or pt.gender = '' then 'UNK' 
  else pt.gender
  end as gender 
, case 
  when sochist.tobacco_use is null then 'UNK'
  else sochist.tobacco_use
  end as tobacco_use
, count(*) as count
from dbo.emr_patient pt
-- join dbo.emr_socialhistory sochist on sohist.patient_id = pt.id
join (
select a.patient_id as patient_id
-- , case when b.mapped_use is null then a.tobacco_use else b.mapped_use end as tobacco_use
, case when b.mapped_value is null then a.tobacco_use else b.mapped_value end as tobacco_use
from
dbo.emr_socialhistory a
-- left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use
) sochist
on sochist.patient_id = pt.id
--
group by gender, tobacco_use;
select * from cii_qa.xtab_gender_by_tobacco_use 
order by gender, tobacco_use;

-- 37: Crosstab of emr_patient.gender by emr_immunization.name
if object_id('cii_qa.xtab_gender_by_imm_name', 'U') is not null drop table cii_qa.xtab_gender_by_imm_name;
create table cii_qa.xtab_gender_by_imm_name
(gender varchar(MAX), immunization_name varchar(MAX), count integer);
insert into cii_qa.xtab_gender_by_imm_name 
(gender, immunization_name, count)
select
  case
  when pt.gender is null or pt.gender = '' then 'UNK' 
  else pt.gender
  end as gender
, case 
  when imm.name is null then 'UNK'
  else imm.name
  end as immunization_name
, count(*) as count
from dbo.emr_patient pt
join dbo.emr_immunization imm on imm.patient_id = pt.id
group by gender, name;
select * from cii_qa.xtab_gender_by_imm_name
order by gender, immunization_name;

-- 38: Crosstab of emr_encounter.pregnant by emr_encounter_dx_codes.dx_code_id
if object_id('cii_qa.xtab_pregnant_by_dx_code_id', 'U') is not null drop table cii_qa.xtab_pregnant_by_dx_code_id;
create table cii_qa.xtab_pregnant_by_dx_code_id
(dx_code_id varchar(MAX), pregnant varchar(MAX), n integer);
insert into cii_qa.xtab_pregnant_by_dx_code_id 
(dx_code_id, pregnant, n)
select m.dx_code_id, m.pregnant, count(*) as count
from (
select
  enc.pregnant as pregnant
, case 
  when edxc.dx_code_id is null or edxc.dx_code_id = '' then 'UNK' 
  when charindex('.',dx_code_id) > 0 then substring(dx_code_id, 1, charindex('.',dx_code_id)-1)
  else edxc.dx_code_id 
  end as dx_code_id
 -- , count(*) as count
from emr_encounter_dx_codes edxc
join  -- emr_encounter enc 
     (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as pregnant
     from emr_encounter enc
     join emr_patient pt on pt.id = enc.patient_id       -- (14.16) restrict to female patients
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     where upper(substring(pt.gender,1,1)) = 'F'   -- (14.16) restrict to female patients
     ) enc
on enc.encounter_id = edxc.encounter_id) m
where lower(dx_code_id) <> 'icd9:799.9' -- v0.17:2
group by pregnant, dx_code_id
; 
select * from cii_qa.xtab_pregnant_by_dx_code_id order by dx_code_id, pregnant;

-- 41: Crosstab of emr_encounter.pregnant by emr_socialhistory.tobacco_use
if object_id('cii_qa.xtab_pregnant_by_tobacco_use', 'U') is not null drop table cii_qa.xtab_pregnant_by_tobacco_use;
create table cii_qa.xtab_pregnant_by_tobacco_use
(pregnant varchar(MAX), tobacco_use varchar(MAX), n integer);
insert into cii_qa.xtab_pregnant_by_tobacco_use 
(pregnant, tobacco_use, n)
select
  lower(substring(cast(enc.pregnant as varchar(MAX)), 1, 1)) as pregnant
, case 
  when sochist.tobacco_use is null or sochist.tobacco_use = '' then 'UNK' 
  else sochist.tobacco_use end as tobacco_use
, count(*) as n 
from dbo.emr_patient pt
-- join dbo.emr_socialhistory sochist on sochist.patient_id = pt.id
-- join (select patient_id, coalesce(b.mapped_use, b.tobacco_use) as tobacco_use from dbo.emr_socialhistory a
--       left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use
join (select patient_id, coalesce(b.mapped_value, b.src_value) as tobacco_use from dbo.emr_socialhistory a
      left outer join (
        select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
        ) b on b.src_value = a.tobacco_use
     ) sochist on sochist.patient_id = pt.id
-- join public.emr_encounter enc on enc.patient_id = pt.id
join (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     ) enc on enc.patient_id = pt.id
where upper(pt.gender) = 'F'
group by pregnant, tobacco_use
; 
select * from cii_qa.xtab_pregnant_by_tobacco_use order by pregnant, tobacco_use;

-- 42: Crosstab of emr_encounter.pregnant by emr_immunization.name
if object_id('cii_qa.xtab_pregnant_by_imm_name', 'U') is not null drop table cii_qa.xtab_pregnant_by_imm_name;
create table cii_qa.xtab_pregnant_by_imm_name
(pregnant varchar(MAX), immunization_name varchar(MAX), n integer);
insert into cii_qa.xtab_pregnant_by_imm_name 
(pregnant, immunization_name, n)
select
  lower(substring(cast(enc.is_pregnant as varchar(MAX)), 1, 1)) as pregnant
, case 
  when imm.name is null or imm.name = '' then 'UNK' 
  else imm.name end as immunization_name
,  count(*) as n 
from dbo.emr_patient      pt
join dbo.emr_immunization imm on imm.patient_id = pt.id
--join dbo.emr_encounter    enc on enc.patient_id = pt.id
join (
     select enc.patient_id as patient_id, enc.id as encounter_id, ts.name as event_name
     , case 
       when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
       else 'n'
       end as is_pregnant
     from emr_encounter enc
     left outer join hef_timespan ts
     on ts.patient_id = enc.patient_id
     ) enc on enc.patient_id = pt.id
where upper(substring(pt.gender,1,1)) = 'F' --14.16 limit to females
group by is_pregnant, name
; 
select * from cii_qa.xtab_pregnant_by_imm_name order by pregnant, immunization_name;

-- 46: Crosstab of tobacco_use by immunization_name
if object_id('cii_qa.xtab_tobacco_use_by_imm_name', 'U') is not null drop table cii_qa.xtab_tobacco_use_by_imm_name;
create table cii_qa.xtab_tobacco_use_by_imm_name
(tobacco_use varchar(MAX), immunization_name varchar(MAX), count integer);
insert into cii_qa.xtab_tobacco_use_by_imm_name 
(tobacco_use, immunization_name, count)
select
  case 
  when sochist.tobacco_use is null or sochist.tobacco_use = '' then 'UNK' 
  else sochist.tobacco_use end as tobacco_use
, case 
  when imm.name is null or imm.name = '' then 'UNK' 
  else imm.name end as immunization_name
, count(*) as count
from dbo.emr_patient       pt
-- join dbo.emr_socialhistory sochist on sohist.patient_id = pt.id
join (
select a.patient_id as patient_id
-- , case when b.mapped_use is null then a.tobacco_use else b.mapped_use end as tobacco_use
, case when b.mapped_value is null then a.tobacco_use else b.mapped_value end as tobacco_use

from
dbo.emr_socialhistory a
-- left outer join cii_qa.tobacco_use_map b on b.tobacco_use = a.tobacco_use
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use
) sochist
on sochist.patient_id = pt.id
--
join dbo.emr_immunization  imm on imm.patient_id = pt.id
group by sochist.tobacco_use, name;
select * from cii_qa.xtab_tobacco_use_by_imm_name
order by tobacco_use, immunization_name;

-- 47: Crosstab of zip code by state
if object_id('cii_qa.xtab_zip_by_state', 'U') is not null drop table cii_qa.xtab_zip_by_state;
create table cii_qa.xtab_zip_by_state
(state varchar(MAX), zip varchar(MAX), n integer);
insert into cii_qa.xtab_zip_by_state 
(state, zip, n)
select state, zip, count(*) as n 
from (
  select
    case
    when pt.zip is null or pt.zip = '' then 'UNK'
    else substring(pt.zip, 1, 5)
    end as zip
  , case 
    when pt.state is null or pt.state = '' then 'UNK' 
    else upper(pt.state) 
    end as state
  from dbo.emr_patient pt
    ) x
group by state, zip
; 
select * from cii_qa.xtab_zip_by_state order by state, zip;