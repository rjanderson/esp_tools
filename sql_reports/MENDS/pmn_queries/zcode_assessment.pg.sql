with pat_counts as 
  (select to_char(e.date,'yyyy') as year, 
       count(*) as total_count_of_dx_codes,
       count(case when (dx.dx_code_id ilike 'icd10:z55%' 
                                 or dx.dx_code_id ilike 'icd10:z56%'
                                 or dx.dx_code_id ilike 'icd10:z57%'
                                 or dx.dx_code_id ilike 'icd10:z58%'
                                 or dx.dx_code_id ilike 'icd10:z59%'
                                 or dx.dx_code_id ilike 'icd10:z60%'
                                 or dx.dx_code_id ilike 'icd10:z61%'
                                 or dx.dx_code_id ilike 'icd10:z62%'
                                 or dx.dx_code_id ilike 'icd10:z63%'
                                 or dx.dx_code_id ilike 'icd10:z64%'
                                 or dx.dx_code_id ilike 'icd10:z65%') then dx.dx_code_id 
                           else null end) as total_count_of_z_codes,
       count(distinct e.patient_id) as total_patients_with_dx_codes,
       count(distinct case when (dx.dx_code_id ilike 'icd10:z55%' 
                                 or dx.dx_code_id ilike 'icd10:z56%'
                                 or dx.dx_code_id ilike 'icd10:z57%'
                                 or dx.dx_code_id ilike 'icd10:z58%'
                                 or dx.dx_code_id ilike 'icd10:z59%'
                                 or dx.dx_code_id ilike 'icd10:z60%'
                                 or dx.dx_code_id ilike 'icd10:z61%'
                                 or dx.dx_code_id ilike 'icd10:z62%'
                                 or dx.dx_code_id ilike 'icd10:z63%'
                                 or dx.dx_code_id ilike 'icd10:z64%'
                                 or dx.dx_code_id ilike 'icd10:z65%')
                                 then e.patient_id 
                           else null end) as total_patients_with_z_codes
   from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id 
   group by to_char(e.date,'yyyy')),
 code_counts as 
  (select to_char(e.date,'yyyy') as year, dx.dx_code_id, 
       count(*) as zcode_counts
   from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id 
   where (dx.dx_code_id ilike 'icd10:z55%' 
                                 or dx.dx_code_id ilike 'icd10:z56%'
                                 or dx.dx_code_id ilike 'icd10:z57%'
                                 or dx.dx_code_id ilike 'icd10:z58%'
                                 or dx.dx_code_id ilike 'icd10:z59%'
                                 or dx.dx_code_id ilike 'icd10:z60%'
                                 or dx.dx_code_id ilike 'icd10:z61%'
                                 or dx.dx_code_id ilike 'icd10:z62%'
                                 or dx.dx_code_id ilike 'icd10:z63%'
                                 or dx.dx_code_id ilike 'icd10:z64%'
                                 or dx.dx_code_id ilike 'icd10:z65%')
   group by to_char(e.date,'yyyy'), dx.dx_code_id),
 code_counts_with_row_number as 
  (select *, row_number() over (partition by year order by zcode_counts desc) as rownum 
   from code_counts)
 select pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes,
     max(dx_code_id) filter (where rownum =1) as first_code,
     max(zcode_counts) filter (where rownum =1) as first_code_count,
     max(dx_code_id) filter (where rownum =2) as second_code,
     max(zcode_counts) filter (where rownum =2) as second_code_count,
     max(dx_code_id) filter (where rownum =3) as third_code,
     max(zcode_counts) filter (where rownum =3) as third_code_count,
     max(dx_code_id) filter (where rownum =4) as fourth_code,
     max(zcode_counts) filter (where rownum =4) as fourth_code_count,
     max(dx_code_id) filter (where rownum =5) as fifth_code,
     max(zcode_counts) filter (where rownum =5) as fifth_code_count
 into temporary zcodes
 from pat_counts pc join code_counts_with_row_number ccrn on pc.year=ccrn.year
 group by pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes;
     
 \copy zcodes to '/tmp/zcodes.csv' csv header delimiter ',';
