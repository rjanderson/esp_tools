with pat_counts as 
  (select dty.year, 
       count(*) as total_count_of_dx_codes,
       count(case when (lower(dx.dx_code_id like 'icd10:z55%' 
                                 or lower(dx.dx_code_id) like 'icd10:z56%'
                                 or lower(dx.dx_code_id) like 'icd10:z57%'
                                 or lower(dx.dx_code_id) like 'icd10:z58%'
                                 or lower(dx.dx_code_id) like 'icd10:z59%'
                                 or lower(dx.dx_code_id) like 'icd10:z60%'
                                 or lower(dx.dx_code_id) like 'icd10:z61%'
                                 or lower(dx.dx_code_id) like 'icd10:z62%'
                                 or lower(dx.dx_code_id) like 'icd10:z63%'
                                 or lower(dx.dx_code_id) like 'icd10:z64%'
                                 or lower(dx.dx_code_id) like 'icd10:z65%') then dx.dx_code_id 
                           else null end) as total_count_of_z_codes,
       count(distinct e.patient_id) as total_patients_with_dx_codes,
       count(distinct case when (lower(dx.dx_code_id) like 'icd10:z55%' 
                                 or lower(dx.dx_code_id) like 'icd10:z56%'
                                 or lower(dx.dx_code_id) like 'icd10:z57%'
                                 or lower(dx.dx_code_id) like 'icd10:z58%'
                                 or lower(dx.dx_code_id) like 'icd10:z59%'
                                 or lower(dx.dx_code_id) like 'icd10:z60%'
                                 or lower(dx.dx_code_id) like 'icd10:z61%'
                                 or lower(dx.dx_code_id) like 'icd10:z62%'
                                 or lower(dx.dx_code_id) like 'icd10:z63%'
                                 or lower(dx.dx_code_id) like 'icd10:z64%'
                                 or lower(dx.dx_code_id) like 'icd10:z65%')
                                 then e.patient_id 
                           else null end) as total_patients_with_z_codes
   from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id 
   join gen_pop_tools.date_to_year dty on dty.date=e.date
   group by dty.year),
 code_counts as 
  (select dty.year, dx.dx_code_id, 
       count(*) as zcode_counts
   from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id 
   join gen_pop_tools.date_to_year dty on dty.date=e.date
   where (lower(dx.dx_code_id) like 'icd10:z55%' 
                                 or lower(dx.dx_code_id) ilike 'icd10:z56%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z57%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z58%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z59%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z60%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z61%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z62%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z63%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z64%'
                                 or lower(dx.dx_code_id) ilike 'icd10:z65%')
   group by dty.year, dx.dx_code_id),
 code_counts_with_row_number as 
  (select *, row_number() over (partition by year order by zcode_counts desc) as rownum 
   from code_counts),
 raw_totals as
 (select pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes,
     max(case when rownum=1 then dx_code_id end) as first_code,
     max(case when rownum=1 then zcode_counts end) as first_code_count,
     max(case when rownum=2 then dx_code_id end)  as second_code,
     max(case when rownum=2 then zcode_counts end) as second_code_count,
     max(case when rownum=3 then dx_code_id end) as third_code,
     max(case when rownum=3 then zcode_counts end) as third_code_count,
     max(case when rownum=4 then dx_code_id end) as fourth_code,
     max(case when rownum=4 then zcode_counts end) as fourth_code_count,
    max(case when rownum=5 then dx_code_id end) as fifth_code,
     max(case when rownum=5 then zcode_counts end) as fifth_code_count
 from pat_counts pc join code_counts_with_row_number ccrn on pc.year=ccrn.year
 group by pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes)
  select year, 
    case when total_count_of_dx_codes <= 10 then null else total_count_of_dx_codes end as total_count_of_dx_codes,
    case when total_count_of_z_codes <= 10 then null else total_count_of_z_codes end as total_count_of_z_codes,
    case when total_patients_with_dx_codes <= 10 then null else total_patients_with_dx_codes end as total_patients_with_dx_codes,
    case when total_patients_with_z_codes <= 10 then null else total_patients_with_z_codes end as total_patients_with_z_codes,
    first_code,
    case when first_code_count <= 10 then null else first_code_count end as first_code_count,
    second_code,
    case when second_code_count <= 10 then null else second_code_count end as second_code_count,
    third_code,
    case when third_code_count <= 10 then null else third_code_count end as third_code_count,
    fourth_code,
    case when fourth_code_count <= 10 then null else fourth_code_count end as fourth_code_count,
    fifth_code,
    case when fifth_code_count <= 10 then null else fifth_code_count end as fifth_code_count
  from raw_totals;
