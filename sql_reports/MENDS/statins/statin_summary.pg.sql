select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  group by status order by status;
  
--ever prescribed a statin
select count(*), 
  case substr(status, 2,1) 
       when 'A' then 'ASCVD'
       when 'D' then 'Diabetes'
       when 'H' then 'Hyperchol'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date, t1.case_id
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date  
  order by t0.patient_id, t1.date desc) t00
  where substr(status, 2,1) in ('A','D','H') 
    and exists (select null from nodis_caseactivehistory t2 where t2.case_id=t00.case_id and substr(t2.status,1,1) in ('O','H'))
  group by substr(status,2,1) order by substr(status,2,1);

  --statin eligible patients without statin rx and NO rx records
  Select count(*), 
  case when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       end counts
from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where status in ('NA','ND','NH')
    and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id)
  group by status;
  
  
  
  --all ldl patients
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c.patient_id) c.patient_id, cah.status
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  group by substr(status,1,1);
  
  --ldl patients with no rx records
select count(*) as counts from (
  select distinct on (c.patient_id) c.patient_id, cah.status
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  where status in ('NA','ND','NH')
    and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);

--ldl patients ever prescribed statins
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c.patient_id) c.patient_id, cah.status, cah.case_id
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
  group by substr(status,1,1);
  

  
 --all diabetes patients
  select count(*) as counts, substr(status,1,1) from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  group by substr(status,1,1);

--all diabetes patients with no RX
  select count(*) as counts from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  where status in ('NA','ND','NH')
  and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);

--Diabetes patients ever prescribed a statin  
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status, cah.case_id
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
  group by substr(status,1,1);
 
 
 
 --non-statin eligible
  select count(*) as counts, pattern from (
  select distinct on (p.id) p.id patient_id, ht.pattern
  from emr_patient p 
  left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
  left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
  where c.id is null
  order by p.id, ht.start_date desc) t00
  group by pattern;
  
--not statin eligible, no RX
  Select count(*) from (
  select distinct on (p.id) p.id patient_id, ht.pattern
  from emr_patient p 
  left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
  left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
  where c.id is null  and ht.patient_id is null
  order by p.id, ht.start_date desc) t00
  where not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);
  
  --here are the 55 most common prescriptions for patients who are statin-eligible.
  select name, count(*) as count
  from emr_prescription p
  where exists (select null from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where status in ('NA','ND','NH') and t00.patient_id=p.patient_id)
  group by name order by count(*) desc limit 55;


-->0 cholesterol tests  
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_labresult l join conf_labtestmap lm on lm.native_code=l.native_code where l.patient_id=t00.patient_id 
                  and lm.test_name in ('cholesterol-hdl','triglycerides','cholesterol-ldl','cholesterol-total'))
  group by status order by status;

-->0 rx 
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_prescription p where t00.patient_id=p.patient_id)
  group by status order by status;
  
--greater >1 ambulatory enc in past two years
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_encounter e 
                join gen_pop_tools.rs_conf_mapping m on m.src_field='raw_encounter_type' and m.src_value=e.raw_encounter_type and m.mapped_value='inpatient'
                where t00.patient_id=e.patient_id and e.date between '2021-01-01'::date and '2022-12-31'::Date)
  group by status order by status;




  
