select count(*), t0.name
into temporary tmp_other_prescriptions
from emr_prescription t0
join nodis_case t1 on t1.patient_id=t0.patient_id and t1.condition='statin'
where not exists (select null from hef_event t2 where t2.object_id=t0.id and t2.name ilike '%statin%');

select count(*), 
  case when t1.patient_id is null then 'no rx'
       else 'rx' 
  end as has_rx
from nodis_case t0
join (select distinct patient_id from emr_prescription)
where t0.condition='statin';



