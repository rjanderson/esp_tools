  with t0 as (select  aa patient_id, 0 as asthma, bz as primary_payer, bb as smoking, ae as zip, cb as census,
            case when "as"=5 then 4 else "as" end as ldl, au as a1c, to_date(af||ag, 'yyyymm') as asthma_date
            from gen_pop_tools.tt_out t_0
            where not exists (select null from gen_pop_tools.tt_out t_1 where t_1.bc=1 
                               and to_date(t_1.af||t_1.ag,'yyyymm') between '2021-01-01'::date and '2022-12-31'::date 
                               and t_1.aa=t_0.aa)
               and af='2022' and ag='12'),
     t1 as (select distinct on (t00.patient_id) t00.patient_id, t00.date, t00.bp_systolic
            from gen_pop_tools.tt_enc_bp t00
            join t0 on t0.patient_id=t00.patient_id and t0.asthma_date>=t00.date
            order by t00.patient_id, t00.date desc),
     t2 as (select distinct on (t01.patient_id) t01.patient_id, t01.date, t01.bmi
            from emr_encounter t01
            join t0 on t0.patient_id=t01.patient_id and t0.asthma_date>=t01.date and t01.BMI between 0 and 200
            order by t01.patient_id, t01.date desc),
     t3 as (select distinct on (t02.patient_id) t02.patient_id, to_date(t02.year_month,'yyyy_mm') date, t02.max_trig1 as trig
            from gen_pop_tools.tt_trig1 t02
            join t0 on t0.patient_id=t02.patient_id and t0.asthma_date>=to_date(t02.year_month,'yyyy_mm')
            order by t02.patient_id, to_date(t02.year_month,'yyyy_mm') desc),
     t4 as (select p.id as patient_id, t0.asthma,
              case
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 0 and 4 then '0-4'::varchar(5)
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 5 and 9 then '5-9'::varchar(5)
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 10 and 17 then '10-17'::varchar(5)
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 18 and 29 then '18-29'::varchar(5)
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 30 and 44 then '30-44'::varchar(5)
                when extract(YEAR from age('2022-01-01'::date, p.date_of_birth)) between 45 and 64 then '45-64'::varchar(5)
                else '65+'::varchar(5)
              end age_group,
              case
                when race.mapped_value is not null then race.mapped_value
                else 'NULL Race'
              end race,
              case
                when ethn.mapped_value is not null then ethn.mapped_value
                else 'NULL ethn'
              end ethnicity,
              case 
                when sex.mapped_value is not null then sex.mapped_value
                else 'NULL Gender'::varchar(4)
              end gender,
              case
                when t0.primary_payer=0 then 'Other - Unknown - Self-Pay'
                when t0.primary_payer=4 then 'Medicare'
                when t0.primary_payer=2 then 'Auto - Worker''s Comp'
                when t0.primary_payer=3 then 'Medicaid'
                when t0.primary_payer=1 then 'Commercial - Blue Cross'
                else 'NULL payer'
              end payer,
              case
                when t0.smoking=2 then 'Passive Smoker'
                when t0.smoking=3 then 'Former Smoker'
                when t0.smoking=4 then 'Current Smoker'
                when t0.smoking=1 then 'Never Smoker'
                else 'NULL smoking'
              end smoker,
              case
                when t2.bmi <= 18.4 then 'Underweight'
                when t2.bmi > 18.4 and t2.bmi<=24.9 then 'Normalweight'
                when t2.bmi > 24.9 and t2.bmi<=29.9 then 'Overweight'
                when t2.bmi > 29.9 then 'Obese'
                else 'NULL BMI'
              end bmi,
              case
                when t1.bp_systolic <=130 then 'Normal BP Grp1'
                when t1.bp_systolic > 130 and t1.bp_systolic <140 then 'Borderline BP Grp1'
                when t1.bp_systolic >= 140 then 'High BP Grp1'
                else 'NULL BP Grp1'
              end bp_grp1,
              case
                when t1.bp_systolic <120 then 'Normal BP Grp2'
                when t1.bp_systolic >= 120 and t1.bp_systolic <130 then 'Elevated BP Grp2'
                when t1.bp_systolic >= 130 and t1.bp_systolic <140 then 'Stage 1 BP Grp2'
                when t1.bp_systolic >= 140 then 'Stage 2 BP Grp2'
                else 'NULL BP Grp2'
              end bp_grp2,
              case 
                when t0.ldl = 1 then '<100'
                when t0.ldl = 2 then '100-129' 
                when t0.ldl = 3 then '130-159'
                when t0.ldl = 4 then '>160'
                else 'NULL LDL' 
              end ldl, 
              case
                when t3.trig < 200 then 'trig former ideal'
                when t3.trig >=200 and t3.trig < 500 then 'trig former elevated'
                when t3.trig >= 500 then 'trig former high'
                else 'NULL trig former' 
              end trig_former,
              case 
                when t3.trig < 150 then 'trig current ideal'
                when t3.trig >=150 and t3.trig < 200 then 'trig current boderline high'
                when t3.trig >=200 and t3.trig < 500 then 'trig current high'
                when t3.trig >= 500 then 'trig current very high'
                else 'NULL trig current' 
              end trig_current, 
              case 
                when t0.a1c = 1 then '<5.6'
                when t0.a1c = 2 then '5.7-6.4' 
                when t0.a1c = 3 then '6.5-8'
                when t0.a1c = 4 then '>8'
                else 'NULL A1C' 
              end a1c, 
              case when t0.zip is not null then t0.zip else 'NULL zip' end zip, 
              case when t0.census is not null then to_char(t0.census,'fm000000')  else 'NULL census' end census
            from emr_patient p
            left join gen_pop_tools.rs_conf_mapping race on p.race=race.src_value and race.src_field='race'
            left join gen_pop_tools.rs_conf_mapping ethn on p.ethnicity=ethn.src_value and ethn.src_field='ethnicity'
            left join gen_pop_tools.rs_conf_mapping sex on p.gender=sex.src_value and sex.src_field='gender'
            join t0 on p.id=t0.patient_id
            left join t1 on t1.patient_id=t0.patient_id
            left join t2 on t2.patient_id=t0.patient_id
            left join t3 on t3.patient_id=t0.patient_id
            where p.date_of_birth between '1910-01-01'::date and now()
            )
     select 
       case when count(asthma) >= 5 then count(asthma) else null end as asthma_counts,
       age_group, race, ethnicity, gender, payer, smoker, bmi, bp_grp1, bp_grp2, ldl, 
       trig_former, trig_current, t4.zip, census 
     from t4
     join gen_pop_tools.county_censustract t5 on t5.censustract=t4.census
     join gen_pop_tools.county_zip t6 on t6.zip=t4.zip
     group by grouping sets (
       (age_group, race), (age_group,ethnicity), (age_group,gender), (age_group,payer),(age_group,smoker), (age_group,BMI),
       (age_group, bp_grp1), (age_group,bp_grp2), (age_group,ldl), (age_group, trig_former), (age_group, trig_current),
       (race,ethnicity), (race,gender), (race,payer),(race,smoker), (race,BMI),
       (race, bp_grp1), (race,bp_grp2), (race,ldl), (race, trig_former), (race, trig_current),
       (ethnicity,gender), (ethnicity,payer),(ethnicity,smoker), (ethnicity,BMI),
       (ethnicity, bp_grp1), (ethnicity,bp_grp2), (ethnicity,ldl), (ethnicity, trig_former), (ethnicity, trig_current),
       (gender,payer),(gender,smoker), (gender,BMI),
       (gender, bp_grp1), (gender,bp_grp2), (gender,ldl), (gender, trig_former), (gender, trig_current),
       (payer,smoker), (payer,BMI),
       (payer, bp_grp1), (payer,bp_grp2), (payer,ldl), (payer, trig_former), (payer, trig_current),
       (smoker,BMI),
       (smoker, bp_grp1), (smoker,bp_grp2), (smoker,ldl), (smoker, trig_former), (smoker, trig_current),
       (BMI, bp_grp1), (BMI,bp_grp2), (BMI,ldl), (BMI, trig_former), (BMI, trig_current),
       (bp_grp1,ldl), (bp_grp1, trig_former), (bp_grp1, trig_current),
       (bp_grp2,ldl), (bp_grp2, trig_former), (bp_grp2, trig_current),
       (ldl, trig_former), (ldl, trig_current), (trig_former, trig_current),
       (age_group, census), (race, census), (ethnicity, census), (gender,census), (payer,census), (smoker,census),
       (BMI, census), (bp_grp1, census), (bp_grp2,census), (ldl, census), (trig_former, census), (trig_current,census),
       (age_group, t4.zip), (race, t4.zip), (ethnicity, t4.zip), (gender, t4.zip), (payer, t4.zip), (smoker, t4.zip),
       (BMI, t4.zip), (bp_grp1, t4.zip), (bp_grp2,t4.zip), (ldl, t4.zip), (trig_former, t4.zip), (trig_current,t4.zip),
       (age_group), (race), (ethnicity), (gender), (payer), (smoker), (BMI), (bp_grp1), (bp_grp2),
       (ldl), (trig_former), (trig_current), (t4.zip), (census), () )

