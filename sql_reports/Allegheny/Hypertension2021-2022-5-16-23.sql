select disease, age_group, race, ethnicity, gender, primary_payer, zip, census, 
case when count(*) <= 5 then null else count(*) end as counts 
from 
(select p.patient_id, 
case when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 18 and 24 then '18-24'::varchar(5) 
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 25 and 34 then '25-34'::varchar(5) 
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 35 and 44 then '35-44'::varchar(5) 
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 45 and 54 then '45-54'::varchar(5) 
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 55 and 64 then '55-64'::varchar(5)
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) between 65 and 74 then '65-74'::varchar(5)
     when extract(YEAR from age('2022-01-01'::date, date_of_birth)) > 75 then '75+'::varchar(5)
	 else 'NA'
end age_group, 
coalesce(nullif(trim(p.gender),''),'NA') gender, 
coalesce(nullif(trim(p.race),''),'NA') race, 
coalesce(nullif(trim(p.ethnicity),''),'NA') ethnicity,
coalesce(nullif(trim(pp.primary_payer),''),'NA') primary_payer,
coalesce(nullif(trim(cz.zip),''),'NA') zip,
coalesce(nullif(trim(cc.censustract),''),'NA') census,
case when cah.status = 'C' then 'Controlled Hypertension'
     when cah.status = 'U' then 'Uncontrolled Hypertension'
	 when cah.status = 'UK' then 'Unknown (no recent BP data)'
	 else 'NA'
end disease
from gen_pop_tools.tt_pat_alt p 
join nodis_case c on p.patient_id=c.patient_id and c.condition = 'diagnosedhypertension' 
join (select *, max(date) over (partition by case_id) maxdt from nodis_caseactivehistory) cah on cah.case_id=c.id and cah.date=cah.maxdt 
     and cah.status in ('U','C','UK') 
left join (select distinct on (patient_id)
		      patient_id, year_month, cm.mapped_value primary_payer
		   from gen_pop_tools.tt_primary_payer pp
		   join gen_pop_tools.rs_conf_mapping cm on cm.code=pp.primary_payer and cm.src_field='primary_payer'
		   order by patient_id, year_month desc) pp
  on pp.patient_id=p.patient_id and pp.year_month >='2020_12' and pp.year_month < '2023_01' 
left join gen_pop_tools.county_zip cz on cz.zip=p.zip
left join gen_pop_tools.patient_census_tract ct on ct.patient_id=p.patient_id
left join gen_pop_tools.county_censustract cc on cc.censustract=trim(to_char(ct.census_tract_id,'9999999999'))
where  extract(YEAR from age(now(), date_of_birth)) >=18     
) t0
group by grouping sets ((disease, zip), (disease, zip, age_group),(disease, zip, race),(disease, zip, ethnicity),(disease, zip, gender), (disease, zip, primary_payer), 
                        (disease, census),(disease, census, age_group),(disease, census, race),(disease, census, ethnicity),(disease, census, gender), (disease, census, primary_payer), 
                        (disease, age_group),(disease, race),(disease, ethnicity),(disease, gender),(disease, primary_payer),
						(disease, age_group, gender), (disease, age_group, race),(disease, age_group,primary_payer), (disease, age_group, ethnicity),
                        (disease, race, gender),(disease, race,primary_payer), (disease, gender, primary_payer)
						);