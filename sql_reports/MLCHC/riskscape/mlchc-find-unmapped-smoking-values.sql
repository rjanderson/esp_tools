select count(*), tobacco_use
from emr_socialhistory
where tobacco_use not in (select src_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use' )
and tobacco_use not in (
'',
'HEALTH_MAINTENANCE_COMPLETED',
'U',
'~',
'What is your current tobacco use status? unknown i',
'unknown',
'UnknownIfEver',
'_____',
'.',
'____',
'NULL',
'not documented',
'What is your current tobacco use status?  n/a',
'What is your current tobacco use status?  Pt refus',
'What is your current tobacco use status?  pt refus',
'unclear',
'Not identified today.',
'What is your current tobacco use status?  per pati',
'What is your current tobacco use status?  seen by',
'X, X',
'?',
'marijuana',
'N/A',
'What is your current tobacco use status?  not asse',
'o',
'Treatment Goals:   Patient preferences and Lifest',
'-',
'What is your current tobacco use status?  States s',
'I don''t know',
'contraindicated',
'yrs',
'unsure',
'no specific goal'
)
and tobacco_use is not null
and tobacco_use !~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}' 
group by tobacco_use
having count(*) >1
order by count(*) desc;





