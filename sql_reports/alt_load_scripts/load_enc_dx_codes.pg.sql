drop table if exists temp_enc_dx_codes_monthly;
select natural_key, dx_code_id into temp_enc_dx_codes_monthly
from (select natural_key, unnest(string_to_array(dx_code_id,';'))::varchar(20) as dx_code_id 
	  from temp_enc where date between :'startdt' and :'enddt') t0 
where trim(dx_code_id) <> '';
drop table if exists enc_keys;
select id, natural_key
into enc_keys
from emr_encounter where date between :'startdt'::date and :'enddt'::date;
insert into emr_encounter_dx_codes (encounter_id, dx_code_id)
select distinct e.id, tedx.dx_code_id
from enc_keys e 
join temp_enc_dx_codes_monthly tedx on e.natural_key=tedx.natural_key;
drop table temp_enc_dx_codes_monthly;
drop table enc_keys;
