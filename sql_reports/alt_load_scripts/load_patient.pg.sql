insert into emr_patient (natural_key, created_timestamp, updated_timestamp, mrn,
    city, state, zip, zip5, date_of_birth, cdate_of_birth, gender, race, ethnicity,
    provenance_id)
select natural_key, now(), now(), mrn, city, state, zip, substr(zip,1,5), 
   to_timestamp(date_of_birth,'yyyymmdd'), date_of_birth, gender, race, ethnicity, 1
   from temp_pat;
