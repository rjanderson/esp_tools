insert into emr_socialhistory 
   (natural_key,created_timestamp,updated_timestamp,date, 
	mrn,tobacco_use,patient_id,provenance_id,provider_id)
select distinct s.natural_key,now(),now(),date_noted::date,
    s.mrn,tobacco_use,
    p.id, 1, 1
from temp_soc s
join emr_patient p on s.patient_natural_key=p.natural_key
where s.date_noted between :'startdt' and :'enddt';
