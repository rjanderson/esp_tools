#run this after the quarterly extract is done
cd /data/epic/archive
rm epic*.esp.*
psql -f /srv/esp/scripts/load_scripts/create_tmp_load_tables.pg.sql
psql -c "\copy temp_pat from '/data/epic/incoming/epicmem.esp.04012018' delimiter '^'"
cd /data/epic/incoming/
mv epicmem.esp.04012018 ../archive/
for i in `seq 1 48`;
  do ldate=$(date -d "20180301 +$i month" +%m%d%Y);
  query="\copy temp_enc from '/data/epic/incoming/vis/epicvis.esp.$ldate' delimiter '^'";
  psql -c "$query"
done
psql -c "create unique index temp_enc_natural_key_idx on temp_enc (natural_key)"
psql -c "create index temp_enc_date_idx on temp_enc (date)"
cd vis
for F in epicvis.esp.*; do mv $F ../../archive/
for i in `seq 1 48`;
  do ldate=$(date -d "20180301 +$i month" +%m%d%Y);
  query="\copy temp_res from '/data/epic/incoming/res/epicres.esp.$ldate' delimiter '^'";
  psql -c "$query"
done
psql -c "create index temp_res_date_idx on temp_res (date)"
cd ../res
for F in epicres.esp.*; do mv $F ../../archive/
for i in `seq 1 48`;
  do ldate=$(date -d "20180301 +$i month" +%m%d%Y);
  query="\copy temp_soc from '/data/epic/incoming/soc/epicsoc.esp.$ldate' delimiter '^'";
  psql -c "$query"
done
psql -c "create index temp_soc_date_idx on temp_soc (date_noted)"
cd ../soc
for F in epicsoc.esp.*; do mv $F ../../archive/
for i in `seq 1 48`;
  do ldate=$(date -d "20180301 +$i month" +%m%d%Y);
  query="\copy temp_imm from '/data/epic/incoming/imm/epicimm.esp.$ldate' delimiter '^'";
  psql -c "$query"
done
psql -c "create index temp_imm_date_idx on temp_imm (date)"
cd ../imm
for F in epicimm.esp.*; do mv $F ../../archive/
for i in `seq 1 48`;
  do ldate=$(date -d "20180301 +$i month" +%m%d%Y);
  query="\copy temp_med from '/data/epic/incoming/med/epicmed.esp.$ldate' delimiter '^'";
  psql -c "$query"
done
psql -c "create index temp_med_date_idx on temp_med (date)"
cd ../med
for F in epicmed.esp.*; do mv $F ../../archive/
