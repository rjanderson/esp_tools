--Get meds for selected patients who have sti records
drop table if exists temp_patient_meds;
create table temp_patient_meds as select emr_prescription.patient_id, emr_prescription.date, name, dose, quantity, frequency, start_date, end_date 
from emr_prescription, emr_stiencounterextended where 
emr_prescription.patient_id = emr_stiencounterextended.patient_id;

--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key and enc.date >= '01-01-2015' and enc.date < '01-01-2016';


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_meds;
create table temp_patient_stivisits_meds as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, name, dose, quantity, frequency, start_date, end_date
from temp_patient_stivisits stivis
join temp_patient_meds on
temp_patient_meds.date = stivis.encounter_date
and temp_patient_meds.patient_id = stivis.patient_id;



drop table if exists temp_ssun_meds_report;
create table temp_ssun_meds_report as select t.patient_id as "F4_PatientID", t.encounter_id as "F4_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F4_Visdate",
case 
	when t.name ilike '%amoxicillin%' or t.name ilike '%augmentin%' 
		or t.name = 'AMOX TR-POTASSIUM CLAVULANATE' then 10
	when t.name ilike '%ampicillin%' 
		or t.name = 'AMPICIL TRIHYD/PROBENECID' then 11
	when t.name ilike '%azithromycin%' or t.name ilike '%zithromax%' then 20
	when t.name ilike '%cefdinir%' or t.name ilike '%omincef%' then 36
	when t.name ilike '%cipro%' then 40
	when t.name ilike '%clindamycin%' or t.name ilike '%cleocin%'
		or t.name = 'CLINDESSE' then 22
	when t.name ilike '%LEVOFLOX%' or t.name = 'LEVAQUIN' then 41
	when t.name ilike '%MOXIFLOX%' then 42
	when t.name ilike '%CEFIXIME%' or t.name = 'SUPRAX' then 30
	when t.name ilike '%cefotaxime%'then 32
	when t.name ilike '%cefoxitin%' then 33
	when t.name ilike '%cefuroxime%' then 38
	when t.name ilike '%doxycycline%' or t.name = 'VIBRAMYCIN'
		or t.name = 'DORYX' or t.name = 'ORAXYL' or t.name = 'VIBRA-TABS' then 50
	when t.name ilike '%erythromycin base%' then 21
	when t.name = 'GATIFLOXACIN' then 88
	when t.name ilike '%gentamicin%' or t.name = 'GENTAM SULF/SODIUM CHLORIDE' then 23
	when t.name = 'FACTIVE' or t.name = 'GEMIFLOXACIN MESYLATE' then 44
	when t.name ilike '%metronidazole%' or t.name = 'FLAGYL' 
		or t.name ilike '%metrogel%' then  60
	when t.name = 'CEFTIBUTEN' then 35
	when t.name ilike '%ceftriaxone%' then 37
	when t.name ilike '%cefpodoxime%' then 34
	when t.name ilike '%tinidazole%' or t.name = 'TINDAMAX' then 61
	when t.name = 'OFLOXACIN' then 43
	when t.name = 'TRUVADA' or t.name = 'EMTRICITABINE/TENOFOVIR' then 70
	else 88
end as "F4_Medication",

case 
	when (t.name not ilike '%amoxicillin%' 
		and t.name not ilike '%augmentin%' 
		and t.name != 'AMOX TR-POTASSIUM CLAVULANATE' 
		and t.name not ilike '%ampicillin%' 
		and t.name != 'AMPICIL TRIHYD/PROBENECID'
		and t.name not ilike '%azithromycin%' 
		and t.name not ilike '%zithromax%' 
		and t.name not ilike '%cefdinir%' 
		and t.name not ilike '%omincef%' 
		and t.name not ilike '%cipro%' 
		and t.name not ilike '%clindamycin%'
		and t.name not ilike '%cleocin%'
		and t.name != 'CLINDESSE' 
		and t.name not ilike '%LEVOFLOX%' 
		and t.name != 'LEVAQUIN' 
		and t.name not ilike '%MOXIFLOX%' 
		and t.name != 'CEFIXIME' 
		and t.name != 'SUPRAX' 
		and t.name not ilike '%cefotaxime%'
		and t.name not ilike '%cefoxitin%'
		and t.name not ilike '%cefuroxime%' 
		and t.name not ilike '%doxycycline%' 
		and t.name != 'VIBRAMYCIN'
		and t.name != 'DORYX' 
		and t.name != 'ORAXYL' 
		and t.name != 'VIBRA-TABS' 
		and t.name not ilike '%erythromycin base%' 
		and t.name != 'GATIFLOXACIN' 
		and t.name not ilike '%gentamicin%' 
		and t.name != 'GENTAM SULF/SODIUM CHLandIDE' 
		and t.name != 'FACTIVE' 
		and t.name != 'GEMIFLOXACIN MESYLATE' 
		and t.name not ilike '%metronidazole%' 
		and t.name != 'FLAGYL' 
		and t.name not ilike '%metrogel%' 
		and t.name != 'CEFTIBUTEN' 
		and t.name not ilike '%ceftriaxone%' 
		and t.name not ilike '%cefpodoxime%'
		and t.name not ilike '%tinidazole%' 
		and t.name != 'TINDAMAX' 
		and t.name != 'OFLOXACIN' 
		and t.name != 'TRUVADA' 
		and t.name != 'EMTRICITABINE/TENOFOVIR' )
	then t.name
	else null
end as "F4_Medication_other",

null"F4_Dosage",


null "F4_Number_doses",


null  "F4_Dose_freq",

	case
	when (t.end_date - t.start_date <1 ) or (t.end_date - t.start_date >100) then null
	when (t.end_date - t.start_date) = 1 then 1
	when (t.end_date - t.start_date) = 3 then 2
	when (t.end_date - t.start_date) = 5 then 3
	when (t.end_date - t.start_date) = 7 then 4
	when (t.end_date - t.start_date) = 10 then 5
	when (t	.end_date - t.start_date) = 14 then 6
	else 8
end as "F4_Duration"

from temp_patient_stivisits_meds t;
\copy temp_ssun_meds_report to '/tmp/SSUN_meds.csv' csv header

