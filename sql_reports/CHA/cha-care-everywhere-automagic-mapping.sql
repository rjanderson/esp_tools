--covid19_pcr
DROP TABLE IF EXISTS kre_report.carev_covid19_pcr;
CREATE TABLE kre_report.carev_covid19_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE EVERWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SARS COV2 RNA CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode)
and native_name !~ ('SOURCE|POC|RAPID');

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_pcr);

DROP TABLE IF EXISTS kre_report.carev_covid19_pcr;


--covid19_igg
DROP TABLE IF EXISTS kre_report.carev_covid19_igg;
CREATE TABLE kre_report.carev_covid19_igg AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE IGG INDEX CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_igg', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_igg);

DROP TABLE IF EXISTS kre_report.carev_covid19_igg;

--covid19_igm
DROP TABLE IF EXISTS kre_report.carev_covid19_igm;
CREATE TABLE kre_report.carev_covid19_igm AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGM CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_igm', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_igm);

DROP TABLE IF EXISTS kre_report.carev_covid19_igm;



--influenza
DROP TABLE IF EXISTS kre_report.carev_influenza;
CREATE TABLE kre_report.carev_influenza AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- INFLUENZA%'
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN POC CARE EVERYWHERE%'
	  )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'influenza', native_code, 'exact', 'both', null, true, null, null, null, null, 
 'care everywhere automagic add' FROM kre_report.carev_influenza);

DROP TABLE IF EXISTS kre_report.carev_influenza;




--covid19_ag
DROP TABLE IF EXISTS kre_report.carev_covid19_ag;
CREATE TABLE kre_report.carev_covid19_ag AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN POC CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_ag', native_code, 'exact', 'both', null, true, null, null, null, null, 'care everywhere automagic add' FROM kre_report.carev_covid19_ag);

DROP TABLE IF EXISTS kre_report.carev_covid19_ag;

--covid19_ab_total
DROP TABLE IF EXISTS kre_report.carev_covid19_ab_total;
CREATE TABLE kre_report.carev_covid19_ab_total AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 AB NUCLEOCAPSID AB CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY%'
	   OR native_name ilike 'SARS-COV-2 AB NUCLEOCAPSID CARE EVERYWHERE-- SARS-COV-2 AB NUCLEOCAPSID AB CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_ab_total', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_ab_total);

DROP TABLE IF EXISTS kre_report.carev_covid19_ab_total;


--creatinine
DROP TABLE IF EXISTS kre_report.carev_creatinine;
CREATE TABLE kre_report.carev_creatinine AS
select * from emr_labtestconcordance
where (native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE CARE EVERWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE POC CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE POC CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE POC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE CARE EVERWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'creatinine', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_creatinine);

DROP TABLE IF EXISTS kre_report.carev_creatinine;

--a1c
DROP TABLE IF EXISTS kre_report.carev_a1c;
CREATE TABLE kre_report.carev_a1c AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOGIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- GLYCOHEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'a1c', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_a1c);

DROP TABLE IF EXISTS kre_report.carev_a1c;

--hemoglobin
DROP TABLE IF EXISTS kre_report.carev_hemoglobin;
CREATE TABLE kre_report.carev_hemoglobin AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMOGLOBIN CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN PLASMA CARE EVERYWHERE-- HEMOGLOBIN TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN POC CARE EVERYWHERE-- HEMOGLOBIN POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOLOBIN AND HEMATOCRIT CARE EVERYWHERE-- HEMOGLOBIN POC CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hemoglobin', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_hemoglobin);

DROP TABLE IF EXISTS kre_report.carev_hemoglobin;


--hepatitis_a_igm_antibody
DROP TABLE IF EXISTS kre_report.carev_hepatitis_a_igm_antibody;
CREATE TABLE kre_report.carev_hepatitis_a_igm_antibody AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_a_igm_antibody', native_code, 'exact', 'both', null, true, '22314-9', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_a_igm_antibody);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_a_igm_antibody;


--hepatitis_b_core_ab
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_ab;
CREATE TABLE kre_report.carev_hepatitis_b_core_ab AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE TOTAL ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_core_ab', native_code, 'exact', 'both', null, true, '16933-4', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_core_ab);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_ab;


--hepatitis_b_e_antigen
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_e_antigen;
CREATE TABLE kre_report.carev_hepatitis_b_e_antigen AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS BE ANTIGEN CARE EVERYWHERE-- HEPATITIS BE ANTIGEN CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS BE ANTIGEN ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIGEN CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_e_antigen', native_code, 'exact', 'both', null, true, '13954-3', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_e_antigen);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_e_antigen;

--hepatitis_b_surface_antigen
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_surface_antigen;
CREATE TABLE kre_report.carev_hepatitis_b_surface_antigen AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_surface_antigen', native_code, 'exact', 'both', null, true, '13954-3', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_surface_antigen);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_surface_antigen;



--hepatitis_c_elisa
DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_elisa;
CREATE TABLE kre_report.carev_hepatitis_c_elisa AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_c_elisa', native_code, 'exact', 'both', null, true, '16128-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_c_elisa);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_elisa;


--hepatitis_c_rna
--QUALITATIVE RESULTS - PCR IN NAME
DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_rna;
CREATE TABLE kre_report.carev_hepatitis_c_rna AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS C RNA PCR CARE EVERYWHERE-- HEPATITIS C RNA PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_c_rna', native_code, 'exact', 'both', null, true, '5012-0', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_c_rna);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_rna;


--hepatitis_b_core_antigen_igm_antibody
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_antigen_igm_antibody;
CREATE TABLE kre_report.carev_hepatitis_b_core_antigen_igm_antibody AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE IGM ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_core_antigen_igm_antibody', native_code, 'exact', 'both', null, true, '31204-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_core_antigen_igm_antibody);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_antigen_igm_antibody;


--hematocrit
DROP TABLE IF EXISTS kre_report.carev_hematocrit;
CREATE TABLE kre_report.carev_hematocrit AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMATOCRIT POC CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
       OR native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOLOBIN AND HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT MANUAL CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hematocrit', native_code, 'exact', 'both', null, true, '20570-8', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_hematocrit);

DROP TABLE IF EXISTS kre_report.carev_hematocrit;

--platelet_count
DROP TABLE IF EXISTS kre_report.carev_platelet_count;
CREATE TABLE kre_report.carev_platelet_count AS
select * from emr_labtestconcordance
where (native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- PLATELET COUNT CARE EVERYWHERE%'
       OR native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- PLATELET COUNT CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'platelet_count', native_code, 'exact', 'both', null, true, '13056-7', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_platelet_count);

DROP TABLE IF EXISTS kre_report.carev_platelet_count;


--rsv
DROP TABLE IF EXISTS kre_report.carev_rsv;
CREATE TABLE kre_report.carev_rsv AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- RSV % CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RSV ANTIGEN CARE EVERYWHERE-- RSV ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'RSV CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'rsv', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_rsv);

DROP TABLE IF EXISTS kre_report.carev_rsv;

--adenovirus
DROP TABLE IF EXISTS kre_report.carev_adenovirus;
CREATE TABLE kre_report.carev_adenovirus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- ADENOVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- ADENOVIRUS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ADENOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- ADENOVIRUS CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'adenovirus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_adenovirus);

DROP TABLE IF EXISTS kre_report.carev_adenovirus;


--parapertussis
DROP TABLE IF EXISTS kre_report.carev_parapertussis;
CREATE TABLE kre_report.carev_parapertussis AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'parapertussis', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_parapertussis);

DROP TABLE IF EXISTS kre_report.carev_parapertussis;

--pertussis_pcr
DROP TABLE IF EXISTS kre_report.carev_pertussis_pcr;
CREATE TABLE kre_report.carev_pertussis_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA PERTUSSIS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PERTUSSIS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PERTUSSIS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'pertussis_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_pertussis_pcr);

DROP TABLE IF EXISTS kre_report.carev_pertussis_pcr;

--h_metapneumovirus
DROP TABLE IF EXISTS kre_report.carev_h_metapneumovirus;
CREATE TABLE kre_report.carev_h_metapneumovirus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'h_metapneumovirus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_h_metapneumovirus);

DROP TABLE IF EXISTS kre_report.carev_h_metapneumovirus;

--rhino_entero_virus
DROP TABLE IF EXISTS kre_report.carev_rhino_entero_virus;
CREATE TABLE kre_report.carev_rhino_entero_virus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- HUMAN RHINOVIRUS/ENTEROVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN RHINOVIRUS/ENTEROVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RHINOVIRUS / ENTEROVIRUS CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'rhino_entero_virus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_rhino_entero_virus);

DROP TABLE IF EXISTS kre_report.carev_rhino_entero_virus;


--parainfluenza
DROP TABLE IF EXISTS kre_report.carev_parainfluenza;
CREATE TABLE kre_report.carev_parainfluenza AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- PARAINFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PARAINFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- % PARAINFLUENZA % CARE EVERYWHERE%' 
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'parainfluenza', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_parainfluenza);

DROP TABLE IF EXISTS kre_report.carev_parainfluenza;

--c_pneumoniae_pcr
DROP TABLE IF EXISTS kre_report.carev_c_pneumoniae_pcr;
CREATE TABLE kre_report.carev_c_pneumoniae_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CHLAMYDIA PNEUMONIA CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'c_pneumoniae_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_c_pneumoniae_pcr);

DROP TABLE IF EXISTS kre_report.carev_c_pneumoniae_pcr;

--m_pneumoniae_pcr
DROP TABLE IF EXISTS kre_report.carev_m_pneumoniae_pcr;
CREATE TABLE kre_report.carev_m_pneumoniae_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- MYCOPLASMA PNEUMONIAE CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'm_pneumoniae_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_m_pneumoniae_pcr);

DROP TABLE IF EXISTS kre_report.carev_m_pneumoniae_pcr;

--coronavirus_non19
DROP TABLE IF EXISTS kre_report.carev_coronavirus_non19;
CREATE TABLE kre_report.carev_coronavirus_non19 AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS 229E CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS HKU1 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS NL63 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS OC43 CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'coronavirus_non19', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_coronavirus_non19);

DROP TABLE IF EXISTS kre_report.carev_coronavirus_non19;


/* --hiv_elisa_1and2
   -- DISABLE FOR NOW PENDING REVIEW
DROP TABLE IF EXISTS kre_report.carev_hiv_elisa_1and2;
CREATE TABLE kre_report.carev_hiv_elisa_1and2 AS
select * from emr_labtestconcordance
where (native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV RAPID CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY SCREEN%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV-1/2 ANTIBODY RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV-1/2 ANTIBODY RAPID CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hiv_elisa', native_code, 'exact', 'both', null, true, '43010-8', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hiv_elisa_1and2);

DROP TABLE IF EXISTS kre_report.carev_hiv_elisa_1and2; */


--hiv_ag_ab
DROP TABLE IF EXISTS kre_report.carev_hiv_ag_ab;
CREATE TABLE kre_report.carev_hiv_ag_ab AS
select * from emr_labtestconcordance
where (native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY/ P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hiv_ag_ab', native_code, 'exact', 'both', null, true, '56888-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hiv_ag_ab);

DROP TABLE IF EXISTS kre_report.carev_hiv_ag_ab;


--wbc
DROP TABLE IF EXISTS kre_report.carev_wbc;
CREATE TABLE kre_report.carev_wbc AS
select * from emr_labtestconcordance
where (native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
	   OR native_name ilike 'WHITE BLOOD CELL COUNT CARE EVERYWHERE-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'wbc', native_code, 'exact', 'both', null, true, '26464-8', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_wbc);

DROP TABLE IF EXISTS kre_report.carev_wbc;




--all IGNORES
DROP TABLE IF EXISTS kre_report.carev_ignores;
CREATE TABLE kre_report.carev_ignores AS
select DISTINCT native_code, native_name from emr_labtestconcordance
where (native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- DAT CORD BLOOD CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- ESTIMATED AVERAGE GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- MEAN PLASMA GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DAT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- MEAN PLATELET VOLUME CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA HOLMESII DNA CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1 P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CT VALUE TARGET 1 CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ABO GROUP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RH TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- WILL PATIENT BE ADMITTED? CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUT OFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL/CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUT OFF CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- POC QC PERFORMED? CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- PROCEDURAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- S/CO RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'	 
       OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 CARE EVERWHERE SOURCE%'
       OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'	   
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COMMENT, COVID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 CARE EVERWHERE SOURCE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- EMPLOYED IN A HEALTHCARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- ETHNICITY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- METHOD SUMMARY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- MOLECULAR DEVICE ID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT RACE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SOURCE, COVID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TEST INFORMATION CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT RACE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TEST PERFORMED BY CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CARE EVERYWHERE-- DAT CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CARE EVERYWHERE-- DAT IGG CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- ABO GROUP CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- DAT ANTI IGG CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- RH TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'ESTIMATED GLOMERULAR FILTRATION RATE CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'ESTIMATED GLOMERULAR FILTRATION RATE CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- CALCULATED MEAN GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- MEAN BLOOD GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN PLASMA CARE EVERYWHERE-- OXYHEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- PERFORMING LAB CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS D ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS D ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS D ANTIGEN CARE EVERYWHERE-- HEPATITIS D VIRUS ANTIGEN%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS E IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS E IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP A IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP C AB CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP C IGM CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- CONSENT FORM CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- HIV CARTRIDGE LOT # CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- REFERRAL SOURCE CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- SAMPLE PORT QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- KIT LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- NOTE, HIV 1/2 AB + HIV 1 AG CARE EVERYWHERE%'
	   OR native_name ilike 'NFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- QC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ABO/RH CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID TESTING STATUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- FILE HISTORY CHECK CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- PRIOR TEST DATE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUT OFFCARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEP ACUTE INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUTOFF RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INR CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'INR POC CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'INR POC CARE EVERYWHERE-- INR POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- ANTICOAG THERAPY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- ISOLATION GUIDELINES CARE EVERY%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PRIOR TEST DATE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- CONFIRMATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUT OFFCARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- TESTING LOCATION CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- ANTICOAG THERAPY CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- HEALTHCARE WORKER CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'	   
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_ignoredcode (id, native_code) 
(SELECT nextval('conf_ignoredcode_id_seq'), native_code FROM kre_report.carev_ignores);

DROP TABLE IF EXISTS kre_report.carev_ignores;